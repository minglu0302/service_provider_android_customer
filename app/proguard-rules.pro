# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/embed/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
# Twilio Client
#-keep class com.twilio.** { *; }

# Apache HttpClient
#-dontwarn org.apache.http.**
-dontobfuscate
-keep class com.amazonaws.** { *; }
-keepnames class com.amazonaws.** { *; }
-dontwarn com.amazonaws.**
-dontwarn com.fasterxml.**

 #### -- Picasso --
 -dontwarn com.squareup.picasso.**

 #### -- OkHttp --
 -dontwarn com.squareup.okhttp.internal.**
 -dontwarn okio.**
 -keep class com.utility.WaveDrawable {*;}

 # io card
-keep class io.card.payment.** {*;}

 -dontwarn javax.naming.**
 -dontwarn org.apache.**
 -keep class junit.** { *; }
 -dontwarn junit.**
