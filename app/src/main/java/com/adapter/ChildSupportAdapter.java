package com.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.iserve.passenger.R;
import com.pojo.ChildViewDraw;
import java.util.ArrayList;

/**
 * Created by embed on 20/9/16.
 *
 */
public class ChildSupportAdapter  extends ArrayAdapter<ChildViewDraw> {
    private Context context;
    private ArrayList<ChildViewDraw> supportList = new ArrayList<>();

    public ChildSupportAdapter(Context context, ArrayList<ChildViewDraw> supportList) {
        super(context, R.layout.support_list_row, supportList);
        this.context = context;
        this.supportList = supportList;
    }
    private class ViewHolder {

        TextView text;

    }
    @Override
    public ChildViewDraw getItem(int position) {
        return supportList.get(position);
    }

    @Override
    public int getCount() {
        return supportList.size();
    }
    /*********************************************************************************************************************/

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null || convertView.getTag() == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.support_list_row, parent, false);
            holder.text = (TextView) convertView.findViewById(R.id.list_row_text);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        holder.text.setText(supportList.get(position).getTag());
        return convertView;

    }
}

