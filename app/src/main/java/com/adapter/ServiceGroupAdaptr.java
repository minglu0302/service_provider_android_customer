package com.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.iserve.passenger.R;
import com.pojo.DataService;
import com.utility.Variableconstant;
import java.util.ArrayList;

/**
 * Created by embed on 24/9/16.
 *
 */
public class ServiceGroupAdaptr extends BaseAdapter
{
    private Context mContext;
    private ArrayList<DataService> servicedata;
    private Typeface lightfont;
    public ServiceGroupAdaptr(Context context, ArrayList<DataService> servicedata)
    {
        this.mContext = context;
        this.servicedata = servicedata;
        lightfont = Typeface.createFromAsset(mContext.getAssets(), Variableconstant.regularfont);
    }

    @Override
    public int getCount()
    {
        return servicedata.size();
    }

    @Override
    public Object getItem(int position)
    {
        return servicedata.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return servicedata.size();
    }

    public class ViewHolder
    {
        TextView groupname,astersik,arrowtxt,tvselectedservice;

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if(convertView==null||convertView.getTag()==null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.servicegroupadaptr, parent, false);
            holder.groupname = (TextView) convertView.findViewById(R.id.groupname);
            holder.astersik = (TextView) convertView.findViewById(R.id.astersik);
            holder.arrowtxt = (TextView) convertView.findViewById(R.id.arrowtxt);
            holder.tvselectedservice = (TextView) convertView.findViewById(R.id.tvselectedservice);

        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

      String selectedservice = "";


        for(int i=0;i<servicedata.get(position).getServices().size();i++)
        {
            if(servicedata.get(position).getServices().get(i).isServicegroupflag())
            {

               if(selectedservice.equals(""))
               {
                   selectedservice = servicedata.get(position).getServices().get(i).getSname();
               }
                else
               {
                   selectedservice = selectedservice+", "+servicedata.get(position).getServices().get(i).getSname();
               }
                holder.tvselectedservice.setText(selectedservice);
            }
        }

        holder.groupname.setTypeface(lightfont);
        holder.arrowtxt.setTypeface(lightfont);
        holder.astersik.setTypeface(lightfont);
        holder.groupname.setText(servicedata.get(position).getGroup_name());
        if(servicedata.get(position).getCmand().equals("1"))
        {
            holder.astersik.setVisibility(View.VISIBLE);
            servicedata.get(position).setMandatorydata(true);
        }
        else
        {
            holder.astersik.setVisibility(View.GONE);
            servicedata.get(position).setMandatorydata(false);
        }

        return convertView;
    }
}

