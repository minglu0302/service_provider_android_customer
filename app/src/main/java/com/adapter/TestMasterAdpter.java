package com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import com.iserve.passenger.R;
import com.pojo.PubNubMas;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.utility.CircleTransform;
import com.utility.Scaler;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by embed on 19/9/16.
 *
 */
public class TestMasterAdpter  extends BaseAdapter
{
    private Context mContext;
    private ArrayList<PubNubMas> masListData = new ArrayList<>();

    double size[];
    public TestMasterAdpter(Context mContext, ArrayList<PubNubMas> masListData) {
        this.mContext = mContext;
        this.masListData = masListData;
    }


    private class ViewHolder {
        ImageView masproim;
        TextView masdtlnametv,masdistance,priceperhr,tvpriceperhr;
        RatingBar driver_rating;
        ProgressBar progressBar;
    }

    @Override
    public int getCount() {

        return masListData.size();
    }

    @Override
    public PubNubMas getItem(int position) {
        return masListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final PubNubMas masdtl = masListData.get(position);
        final ViewHolder holder;
        if(convertView==null||convertView.getTag() == null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.masterlist_adaptr, parent, false);
            holder.masdtlnametv = (TextView) convertView.findViewById(R.id.masdtlnametv);
            holder.masdistance = (TextView) convertView.findViewById(R.id.masdistance);
            holder.priceperhr = (TextView) convertView.findViewById(R.id.priceperhr);
            holder.tvpriceperhr = (TextView) convertView.findViewById(R.id.tvpriceperhr);
            holder.driver_rating = (RatingBar) convertView.findViewById(R.id.driver_rating);
            holder.masproim = (ImageView) convertView.findViewById(R.id.masproim);
            holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progressBar_splsh);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        Typeface typeregular = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular_0.ttf");
        Typeface typebold = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Semibold_0.ttf");
        holder.masdtlnametv.setText(masdtl.getN());
        String price= "";
        try
        {
             price = mContext.getResources().getString(R.string.currencySymbol)+" "
                    +(Integer.parseInt(masdtl.getAmt().trim()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        NumberFormat nf_out = NumberFormat.getNumberInstance(Locale.US);
        nf_out.setMaximumFractionDigits(2);
        nf_out.setGroupingUsed(false);
        if(!masdtl.getD().equals(""))
        {
            String faremount=nf_out.format(Double.parseDouble(masdtl.getD())/1000);
            double kilometerDistance = Double.parseDouble(faremount);//*1.6;
            @SuppressLint("DefaultLocale") String kilometer = String.format("%.2f", kilometerDistance);
            String tempdist = kilometer + " " + mContext.getResources().getString(R.string.distance);
            holder.masdistance.setText(tempdist);
        }
        else
        {
            String tempdist = "";
            holder.masdistance.setText(tempdist);
        }
        float fRating = Float.parseFloat(masdtl.getRat());
        holder.driver_rating.setIsIndicator(true);
        holder.driver_rating.setRating(fRating);
        holder.masdtlnametv.setTypeface(typebold);
        holder.priceperhr.setText(price);
        holder.priceperhr.setTypeface(typebold);
        holder.tvpriceperhr.setTypeface(typeregular);
        holder.masdistance.setTypeface(typeregular);
        size = Scaler.getScalingFactor(mContext);
        String url = masdtl.getI();
        double width1 = size[0] * 80;
        double height1 = size[1] * 80;

        if(!url.equals(""))
        {
            Picasso.with(mContext).load(url)
                    .centerCrop().transform(new CircleTransform())
                    .resize((int)width1,(int)height1)
                    .noFade()
                    .placeholder(R.drawable.booking_history_profile_default_image)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
                        {
                            holder.progressBar.setVisibility(View.GONE);
                            holder.masproim.setImageBitmap(bitmap);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            holder.progressBar.setVisibility(View.GONE);
                            holder.masproim.setImageDrawable(errorDrawable);
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            holder.progressBar.setVisibility(View.VISIBLE);
                            holder.masproim.setImageDrawable(placeHolderDrawable);
                        }
                    });
        }
        else
        {
            holder.progressBar.setVisibility(View.GONE);
        }
        return convertView;
    }
}
