package com.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.iserve.passenger.R;
import com.utility.Utility;
import java.util.ArrayList;
/**
 * Created by embed on 11/8/15.
 *
 */
public class SpinerAdaptor extends ArrayAdapter
{
    private Context mcontext;

    private ArrayList<String> range;

    public SpinerAdaptor(Context context,ArrayList<String>range){
        super(context, R.layout.spinnerlayout, range);
        this.range = range;
        long start = System.currentTimeMillis();

        this.mcontext = context;

        for(int i = 1;i<25;i++)
        {
            range.add(String.valueOf(i));
        }
        Utility.printLog("TOTAL TIME" + (System.currentTimeMillis() - start));
    }

    private class ViewHolder
    {

        TextView quantitytextview;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public void setCustomText(String customText) {
        notifyDataSetChanged();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }


    private View getCustomView(int position, View convertView,
                               ViewGroup parent)
    {
        ViewHolder holder = new ViewHolder();
        if(convertView==null || convertView.getTag()==null) {
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.dropdownview,parent,false);
            holder.quantitytextview = (TextView) convertView.findViewById(R.id.quantitytextview);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.quantitytextview.setText(range.get(position));

        return convertView;
    }
}
