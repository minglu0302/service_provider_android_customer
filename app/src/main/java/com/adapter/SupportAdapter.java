package com.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.iserve.passenger.R;
import com.pojo.Support_values;
import java.util.ArrayList;

public class SupportAdapter extends ArrayAdapter<Support_values>
{
	private Context context;
	private ArrayList<Support_values> supportList=new ArrayList<>();
	public SupportAdapter(Context context, ArrayList<Support_values> supportList)//int resourceId,
	{
		super(context, R.layout.support_list_row,supportList);
		this.context=context;
		this.supportList=supportList;
	}
	private class ViewHolder
	{
		TextView text;
	}
	@Override
	public Support_values getItem(int position){
		return supportList.get(position);
	}
	@Override
	public int getCount() {

		return supportList.size();
	}
	/*********************************************************************************************************************/
	@NonNull
	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		ViewHolder holder;
		if(convertView==null||convertView.getTag()==null)
		{
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.support_list_row, parent, false);
			holder.text=(TextView) convertView.findViewById(R.id.list_row_text);
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder) convertView.getTag();
		holder.text.setText(supportList.get(position).getTag());
		return convertView;
	}
}
