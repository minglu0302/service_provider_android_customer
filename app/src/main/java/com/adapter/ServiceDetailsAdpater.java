package com.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.iserve.passenger.R;
import com.iserve.passenger.ServiceDetials;
import com.pojo.ServiceGroup;
import com.utility.Variableconstant;

import java.util.ArrayList;

/**
 * Created by embed on 26/9/16.
 *
 */
public class ServiceDetailsAdpater  extends BaseAdapter
{
    private Context mContext;
    private ArrayList<ServiceGroup> servicedata;
    private String cmul;
    private int selectedPosition = -1;
    private ServiceDetials serviceDetails;
    private int counter = 1;
    private Typeface lightfont;


    public ServiceDetailsAdpater(Context context, ArrayList<ServiceGroup> servicedata,String cmul)
    {

        this.mContext = context;
        this.servicedata = servicedata;
        this.cmul = cmul;
        serviceDetails = (ServiceDetials)mContext;
        lightfont = Typeface.createFromAsset(mContext.getAssets(), Variableconstant.regularfont);
    }

    @Override
    public int getCount()
    {
        return servicedata.size();
    }

    @Override
    public Object getItem(int position)
    {
        return servicedata.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return servicedata.size();
    }

    public class ViewHolder
    {
        TextView tvservicedtl,tvprice;
        SwitchCompat switchonoff;
        CheckBox check;

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {

        final ViewHolder holder;
        if(convertView==null||convertView.getTag()==null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.servicedtlslistadaptr, parent, false);
            holder.tvservicedtl = (TextView) convertView.findViewById(R.id.tvservicedtl);
            holder.tvprice = (TextView) convertView.findViewById(R.id.tvprice);
            holder.switchonoff = (SwitchCompat) convertView.findViewById(R.id.switchonoff);
            holder.check = (CheckBox) convertView.findViewById(R.id.check);
            if (servicedata.get(position).isServicegroupflag()){
                holder.switchonoff.setChecked(true);
                holder.switchonoff.isChecked();
            }

            holder.switchonoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    if(isChecked)
                    {
                        servicedata.get(position).setServicegroupflag(true);
                        serviceDetails.serviceGroups.get(position).setServicegroupflag(true);
                    }
                    else
                    {
                        servicedata.get(position).setServicegroupflag(false);
                        serviceDetails.serviceGroups.get(position).setServicegroupflag(false);

                    }
                }
            });
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvprice.setTypeface(lightfont);
        holder.tvservicedtl.setTypeface(lightfont);

        String price = mContext.getResources().getString(R.string.currencySymbol)+" "+servicedata.get(position).getFixed_price();
        holder.tvservicedtl.setText(servicedata.get(position).getSname());
        holder.tvprice.setText(price);

        if(cmul.equals("1"))
        {
            holder.switchonoff.setVisibility(View.VISIBLE);
            holder.check.setVisibility(View.GONE);

        }
        else
        {
            holder.switchonoff.setVisibility(View.GONE);
            holder.check.setVisibility(View.VISIBLE);
            holder.check.setTag(position);
            if(counter==1)
            {
                if (servicedata.get(position).isServicegroupflag()){
                    counter++;
                    holder.check.setChecked(true);
                    holder.check.isChecked();
                    selectedPosition = position;
                    notifyDataSetChanged();
                }
            }



            if (position == selectedPosition)
            {
                holder.check.setChecked(true);
                servicedata.get(position).setServicegroupflag(true);
                serviceDetails.serviceGroups.get(position).setServicegroupflag(true);
            }
            else
            {
                holder.check.setChecked(false);
                servicedata.get(position).setServicegroupflag(false);
                serviceDetails.serviceGroups.get(position).setServicegroupflag(false);
            }
            holder.check.setOnClickListener(onStateChangedListener(holder.check, position));
        }

        return convertView;
    }
    private View.OnClickListener onStateChangedListener(final CheckBox checkBox, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    selectedPosition = position;
                } else {
                    selectedPosition = -1;

                }
                notifyDataSetChanged();
            }
        };
    }
}
