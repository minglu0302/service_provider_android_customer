package com.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.iserve.passenger.AddressFragment;
import com.iserve.passenger.Address_Confirm;
import com.iserve.passenger.R;
import com.pojo.Saving_Address;
import java.util.ArrayList;

/**
 * Created by embed on 21/8/15.
 * class Address_Adaptor
 */
public class Address_Adaptor extends BaseAdapter implements AdapterView.OnItemClickListener
{
    private Activity mContext;
    private ArrayList<Saving_Address> adrListData = new ArrayList<>();
    private AddressFragment addressFragment;
    private Address_Confirm addressConfirm;
    private boolean flag;
    public Address_Adaptor(Activity context, ArrayList<Saving_Address> addresslist,AddressFragment addressFragment) {
        super();
        this.adrListData = addresslist;
        this.mContext = context;
        this.addressFragment = addressFragment;
    }
    public Address_Adaptor(Activity context, ArrayList<Saving_Address> addresslist, Address_Confirm addressConfirm,boolean flag) {
        super();
        this.adrListData = addresslist;
        this.mContext = context;
        this.addressConfirm = addressConfirm;
        this.flag = flag;
    }
    private class ViewHolder {
        ImageView addressdeletiv,selectedtype;
        RelativeLayout deletbutton,address1st;
        TextView addresstv,addresshometv,housenotv,houseno,landmark;
    }
    @Override
    public Saving_Address getItem(int position) {
        return adrListData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getCount() {

        return adrListData.size();
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        final Saving_Address adrsItem = adrListData.get(position);
        ViewHolder holder;
        if(convertView==null||convertView.getTag() == null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.view_address_entry, parent, false);
            holder.addresstv = (TextView) convertView.findViewById(R.id.addresstv);
            holder.addresshometv = (TextView) convertView.findViewById(R.id.addresshometv);
            holder.addressdeletiv = (ImageView) convertView.findViewById(R.id.addressdeletiv);
            holder.selectedtype = (ImageView) convertView.findViewById(R.id.selectedtype);
            holder.deletbutton = (RelativeLayout) convertView.findViewById(R.id.deletbutton);
            holder.address1st = (RelativeLayout) convertView.findViewById(R.id.address1st);
            holder.housenotv = (TextView) convertView.findViewById(R.id.housenotv);
            holder.houseno = (TextView) convertView.findViewById(R.id.houseno);
            holder.landmark = (TextView) convertView.findViewById(R.id.landmark);
            if(!flag)
            {
                holder.deletbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AddressFragment addressSelect = addressFragment;
                        addressSelect.deletefromdb(position);
                    }
                });
            }
            else
            {
                holder.address1st.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {

                        Address_Confirm address_select=addressConfirm;
                        address_select.backaddress(position);
                    }
                });
                holder.deletbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Address_Confirm addressSelect =addressConfirm;
                        addressSelect.deletefromdb(position);
                    }
                });
            }
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        Typeface addresstype = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Regular_0.ttf");
        Typeface tagtype = Typeface.createFromAsset(mContext.getAssets(), "fonts/OpenSans-Semibold_0.ttf");
        String fulladdress;
        if(adrsItem.getArea().equals(""))
        {
            fulladdress = adrsItem.getCity();
        }
        else
        {
            fulladdress = adrsItem.getArea()+","+adrsItem.getCity();
        }
        Log.i("TAG","AddressAdaptr "+adrsItem.getArea()+" city "+adrsItem.getCity()+" country ");
        String landmark = "Landmark: "+adrsItem.getAddress2();
        holder.landmark.setText(landmark);
        holder.landmark.setTypeface(addresstype);
        holder.addresstv.setText(fulladdress);
        holder.addresstv.setTypeface(addresstype);
        holder.addresshometv.setText(adrsItem.getAddresstype());
        holder.addresshometv.setTypeface(tagtype);
        holder.housenotv.setText(adrsItem.getHouseno());
        holder.housenotv.setTypeface(addresstype);
        holder.houseno.setTypeface(addresstype);
        if(adrsItem.getAddresstype().equals(mContext.getResources().getString(R.string.homeaddress)))
        {
            holder.selectedtype.setImageResource(R.drawable.home_icn);
        }
        else if(adrsItem.getAddresstype().equals(mContext.getResources().getString(R.string.officeaddress)))
        {
            holder.selectedtype.setImageResource(R.drawable.office__icn);
        }
        else
        {
            holder.selectedtype.setImageResource(R.drawable.other_icn);
        }
        return convertView;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {

    }


}
