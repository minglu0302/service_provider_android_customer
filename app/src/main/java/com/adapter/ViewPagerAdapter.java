package com.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.iserve.passenger.Image_Gallery_Frag;

import java.util.ArrayList;

/**
 * Created by embed on 8/11/16.
 *
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<String> images;

    public ViewPagerAdapter(FragmentManager fm, ArrayList<String> images) {
        super(fm);
        this.images = images;
    }

    @Override
    public Fragment getItem(int position) {
        return Image_Gallery_Frag.getInstance(images.get(position));
    }

    @Override
    public int getCount() {
        return images.size();
    }
}
