package com.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.iserve.passenger.R;
import com.pojo.TotalMessage;
import com.utility.Variableconstant;

import java.util.ArrayList;

/**
 * Created by embed on 26/12/16.
 *
 */
public class ChattingAdaptr extends RecyclerView.Adapter
{

    private Context mcontext;
    private ArrayList<TotalMessage>chatingSocketResponces;
    public ChattingAdaptr(Context mcontext, ArrayList<TotalMessage> chatingSocketResponces)
    {
        this.mcontext = mcontext;
        this.chatingSocketResponces = chatingSocketResponces;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.chattingsingle,parent,false);

        return new ViewHldr(view);
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHldr hldr = (ViewHldr) holder;

        Typeface regularfont = Typeface.createFromAsset(mcontext.getAssets(), Variableconstant.regularfont);
        if(chatingSocketResponces.get(position).getUsertype().equals("2"))
        {
            hldr.tvrecvr.setText(chatingSocketResponces.get(position).getPayload());
                hldr.txtTimerecvr.setText(chatingSocketResponces.get(position).getDt());
            String recvrtxt = (Html.fromHtml(hldr.tvrecvr.getText().toString()) + "                 ");
            hldr.tvrecvr.setText(recvrtxt); // 10 spaces
            if(chatingSocketResponces.get(position).getStatus().equals("1"))
            {
                hldr.ivmsgstatus.setImageResource(R.drawable.msg_status_server_receive);
            }
            else if(chatingSocketResponces.get(position).getStatus().equals("2"))
            {
                hldr.ivmsgstatus.setImageResource(R.drawable.msg_status_client_received);
            }
            hldr.lftfram.setVisibility(View.GONE);
            hldr.rightfram.setVisibility(View.VISIBLE);
        }
        else if(chatingSocketResponces.get(position).getUsertype().equals("1"))
        {
            hldr.tvsendr.setText(chatingSocketResponces.get(position).getPayload());
            hldr.txtTimesendr.setText(chatingSocketResponces.get(position).getDt());
            String sendrtxt = (Html.fromHtml(hldr.tvsendr.getText().toString()) + "            ");
            hldr.tvsendr.setText(sendrtxt);
            hldr.lftfram.setVisibility(View.VISIBLE);
            hldr.rightfram.setVisibility(View.GONE);
        }
        setTypeFace(hldr,regularfont);
    }

    private void setTypeFace(ViewHldr hldr, Typeface regularfont) {
        hldr.tvsendr.setTypeface(regularfont);
        hldr.tvrecvr.setTypeface(regularfont);
        hldr.txtTimerecvr.setTypeface(regularfont);
        hldr.txtTimesendr.setTypeface(regularfont);
    }

    @Override
    public int getItemCount() {

        return chatingSocketResponces.size();
    }
    private class ViewHldr extends RecyclerView.ViewHolder {

        TextView tvsendr,tvrecvr,txtTimerecvr,txtTimesendr;
        FrameLayout rightfram,lftfram;
        ImageView ivmsgstatus;
        ViewHldr(View itemView) {
            super(itemView);
            tvsendr = (TextView) itemView.findViewById(R.id.tvsendr);
            txtTimesendr = (TextView) itemView.findViewById(R.id.txtTimesendr);
            tvrecvr = (TextView) itemView.findViewById(R.id.tvrecvr);
            txtTimerecvr = (TextView) itemView.findViewById(R.id.txtTimerecvr);
            rightfram = (FrameLayout) itemView.findViewById(R.id.rightfram);
            lftfram = (FrameLayout) itemView.findViewById(R.id.lftfram);
            ivmsgstatus = (ImageView) itemView.findViewById(R.id.ivmsgstatus);
        }
    }
}
