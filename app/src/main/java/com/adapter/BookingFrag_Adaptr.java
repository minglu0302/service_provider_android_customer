package com.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.iserve.passenger.BookingFragment;
import com.iserve.passenger.R;
import com.pojo.ContainRow_Item;
import com.squareup.picasso.Picasso;
import com.utility.CircleTransform;
import com.utility.Scaler;
import com.utility.Variableconstant;

import java.util.ArrayList;

/**
 * Created by embed on 27/10/16.
 *
 */
public class BookingFrag_Adaptr extends RecyclerView.Adapter
{

    private Context mcontext;
    private ArrayList<ContainRow_Item> nubType = new ArrayList<>();
    double size[];
    private BookingFragment bookingFragment ;
    public BookingFrag_Adaptr(Context mcontext, ArrayList<ContainRow_Item> nubType,BookingFragment bookingFragment)
    {
        this.mcontext = mcontext;
        this.nubType = nubType;
        this.bookingFragment = bookingFragment;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(mcontext).inflate(R.layout.bookingappntmnt,parent,false);

        return new ViewHldr(view);
    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof  ViewHldr)
        {
            final ViewHldr hldr = (ViewHldr) holder;
           // Typeface lightfont = Typeface.createFromAsset(mcontext.getAssets(), Variableconstant.lightfont);
            Typeface regularfont = Typeface.createFromAsset(mcontext.getAssets(), Variableconstant.regularfont);
            if(nubType.get(position).isFirst())
            {
                hldr.rlmaincard.setVisibility(View.VISIBLE);
                if(nubType.get(position).getFdata()==null)
                {
                    hldr.ongoinghistory.setText(mcontext.getResources().getString(R.string.ongoinbooking));
                }
                else
                {
                    hldr.ongoinghistory.setText(mcontext.getResources().getString(R.string.pastbooking));
                }
            }
            else
            {
                hldr.rlmaincard.setVisibility(View.GONE);
            }
            size = Scaler.getScalingFactor(mcontext);
            double width1 = size[0] * 80;
            double height1 = size[1] * 80;

            if(!nubType.get(position).getpPic().equals(""))
            {
                Picasso.with(mcontext).load(nubType.get(position).getpPic()).
                        centerCrop().
                        transform(new CircleTransform())
                        .resize((int)width1,(int)height1)
                        .placeholder(R.drawable.booking_history_profile_default_image)
                        .into(hldr.ivprovprofle);
            }
            hldr.tvproprofession.setText(nubType.get(position).getCat_name());
            String name = nubType.get(position).getFname()+" "+nubType.get(position).getLname();
            hldr.tvproname.setText(name);
            String apptmentdatetime = nubType.get(position).getApntDate()+" . "+nubType.get(position).getApntTime();
            hldr.tvappointtime.setText(apptmentdatetime);
            hldr.tvaddress.setText(nubType.get(position).getAddrLine1());
            hldr.tvappointstatus.setText(nubType.get(position).getStatus());

            setTypeFace(regularfont,hldr);
            switch (nubType.get(position).getStatCode())
            {
                case "3":
                    hldr.tvappointstatus.setTextColor(Color.RED);
                    break;
                case "7":
                    if(Build.VERSION.SDK_INT>=23)
                        hldr.tvappointstatus.setTextColor(mcontext.getResources().getColor(R.color.parrotgreen,mcontext.getTheme()));
                    else
                        hldr.tvappointstatus.setTextColor(mcontext.getResources().getColor(R.color.parrotgreen));


                    break;
                case "9":
                    hldr.tvappointstatus.setTextColor(Color.BLACK);
                    String canelamt = mcontext.getResources().getString(R.string.currencySymbol)+" "+nubType.get(position).getCancelAmount();
                    hldr.cancelfee.setText(canelamt);
                    break;
                default:
                    hldr.tvappointstatus.setTextColor(Color.BLACK);
                    break;
            }

            String text = mcontext.getResources().getString(R.string.bookingid)+" "+nubType.get(position).getBid();
            hldr.tvbookingid.setText(text);


            hldr.aaptrlout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bookingFragment.itemclickedmethod(nubType.get(holder.getAdapterPosition()));

                }
            });
        }
    }
    private void setTypeFace(Typeface regularfont, ViewHldr hldr) {
        hldr.ongoinghistory.setTypeface(regularfont);
        hldr.tvappointtime.setTypeface(regularfont);
        hldr.tvappointstatus.setTypeface(regularfont);
        hldr.tvproname.setTypeface(regularfont);
        hldr.tvaddress.setTypeface(regularfont);
        hldr.tvproprofession.setTypeface(regularfont);
        hldr.tvbookingid.setTypeface(regularfont);
        hldr.cancelfee.setTypeface(regularfont);
    }

    @Override
    public long getItemId(int position) {
        return nubType.size();
    }

    @Override
    public int getItemCount() {
        return nubType.size();
    }


    private class ViewHldr extends RecyclerView.ViewHolder {
        TextView ongoinghistory,tvappointtime,tvappointstatus,tvproname,tvaddress
                ,tvproprofession,tvbookingid,cancelfee;
        ImageView ivprovprofle;
        RelativeLayout rlmaincard,aaptrlout;

        private ViewHldr(View itemView) {
            super(itemView);
            ivprovprofle = (ImageView) itemView.findViewById(R.id.ivprovprofle);
            ongoinghistory = (TextView) itemView.findViewById(R.id.ongoinghistory);
            tvappointtime = (TextView) itemView.findViewById(R.id.tvappointtime);
            tvappointstatus = (TextView) itemView.findViewById(R.id.tvappointstatus);
            tvproname = (TextView) itemView.findViewById(R.id.tvproname);
            tvaddress = (TextView) itemView.findViewById(R.id.tvaddress);
            tvproprofession = (TextView) itemView.findViewById(R.id.tvproprofession);
            tvbookingid = (TextView) itemView.findViewById(R.id.tvbookingid);
            cancelfee = (TextView) itemView.findViewById(R.id.cancelfee);
            rlmaincard = (RelativeLayout) itemView.findViewById(R.id.rlmaincard);
            aaptrlout = (RelativeLayout) itemView.findViewById(R.id.aaptrlout);

        }
    }

}
