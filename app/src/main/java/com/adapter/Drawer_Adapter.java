package com.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iserve.passenger.R;
import com.utility.Variableconstant;

/**
 * <h>Drawer_Adapter</h>
 * Created by embed on 14/12/15.
 *
 */
public class Drawer_Adapter extends ArrayAdapter
{
    Context context;
    private int [] images;
    private String [] value;
    private Typeface rowTextFont;
    public Drawer_Adapter(Context context, String[] values, int[] images) {
        super(context, R.layout.drawer_item, R.id.drawer_text,values);
        this.context=context;
        rowTextFont=Typeface.createFromAsset(context.getAssets(), Variableconstant.regularfont);
        this.images=images;
        this.value = values;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("ViewHolder") View row =layoutInflater.inflate(R.layout.drawer_item,parent,false);
        TextView rowText = (TextView) row.findViewById(R.id.drawer_text);
        ImageView drawer_image = (ImageView) row.findViewById(R.id.drawer_image);
        rowText.setTypeface(rowTextFont);
        rowText.setText(value[position]);
        drawer_image.setImageResource(images[position]);
        return row;
    }
}
