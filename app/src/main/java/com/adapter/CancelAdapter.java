package com.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.iserve.passenger.LiveBookingStatus;
import com.iserve.passenger.R;
import com.utility.Variableconstant;

import java.util.ArrayList;

/**
 * Created by embed on 16/8/16.
 *
 */
public class CancelAdapter extends BaseAdapter
{
    private Context mContext;
    private ArrayList<String> provider = new ArrayList<>();
    private Typeface regular;
    public CancelAdapter(Context context, ArrayList<String> provider)
    {

        this.mContext = context;
        this.provider = provider;
    }
    @Override
    public int getCount()
    {
        return provider.size();
    }

    @Override
    public Object getItem(int position)
    {

        return provider.get(position);
    }

    @Override
    public long getItemId(int position)
    {

        return provider.size();
    }

    public class ViewHolder
    {
        TextView tvforcancel;
        ImageView imageis;

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if(convertView==null||convertView.getTag()==null)
        {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cancel_bookingadapter, parent, false);
            holder.tvforcancel = (TextView) convertView.findViewById(R.id.tvforcancel);
            holder.imageis = (ImageView) convertView.findViewById(R.id.imageis);

            LiveBookingStatus liveBookingStatus = (LiveBookingStatus) mContext;
            regular = Typeface.createFromAsset(mContext.getAssets(), Variableconstant.regularfont);
            if(position == liveBookingStatus.selectedpostion){
                holder.imageis.setVisibility(View.VISIBLE);

            }else{
                holder.imageis.setVisibility(View.GONE);
            }
            holder.tvforcancel.setTypeface(regular);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvforcancel.setText(provider.get(position));

        return convertView;
    }
}
