package com.utility;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.iserve.passenger.ChattingActivity;
import com.iserve.passenger.Config;
import com.iserve.passenger.Notification_Handler;
import com.iserve.passenger.R;
import com.iserve.passenger.Splash_Screen;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pojo.ChatPushPojo;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by embed on 7/11/16.
 *
 */
public class MyFirebaseMessagingService  extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private NotificationUtils notificationUtils;
    String action= "";
    String message ="";
    String bid = "";
    SessionManager manager;
    String PicUrl = "";
    Context mcontext;
    String prolat = "";
    Gson gson;
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;
        if (remoteMessage.getNotification() != null)
        {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        if (remoteMessage.getData().size() > 0)
        {
            Log.d(TAG, "Notification payload: " + remoteMessage.getData());
            message = remoteMessage.getData().get("payload");
            mcontext = this;
            action = remoteMessage.getData().get("st");
            PicUrl = remoteMessage.getData().get("pic");
            bid = remoteMessage.getData().get("bid");
            prolat = remoteMessage.getData().get("dl");
            manager = new SessionManager(this);
            manager.setACTION(action);
            manager.setBID(bid);

            if(action.equals("5"))
            {
                if(Variableconstant.isLIVBOKINOPN)
                {
                    manager.setPUSHMSGFORMAP(message);
                    manager.setPUSHACTIONMAP(action);
                    manager.setPUSHBIDMAP(bid);
                    manager.setPUSHLATLNGMAP(prolat);
                }
                else
                {
                    manager.setPUSHMSGFORMAP("");
                }

            }
            switch (action) {
                case "23":

                    Intent pushNotification = new Intent(getApplicationContext(), Notification_Handler.class);
                    pushNotification.putExtra("message", message);
                    pushNotification.putExtra("statcode", this.action);
                    pushNotification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(pushNotification);
                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                    notificationUtils.playNotificationSound();
                    break;
                case "8":
                    if (!Variableconstant.forChatting) {
                        sendNotification(message);
                    }
                    break;
                default:
                    handleDataMessage(action, PicUrl, bid, prolat);
                    break;
            }

        }
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }
    // [END receive_message]

    /**
     *
     * @param message notification message
     */

    private void handleNotification(String message)
    {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            if(!action.equals("2"))
            {
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                pushNotification.putExtra("statcode",action);
                pushNotification.putExtra("bid",bid);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();
            }

        }/*else
        {
            sendNotification(message);
        }*/
    }


    /**
     *
     * @param action
     * @param picUrl
     * @param bid
     * @param prolat
     */

    private void handleDataMessage(String action, String picUrl, String bid, String prolat) {

            this.action = action;
            this.PicUrl = picUrl;
            this.bid = bid;
            this.prolat = prolat;
            String title = getResources().getString(R.string.app_name);
            String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());


            Log.e(TAG, "action " + this.action);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "imageUrl: " + PicUrl);
            Log.e(TAG, "prolat: " + this.prolat);
            Log.e(TAG, "bid: " + this.bid);
            Log.e(TAG, "timestamp: " + timestamp);


            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
              //  Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                if(!this.action.equals("2"))
                {
                    if(this.action.equals("5"))
                    {
                       if(!Variableconstant.isLIVBOKINOPN)
                       {
                           genralmethod();
                       }
                    }
                    else {
                        genralmethod();
                    }

                }

            } else {
                // app is in background, show the notification in notification tray

                if(!this.action.equals("2"))
                {
                    Intent resultIntent = new Intent(getApplicationContext(), Notification_Handler.class);//MainActivity
                    resultIntent.putExtra("message", message);
                    resultIntent.putExtra("statcode", this.action);
                    resultIntent.putExtra("bid", this.bid);
                    if(this.action.equals("5"))
                    {
                        resultIntent.putExtra("prolat", this.prolat);
                    }

                  /*  NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                    notificationUtils.playNotificationSound();*/
                    // check for image attachment
                    if (TextUtils.isEmpty(PicUrl)) {
                        showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, PicUrl);
                    }
                }

            }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody)
    {

        gson = new Gson();
        ChatPushPojo chatPushPojo =  gson.fromJson(messageBody,ChatPushPojo.class);
        Bitmap icon1 = BitmapFactory.decodeResource(getResources(),
                    R.drawable.ic_launcher);

       /* NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(chatPushPojo.getMsg());
        bigText.setBigContentTitle(getResources().getString(R.string.app_name));
        bigText.setSummaryText("" + getResources().getString(R.string.app_website));*/
        int numMessages = 0;

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setSummaryText("" + getResources().getString(R.string.app_website));
        inboxStyle.setBigContentTitle(chatPushPojo.getMsg());
        String[] events = new String[6];
        // Moves events into the expanded layout
        // Moves events into the expanded layout
        for (int i=0; i < events.length; i++) {

            inboxStyle.addLine(events[i]);
        }
       // mBuilder.setStyle(inboxStyle);
        Log.d(TAG, "action: " + chatPushPojo.getMsg());
        Intent intent = new Intent(this, ChattingActivity.class);
        intent.putExtra("bid",chatPushPojo.getBid());
        intent.putExtra("proimg",chatPushPojo.getPic());
        intent.putExtra("providername",chatPushPojo.getName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.share_iserve_logo)
                .setLargeIcon(icon1)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(chatPushPojo.getMsg())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(inboxStyle)
                .setNumber(++numMessages)
                .setContentIntent(pendingIntent);
                 NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1 /* ID of notification */, notificationBuilder.build());
    }

    public void genralmethod()
    {
        Intent pushNotification = new Intent(getApplicationContext(), Notification_Handler.class);
        pushNotification.putExtra("message", message);
        pushNotification.putExtra("statcode", this.action);
        pushNotification.putExtra("bid", this.bid);

        if(this.action.equals("5"))
        {
            pushNotification.putExtra("prolat", this.prolat);
        }
        pushNotification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(pushNotification);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.playNotificationSound();
    }
}