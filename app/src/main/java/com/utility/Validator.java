package com.utility;


import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by administrator on 11/5/15.
 *
 */
public class Validator {


    public boolean firstName_status(String firstName) {
        Boolean flag = true;
        for (int i = 0; i < firstName.length(); i++) {
            if (!((firstName.charAt(i) >= 'A' && firstName.charAt(i) <= 'Z') || (firstName.charAt(i) >= 'a' && firstName.charAt(i) <= 'z'))) {
                flag = false;
                break;
            }

        }
        return flag;
    }

    public boolean lastName_status(String lastName) {
        Boolean flag = true;
        for (int i = 0; i < lastName.length(); i++) {
            if (!((lastName.charAt(i) >= 'A' && lastName.charAt(i) <= 'Z') || (lastName.charAt(i) >= 'a' && lastName.charAt(i) <= 'z'))) {
                flag = false;
                break;
            }

        }

        return flag;
    }

    public boolean emailValidation(String email)
    {


        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();




        /*String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();*/
    }

    public boolean emailId_status(String email) {
        int length = email.length();
        boolean flag1 = false;
        boolean flag2 = false;
        boolean flag3 = false;
        boolean flag4 = false;
        boolean flag5 = false;
        boolean flag6 = false;
        boolean flag7 = false;
        int count = 0;

        //Condition 1 lenght shoul be >3 and <20
        flag1 = (length > 3 && length < 30);

        //The emailId must include "@" followed by a minimum of 1 and maximum of 2 "." characters.
        int temp = email.length();
        if (email.contains("@")) {
            flag2 = true;
            int a = email.indexOf("@");
            for (int i = a; i < temp; i++) {
                if (email.charAt(i) == '.') {
                    flag3 = true;
                    count = count + 1;
                }
            }
            flag4 = !(count < 1 || count > 2);
        } else {
            flag2 = false;

        }


        //The substring before "@" must contain a combination of Upper Case, Lower Case and "_"
        //Unable to get the right RegEx here!
        flag5 = email.matches("[A-Z a-z 0-9_ .]+@.*");

        //The first letter of the email Id must be in Upper Case.
        flag6 = (email.charAt(0) >= 'A' && email.charAt(0) <= 'Z') || (email.charAt(0) >= 'a' && email.charAt(0) <= 'z');
        return flag1 && flag2 && flag3 && flag4 && flag5 && flag6;


    }

    public boolean contac_Status(String no) {
        return no.matches("[0-9]{10}");
    }

    public boolean passStatus(String pass)
    {
        if(pass.matches("(?=.*[@#$%!])"))
        {
            Log.i("Validatepass ","password "+pass.matches("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{7,40})"));
            return pass.matches("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{7,40})");
        }
        else
        {
            Log.i("Validatepass ","password "+pass.matches("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[0-9]).{7,40})"));
            return pass.matches("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z]).{7,40})");
        }


    }
}