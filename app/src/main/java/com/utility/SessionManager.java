package com.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;

//import com.pojo.PubNub_pojo;

/**
 * Created by embed on 10/5/16.
 *
 */
public class SessionManager
{

    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name



    public static final String PREF_NAME ="iServePref";

    public static String Coustom_email="coustomer_email";

    public final String PUNUB_RES = "pubnubres";

    public final String LATITUDE = "latitude";

    public final String LONGITUDE = "longitude";

    public final String mAssArry = "massarr";

    public final String ServiceProviderNAme = "serviceprovidername";

    public final String ServiceProvdr = "serviceprovider";

    public final String SavedAddress = "saveaddredd";

    public final String ArraImage = "arryimage";

    private static final String PROFILE_PIC = "profilepic";

    private static final String PROPROFILE_PIC = "proprofilepic";

    private static final String Device_Id = "device_id";

    private static final String CUSTOMER_ID = "customerid";

    public static final String DOCTOR_SLOTS = "doctor_slots";

    private static final String CUSTOMER_FNME = "customer_fmne";

    private static final String CUSTOMER_LNME = "customer_lnme";

    public final String JOBSID = "jobSid";

    public final String JOBLATI = "joblati";

    public final String JOBLONGI = "joblongi";

    public final String Payload = "payload";

    public final String ProAccept = "proaccept";

    public final String ProOntheWay = "proOntheway";

    public final String ACTION = "action";

    public final String BID = "bid";

    public final String TIME_ST = "time_st";

    public final String SavedTime = "savedtime";

    public final String SavedSystemTime = "savedsystmtime";

    public final String Language = "language";

    public final String Oldpassword = "oldpassword";

    public final String StripeKey = "stripekey";
    private final String PUSHMSGFORMAP = "pushformap";
    private final String PUSHBIDMAP = "pushbidmap";
    private final String PUSHACTIONMAP = "pushacionmap";
    private final String PUSHLATLNGMAP = "pushlatlngmap";
    private final String BIDEvent = "bidevent";

    // Constructor
    public SessionManager(Context context)
    {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
        //editor.commit();
    }

    public boolean isLogin(){
        return pref.getBoolean("is_LOGIN",false);

    }
    public void setIsLogin(boolean value){
        editor.putBoolean("is_LOGIN",value);
        editor.commit();
    }

    public void clearSession()
    {
        editor.clear();
        editor.commit();
    }


    public void setRegistrationId(String RegistrationId){
        editor.putString("getRegistrationId", RegistrationId);
        editor.commit();
    }

    public String getRegistrationId() {
        return pref.getString("getRegistrationId","");

    }

    public void setSession(String got_Session){
        editor.putString("got_Session", got_Session);
        editor.commit();
    }

    public String getSession() {
        return pref.getString("got_Session","");

    }


    public String getSavedAddress()
    {
        return pref.getString(SavedAddress,"");
    }

    public void setSavedAddress(String savedAddress)
    {
        editor.putString(SavedAddress,savedAddress);
        editor.commit();
    }

    public void setSocketRes(String punubRes)
    {
        editor.putString(PUNUB_RES,punubRes);
        editor.commit();
    }

    public String getPUNUB_RES()
    {
        return pref.getString(PUNUB_RES,"");
    }


    public String getOldpassword(){
        return pref.getString(Oldpassword, "");
    }

    public void setOldpassword(String oldpassword){
        editor.putString(Oldpassword, oldpassword);
        editor.commit();
    }


    public void setCoupon(String coupon){
        editor.putString("coupon", coupon);
        editor.commit();
    }

    public String getCoupon(){
        return pref.getString("coupon","");

    }

    public void setPUSHMSGFORMAP(String pushmsgformap) {
        editor.putString(PUSHMSGFORMAP, pushmsgformap);
        editor.commit();
    }
    public String getPUSHMSGFORMAP()
    {
        return pref.getString(PUSHMSGFORMAP, "");
    }
    void setPUSHBIDMAP(String pushbidmap) {
        editor.putString(PUSHBIDMAP, pushbidmap);
        editor.commit();
    }
    public String getPUSHBIDMAP()
    {
        return pref.getString(PUSHBIDMAP, "");
    }
    void setPUSHACTIONMAP(String pushactionmap) {
        editor.putString(PUSHACTIONMAP, pushactionmap);
        editor.commit();
    }
    public String getPUSHACTIONMAP()
    {
        return pref.getString(PUSHACTIONMAP, "");
    }
    void setPUSHLATLNGMAP(String pushlatlngmap) {
        editor.putString(PUSHLATLNGMAP, pushlatlngmap);
        editor.commit();
    }
    public String getPUSHLATLNGMAP()
    {
        return pref.getString(PUSHLATLNGMAP, "");
    }

    public void storecoustomerEmail(String detail) {
        editor.putString(Coustom_email, detail);
        editor.commit();
    }
    public String getCoustomerEmail()
    {
        return pref.getString(Coustom_email, "");
    }


    public void SetChannel(String value){

        editor.putString("Coustomer_Chhanel", value);
        editor.commit();

    }
    public String getChannel(){
        return pref.getString("Coustomer_Chhanel", "");
    }

    public void setLanguage(String language)
    {
        editor.putString(Language,language);
        editor.commit();
    }
    public String getLanguage(){
        return pref.getString(Language, "");
    }

  /********************************************************/

  public String getLATITUDE()
  {
      return pref.getString(LATITUDE,"");
  }

  public void setLATITUDE(String latitude)
  {
      editor.putString(LATITUDE,latitude);
      editor.commit();
  }

    /********************************************************/

    public String getLONGITUDE()
    {
        return pref.getString(LONGITUDE,"");
    }

    public void setLONGITUDE(String longitude)
    {
        editor.putString(LONGITUDE,longitude);
        editor.commit();
    }

    /********************************************************/

    public String getJOBLATI()
    {
        return pref.getString(JOBLATI,null);
    }

    public void setJOBLATI(String joblati)
    {
        editor.putString(JOBLATI,joblati);
        editor.commit();
    }

    /********************************************************/

    public String getJOBLONGI()
    {
        return pref.getString(JOBLONGI,null);
    }

    public void setJOBLONGI(String joblongi)
    {
        editor.putString(JOBLONGI,joblongi);
        editor.commit();
    }

    /********************************************************/

    public String getJOBSID()
    {
        return pref.getString(JOBSID,"");
    }

    public void setJOBSID(String jobsid)
    {
        editor.putString(JOBSID,jobsid);
        editor.commit();
    }

    /********************************************************/



   /* public String getmAssArry()
    {
        return pref.getString(mAssArry,null);
    }

    public void setmAssArry(String massArry)
    {
        editor.putString(mAssArry,massArry);
        editor.commit();
    }*/

    /********************************************************/

    public String getServiceProviderNAme()
    {
        return pref.getString(ServiceProviderNAme,null);
    }

    public void setServiceProviderNAme(String serviceProviderme)
    {
        editor.putString(ServiceProviderNAme,serviceProviderme);
        editor.commit();
    }//ArraImage

    /********************************************************/

    public String getServiceProvdr()
    {
        return pref.getString(ServiceProvdr,"");
    }

    public void setServiceProvdr(String serviceprovdr)
    {
        editor.putString(ServiceProvdr,serviceprovdr);
        editor.commit();
    }

    /********************************************************/

    public String getStripeKey()
    {
        return pref.getString(StripeKey,"");
    }

    public void setStripeKey(String stripeKey)
    {
        editor.putString(StripeKey,stripeKey);
        editor.commit();
    }

    /********************************************************/

    /*public String getArraImage()
    {
        return pref.getString(ArraImage,"");
    }

    public void setArraImage(String arraImage)
    {
        editor.putString(ArraImage,arraImage);
        editor.commit();
    }*/
    /*************************************************************/
    public void setProfilePic(String profilePic)
    {
        editor.putString(PROFILE_PIC,profilePic);
        editor.commit();
    }
    public String getProfilePic()
    {
        return pref.getString(PROFILE_PIC,"");
    }

    /*************************************************************/
    public void setDevice_Id(String deviceId)
    {
        editor.putString(Device_Id,deviceId);
        editor.commit();
    }
    public String getDevice_Id()
    {
        return pref.getString(Device_Id,null);
    }

    /*************************************************************/
    public void setCustomerId(String customerId)
    {
        editor.putString(CUSTOMER_ID,customerId);
        editor.commit();
    }
    public String getCustomerId()
    {
        return pref.getString(CUSTOMER_ID,null);
    }

    /*************************************************************/
    /*public void setCatId(String catId)
    {
        editor.putString(CAT_ID,catId);
        editor.commit();
    }
    public String getCatId()
    {
        return pref.getString(CAT_ID,null);
    }*/

    /*************************************************************/

    public void setCustomerFnme(String customerFnme)
    {
        editor.putString(CUSTOMER_FNME,customerFnme);
        editor.commit();
    }
    public String getCustomerFnme()
    {
        return pref.getString(CUSTOMER_FNME,"");
    }

    /*************************************************************/

    public void setCustomerLnme(String customerLnme)
    {
        editor.putString(CUSTOMER_LNME,customerLnme);
        editor.commit();
    }
    public String getCustomerLnme()
    {
        return pref.getString(CUSTOMER_LNME,"");
    }

    /***************************************************/

   /* public void setIsTimer(Boolean model)
    {
        editor.putBoolean(TIMER_FLAG, model);
        editor.commit();
    }
    public boolean isTimer()
    {
        return pref.getBoolean(TIMER_FLAG, false);
    }*/


    /*public void setTimeWhile_Paused(String timePaused)
    {
        editor.putString(TIMEWHILE_PAUSED, timePaused);
        editor.commit();
    }
    public String getTimeWhile_Paused()
    {
        return pref.getString(TIMEWHILE_PAUSED, "-1");
    }*/

   /* public void setElapsedTime(int elapsedTime) {
        editor.putInt(ELAPSED_TIME, elapsedTime);
        editor.commit();
    }
    public int getElapsedTime()
    {
        return pref.getInt(ELAPSED_TIME, -1);
    }*/

    /***************************************************///

    public void setProprofilePic(String proprofilePic) {
        editor.putString(PROPROFILE_PIC, proprofilePic);
        editor.commit();
    }
    public String getProprofilePic()
    {
        return pref.getString(PROPROFILE_PIC, "");
    }

    /***************************************************///Payload

    public void setPayload(String payload) {
        editor.putString(Payload, payload);
        editor.commit();
    }
    public String getPayload()
    {
        return pref.getString(Payload, "");
    }

    /***************************************************/


    public boolean isProAccept()
    {
        return pref.getBoolean(ProAccept, false);
    }

    public void setProAccept(boolean proaccept)
    {
        editor.putBoolean(ProAccept, proaccept);
        editor.commit();
    }

    /***************************************************/


    public boolean isProOntheWay()
    {
       // boolean flag =pref.getBoolean(ProOntheWay, false);
        return pref.getBoolean(ProOntheWay, false);
    }

    public void setProOntheWay(boolean proOntheWay)
    {
        editor.putBoolean(ProOntheWay, proOntheWay);
        editor.commit();
    }


    /***************************************************/


    /*public boolean isProArrived()
    {
       // boolean flag =pref.getBoolean(ProArrived, false);
        return pref.getBoolean(ProArrived, false);
    }

    public void setProArrived(boolean proarrived)
    {
        editor.putBoolean(ProArrived, proarrived);
        editor.commit();
    }*/

    /***************************************************/


    /*public boolean isProStarted()
    {
        boolean flag =pref.getBoolean(ProStarted, false);
        return flag;
    }

    public void setProStarted(boolean proStarted)
    {
        editor.putBoolean(ProStarted, proStarted);
        editor.commit();
    }*/
    /***************************************************/

    public void setACTION(String action) {
        editor.putString(ACTION, action);
        editor.commit();
    }
    public String getACTION()
    {
        return pref.getString(ACTION, "");
    }

    /***************************************************/

    public void setBID(String bid) {
        editor.putString(BID, bid);
        editor.commit();
    }
    public String getBID()
    {
        return pref.getString(BID, "");
    }


    /************************************************************/

    public void setDoctorSlots(String doctorSlots)
    {
        editor.putString(DOCTOR_SLOTS,doctorSlots);
        editor.commit();
    }
    public String getDoctorSlots()
    {
        return pref.getString(DOCTOR_SLOTS,null);
    }

    /***************************************************/

    public void setTIME_ST(String time_st) {
        editor.putString(TIME_ST, time_st);
        editor.commit();
    }
    public String getTIME_ST()
    {
        return pref.getString(TIME_ST, "");
    }

    /***************************************************/

    public void setSavedTime(long savedTime) {
        editor.putLong(SavedTime, savedTime);
        editor.commit();
    }
    public long getSavedTime()
    {
        return pref.getLong(SavedTime, 0);
    }

    public void setSavedSystemTime(long savedsystemtime) {
        editor.putLong(SavedSystemTime, savedsystemtime);
        editor.commit();
    }
    public long getSavedSystemTime()
    {
        return pref.getLong(SavedSystemTime, 0);
    }












    public void setJobIds(ArrayList<String> jobIds)
    {
        String job_ids = "";
        if(jobIds != null)
        {
            job_ids = TextUtils.join(",", jobIds);
        }

        editor.putString("Job_Ids", job_ids);
        editor.commit();
    }

    public ArrayList<String> getJobIds()
    {
        String str = pref.getString("Job_Ids", "");
        ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(str.split(",")));
        return arrayList;
    }
    public void setBIDEvent(String bidEvent){
        editor.putString(BIDEvent, bidEvent);
        editor.commit();
    }

    public String getBIDEvent() {
        return pref.getString(BIDEvent,"");
    }
}

