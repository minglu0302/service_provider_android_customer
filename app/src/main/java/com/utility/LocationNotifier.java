package com.utility;

import android.location.Location;

public interface LocationNotifier {

	void updatedInfo(String info);
	void locationUpdates(Location location);
	void locationFailed(String message);

}
