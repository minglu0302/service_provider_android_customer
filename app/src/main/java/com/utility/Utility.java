package com.utility;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.CalendarContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

//import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iserve.passenger.Login_Page;
import com.iserve.passenger.MenuActivity;
import com.iserve.passenger.R;
import com.iserve.passenger.Splash_Screen;
//import com.squareup.okhttp.MediaType;
//import com.squareup.okhttp.OkHttpClient;
//import com.squareup.okhttp.Request;
//import com.squareup.okhttp.RequestBody;
//import com.squareup.okhttp.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import io.socket.client.IO;
import io.socket.client.Socket;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static java.lang.Math.PI;
import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

/**
 * <h>Utility</h>
 * Created by embed on 5/12/15.
 */
public class Utility
{


    public static void printLog(String... msg)
    {
        String str="";
        for(String i : msg)
        {
            str= str+"\n"+i;
        }
        Log.i("iServe ", str);
    }

    public static void statusbar(Activity activity)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            int blueColorValue = Color.parseColor("#808080");//#2598ED
            window.setStatusBarColor(blueColorValue);
        }
    }

    public static String currencySymbol()
    {


        // create a currency for uk locale
        Locale locale = Locale.UK;
        Currency curr = Currency.getInstance(locale);

        // get and print the symbol of the currency
        String symbol = curr.getSymbol(locale);
        System.out.println("Currency symbol is = " + symbol);
        return symbol;
    }


    public static String callhttpRequest(String url) {
        System.out.println("com.com.utility url..." + url);
        url=url.replaceAll(" ", "%20");
        String resp = null;
        HttpGet httpRequest;
        try {
            httpRequest = new HttpGet(url);
            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 60000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 60000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
            HttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(httpRequest);
            HttpEntity entity = response.getEntity();
            BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
            final long contentLength = bufHttpEntity.getContentLength();
            if ((contentLength >= 0))
            {
                InputStream is = bufHttpEntity.getContent();
                int tobeRead = is.available();
                System.out.println("Utility callhttpRequest tobeRead.."+tobeRead);
                ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
                int ch;

                while ((ch = is.read()) != -1)
                {
                    bytestream.write(ch);
                }

                resp = new String(bytestream.toByteArray());
                System.out.println("Utility callhttpRequest resp.."+resp);
            }
        } catch (MalformedURLException e) {
            System.out.println("Utility callhttpRequest.."+e);
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            System.out.println("Utility callhttpRequest.."+e);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Utility callhttpRequest.."+e);
            e.printStackTrace();
        }catch (Exception e) {
            System.out.println("Utility Exception.."+e);
        }
        return resp;
    }


    //Http post method

    public static HttpResponse doPost(String url, Map<String, String> kvPairs)
            throws IOException
    {
        // HttpClient httpclient = new DefaultHttpClient();

        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpClient httpclient = defaultHttpClient;

        HttpPost httppost = new HttpPost(url);

        if (kvPairs != null || kvPairs.isEmpty() == false)
        {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(kvPairs.size());
            String k, v;
            Iterator<String> itKeys = kvPairs.keySet().iterator();

            while (itKeys.hasNext())
            {
                k = itKeys.next();
                v = kvPairs.get(k);
                nameValuePairs.add(new BasicNameValuePair(k, v));
            }

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
        }

        HttpResponse response;
        response = httpclient.execute(httppost);
        Log.i("TAG", "doPost response........."+response);
        return response;
    }



    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[2084];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    /************************************************    for checking internet connection*******/
    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivity = null;
        boolean isNetworkAvail = false;
        try {
            connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info)
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            printLog("info for network", anInfo.getTypeName() + " " + anInfo.getExtraInfo());
                            return true;
                        }
                }
            }
            return false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally{
            if (connectivity != null) {
                connectivity = null;
            }
        }
        return isNetworkAvail;
    }

    /************************************************   show the alert*******/


    public static void ShowAlert(String msg,Context context)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle("Note:");

        // set dialog message
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)

                .setNegativeButton("OK",new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int id)
                    {
                        //closing the application
                        dialog.dismiss();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

    }

    public static ProgressDialog GetProcessDialog(Activity activity)
    {
        // prepare the dialog box
        ProgressDialog dialog = new ProgressDialog(activity);
        // make the progress bar cancelable
        dialog.setCancelable(true);
        // set a message text
        dialog.setMessage("Loading...");

        // show it
        return dialog;
    }



    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {

            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /********************************************************************************************************************/
    //GETTING CURRENT DATE
    public static String date(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));
        return formater.format(date);
    }

    /********************************************************************************************************************/
    //GETTING CURRENT DATE
    public static String dateintwtfour(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formater.format(date);
    }

    /********************************************************************************************************************/
    //GETTING CURRENT DATE
    public static String dateMonth(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM");
        return formater.format(date);
    }

    /********************************************************************************************************************/
    //GETTING CURRENT DATE
    public static String localTime(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        SimpleDateFormat formater = new SimpleDateFormat("Z");
        return formater.format(date);
    }



    public static String getLocalTimeByUtcDateTime(String stringdate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//2016-11-26 07:41:04
        String returnStringDate="";
        try {
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = sdf.parse(stringdate);
            sdf.setTimeZone(TimeZone.getDefault());
            returnStringDate=sdf.format(date);
            Utility.printLog("gmt date "+returnStringDate);
        }catch (Exception e)
        {
            Utility.printLog("catch reasons for gmt date "+e.getMessage());
        }
        return returnStringDate;
    }
    public static void doJsonRequest(String request_Url,JSONObject requestParameters,JsonRequestCallback callbacks)
    {
        Utility.JsonHttpRequestData data = new Utility.JsonHttpRequestData();
        data.request_Url = request_Url;
        data.requestParameter = requestParameters;
        data.callbacks = callbacks;

        new JsonHttpRequest().execute(data);
    }

    private static  class JsonHttpRequestData
    {
        String request_Url;
        JSONObject requestParameter;
        JsonRequestCallback callbacks;
    }
    /********************************************************************************************************************/

    private  static class JsonHttpRequest extends AsyncTask<JsonHttpRequestData, Void, String>
    {
        JsonRequestCallback callbacks;
        boolean error =false;

        @Override
        protected void onPreExecute()
        {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(JsonHttpRequestData... params)
        {
            // TODO Auto-generated method stub
            callbacks = params[0].callbacks;
            String result="";

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(params[0].request_Url);

            HttpConnectionParams.setConnectionTimeout(client.getParams(), 60000); //Timeout Limit
            HttpResponse http_response;

            StringEntity request;
            try
            {
                request = new StringEntity( params[0].requestParameter.toString(),HTTP.UTF_8);

                // To Check sending parameters open following comment
                Utility.printLog("Request Data: "+params[0].requestParameter.toString());

                request.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(request);
                http_response = client.execute(post);
                result = EntityUtils.toString(http_response.getEntity());
            }
            catch (UnsupportedEncodingException e)
            {
                error= true;
                result = e.toString();
                e.printStackTrace();
            }
            catch (ClientProtocolException e)
            {
                error= true;
                result = e.toString();
                e.printStackTrace();
            }
            catch (IOException e)
            {
                error= true;
                result = e.toString();
                e.printStackTrace();
            }
            catch (Exception e)
            {
                error= true;
                result = e.toString();
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            if(!error)
            {
                callbacks.onSuccess(result);
            }
            else
            {
                callbacks.onError(result);
            }
        }
    }


    public interface JsonRequestCallback
    {
        /**
         * Called When Success result of JSON request
         *
         * @param result if get successful response
         */
        void onSuccess(String result);


        /**
         * Called When Error result of JSON request
         *
         * @param error if get error response
         */
        void onError(String error);

    }
    /***************************************************OKHTTP JSON Request*****************************************************/

    public static Bitmap getRoundedCroppedBitmap(Bitmap bitmap, int radius)
    {

        Bitmap finalBitmap;
        if (bitmap.getWidth() != radius || bitmap.getHeight() != radius)
            finalBitmap = Bitmap.createScaledBitmap(bitmap, radius, radius,
                    false);
        else
            finalBitmap = bitmap;
        Bitmap output = Bitmap.createBitmap(finalBitmap.getWidth(),
                finalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        if (bitmap != null && !bitmap.isRecycled()) {
            bitmap.recycle();
            // result = null;
        }

        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, finalBitmap.getWidth(),
                finalBitmap.getHeight());

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.parseColor("#BAB399"));
        canvas.drawCircle(finalBitmap.getWidth() / 2 + 0.7f,
                finalBitmap.getHeight() / 2 + 0.7f,
                finalBitmap.getWidth() / 2 + 0.1f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        if(finalBitmap!=null && !finalBitmap.isRecycled())
        canvas.drawBitmap(finalBitmap, rect, rect, paint);



        return output;
    }


    /**
     * TODO 根据包名判断是否在最前面显示
     *
     * @param context {@link Context}
     * @param packageName
     * @author Melvin
     * @date 2013-4-23
     * @return boolean
     */
    public static boolean isTopActivity(Context context, String packageName) {
        if (context == null ) {
            return false;
        }//|| isNull(packageName)
        int id = context.checkCallingOrSelfPermission(android.Manifest.permission.GET_TASKS);
        if (PackageManager.PERMISSION_GRANTED != id) {
            return false;
        }

        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasksInfo = activityManager.getRunningTasks(1);
        if (tasksInfo.size() > 0) {
            if (packageName.equals(tasksInfo.get(0).topActivity.getPackageName())) {
                return true;
            }
        }
        return false;
    }


    public static String doubleformate(String datavalue)
    {
        NumberFormat  formatter = new DecimalFormat("#0.00");
        String datavalueIs;
        if(datavalue.contains(","))
        {
            String value = datavalue.replace(",",".");
            datavalueIs = formatter.format(Double.parseDouble(value));

        }
        else
        {
            datavalueIs = formatter.format(Double.parseDouble(datavalue));
        }

        return datavalueIs;
    }


    public void setAmtOnRecept(String amt, TextView textView, String currencySybmol)
    {
        if(amt.equals(""))
        {
            String timefee = currencySybmol+" 0.00";
            textView.setText(timefee);
        }else
        {
            String timefee =currencySybmol+" "+ Utility.doubleformate(amt);
            textView.setText(timefee);
        }
    }

    public static LatLngBounds setBound(double lat, double lng)
    {
        LatLng pos1,pos2;
        pos1 = latLngAtDistance(lat,lng,50000,225.0);
        pos2 = latLngAtDistance(lat,lng,50000,45.0);
        return new LatLngBounds(pos1, pos2);
    }

    private static LatLng latLngAtDistance(double latitude, double longitude, double distanceInMetres, double angle) {
        double brngRad = toRadians(angle);
        double latRad = toRadians(latitude);
        double lonRad = toRadians(longitude);
        int earthRadiusInMetres = 6371000;
        double distFrac = distanceInMetres / earthRadiusInMetres;

        double latitudeResult = asin(sin(latRad) * cos(distFrac) + cos(latRad) * sin(distFrac) * cos(brngRad));
        double a = atan2(sin(brngRad) * sin(distFrac) * cos(latRad), cos(distFrac) - sin(latRad) * sin(latitudeResult));
        double longitudeResult = (lonRad + a + 3 * PI) % (2 * PI) - PI;

        System.out.println("latitude: " + toDegrees(latitudeResult) + ", longitude: " + toDegrees(longitudeResult));

        return new LatLng(toDegrees(latitudeResult),toDegrees(longitudeResult));
    }
    public static Bitmap setCreditCardLogo(String cardMethod, Context context)
    {
        Bitmap anImage;
        Drawable myDrawable;
        switch (cardMethod)
        {

            case "Visa":
                myDrawable = context.getResources().getDrawable(R.drawable.visa);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            case "MasterCard":
                myDrawable = context.getResources().getDrawable(R.drawable.master_card);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            case "American Express":
                myDrawable = context.getResources().getDrawable(R.drawable.amex);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            case "Discover":
                myDrawable = context.getResources().getDrawable(R.drawable.amex_back);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            default:
                myDrawable = context.getResources().getDrawable(R.drawable.cc_back);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
        }
        return anImage;
    }

    public static void hideInputMethod(Context context, View view){
        InputMethodManager inputMethodManager= (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void deleteEventFromCalender(String bookid,SessionManager manager,Context context) {
        Gson gson = new Gson();
        String json = manager.getBIDEvent();
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        Log.d("TAG"," JSONOBJ "+json+ " TYPE "+type);
        ArrayList<String> arrayList = gson.fromJson(json,type);
        if(arrayList!=null)
        {
            for(String s:arrayList){
                String arr[]=s.split(",");
                if(bookid.equals(arr[0])){
                    Log.d("SHIJEN", "deleteEventFromCalender: "+arr[1]);
                    long eventID = Long.parseLong(arr[1]);
                    Uri deleteUri = null;
                    deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, eventID);
                    int rows = context.getContentResolver().delete(deleteUri, null, null);
//                Log.i("deleteEventFromCalender", "Rows deleted: " + rows);
                    arrayList.remove(s);
                    String bidEventJson = gson.toJson(arrayList);
                    manager.setBIDEvent(bidEventJson);
                    break;

                }
            }
        }

    }

    public static void setMAnagerWithBID(Context mcontext,SessionManager manager)
    {
        Log.d("TAG","TAGISOUT "+mcontext);
        Intent intent = new Intent(mcontext, Splash_Screen.class);
        manager.setIsLogin(false);
        Variableconstant.BIDEvent=manager.getBIDEvent();
        manager.clearSession();
        manager.setBIDEvent(Variableconstant.BIDEvent);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        mcontext.startActivity(intent);
        ((Activity)mcontext).finish();

    }

    /*public static void setManagerWithBID(Activity mcontext,SessionManager manager)
    {
        Log.d("TAG","TAGISOUT "+mcontext);
        Intent intent = new Intent(mcontext, Splash_Screen.class);
        manager.setIsLogin(false);
        Variableconstant.BIDEvent=manager.getBIDEvent();
        manager.clearSession();
        manager.setBIDEvent(Variableconstant.BIDEvent);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        mcontext.startActivity(intent);
        mcontext.finish();

    }*/
}
