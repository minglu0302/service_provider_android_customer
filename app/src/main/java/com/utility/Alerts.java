package com.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;

import com.iserve.passenger.MenuActivity;
import com.iserve.passenger.R;


/**
 * Created by rahul on 23/7/15.
 */
public class Alerts extends Activity
{

    Context mcontext;



    public Alerts(Context mcontext)
    {
        this.mcontext = mcontext;
    }





    public void  showLocationAlert(final Context context)
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle(context.getResources().getString(R.string.location_alert_title));

        // Setting Dialog Message
        alertDialog.setMessage(context.getResources().getString(R.string.location_alert_message));

        alertDialog.setCancelable(false);


        // On pressing Settings button
        alertDialog.setPositiveButton(context.getResources().getString(R.string.action_settings),
                new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,int which)
            {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton(context.getResources().getString(R.string.action_cancel), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {


                dialog.dismiss();
                ((Activity)context).finish();

            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void  showNetworkAlert(final Context context)
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle(context.getResources().getString(R.string.network_alert_title));
        alertDialog.setCancelable(false);

        // Setting Dialog Message
        alertDialog.setMessage(context.getResources().getString(R.string.network_alert_message));


        // On pressing Settings button
        alertDialog.setPositiveButton(context.getResources().getString(R.string.action_settings), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                dialog.dismiss();
                context.startActivity(intent);


            }
        });


        // Showing Alert Message
        alertDialog.show();
    }
    public void problemLoadingAlert(final Context context,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //setting Dialog Title
        builder.setTitle(context.getResources().getString(R.string.system_error));
        builder.setCancelable(false);

        //setting message for alert box
        builder.setMessage(message);

        // on pressing cancel button
        builder.setPositiveButton(context.getResources().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        // Showing Alert Message
        builder.show();
    }

    public void cancelAlert(String message,String cancel){
        AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);

        //setting Dialog Title
        builder.setTitle(cancel);
        builder.setCancelable(false);

        //setting message for alert box
        builder.setMessage(message);

        // on pressing cancel button
        builder.setPositiveButton(mcontext.getResources().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(mcontext, MenuActivity.class);
                startActivity(intent);
                finish();
                dialog.dismiss();
            }
        });
        // Showing Alert Message
        builder.show();
    }
    public void promoAlert(final Context context,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //setting Dialog Title
        builder.setTitle(context.getResources().getString(R.string.thnku));//thnku  system_error
        builder.setCancelable(false);

        //setting message for alert box
        builder.setMessage(message);

        // on pressing cancel button
        builder.setPositiveButton(context.getResources().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        // Showing Alert Message
        builder.show();
    }
    public void alert(final Context context,String message)
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle(context.getResources().getString(R.string.system_error));
        alertDialog.setCancelable(false);

        // Setting Dialog Message
        alertDialog.setMessage(message);


        // On pressing Settings button
        alertDialog.setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });


        // Showing Alert Message
        alertDialog.show();
    }
}
