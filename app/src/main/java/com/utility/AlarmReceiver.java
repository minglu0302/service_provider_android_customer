package com.utility;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.iserve.passenger.R;
import com.iserve.passenger.Splash_Screen;

/**
 * Created by embed on 17/1/17.
 *
 */
public class AlarmReceiver extends BroadcastReceiver {
    int notifyId=1;
    @Override
    public void onReceive(Context context, Intent intent) {
        //Toast.makeText(context,"Alarm has been set",Toast.LENGTH_SHORT).show();
    /*Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    Ringtone r = RingtoneManager.getRingtone(context, notification);
    r.play();*/

       /* MyFirebaseMessagingService myFirebaseMessagingService = new MyFirebaseMessagingService();

        myFirebaseMessagingService.sendNotification("You Have Booking");*/
        String messageBody =""+intent.getStringExtra("pk");
        /*Log.i("TAG ","ALAMR");
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mNotify=new NotificationCompat.Builder(context);
        mNotify.setSmallIcon(R.drawable.ic_launcher);
        mNotify.setContentTitle("Coding");
        mNotify.setContentText("INVENTO: Coding competition is going to be conducted today.");
        mNotify.setPriority(NotificationCompat.PRIORITY_HIGH);
        mNotify.setSound(defaultSoundUri);
        Intent resultIntent=new Intent(context,Splash_Screen.class);
        TaskStackBuilder stackBuilder=TaskStackBuilder.create(context);
        stackBuilder.addParentStack(Splash_Screen.class); //add the to-be-displayed activity to the top of stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mNotify.setContentIntent(resultPendingIntent);
        NotificationManager notificationManager=(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notifyId,mNotify.build());*/


        Bitmap icon1 = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.ic_launcher);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(messageBody);
        bigText.setBigContentTitle(context.getResources().getString(R.string.app_name));
        bigText.setSummaryText("" + context.getResources().getString(R.string.app_website));//extras.getString("smail")
        int numMessages = 0;
        Log.d("TAG", "action: " + messageBody);
        Intent rEintent = new Intent(context, Splash_Screen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, rEintent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.share_iserve_logo)
                .setLargeIcon(icon1)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
