package com.utility;

/**
 * Created by embed on 8/12/15.
 *
 */
public class Variableconstant
{
    //http://103.236.163.48/services.php
    public static final String service_url = "";//Service url

    public static final String pics_path="";//Image pics path

    public static final  String SOCKET_PATH="";// Socket path

    //public static final String project_id="61157302116";
    public static final String project_id="968470568666";

   // public static final String SERVERKEY ="AIzaSyA59rz78fX082cGbzzR5gP7E2xpqc35xzo";
    public static final String SERVERKEY ="";//Fcm notification serverKey

    public static final String PARENT_FOLDER="appName";

    public static final String TEMP_PHOTO_FILE_NAME = "appname.jpg";

    public static final String Amazonbucket = "appName/ProfileImages";
    public static final String AmazonProfileFolderName = "ProfileImages";

    public static final String AmazonJobbucket = "appName/JobImages";

    public static final String Amazoncognitoid ="";

    public static final String TERMS_LINK=""; //terms and condition link
    public static final String PRIVECY_LINK=""; //Privacy policy link
    public static final String LOGOUTURL=service_url+"logout";
    public static final String SIGNUPURL=service_url+"slaveSignup";
    public static final String SIGNIN_URL=service_url+"slaveLogin";
    public static final String EMAILVALIDATION=service_url+"validateEmailZip";
    public static final String PHONENOVALIDATION=service_url+"checkMobile";
    public static final String GETVERIFICATIONCODE=service_url+"getVerificationCode";
    public static final String VERIFYPHONE=service_url+"verifyPhone";
    public static final String FORGOTPASS=service_url+"forgotPassword";
    public static final String LIVEBOOKING=service_url+"liveBooking";
    public static final String GETPROFILE=service_url+"getProfile";
    public static final String UPDATEPROFILE=service_url+"updateProfile";
    public static final String CHECKCOUPON=service_url+"checkCoupon";
    public static final String SUPPORT = service_url+"support";
    public static final String REMOVECARD = service_url+"removeCard";
    public static final String CHILDSUPPORT=service_url;
    public static final String ADDCARD=service_url+"addCard";
    public static final String GETCARD=service_url+"getCards";
    public static final String lightfont = "fonts/OpenSans-Light_0.ttf";
    public static final String regularfont = "fonts/OpenSans-Regular_0.ttf";
    public static final String italicfont = "fonts/OpenSans-Italic_0.ttf";
    public static final String BoldFont = "fonts/OpenSans-Bold_0.ttf";
    public static final String SemiBoldFont = "fonts/OpenSans-Semibold_0.ttf";
    public static boolean showProfile = false;
    public static boolean comingfromlaterbooking = true;
    public static boolean cmngFrmsrvlst = false;
    public static boolean comgfrmConfirmscrn = false;
    public static boolean backpressforlist = false;
    public static boolean Service_group = false;
    public static boolean onresumeflag = false;
    public static boolean forParticularBooking = false;
    public static boolean forChatting = false;
    public static boolean isLIVBOKINOPN = false;
    public static int ent_btype = -1;
    public static int ent_forlaterbooking = -1;
    public static String ent_dt = "";
    public static String ent_SlotDate = "";
    public static String ent_txtValue = "";
    public static String now_servicegroup = "";
    public static int selectedlanid = 0;
    public static int pro_forlatrboking = 0;
    static String BIDEvent = "";
}
