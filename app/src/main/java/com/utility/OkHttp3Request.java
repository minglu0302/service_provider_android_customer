package com.utility;


import android.os.AsyncTask;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Shubham on 6/11/2015.
 */
public class OkHttp3Request
{
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    /**
     * To make JSON request
     *
     * @param request_Url :Provide Service URl to make request.
     * @param requestParameters :Provide JSONObject as request parameter.
     * @param callbacks :JsonRequestCallback defined in Utility for success or error callbacks.
     */

    public static void doOkHttp3Request(String request_Url, FormBody requestParameters, JsonRequestCallback callbacks)
    {
        OKHttpPostRequestData data = new OKHttpPostRequestData();
        data.request_Url = request_Url;
        data.requestParameter = requestParameters;
        data.callbacks = callbacks;

        new OkHttp3JsonRequest().execute(data);
    }
    private static class OKHttpPostRequestData
    {
        String request_Url;
        FormBody requestParameter;
        JsonRequestCallback callbacks;
    }
    /*****************************************************/
    private  static class OkHttp3JsonRequest extends AsyncTask<OKHttpPostRequestData, Void, String>
    {
        JsonRequestCallback callbacks;
        boolean error =false;
        @Override
        protected void onPreExecute()
        {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(OKHttpPostRequestData... params)
        {
            // TODO Auto-generated method stub
            callbacks = params[0].callbacks;
            String result="";

            try
            {
                OkHttpClient client = OkHttpInstance.getInstance();         //This is the OkHttp3Client connection.

                Request request = new Request.Builder()
                        .url(params[0].request_Url)
                        .post(params[0].requestParameter)
                        .build();
                Response response = client.newCall(request).execute();
                result= response.body().string();
                response.body().close();
                if (! response.isSuccessful()) throw new IOException("unexpected code "+ response);
            }
            catch (Exception e)
            {
                // TODO: handle exception
                error= true;
                result = e.toString();
                e.printStackTrace();
            }
            return result;
        }
        /*****************************************************/

        @Override
        protected void onPostExecute(String result)
        {
            // TODO Auto-generated method stub

            super.onPostExecute(result);
            if(!error)
            {
                callbacks.onSuccess(result);
            }
            else
            {
                callbacks.onError(result);
            }
        }
    }
    /**************************Callback Interface***************************/

    public interface JsonRequestCallback
    {
        /**
         * Called When Success result of JSON request
         *
         * @param result
         */
        void onSuccess(String result);


        /**
         * Called When Error result of JSON request
         *
         * @param error
         */
        void onError(String error);

    }

    //Singleton Class.
    private static class OkHttpInstance
    {
        private static OkHttpClient okHttpClient = null;
        private OkHttpInstance()
        {
            //to avoid instantiation.
        }

        public static OkHttpClient getInstance()
        {
            if (okHttpClient == null) {
                okHttpClient = new OkHttpClient()
                                .newBuilder()
                                .readTimeout(50, TimeUnit.SECONDS)
                                .writeTimeout(50, TimeUnit.SECONDS)
                                .build();
            }
            return okHttpClient;
        }
    }
    /*****************************************************/



    /* Example

    * RequestBody requestBody = new FormEncodingBuilder()
                        .add("ent_sess_token", sessionTokenHomeFrag)
                        .add("ent_dev_id", deviceId)
                        .add("ent_status", masterStatus)
                        .add("ent_date_time", currentDate)
                        .build();
                Utility.printLog("HomeFrag updateMasterStatus requestBody: " + requestBody);

                OkHttp3Request.doOkHttp3Request(ServiceUrls.UPDATE_MASTER_STATUS, requestBody, new OkHttp3Request.JsonRequestCallback()
                {
                    @Override
                    public void onSuccess(String result)
                    {
                        Utility.printLog("HomeFrag updateMasterStatus onSuccess JSON DATA" + result);
                        updateMasterStatusHandler(result);
                    }

                    @Override
                    public void onError(String error)
                    {
                        if (pDialog != null)
                        {
                            pDialog.dismiss();
                            pDialog = null;
                        }
                        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                        Utility.printLog("HomeFrag updateMasterStatus onError JSON DATA Error" + error);
                    }
                });
    *
    *
    * */
}
