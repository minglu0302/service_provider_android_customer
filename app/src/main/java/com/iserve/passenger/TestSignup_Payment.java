package com.iserve.passenger;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.pojo.GetCard_pojo;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.utility.AppPermissionsRunTime;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;

/**
 * Created by embed on 11/10/16.
 *
 */
public class TestSignup_Payment extends AppCompatActivity implements View.OnClickListener
{
    TextView tvdonepayment,addcard,tvaddhome,toolbarhometxv,tvscancard,poweredby;
    FrameLayout fframelayt;
    TextInputLayout cardlayout,cardnamelayout,expirymonthlayout,expiryeryrlayout,cardcvv;
    EditText etcard,etcardname,etExpiryDate,etcardcvv;
    RelativeLayout rldone,rlscan;
    RelativeLayout stripe_paypal;
    LinearLayout lloutname;
    private String coming_From;
    String result;
    int expiryMonth,expiryYear;
    ProgressBar progressbar;
    SessionManager manager;
    GetCard_pojo response;
    private boolean scanflag = false;
    CreditCard scanResult;
    private int MY_SCAN_REQUEST_CODE = 100;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 127;
    String access_token;
    Typeface regularfont;
    public String PUBLISHABLE_KEY;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        setContentView(R.layout.testsingup_payment);
        Intent intent=getIntent();
        if(intent!=null)
        {
            coming_From=getIntent().getStringExtra("coming_From");
            Utility.printLog("coming_From signup payment :: " + coming_From);
        }
        regularfont = Typeface.createFromAsset(this.getAssets(),Variableconstant.regularfont);
        manager = new SessionManager(this);
        initialize();
    }
    private void initialize()
    {
        PUBLISHABLE_KEY =manager.getStripeKey();
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        stripe_paypal = (RelativeLayout) findViewById(R.id.stripe_paypal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toobar);
        tvdonepayment = (TextView) findViewById(R.id.tvdonepayment);
        tvscancard = (TextView) findViewById(R.id.tvscancard);
        addcard = (TextView) findViewById(R.id.addcard);
        tvaddhome = (TextView) findViewById(R.id.tvaddhome);
        toolbarhometxv = (TextView) findViewById(R.id.toolbarhometxv);
        poweredby = (TextView) findViewById(R.id.poweredby);
        cardlayout = (TextInputLayout) findViewById(R.id.cardlayout);
        cardnamelayout = (TextInputLayout) findViewById(R.id.cardnamelayout);
        //expirymonthlayout = (TextInputLayout) findViewById(R.id.expirymonthlayout);
        //expiryeryrlayout = (TextInputLayout) findViewById(R.id.expiryeryrlayout);
        cardcvv = (TextInputLayout) findViewById(R.id.cardcvv);
        etcard = (EditText) findViewById(R.id.etcard);
        etcardname = (EditText) findViewById(R.id.etcardname);
        etExpiryDate=(EditText) findViewById(R.id.etExpiryDate);
        //etexpirymonth = (EditText) findViewById(R.id.etexpirymonth);
        //etexpiryyr = (EditText) findViewById(R.id.etexpiryyr);
        etcardcvv = (EditText) findViewById(R.id.etcardcvv);
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        rlscan = (RelativeLayout) findViewById(R.id.rlscan);
        rldone = (RelativeLayout) findViewById(R.id.rldone);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        fframelayt.setVisibility(View.VISIBLE);
        if(coming_From.equals("payment_Fragment") || coming_From.equals("LiveBooking")){
            toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
            getSupportActionBar().setTitle("");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            tvaddhome.setVisibility(View.GONE);
            toolbarhometxv.setText(getResources().getString(R.string.addcard));
            toolbarhometxv.setVisibility(View.VISIBLE);
            addcard.setVisibility(View.GONE);
        }
        else
        {
            tvaddhome.setVisibility(View.VISIBLE);
            tvaddhome.setText(getResources().getString(R.string.skip));
            toolbarhometxv.setText(getResources().getString(R.string.addcard));
            toolbarhometxv.setVisibility(View.VISIBLE);
            addcard.setVisibility(View.GONE);
        }
        tvdonepayment.setOnClickListener(this);
        tvaddhome.setOnClickListener(this);
        tvscancard.setOnClickListener(this);
        etExpiryDate.setFocusable(false);
        etExpiryDate.setOnClickListener(this);
        //expirymonthlayout.setOnClickListener(this);

        setfont();
        tvdonepayment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_DONE) {
                    calldonebutton();
                    return true;
                }
                return false;
            }
        });
    }


    private void setfont() {
        etcard.setTypeface(regularfont);
        etcardname.setTypeface(regularfont);
        etExpiryDate.setTypeface(regularfont);
        etcardcvv.setTypeface(regularfont);
        tvscancard.setTypeface(regularfont);
        tvdonepayment.setTypeface(regularfont);
        toolbarhometxv.setTypeface(regularfont);
        poweredby.setTypeface(regularfont);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        finish();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvaddhome:
                Intent intent = new Intent(TestSignup_Payment.this,MenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
            case R.id.tvdonepayment:
                calldonebutton();
                break;
            case R.id.tvscancard:
                if (Build.VERSION.SDK_INT >= 23) {
                    myPermissionConstantsArrayList = new ArrayList<>();
                    myPermissionConstantsArrayList.clear();
                    myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
                    if (AppPermissionsRunTime.checkPermission(TestSignup_Payment.this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE)) {
                        //TODO: if all permissions are already granted
                        onScanPress();
                    }
                } else {
                    onScanPress();
                }
                break;

            case R.id.etExpiryDate:
            case R.id.lloutname:

                final Dialog dialog =new Dialog(TestSignup_Payment.this);
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                View inflate = inflater.inflate(R.layout.date_year_picker,null);
                final NumberPicker month= (NumberPicker) inflate.findViewById(R.id.numberPickerMonth);
                final NumberPicker year= (NumberPicker) inflate.findViewById(R.id.numberPickerYear);
                final TextView tvMonthDialog= (TextView) inflate.findViewById(R.id.dialogMonth);
                final TextView tvYearDialog= (TextView) inflate.findViewById(R.id.dialogYear);
                Button done= (Button) inflate.findViewById(R.id.done);
                month.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        expiryMonth=Integer.parseInt(String.format("%02d",newVal));
                        tvMonthDialog.setText(expiryMonth+"");
                    }
                });
                year.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        expiryYear=newVal%100;
                        tvYearDialog.setText(expiryYear+"");
                    }
                });
                month.setMinValue(01);
                month.setMaxValue(12);
                year.setMaxValue(Calendar.getInstance().get(Calendar.YEAR)+30);
                year.setMinValue(Calendar.getInstance().get(Calendar.YEAR));
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        expiryMonth=month.getValue();
                        expiryYear=year.getValue()%100;
                        etExpiryDate.setText(String.format("%02d",expiryMonth)+"/"+expiryYear);
                        dialog.dismiss();
                    }
                });
                dialog.setContentView(inflate);
                dialog.show();
                break;
        }
    }
    private void onScanPress()
    {
        Intent scanIntent = new Intent(this, CardIOActivity.class);
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: true
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO,true);
        scanIntent.putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON,false);
        scanIntent.putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME,true );
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }
    private void savecardas() {
        Card card = new Card(
                etcard.getText().toString().trim(),
                expiryMonth,
                expiryYear,
                etcardcvv.getText().toString()
        );
        card.validateNumber();
        card.validateCVC();
        boolean validation = card.validateCard();
        if(validation)
        {
            progressbar.setVisibility(View.VISIBLE);
            stripe_paypal.setVisibility(View.GONE);
            tvdonepayment.setClickable(false);
            tvscancard.setClickable(false);
            tvdonepayment.setEnabled(false);
            tvscancard.setEnabled(false);
            rldone.setBackgroundColor(getResources().getColor(R.color.blueloginregister));
            rlscan.setBackgroundColor(getResources().getColor(R.color.blueloginregister));
            tvdonepayment.setTextColor(getResources().getColor(R.color.apptgray));
            tvscancard.setTextColor(getResources().getColor(R.color.apptgray));
            new Stripe(this).createToken(card, PUBLISHABLE_KEY, new TokenCallback() {
                @Override
                public void onError(Exception error) {
                    progressbar.setVisibility(View.GONE);
                    stripe_paypal.setVisibility(View.VISIBLE);
                    tvdonepayment.setClickable(true);
                    tvscancard.setClickable(true);
                    tvscancard.setEnabled(true);
                    tvdonepayment.setEnabled(true);
                    rldone.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
                    rlscan.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
                    tvdonepayment.setTextColor(getResources().getColor(R.color.whitee));
                    tvscancard.setTextColor(getResources().getColor(R.color.whitee));
                }
                @Override
                public void onSuccess(com.stripe.android.model.Token token)
                {
                    Utility.printLog("Token getId "+token.getId());
                    access_token=token.getId();
                    Utility.printLog("Token getCreated"+token.getCreated());
                    Utility.printLog( "Token getLivemode" + token.getLivemode());
                    Utility.printLog("Token getUsed" + token.getUsed());
                    if(Utility.isNetworkAvailable(TestSignup_Payment.this)){
                        callpaymentmethod();
                    }
                    else
                        Utility.ShowAlert(getResources().getString(R.string.network_alert_title), TestSignup_Payment.this);
                }
            });
        }
        else
        {
            Utility.ShowAlert(getResources().getString(R.string.You_did_not_enter_valid_card), TestSignup_Payment.this);
        }
    }
    public String getCardType()
    {
        String str = "";
        if(!etcard.getText().toString().trim().equals("") && etcard.getText().toString().trim().length()>=13) {
            if (etcard.getText().toString().charAt(0) == '4' && (etcard.getText().toString().length() == 13 || etcard.getText().toString().length() == 16)) {
                str = "VISA";
            } else if (etcard.getText().toString().charAt(0) == '5' && (etcard.getText().toString().charAt(1) >= '1' && etcard.getText().toString().charAt(1) <= '5')
                    && etcard.getText().toString().length() == 16) {
                str = "MASTERCARD";
            } else if (etcard.getText().toString().charAt(0) == '3' && ((etcard.getText().toString().charAt(1) == '0') ||
                    (etcard.getText().toString().charAt(1) == '6') || (etcard.getText().toString().charAt(1) == '8'))
                    && (etcard.getText().toString().length() == 14)) {
                str = "DINERS";
            } else if ((etcard.getText().toString().substring(0, 4).equals("3088") || etcard.getText().toString().substring(0, 4).equals("3096") ||
                    etcard.getText().toString().substring(0, 4).equals("3112") || etcard.getText().toString().substring(0, 4).equals("3158") ||
                    etcard.getText().toString().substring(0, 4).equals("3337"))
                    || (Integer.parseInt(etcard.getText().toString().substring(0, 8)) >= 35280000 &&
                    Integer.parseInt(etcard.getText().toString().substring(0, 8)) <= 35899999) &&
                    etcard.getText().toString().length() == 16) {
                str = "JCB";
            } else if (etcard.getText().toString().charAt(0) == '3' && (etcard.getText().toString().charAt(1) == '4' ||
                    etcard.getText().toString().charAt(1) == '7') && etcard.getText().toString().length() == 15) {
                str = "AMEX";
            }
        }
        return str;
    }
    private void callpaymentmethod()
    {
        if(Utility.isNetworkAvailable(this))
            addcardvault();
    }

    private void addcardvault()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ent_sess_token",manager.getSession());
            jsonObject.put("ent_dev_id",manager.getDevice_Id());
            jsonObject.put("ent_token", access_token);
            jsonObject.put("ent_cust_id", manager.getCustomerId());
            jsonObject.put("ent_date_time",Utility.date());
            Log.i("TAG","cardrequest "+jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Utility.doJsonRequest(Variableconstant.ADDCARD, jsonObject, new Utility.JsonRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                Log.i("TAG","cardresponce "+result);
                if(progressbar!=null)
                {
                    progressbar.setVisibility(View.GONE);
                }
                calladdcardresponce(result);
            }
            @Override
            public void onError(String error)
            {
                if(progressbar!=null)
                {
                    progressbar.setVisibility(View.GONE);
                }
                Log.i("TAG","Error "+error);
            }
        });
    }
    private void calladdcardresponce(String result)
    {
        try {
            Gson gson = new Gson();
            response = gson.fromJson(result, GetCard_pojo.class);
            if(response.getErrNum().equals("50") && response.getErrFlag().equals("0"))
            {
                switch (coming_From)
                {
                    case "confirm_no":
                        Intent intent = new Intent(TestSignup_Payment.this, MenuActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    case "payment_Fragment":
                        finish();
                        break;
                    case "LiveBooking":
                        finish();
                        break;
                }
            }
            else
            {
                stripe_paypal.setVisibility(View.VISIBLE);
                tvdonepayment.setClickable(true);
                tvscancard.setClickable(true);
                tvscancard.setEnabled(true);
                tvdonepayment.setEnabled(true);
                rldone.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
                rlscan.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
                tvdonepayment.setTextColor(getResources().getColor(R.color.whitee));
                tvscancard.setTextColor(getResources().getColor(R.color.whitee));
                Toast.makeText(TestSignup_Payment.this, response.getErrMsg(), Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
            progressbar.setVisibility(View.GONE);
            e.printStackTrace();
            Utility.printLog("" + e);
        }
    }
    public void calldonebutton()
    {
        String  str = getCardType();
        if(!str.trim().equals("") || (str.equals("VISA") || str.equals("MASTERCARD")
                || str.equals("DINERS") || str.equals("JCB") || str.equals("AMEX")))
        {
            if(!etExpiryDate.getText().toString().trim().equals(""))
            {

                if(!etcardcvv.getText().toString().trim().equals(""))
                {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(tvdonepayment.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    savecardas();
                }
                else
                {
                    Toast.makeText(this,getResources().getString(R.string.pleaseentercvv),Toast.LENGTH_SHORT).show();
                }

            }
            else
            {
                Toast.makeText(this,getResources().getString(R.string.enterexpirydate),Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(this,getResources().getString(R.string.entervalidcard),Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(scanflag)
        {
            etcard.setText(scanResult.cardNumber);
            String expiryyear = String.valueOf(scanResult.expiryYear);
            String scancardmonth = String.valueOf(scanResult.expiryMonth);
            if(scanResult.expiryMonth<10)
            {
                scancardmonth = "0"+scancardmonth;
                etExpiryDate.setText(scancardmonth+"/"+expiryyear);
            }
            else
            {
                etExpiryDate.setText(scancardmonth+"/"+expiryyear);
            }
            etcardcvv.setText(scanResult.cvv);
            Log.i("TAG","SCANRESUL "+scanResult.cardNumber+" month "+scanResult.expiryMonth+" year "+scanResult.expiryYear);
            etcardname.requestFocus();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT))
        {
            scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
            Log.i("TAg","SCANON "+scanResult.cvv+" expiry "+scanResult.expiryMonth+" "+scanResult.expiryYear);
            scanflag = true;
        }
    }
    /**
     * predefined method to check run time permissions list call back
     * @param permissions: contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        boolean isDenine = false;
        switch (requestCode)
        {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine)
                {
                    Log.i("Permission","Denied ");
                }
                else
                {
                    onScanPress();
                    //TODO: if permissions granted by user, move forward
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

}
