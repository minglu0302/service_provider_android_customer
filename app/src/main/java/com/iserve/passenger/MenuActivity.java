package com.iserve.passenger;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adapter.Drawer_Adapter;
import com.livechatinc.inappchat.ChatWindowActivity;
import com.squareup.picasso.Picasso;
import com.utility.CircleTransform;
import com.utility.Scaler;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;

/**
 * Created by embed on 14/12/15.
 *
 */
public class MenuActivity extends AppCompatActivity
{

    private Toolbar mtoolbar;
    static ImageView profilePicture;
    static TextView drawer_name;
    FrameLayout fframelayt;

    String deviceId;
    String[] values ;
    public Fragment fragment;
    SessionManager manager;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private long backPressed;
    private Resources resources;
    private ActionBarDrawerToggle mDrawerToggle;
    int[] images = {R.drawable.menu_book_iserve_icon, R.drawable.menu_booking_icon,
        R.drawable.menu_payment_icon,R.drawable.menu_my_address_icon, R.drawable.menu_support_icon
        ,R.drawable.livechat,R.drawable.menu_share_icon, R.drawable.menu_about_icon
        };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);
        manager = new SessionManager(this);
        mtoolbar = (Toolbar) findViewById(R.id.myToolBar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);

        fframelayt.setVisibility(View.VISIBLE);
        resources = getResources();
        values = resources.getStringArray(R.array.navdraw);
        deviceId = manager.getDevice_Id();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.navdrawer);
        Drawer_Adapter drawerAdapter = new Drawer_Adapter(this,values,images);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mtoolbar, R.string.open_drawer, R.string.close_drawer) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager)  MenuActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(MenuActivity.this.getCurrentFocus().getWindowToken(), 0);


            }
        };

        mDrawerToggle.syncState();
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if(Variableconstant.comgfrmConfirmscrn)
        {
            displayView(2);
        }
        else
        {
            displayView(1);
        }
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.dwarer_header, null);
        profilePicture = (ImageView) header.findViewById(R.id.drawer_profile_image);
        drawer_name= (TextView)header.findViewById(R.id.drawer_name);
        ProgressBar progressBar = (ProgressBar) header.findViewById(R.id.progressBar_splsh);
        drawer_name.setTypeface(Typeface.createFromAsset(this.getAssets(), Variableconstant.regularfont));
        try
        {
            String   url=manager.getProfilePic().replace(" ", "%20");
            if(!url.equals(""))
            {
                double   size[]= Scaler.getScalingFactor(this);
                double     height = (120)*size[1];
                double    width = (120)*size[0];

                Picasso.with(MenuActivity.this).load(url)
                        .resize((int) width, (int) height)
                        .transform(new CircleTransform())
                        .placeholder(R.drawable.register_profile_default_image)
                        .into(profilePicture);
                progressBar.setVisibility(View.GONE);
            }
            else
            {
                progressBar.setVisibility(View.GONE);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        if(!manager.getCustomerFnme().trim().equals("")&&!manager.getCustomerLnme().trim().equals(""))
        {
            String fname = manager.getCustomerFnme().substring(0, 1).toUpperCase() + manager.getCustomerFnme().substring(1);
            String lname = manager.getCustomerLnme().substring(0, 1).toUpperCase() + manager.getCustomerLnme().substring(1);
            String name = fname+" "+lname;
            drawer_name.setText(name);
        }
        else if(!manager.getCustomerFnme().trim().equals(""))
        {
            String fname = manager.getCustomerFnme().substring(0, 1).toUpperCase() + manager.getCustomerFnme().substring(1);
            drawer_name.setText(fname);
        }
        header.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, profileFragment).commit();
                // update selected item and title, then close the drawer
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });
        mDrawerList.addHeaderView(header);
        //NavigationDrawerAdapter adapter = new NavigationDrawerAdapter(MenuActivity.this, values, images);
        mDrawerList.setAdapter(drawerAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                displayView(position);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    public void displayView(int position) {
        fragment = null;
        switch (position) {
            case 1:
                fragment = new HomeFragment();
                break;
            case 2:
                fragment = new BookingFragment();
                break;
            case 3:
                fragment = new PaymentFragment();
                break;
            case 4:
                fragment = new AddressFragment();
                break;
            case 5:
                fragment = new SupportFragment();
                break;
            case 6:
                Intent intent = new Intent(MenuActivity.this, com.livechatinc.inappchat.ChatWindowActivity.class);
                intent.putExtra(ChatWindowActivity.KEY_GROUP_ID, "your_group_id");
                intent.putExtra(ChatWindowActivity.KEY_LICENCE_NUMBER, "4711811");
                startActivity(intent);
                break;
            case 7:
                fragment = new ShareFragment();
                break;
            case 8:
                fragment = new AboutFragment();
                break;
            default:
                break;
        }
        if(fragment!=null){
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
        }else{
            Utility.printLog("Fragment Value is null");
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public void onBackPressed()
    {
        if(mDrawerLayout.isDrawerOpen(GravityCompat.START))
        {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }else {
            if (backPressed + 2000 > System.currentTimeMillis()) {
                super.onBackPressed();
            } else {
                Toast.makeText(getBaseContext(), resources.getString(R.string.double_press_exit), Toast.LENGTH_SHORT).show();
            }
            backPressed = System.currentTimeMillis();
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
