package com.iserve.passenger;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.SupportAdapter;
import com.google.gson.Gson;
import com.pojo.ChildViewDraw;
import com.pojo.Support_values;
import com.pojo.Validator_Pojo;
import com.utility.OkHttp3Request;
import com.utility.Utility;
import com.utility.Variableconstant;

import java.util.ArrayList;

import okhttp3.FormBody;

/**
 * Created by embed on 14/12/15.
 *
 */
public class SupportFragment extends Fragment implements AdapterView.OnItemClickListener
{
    public ArrayList<Support_values> supportview =new ArrayList<>();
    private SupportAdapter adapter;
    ImageView tolbarhomelogo;
    TextView toolbarhometxv,tveditbarhometxv,youraddress,yourlocation;
    View view;
    ProgressBar progressbar;
    RelativeLayout imageframelayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.supportfragment,container,false);

/*initialize all fields*/
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        progressbar.setVisibility(View.VISIBLE);
        initialize();
        if(Utility.isNetworkAvailable(getActivity()))
        {
            /* calling support service*/
            callSupportService();

        }

        else
        {
            Toast.makeText(getActivity(),getResources().getString(R.string.network_alert_message),Toast.LENGTH_SHORT).show();
        }


        return view;

    }

    private void initialize()
    {

        toolbarhometxv = (TextView) getActivity().findViewById(R.id.toolbarhometxv);
        yourlocation = (TextView) getActivity().findViewById(R.id.yourlocation);
        youraddress = (TextView) getActivity().findViewById(R.id.youraddress);
        toolbarhometxv.setVisibility(View.VISIBLE);
        toolbarhometxv.setText(getResources().getString(R.string.support));
        tolbarhomelogo = (ImageView) getActivity().findViewById(R.id.tolbarhomelogo);
        tolbarhomelogo.setVisibility(View.GONE);
        tveditbarhometxv = (TextView) getActivity().findViewById(R.id.tveditbarhometxv);
        tveditbarhometxv.setVisibility(View.GONE);
        imageframelayout = (RelativeLayout) getActivity().findViewById(R.id.imageframelayout);
        imageframelayout.setVisibility(View.GONE);
        youraddress.setVisibility(View.GONE);
        yourlocation.setVisibility(View.GONE);
        ListView listView = (ListView) view.findViewById(R.id.supportValues_list);
        adapter = new SupportAdapter(getActivity(), supportview);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    /* calling volley request for support add setting the adapter*/
    public void callSupportService()
    {

        FormBody request = new FormBody.Builder()
                .add("ent_lan", Variableconstant.selectedlanid+"")
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.SUPPORT, request, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog(" add card response " + result);
                progressbar.setVisibility(View.GONE);

                try {
                    Gson gson = new Gson();
                    Validator_Pojo support_pojo = gson.fromJson(result, Validator_Pojo.class);
                    if(support_pojo.getErrFlag().equals("0"))
                    {
                        progressbar.setVisibility(View.GONE);
                        supportview.clear();
                        supportview.addAll(support_pojo.getSupport());
                        adapter.notifyDataSetChanged();
                        Utility.printLog("support count"+adapter.getCount());
                        Utility.printLog("support"+support_pojo.getErrMsg());
                        Utility.printLog("support size "+support_pojo.getSupport().size());
                        Utility.printLog("support size list"+supportview.size());
                        Utility.printLog("support Tag"+supportview.get(0).getTag());
                    }
                    else{
                        progressbar.setVisibility(View.GONE);
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

                        // set title
                        alertDialogBuilder.setTitle(getResources().getString(R.string.alert));

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(getResources().getString(R.string.nointernet))
                                .setCancelable(false)

                                .setNegativeButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //closing the application
                                        getActivity().finish();
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        alertDialog.show();
                    }

                }
                catch (Exception e) {
                    progressbar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Utility.printLog("" + e);
                }



            }

            @Override
            public void onError(String error) {

                progressbar.setVisibility(View.GONE);
            }
        });


    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ArrayList<ChildViewDraw> list;
        list=supportview.get(position).getChilds();

        if(supportview.get(position).getChilds().size()>0)
        {
            Intent intent = new Intent(getActivity(), ChildSupport.class);
            intent.putExtra("Title", supportview.get(position).getTag());
            intent.putExtra("list", list);
            startActivity(intent);
        }
        else if(!supportview.get(position).getLink().equals(""))
        {
            Intent intent = new Intent(getActivity(), Web_ViewActivity.class);
            intent.putExtra("Link", supportview.get(position).getLink());

            intent.putExtra("Title", supportview.get(position).getTag());

            startActivity(intent);
        }
    }
}
