package com.iserve.passenger;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.pojo.GetCard_pojo;
import com.pojo.card_info_pojo;
import com.utility.Alerts;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by embed on 9/12/15.
 *
 */
public class ChangeCardActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemClickListener{


    private ListView card_list;
    private CustomListViewAdapter adapter;
    List<card_info_pojo> rowItems;
    private RelativeLayout add_cc_bt;
    SessionManager manager;
    ProgressDialog pDialog;
    Resources resources;
    GetCard_pojo response;
    JSONObject jsonObject;
    FrameLayout fframelayt;
    TextView actionbartext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_fragment);
        initialize();
    }

    private void initialize() {
        jsonObject = new JSONObject();
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toobar);
        toolbar.setVisibility(View.VISIBLE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        fframelayt.setVisibility(View.VISIBLE);

        actionbartext = (TextView) findViewById(R.id.toolbarhometxv);
        actionbartext.setText(getResources().getString(R.string.card));
        actionbartext.setVisibility(View.VISIBLE);
        card_list = (ListView)findViewById(R.id.cards_list_view);
        LayoutInflater footerinflater = (LayoutInflater)ChangeCardActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View fotter_view = footerinflater.inflate(R.layout.add_card_fotter, null,false);
        card_list.addFooterView(fotter_view);
        add_cc_bt = (RelativeLayout) fotter_view.findViewById(R.id.add_card_rel_fotter);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.Loading));
        pDialog.setCancelable(false);
        resources = ChangeCardActivity.this.getResources();
        manager=new SessionManager(ChangeCardActivity.this);
        rowItems = new ArrayList<>();
        adapter = new CustomListViewAdapter(ChangeCardActivity.this, R.layout.card_list_row, rowItems);
        card_list.setAdapter(adapter);
        add_cc_bt.setOnClickListener(this);
        card_list.setOnItemClickListener(this);

    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_card_rel_fotter) {
            Intent intent = new Intent(ChangeCardActivity.this, TestSignup_Payment.class);
            intent.putExtra("coming_From", "LiveBooking");
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(Utility.isNetworkAvailable(ChangeCardActivity.this)){

                adapter = new CustomListViewAdapter(this, R.layout.card_list_row, rowItems);
                card_list.setAdapter(adapter);
                add_cc_bt.setOnClickListener(this);
                card_list.setOnItemClickListener(this);
                cllGetAllCards();
        }
        else{
            Toast.makeText(ChangeCardActivity.this,getResources().getString(R.string.nointernet),Toast.LENGTH_SHORT).show();
        Alerts alerts=new Alerts(this);
        alerts.showNetworkAlert(this);
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        card_info_pojo row_details =(card_info_pojo)card_list.getItemAtPosition(position);
        Intent returnIntent = new Intent();
        returnIntent.putExtra("cardNo", row_details.getCard_numb());
        returnIntent.putExtra("cardID", row_details.getCard_id());
        returnIntent.putExtra("cardImage", row_details.getCard_image());
        ChangeCardActivity.this.overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
        ChangeCardActivity.this.setResult(RESULT_OK, returnIntent);
        ChangeCardActivity.this.finish();

    }

    public void cllGetAllCards() {
        Utility.printLog(" in side get card service ");
        pDialog.show();
        rowItems.clear();

        try {
            jsonObject.put("ent_sess_token", manager.getSession());
            jsonObject.put("ent_dev_id",manager.getDevice_Id());
            jsonObject.put("ent_cust_id", manager.getCustomerId());
            jsonObject.put("ent_date_time", Utility.dateintwtfour());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Utility.doJsonRequest(Variableconstant.GETCARD, jsonObject, new Utility.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("The Response get card " + result);
                callGetCardServiceResponse(result);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(ChangeCardActivity.this, resources.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                if (pDialog != null) {
                    pDialog.cancel();
                    pDialog.dismiss();
                }
            }
        });

    }
    public void callGetCardServiceResponse(String Response) {
        try {
            Gson gson = new Gson();
            response = gson.fromJson(Response, GetCard_pojo.class);
            if (response.getErrNum().equals("52") && response.getErrFlag().equals("0")) {
                rowItems.clear();
                for (int i = 0; i < response.getCards().size(); i++) {
                    Bitmap bitmap = Utility.setCreditCardLogo(response.getCards().get(i).getType(),this);
                    card_info_pojo item = new card_info_pojo(bitmap, response.getCards().get(i).getLast4(), response.getCards().get(i).getExp_month(),
                            response.getCards().get(i).getExp_year(), response.getCards().get(i).getId());//id
                    rowItems.add(item);
                }
                adapter = new CustomListViewAdapter(ChangeCardActivity.this,
                        R.layout.card_list_row, rowItems);
                card_list.setAdapter(adapter);
            } else {
                Toast.makeText(ChangeCardActivity.this, response.getErrMsg(), Toast.LENGTH_LONG).show();
            }
            if (pDialog != null) {
                pDialog.cancel();
                pDialog.dismiss();
            }
        }
        catch (Exception e) {
            if (pDialog != null) {
                pDialog.cancel();
                pDialog.dismiss();
            }
            e.printStackTrace();
            Utility.printLog("" + e);
        }
    }
   /* private Bitmap setCreditCardLogo(String cardMethod)
    {
        Bitmap anImage;
        Drawable myDrawable;
        switch (cardMethod)
        {
            case "Visa":
                myDrawable = getResources().getDrawable(R.drawable.visa);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            case "MasterCard":
                myDrawable = getResources().getDrawable(R.drawable.master_card);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            case "American Express":
                myDrawable = getResources().getDrawable(R.drawable.amex);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            case "Discover":
                myDrawable = getResources().getDrawable(R.drawable.amex_back);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            default:
                myDrawable = getResources().getDrawable(R.drawable.cc_back);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
        }
        return anImage;
    }*/
    private class CustomListViewAdapter extends ArrayAdapter<card_info_pojo> {
        Context context;
        CustomListViewAdapter(Context context, int resourceId, List<card_info_pojo> items) {
            super(context, resourceId, items);
            this.context = context;
        }
        private class ViewHolder {
            ImageView card_image;
            TextView card_numb;
            RelativeLayout change_card_relative;
        }
        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {

            ViewHolder holder;
            final card_info_pojo rowItem = getItem(position);
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {

                convertView = mInflater.inflate(R.layout.card_list_row, null);
                holder = new ViewHolder();
                holder.card_numb = (TextView) convertView.findViewById(R.id.card_numb_row_change);
                holder.card_image = (ImageView) convertView.findViewById(R.id.card_img_row_change);
                holder.change_card_relative = (RelativeLayout) convertView.findViewById(R.id.change_card_relative);
                convertView.setTag(holder);
            } else
                holder = (ViewHolder) convertView.getTag();
            holder.card_image.setImageBitmap(rowItem.getCard_image());
            holder.card_numb.setText(rowItem.getCard_numb());
            return convertView;
        }
    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
    }
}
