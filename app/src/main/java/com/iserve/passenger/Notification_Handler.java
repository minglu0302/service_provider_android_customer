package com.iserve.passenger;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.utility.NotificationUtils;
import com.utility.SessionManager;
import com.utility.Utility;

/**
 * Created by embed on 22/8/16.
 *
 */
public class Notification_Handler extends AppCompatActivity
{
    String message,bid,action,latlong,dialogtitle;
    String spltlatlonf[];
    String TAG = "Notification_Handler";
    Intent intent;
    ProgressDialog pDialog;
    SessionManager manager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         manager = new SessionManager(this);
         pDialog = new ProgressDialog(this);
        if(getIntent().getExtras()!=null)
        {
            message = getIntent().getStringExtra("message");
            action = getIntent().getStringExtra("statcode");
            if(action.equals("23"))
            {
                bid = "";
                dialogtitle = "Customer ID: "+manager.getCustomerId();
            }
            else {
                bid = getIntent().getStringExtra("bid");
                dialogtitle = "Booking ID: "+bid;
            }
            if(action.equals("5"))
            {
                latlong = getIntent().getStringExtra("prolat");
                spltlatlonf = latlong.split(",");
            }
            if(action.equals("7") ||action.equals("10"))
            {
                NotificationUtils.clearNotifications(Notification_Handler.this);
            }
            Log.i(TAG,"Handlermess "+message+" bid "+bid+" action "+action);

        }


        if(action.equals("23"))
        {
            new AlertDialog.Builder(this)
                    .setTitle(dialogtitle)
                    .setMessage(""+message)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which)
                        {

                            Utility.setMAnagerWithBID(Notification_Handler.this,manager);
                            dialog.dismiss();
                        }
                    })

                    .setIcon(R.drawable.ic_launcher)
                    .show();
        }
        else
        {
         new AlertDialog.Builder(this)
                .setTitle(dialogtitle)
                .setMessage(""+message)
                 .setCancelable(false)
                 .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {

                        if(action.equals("10"))
                        {
                         //   intent = new Intent(Notification_Handler.this,MenuActivity.class);
                            Utility.deleteEventFromCalender(bid,manager,Notification_Handler.this);
                            finish();
                            dialog.dismiss();
                        }
                        else
                        {
                            intent = new Intent(Notification_Handler.this,LiveBookingStatus.class);
                            intent.putExtra("bid", bid);
                            intent.putExtra("statcode", action);
                            if(action.equals("5"))
                            {
                                intent.putExtra("apptlat", spltlatlonf[0]);
                                intent.putExtra("apptlng",spltlatlonf[1]);
                            }
                            intent.putExtra("status", message);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                            dialog.dismiss();
                        }

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        finish();
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_launcher)
            .show();
        }

    }

}
