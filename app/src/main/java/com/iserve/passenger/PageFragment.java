package com.iserve.passenger;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.adapter.TestMasterAdpter;
import com.google.gson.Gson;
import com.pojo.PubNubMas;
import com.pojo.PubNub_pojo;
import com.utility.SessionManager;
import com.utility.Variableconstant;
import org.json.JSONObject;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by embed on 15/9/16.
 * class PageFragment this is for dynamic viewpager
 */
public class PageFragment extends Fragment
{
    public static final String PUM_MESG = "pubmessage";
    public static final String TYP_ID = "typeid";
    public static final String FTYP = "fType";
    public static final String IGROUP = "isgroup";
    ListView masterlistv;
    TextView tvnooneavailbale;
    String TAG = "PageFragment";
    TestMasterAdpter tstmasAdpter;
    String typeId;
    String fType;
    String isgroup;
    public Pager_data_updater pager_data_updater;
    private boolean isView_created;
    ArrayList<PubNubMas> pubNubMases;
    SessionManager manager;
    PubNub_pojo nubPojo;
    Gson gson;
    private String previous_data="";
    private Activity activity;

    public PageFragment()
    {
        intialize_Listere();
    }
    public static PageFragment newInstance(ArrayList<PubNubMas>pubNubMases,String typeid,String fType,String isgroup)//,String pricepermint
    {
        Bundle args = new Bundle();
        args.putSerializable(PUM_MESG,pubNubMases);
        args.putString(TYP_ID,typeid);
        args.putString(FTYP,fType);
        args.putString(IGROUP,isgroup);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle bundle=getArguments();
        pubNubMases =(ArrayList<PubNubMas>)bundle.getSerializable(PUM_MESG);
        typeId = bundle.getString(TYP_ID);
        fType = bundle.getString(FTYP);
        isgroup = bundle.getString(IGROUP);

        gson = new Gson();
        activity=getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        manager = new SessionManager(getContext());
        tvnooneavailbale = (TextView) view.findViewById(R.id.tvnooneavailbale);
        masterlistv = (ListView) view.findViewById(R.id.masterlistv);
        Typeface italictext= Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Italic_0.ttf");
        tvnooneavailbale.setTypeface(italictext);
        tstmasAdpter = new TestMasterAdpter(getActivity(),pubNubMases);
        masterlistv.setAdapter(tstmasAdpter);
        if(pubNubMases.size()>0)
        {

            tstmasAdpter.notifyDataSetChanged();
            tvnooneavailbale.setVisibility(View.GONE);
        }
        else
        {
            tvnooneavailbale.setVisibility(View.VISIBLE);
        }

        masterlistv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), MasterDetails.class);
                manager.setServiceProviderNAme(fType);
                NumberFormat nf_out = NumberFormat.getNumberInstance(Locale.US);
                nf_out.setMaximumFractionDigits(2);
                nf_out.setGroupingUsed(false);
                intent.putExtra("DISTANCE",pubNubMases.get(position).getD());
                intent.putExtra("PID",pubNubMases.get(position).getPid());
                intent.putExtra("AMT",pubNubMases.get(position).getAmt());
                Variableconstant.now_servicegroup = isgroup;
                intent.putExtra("SELECTED_ID",typeId);
                startActivity(intent);
            }
        });

        isView_created=true;
        return view;
    }

    public Pager_data_updater getListener()
    {
        return pager_data_updater;
    }


    private void intialize_Listere()
    {

        pubNubMases= new ArrayList<>();

        pager_data_updater=new Pager_data_updater()
        {
            @Override
            public void onUpdated(JSONObject jsonObject)
            {


                if(!previous_data.equalsIgnoreCase(jsonObject.toString().trim()))
                {
                    pubNubMases.clear();
                    if(isView_created)
                    {
                        previous_data=jsonObject.toString().trim();
                    }else
                    {
                        return;
                    }
                    if(!Variableconstant.comingfromlaterbooking)
                    {
                        manager.setSocketRes(jsonObject.toString());
                    }
                    nubPojo = gson.fromJson(manager.getPUNUB_RES(),PubNub_pojo.class);


                    for(int i=0;i<nubPojo.getMsg().getMasArr().size();i++)
                    {
                        if(typeId.equals(nubPojo.getMsg().getMasArr().get(i).getTid()))
                        pubNubMases.addAll(nubPojo.getMsg().getMasArr().get(i).getMas());
                    }
                    if(pubNubMases.size()>0)
                    {

                        activity.runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {

                                    tstmasAdpter.notifyDataSetChanged();
                            }
                        });
                        tvnooneavailbale.setVisibility(View.GONE);
                    }
                    else
                    {
                        tvnooneavailbale.setVisibility(View.VISIBLE);
                    }

                    Log.i("LISTERNER","cG=HANGED");
                }else
                {
                    Log.i("LISTERNER"," nOTCG=HANGED");
                }
            }
            @Override
            public void onError()
            {
                Toast.makeText(getActivity().getApplicationContext(),""+typeId,Toast.LENGTH_SHORT).show();
            }
        };
    }

}
