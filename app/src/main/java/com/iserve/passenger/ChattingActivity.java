package com.iserve.passenger;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.adapter.ChattingAdaptr;
import com.google.gson.Gson;
import com.pojo.ChatingSocketResponce;
import com.pojo.TotalMessage;
import com.squareup.picasso.Picasso;
import com.utility.CircleTransform;
import com.utility.MyNetworkChangeListner;
import com.utility.NetworkChangeReceiver;
import com.utility.OkHttp3Request;
import com.utility.Scaler;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import okhttp3.FormBody;

/**
 * <h>ChattingActivity</h>
 * Created by embed on 26/12/16.
 *
 */
public class ChattingActivity extends AppCompatActivity
{
    String TAG = "ChattingActivity";
    String bid,proimg,dt,payload,msgid,usertype,deliver,msgtype,providername;
    Toolbar toolbar;
    ImageView profileimg,backpress;
    TextView tvtitletowhom,chatproname;
    RecyclerView rcvChatMsg;
    LinearLayoutManager mLayoutManager;
    EditText etMsg;
    ImageView btnSend;
    ChattingAdaptr cAdapter;
    JSONObject jsonobj;
    SessionManager manager;
    Gson gson;
    ChatingSocketResponce chatingSocketResponce ;
    ArrayList<TotalMessage>totalmsg ;
    double size[];
    Typeface regulrfont;
    ProgressDialog progressBar;
    private boolean isOpenChatting = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
        setContentView(R.layout.chattingactivity);
        manager = new SessionManager(this);
        emitHeartbeat("1");
        isOpenChatting = true;
        mStartSocketListner();
        regulrfont = Typeface.createFromAsset(getAssets(),Variableconstant.regularfont);
        if(getIntent().getExtras()!=null)
        {
            bid = getIntent().getStringExtra("bid");
            proimg = getIntent().getStringExtra("proimg");
            providername = getIntent().getStringExtra("providername");
        }
        initialize();
    }
    private void initialize()
    {
        progressBar = new ProgressDialog(this);
        progressBar.setMessage(getResources().getString(R.string.wait));
        progressBar.setCancelable(false);
        NetworkChangeReceiver  networkChangeReceiver=new NetworkChangeReceiver();
        networkChangeReceiver.setMyNetworkChangeListner(new MyNetworkChangeListner() {
            @Override
            public void onNetworkStateChanges(boolean nwStatus) {
                Utility.printLog("Network Status MainActvty "+nwStatus);
                if(nwStatus)
                {
                    if(isOpenChatting)
                    {
                        emitHeartbeat("1");
                        getallMessage();
                    }
                }
            }
        });
        size = Scaler.getScalingFactor(this);
        totalmsg = new ArrayList<>();
        jsonobj = new JSONObject();
        gson = new Gson();
        double width = size[0] * 50;
        double height = size[1] * 50;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        profileimg = (ImageView) findViewById(R.id.profileimg);
        backpress = (ImageView) findViewById(R.id.backpress);
        tvtitletowhom = (TextView) findViewById(R.id.tvtitletowhom);
        chatproname = (TextView) findViewById(R.id.chatproname);
        etMsg = (EditText) findViewById(R.id.etMsg);
        btnSend = (ImageView) findViewById(R.id.btnSend);
        rcvChatMsg = (RecyclerView) findViewById(R.id.rcvChatMsg);
        mLayoutManager = new LinearLayoutManager(this);
        cAdapter = new ChattingAdaptr(this,totalmsg);
        rcvChatMsg.setLayoutManager(mLayoutManager);
        rcvChatMsg.setItemAnimator(new DefaultItemAnimator());
        rcvChatMsg.setAdapter(cAdapter);
        backpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        String bidtxt = getResources().getString(R.string.bookid)+" "+bid;
        tvtitletowhom.setTypeface(regulrfont);
        tvtitletowhom.setText(bidtxt);
        chatproname.setText(providername);
        chatproname.setTypeface(regulrfont);
        if(!proimg.equals("aa_default_profile_pic.gif") && !proimg.equals(""))
        {
            Picasso.with(this).load(proimg)
                    .centerCrop()
                    .transform(new CircleTransform())
                    .resize((int)width,(int)height)
                    .into(profileimg);
        }
        getallMessage();
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                msgtype = "0";
                payload = etMsg.getText().toString().trim();

                if(etMsg.hasFocus())
                {
                    scrollToBottom();
                }
                if(!etMsg.getText().toString().trim().equals(""))
                {
                    sendmessage(payload,msgtype);
                    etMsg.setText("");
                }
            }
        });
    }
    /**
     * <h2>sendmessage</h2>
     * <p>
     *     sending request to channel a "Message" of socket
     * @param messg message to be send
     * @param msgtype message type, i.e what type of message is it. example text message type ,image message type, video message type
     *                for txt msgtype = 0, for video 2, for image 1;
     * </p>
     */
    private void sendmessage(String messg, String msgtype)
    {
        try {
            long msgid = System.currentTimeMillis();
            jsonobj.put("msgtype",msgtype);
            jsonobj.put("payload",messg);
            jsonobj.put("bid",bid);
            jsonobj.put("msgid", msgid);
            jsonobj.put("usertype","2");
            jsonobj.put("currdt",Utility.date());
            ApplicationController.getInstance().emit("Message", jsonobj);
            TotalMessage totalsend = new TotalMessage();
            totalsend.setDt(getLocalTime(Utility.getLocalTimeByUtcDateTime(Utility.date())));
            totalsend.setMsgid(""+msgid);
            totalsend.setMsgtype(msgtype);
            totalsend.setPayload(messg);
            totalsend.setUsertype("2");
            totalsend.setStatus("1");
            totalmsg.add(totalsend);
            cAdapter.notifyDataSetChanged();
            scrollToBottom();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    /*scrolling to the bottom of the recyclerview*/
    private void scrollToBottom() {
        rcvChatMsg.scrollToPosition(cAdapter.getItemCount() - 1);
    }

    /*calling service to get all message*/
    private void getallMessage()
    {
        progressBar.show();
        FormBody request = new FormBody.Builder()
                .add("ent_bid",bid)
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_user_type","2")
                .build();
        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "getallMessages", request, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                progressBar.dismiss();

                Log.d(TAG,"ALLMS "+result);

                chatingSocketResponce = gson.fromJson(result,ChatingSocketResponce.class);
                if(chatingSocketResponce.getErrFlag().equals("0") && chatingSocketResponce.getErrNum().equals("21"))
                {
                    for(int i = 0;i<chatingSocketResponce.getMessages().size();i++)
                    {
                        chatingSocketResponce.getMessages().get(i)
                                .setDt(getLocalTime(Utility.getLocalTimeByUtcDateTime(chatingSocketResponce.getMessages().get(i).getDt())));
                    }
                    totalmsg.addAll(chatingSocketResponce.getMessages());
                    cAdapter.notifyDataSetChanged();
                    scrollToBottom();
                }
            }
            @Override
            public void onError(String error)
            {
                progressBar.dismiss();
            }
        });
    }
    /**
     * <h2>mStartSocketListner</h2>
     * socket "Message" channel listner
     * @see ApplicationController
     */
    private void mStartSocketListner()
    {
        ApplicationController.initInstance(new SockEmitter(ChattingActivity.this) {
            @Override
            public void execute(String socket_event, JSONObject jsonObject) throws JSONException {
                if(socket_event.equals("Message"))
                {
                    try {
                        if(jsonObject.getString("err").equals("0"))
                        {
                            deliver = jsonObject.getString("deliver");
                            dt = Utility.date();
                            usertype = "2";
                            msgid = jsonObject.getString("msgid");
                            final String err = jsonObject.getString("err");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    notfyAdptrOnScktRespnc(dt,msgid,msgtype,payload,usertype,deliver,err);
                                }
                            });
                        }
                        if(jsonObject.getString("err").equals("2"))
                        {
                            deliver = "";
                            dt =  jsonObject.getString("dt");
                            msgtype = jsonObject.getString("msgtype");
                            payload = jsonObject.getString("payload");
                            usertype = jsonObject.getString("usertype");
                            msgid = jsonObject.getString("msgid");
                            final String err = jsonObject.getString("err");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    notfyAdptrOnScktRespnc(dt,msgid,msgtype,payload,usertype,deliver,err);
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    /**
     * <h2>notfyAdptrOnScktRespnc</h2>
     * @param dt date time
     * @param msgid message id
     * @param msgtype message type
     * @param payload message
     * @param usertype usertype 1 or 2
     * @param deliver message delivered or not
     * @param eror checking the error for delivery and receiving
     */
    private void notfyAdptrOnScktRespnc(String dt, String msgid, String msgtype, String payload, String usertype, String deliver, String eror)
    {
        if(eror.equals("0"))
        {
            for(int i=0;i<totalmsg.size();i++)
            {
                if(msgid.equals(totalmsg.get(i).getMsgid()))
                {
                    totalmsg.get(i).setStatus(deliver);
                    cAdapter.notifyDataSetChanged();
                    break;
                }
            }
        }
        else if(eror.equals("2"))
        {
            TotalMessage totalsend = new TotalMessage();
            totalsend.setDt(getLocalTime(Utility.getLocalTimeByUtcDateTime(dt)));
            totalsend.setMsgid(msgid);
            totalsend.setMsgtype(msgtype);
            totalsend.setPayload(payload);
            totalsend.setUsertype(usertype);
            totalsend.setStatus(deliver);
            totalmsg.add(totalsend);
            cAdapter.notifyDataSetChanged();
            scrollToBottom();
        }
    }
    /**
     * <h2>emitHeartbeat</h2>
     * <p>
     *     sending heart beat means socket is connected or not to the server
     * @param status status "0" means offline(Not connected)
     *               status "1" means Online(connected)
     * </p>
     */
    public void emitHeartbeat(String status)
    {
        JSONObject obj = new JSONObject();
        try
        {
            obj.put("status",status);
            obj.put("cid", manager.getCustomerId());

        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        ApplicationController.getInstance().emit("CustomerStatus", obj);
    }
    /**
     * <h2>getLocalTime</h2>
     * changing the GMT time to a local time
     * @param localTimeByUtcDateTime this is GMT Time
     * @return  local time
     */
    private String getLocalTime(String localTimeByUtcDateTime) {
        String fulltime="";
        String splttimdate[] =localTimeByUtcDateTime.split(" ");
        DateFormat f1 = new SimpleDateFormat("HH:mm:ss",Locale.getDefault());
        try {
            Date d = f1.parse(splttimdate[1]);
            DateFormat f2 = new SimpleDateFormat("h:mm aa", Locale.getDefault());
            fulltime  =  f2.format(d).toLowerCase();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return fulltime;
    }
    @Override
    protected void onPause() {
        super.onPause();
        Variableconstant.forChatting = false;
        isOpenChatting = false;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.side_slide_in, R.anim.stay_still);
        finish();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onResume() {
        super.onResume();
        isOpenChatting = true;
        emitHeartbeat("1");
        Variableconstant.forChatting = true;
    }
}
