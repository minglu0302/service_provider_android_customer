package com.iserve.passenger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.utility.Variableconstant;

public class Terms_Conditions extends AppCompatActivity implements View.OnClickListener {

    FrameLayout fframelayt;
    TextView actionbartext;


    /**
     * <p>Setting content view</p>
     * @param savedInstanceState views
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.termsconditions);

        initializeVariables();
    }

    /**
     * <p>here we are initialize all view element</p>
     */
    private void initializeVariables()
    {
        RelativeLayout terms_conditions_Rl = (RelativeLayout) findViewById(R.id.terms_conditions_Rl);
        RelativeLayout privacy_policy_Rl = (RelativeLayout) findViewById(R.id.privacy_policy_Rl);



        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        fframelayt.setVisibility(View.VISIBLE);
        actionbartext = (TextView) findViewById(R.id.toolbarhometxv);
        actionbartext.setVisibility(View.VISIBLE);
        actionbartext.setText(getResources().getString(R.string.termsandcondition));

        privacy_policy_Rl.setOnClickListener(this);
        terms_conditions_Rl.setOnClickListener(this);
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    /**
     * <p>override the onclick method of OnClickListener</p>
     *  @see View.OnClickListener
     * @param v clicked item view
     */
    @Override
    public void onClick(View v) {

        if(v.getId()== R.id.terms_conditions_Rl)
        {
            Intent intent=new Intent(this,Web_ViewActivity.class);
            intent.putExtra("Link", Variableconstant.TERMS_LINK);
            intent.putExtra("Title", getResources().getString(R.string.terms_and_conditions));
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }

        if(v.getId()== R.id.privacy_policy_Rl)
        {
            Intent intent=new Intent(this,Web_ViewActivity.class);
            intent.putExtra("Link", Variableconstant.PRIVECY_LINK);
            intent.putExtra("Title", getResources().getString(R.string.privacy_policy));
            startActivity(intent);
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }


    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
    }

}
