package com.iserve.passenger;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.countrypic.Country;
import com.countrypic.CountryPicker;
import com.countrypic.CountryPickerListener;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.pojo.Validator_Pojo;
import com.squareup.picasso.Picasso;
import com.utility.Alerts;
import com.utility.AppPermissionsRunTime;
import com.utility.CircleTransform;
import com.utility.OkHttp3Request;
import com.utility.Scaler;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Validator;
import com.utility.Variableconstant;
import org.json.JSONObject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import eu.janmuller.android.simplecropimage.CropImage;
import okhttp3.FormBody;

import static android.os.Build.VERSION_CODES.N;

/**
 * @author 3Embed
 * Created by embed on 3/12/15.
 * <h>Register_Page</h>
 * <p>
 *     this is a AppCompatActivity which implements OnclickListner and OnFocusChangeListner
 *     this class is used for the user to register in to the app
 * </p>
 */
public class Register_Page extends AppCompatActivity implements View.OnClickListener,View.OnFocusChangeListener
{
    EditText register_firstname, register_lastname, register_email, register_mobile, register_password, register_referal;
    RelativeLayout register_facebbok_rrlout, terms_conditionrrlauoyt;
    FrameLayout fframelayt;
    ImageView register_profile_imv;
    CheckBox register_check_selectr;
    ProgressBar progressBar;
    TextView actionbartext, country_code, buttonregister, register_facebbok_button, register_term_condition;
    TextInputLayout firstnamelayout, lastnamelayout, emaillayout, mobile_numberlayout, passwordlayout, refrallayout;
    CoordinatorLayout coordinatorLayout;
    Validator validator;
    Alerts alerts;
    ProgressDialog pDialog;
    SessionManager manager;
    /*for camera */
    private static final int CAMERA_PIC = 1, GALLERY_PIC = 2, CROP_IMAGE = 3;
    private String state;
    private String takenNewImage;
    private File profilePicsDir;
    public static File newFile;
    public static String uplodedImage;
    private Uri newProfileImageUri;
    private boolean isPPSelected = false;
    public boolean isProfilePicSelected = false;
    UploadAmazonS3 upload;
    boolean emailflag = false, phoneflag = false;
    private boolean facebookclickedflag = false;
    public boolean fbClicked = false;
    JSONObject jsonObj;
    Gson gson;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 121;
    private final int REQUEST_CODE_SMS_PERMISSION = 141;
    private ImageView mCountryFlagImageView;
    private CountryPicker mCountryPicker;
    CallbackManager callbackManager;
    String Fb_emailId, Fb_firstName, fb_lastName, fb_birthDay, fb_gender, fb_pic;
    String fbid = "";
    Typeface litefont, regularfont;
    private CharSequence[] options;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.register_layout);
        register_profile_imv = findViewById(R.id.iv_reg_pp);

        jsonObj = new JSONObject();
        gson = new Gson();
        takenNewImage = "iServe"+System.currentTimeMillis() + ".png";
        state = Environment.getExternalStorageState();
        upload = UploadAmazonS3.getInstance(Register_Page.this, Variableconstant.Amazoncognitoid);
        FacebookSdk.sdkInitialize(Register_Page.this);
        callbackManager = CallbackManager.Factory.create();
        initialize();
        setListener();
    }
    /*
    * initializing the Ui Component of the Register Page
    * */
    private void initialize() {
        manager = new SessionManager(this);
        coordinatorLayout = findViewById(R.id
                .coordinatorLayout);
        litefont = Typeface.createFromAsset(this.getAssets(), Variableconstant.lightfont);
        regularfont = Typeface.createFromAsset(this.getAssets(), Variableconstant.regularfont);
        register_password = findViewById(R.id.et_reg_paswrd);
        if (register_password != null) {
            register_password.setOnFocusChangeListener(this);
        }
        register_mobile = findViewById(R.id.et_reg_mobno);
        if (register_mobile != null) {
            register_mobile.setOnFocusChangeListener(this);
        }
        register_email = findViewById(R.id.et_reg_eml);
        if (register_email != null) {
            register_email.setOnFocusChangeListener(this);
        }
        register_lastname = findViewById(R.id.et_reg_lnm);
        register_referal = findViewById(R.id.et_reg_refrl);
        register_firstname = findViewById(R.id.et_reg_fnm);
        register_firstname.requestFocus();
        if (register_firstname != null) {
            register_firstname.setOnFocusChangeListener(this);
        }
        mCountryFlagImageView = findViewById(R.id.selected_country_flag_image_view);
        mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.selectcountry));
        buttonregister = findViewById(R.id.tv_reg_nxt);
        register_facebbok_button = findViewById(R.id.register_facebbok_button);
        register_term_condition = findViewById(R.id.register_term_condition);
        register_facebbok_rrlout = findViewById(R.id.register_facebbok_rrlout);
        terms_conditionrrlauoyt = findViewById(R.id.terms_conditionrrlauoyt);
        register_check_selectr = findViewById(R.id.chckbx_reg_acptTrmCndtn);
        firstnamelayout = findViewById(R.id.til_reg_fnm);
        lastnamelayout = findViewById(R.id.til_reg_lnm);
        refrallayout = findViewById(R.id.til_reg_refrl);
        emaillayout = findViewById(R.id.til_reg_email);
        mobile_numberlayout = findViewById(R.id.til_reg_MobNo);
        passwordlayout = findViewById(R.id.til_reg_paswrd);
        fframelayt = findViewById(R.id.fframelayt);
        Toolbar toolbar = findViewById(R.id.app_toobar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
            getSupportActionBar().setTitle("");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        fframelayt.setVisibility(View.VISIBLE);
        progressBar = findViewById(R.id.progressBar_splsh);

        actionbartext = findViewById(R.id.toolbarhometxv);

        actionbartext.setText(getResources().getString(R.string.registeractionbar));
        actionbartext.setVisibility(View.VISIBLE);

        firstnamelayout.setHintAnimationEnabled(true);
        firstnamelayout.setHint(getResources().getString(R.string.firstname));
        firstnamelayout.setHintTextAppearance(R.style.QText);


        lastnamelayout.setHintAnimationEnabled(true);
        lastnamelayout.setHint(getResources().getString(R.string.lastname));
        lastnamelayout.setHintTextAppearance(R.style.QText);
        emaillayout.setHintAnimationEnabled(true);
        emaillayout.setHint(getResources().getString(R.string.email));
        emaillayout.setHintTextAppearance(R.style.QText);
        mobile_numberlayout.setHintAnimationEnabled(true);
        mobile_numberlayout.setHint(getResources().getString(R.string.mobile_number));
        mobile_numberlayout.setHintTextAppearance(R.style.QText);
        passwordlayout.setHintAnimationEnabled(true);
        passwordlayout.setHint(getResources().getString(R.string.password));
        passwordlayout.setHintTextAppearance(R.style.QText);
        refrallayout.setHintAnimationEnabled(true);
        refrallayout.setHint(getResources().getString(R.string.referalcode));
        refrallayout.setHintTextAppearance(R.style.QText);
        country_code = findViewById(R.id.country_code);
        validator = new Validator();
        alerts = new Alerts(this);
        register_profile_imv.setOnClickListener(this);
        terms_conditionrrlauoyt.setOnClickListener(this);
        register_facebbok_rrlout.setOnClickListener(this);
        country_code.setOnClickListener(this);
        register_check_selectr.setOnClickListener(this);
        buttonregister.setOnClickListener(this);
        register_facebbok_button.setOnClickListener(this);
        register_term_condition.setOnClickListener(this);
        register_referal.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(register_referal.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    return true;
                }
                return false;
            }
        });
        register_referal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (register_referal.getText().toString().length() == 7) {
                    if (Utility.isNetworkAvailable(Register_Page.this)) {
                        referalCodeCheck();

                    } else {
                        alerts.showNetworkAlert(Register_Page.this);
                        register_referal.setText("");
                    }
                }

            }
        });

        typeface();
        if (getIntent().getExtras() != null) {
            register_facebbok_rrlout.setVisibility(View.GONE);
            double size[] = Scaler.getScalingFactor(this);
            double height = (110) * size[1];
            double width = (110) * size[0];
            fbClicked = true;

            fb_pic = getIntent().getStringExtra("fb_pic");
            register_email.setText(getIntent().getStringExtra("fbEmail"));
            register_firstname.setText(getIntent().getStringExtra("fb_frstname"));
            register_lastname.setText(getIntent().getStringExtra("fb_lastname"));
            checkPermission(true,isProfilePicSelected);
            Picasso.with(Register_Page.this).load(fb_pic)
                    .resize((int) width, (int) height)
                    .centerCrop().transform(new CircleTransform())
                    .placeholder(R.drawable.register_profile_default_image)
                    .into(register_profile_imv);
            fbid = getIntent().getStringExtra("fb_id");

            isPPSelected = true;

        } else {
            register_facebbok_rrlout.setVisibility(View.VISIBLE);
            loginFacebookSdk();
        }

    }

    /**
     * <h2>creatFilePath</h2>
     * <p>creating file path for the bitmap resource in the local file dictionary
     * @param bitmap
     * </p>
     */

    private void creatFilePath(Bitmap bitmap) {
        String temp = "iServe" + System.currentTimeMillis();
        takenNewImage = temp + ".png";
        clearOrCreateDir();

        if (Environment.MEDIA_MOUNTED.equals(state)) {

            newFile = new File(profilePicsDir, takenNewImage);
        } else {
            newFile = new File(getFilesDir() + "/" + Variableconstant.PARENT_FOLDER + "/Profile_Pictures");
            if (!newFile.isDirectory()) {
                newFile.isDirectory();
            }
            newFile = new File(newFile, takenNewImage);
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(newFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            uploadToAmazon(newFile);
            if(pDialog!=null)
            {
                pDialog.dismiss();
            }
            if(!fbClicked)
            {
                callConfirmNumber();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {

                if (fos != null) {
                    fos.flush();
                    fos.close();
                }

            } catch (IOException e) {
                e.printStackTrace();

            }
        }
    }
    /*
    * difining font style here*/

    private void typeface() {
        actionbartext.setTypeface(regularfont);
        buttonregister.setTypeface(regularfont);
        buttonregister.setTypeface(regularfont);
        register_term_condition.setTypeface(regularfont);
        firstnamelayout.setTypeface(litefont);
        register_firstname.setTypeface(litefont);
        lastnamelayout.setTypeface(litefont);
        register_lastname.setTypeface(litefont);
        emaillayout.setTypeface(litefont);
        register_email.setTypeface(litefont);
        country_code.setTypeface(litefont);
        mobile_numberlayout.setTypeface(litefont);
        register_mobile.setTypeface(litefont);
        passwordlayout.setTypeface(litefont);
        register_password.setTypeface(litefont);
        refrallayout.setTypeface(litefont);
        register_referal.setTypeface(litefont);
        register_facebbok_button.setTypeface(litefont);
    }

    /**
     * <h>LoginFacebookSdk</h>
     * <p>
     * This method is being called from onCreate method. this method is called
     * when user click on LoginWithFacebook button. it contains three method onSuccess,
     * onCancel and onError. if login will be successfull then success method will be
     * called and in that method we obtain user all details like id, email, name, profile pic
     * etc. onCancel method will be called if user click on facebook with login button and
     * suddenly click back button. onError method will be called if any problem occurs like
     * internet issue.
     * </p>
     */
    private void loginFacebookSdk() {
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("LoginActivity", response.toString());

                        if (response.getError() != null) {
                            // handle error
                        } else {
                            fbid = object.optString("id");
                            Fb_emailId = object.optString("email");
                            fb_gender = object.optString("gender");
                            fb_birthDay = object.optString("birthday");
                            Fb_firstName = object.optString("first_name");
                            fb_lastName = object.optString("last_name");
                            fb_pic = "https://graph.facebook.com/" + fbid + "/picture?width=1000&height=1000";

                            Log.d("email id..", Fb_emailId);
                            if (Fb_emailId != null && !Fb_emailId.equals("")) {
                                register_firstname.setText(Fb_firstName);
                                register_lastname.setText(fb_lastName);
                                register_email.setText(Fb_emailId);

                                register_email.requestFocus();


                                Picasso.with(Register_Page.this).load(fb_pic)
                                        .resize(register_profile_imv.getWidth(), register_profile_imv.getHeight())
                                        .centerCrop().transform(new CircleTransform())
                                        .placeholder(R.drawable.register_profile_default_image)
                                        .into(register_profile_imv);

                                isPPSelected = true;
                                checkPermission(true,isProfilePicSelected);
                              //  new DownloadImage().execute(fb_pic);
                            }

                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday,first_name,last_name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(Register_Page.this, getResources().getString(R.string.loginfailed), Toast.LENGTH_SHORT).show();
                fbid = "";
            }

            @Override
            public void onError(FacebookException error) {
                Utility.ShowAlert(error + "", Register_Page.this);
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        });
    }
    /**********************************************************************************************************************************/
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_term_condition:

                Intent inte = new Intent(this, Terms_Conditions.class);
                startActivity(inte);
                break;

            case R.id.iv_reg_pp:
                checkPermission(false,isProfilePicSelected);
                break;
            case R.id.terms_conditionrrlauoyt:
                break;
            case R.id.register_facebbok_rrlout:

                faceBookClicked();
                break;

            case R.id.register_facebbok_button:

                faceBookClicked();
                break;

            case R.id.tv_reg_nxt:
                boolean isValid;
                if (fbClicked) {
                    isValid = validateFields();
                    if (isValid) {

                        if (!register_mobile.getText().toString().isEmpty()) {
                            if (!phoneflag) {
                                facebookclickedflag = true;
                                if (Utility.isNetworkAvailable(Register_Page.this))
                                    phNoValidationRequest(register_mobile.getText().toString().trim());
                                else
                                    Utility.ShowAlert(getResources().getString(R.string.network_alert_message), Register_Page.this);
                                overridePendingTransition(R.anim.anim_two, R.anim.anim_one);
                            } else {

                                if (Utility.isNetworkAvailable(Register_Page.this))
                                {
                                    checkForSmsPermission();
                                }
                                else
                                    Utility.ShowAlert(getResources().getString(R.string.network_alert_message), Register_Page.this);
                                overridePendingTransition(R.anim.anim_two, R.anim.anim_one);
                            }

                        } else {
                            mobile_numberlayout.setError(getResources().getString(R.string.contact_mandatory));
                            mobile_numberlayout.setErrorEnabled(true);
                        }
                    }
                } else {
                    Boolean flag1,
                            flag5 = true,
                            flag6 = true,
                            flag7 = true,
                            flag9 = true,
                            flag10 = true;
                    if (register_firstname.getText().toString().equals("")) {
                        firstnamelayout.setError(getResources().getString(R.string.first_name_mandatory));
                        flag1 = false;
                    } else
                        flag1 = true;
                    if (register_email.getText().toString().equals("")) {
                        emaillayout.setError(getResources().getString(R.string.email_mandatory));
                        flag5 = false;
                    } else {
                        if (!validator.emailId_status(register_email.getText().toString())) {
                            emaillayout.setError(getResources().getString(R.string.email_invalid));
                            flag6 = false;
                        }
                    }

                    if (register_mobile.getText().toString().equals("")) {
                        mobile_numberlayout.setError(getResources().getString(R.string.contact_mandatory));
                        flag7 = false;
                    }

                    if (register_password.getText().toString().equals("")) {
                        passwordlayout.setError(getResources().getString(R.string.password_mandatory));
                        flag9 = false;
                    } else {
                        if (!validator.passStatus(register_password.getText().toString().trim())) {
                            passwordlayout.setError(getResources().getString(R.string.passwordcontain));
                            flag10 = false;
                        }
                    }

                    if (flag1) {
                        firstnamelayout.setErrorEnabled(false);
                    }
                    if (flag5 && flag6) {
                        emaillayout.setErrorEnabled(false);
                    }
                    if (flag7)
                    {
                        mobile_numberlayout.setErrorEnabled(false);
                    }
                    if (flag9 && flag10) {
                        passwordlayout.setErrorEnabled(false);
                    }


                    if (flag1 && flag5 && flag6 && flag7 && flag9 && flag10 && register_check_selectr.isChecked()) {
                        firstnamelayout.setErrorEnabled(false);
                        emaillayout.setErrorEnabled(false);
                        mobile_numberlayout.setErrorEnabled(false);
                        passwordlayout.setErrorEnabled(false);
                        if (Utility.isNetworkAvailable(Register_Page.this))
                        {
                            checkForSmsPermission();
                        } else
                            alerts.showNetworkAlert(Register_Page.this);
                    } else {
                        if (flag1 && flag5 && flag6 && flag7 && flag9 && flag10 && !register_check_selectr.isChecked())
                            Toast.makeText(Register_Page.this, getResources().getString(R.string.accept_tnc), Toast.LENGTH_LONG).show();
                    }
                }
                break;

            default:

        }
    }

    /**
     * <h2>faceBookClicked</h2>
     * <p>performing the operation when facebook is clicked, calling facebookloging and making
     * fbClicked flag true</p>
     */

    private void faceBookClicked()
    {
        fbClicked = true;
        firstnamelayout.setError("");
        firstnamelayout.setErrorEnabled(false);
        emaillayout.setError("");
        emaillayout.setErrorEnabled(false);
        mobile_numberlayout.setError("");
        mobile_numberlayout.setErrorEnabled(false);
        passwordlayout.setError("");
        passwordlayout.setErrorEnabled(false);
        if (Utility.isNetworkAvailable(Register_Page.this))
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
         else
            Utility.ShowAlert(getResources().getString(R.string.ErrorMsg), Register_Page.this);
    }


    /**
     * service for otpverification
     */
    private void otpVerficationcode()
    {
        String mobileNumberWithoutZero = "";
        if (register_mobile.getText().toString().length() > 0) {
            if (register_mobile.getText().toString().trim().charAt(0) == '0') {
                mobileNumberWithoutZero = register_mobile.getText().toString().substring(1);
            } else {
                mobileNumberWithoutZero = register_mobile.getText().toString().trim();
            }
        }


        FormBody requestform = new FormBody.Builder()
                .add("ent_phone", country_code.getText().toString() + mobileNumberWithoutZero)
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.GETVERIFICATIONCODE, requestform, new OkHttp3Request.JsonRequestCallback() {

            @Override
            public void onSuccess(String result) {
                pDialog.dismiss();

                Log.i("verificaion", "responce " + result);

                optverificationresponce(result);


            }

            @Override
            public void onError(String error) {
                if (pDialog != null) {
                    pDialog.dismiss();
                    //   pDialog = null;
                }
                Toast.makeText(Register_Page.this, error, Toast.LENGTH_SHORT).show();

            }
        });


    }//End of otpverification()

    private void optverificationresponce(String result) {

        if (result != null && !result.equals("")) {
            /*Bundle mbundle = new Bundle();

            mbundle.putString("ent_first_name", register_firstname.getText().toString());
            mbundle.putString("ent_last_name", register_lastname.getText().toString());
            mbundle.putString("ent_referal_code", register_referal.getText().toString());
            mbundle.putString("country_code", country_code.getText().toString());
            mbundle.putString("ent_email", register_email.getText().toString());
            mbundle.putString("ent_fbid", fbid);
            String mobileNumberWithoutZero;
            if (register_mobile.getText().toString().charAt(0) == '0') {
                mobileNumberWithoutZero = register_mobile.getText().toString().substring(1);
                mbundle.putString("ent_mobile", mobileNumberWithoutZero);
            } else {
                mobileNumberWithoutZero = register_mobile.getText().toString();
                mbundle.putString("ent_mobile", mobileNumberWithoutZero);
            }

            mbundle.putString("ent_password", register_password.getText().toString());
            mbundle.putString("ent_push_token", manager.getRegistrationId());
            mbundle.putString("ent_dev_id", manager.getDevice_Id());
            mbundle.putString("ent_app_version", BuildConfig.VERSION_NAME);
            if(fbClicked)
            {
                mbundle.putString("ent_signup_type","2");
            }
            else
            {
                mbundle.putString("ent_signup_type","1");
            }

            if (!isPPSelected)
            {
                Drawable myDrawable = getResources().getDrawable(R.drawable.register_profile_default_image);
                Bitmap anImage = ((BitmapDrawable) myDrawable).getBitmap();
                creatFilePath(anImage);
            }
            Intent intent = new Intent(Register_Page.this, ConfirmNumber.class);
            intent.putExtras(mbundle);
            startActivity(intent);*/
            if (!isPPSelected)
            {
                isProfilePicSelected = true;
                checkPermission(false,isProfilePicSelected);
            }
            else
            {
                callConfirmNumber();
            }
        }
    }

    private void callConfirmNumber()
    {
        Bundle mbundle = new Bundle();

        mbundle.putString("ent_first_name", register_firstname.getText().toString());
        mbundle.putString("ent_last_name", register_lastname.getText().toString());
        mbundle.putString("ent_referal_code", register_referal.getText().toString());
        mbundle.putString("country_code", country_code.getText().toString());
        mbundle.putString("ent_email", register_email.getText().toString());
        mbundle.putString("ent_fbid", fbid);
        String mobileNumberWithoutZero;
        if (register_mobile.getText().toString().charAt(0) == '0') {
            mobileNumberWithoutZero = register_mobile.getText().toString().substring(1);
            mbundle.putString("ent_mobile", mobileNumberWithoutZero);
        } else {
            mobileNumberWithoutZero = register_mobile.getText().toString();
            mbundle.putString("ent_mobile", mobileNumberWithoutZero);
        }
        mbundle.putString("ent_password", register_password.getText().toString());
        mbundle.putString("ent_push_token", manager.getRegistrationId());
        mbundle.putString("ent_dev_id", manager.getDevice_Id());
        mbundle.putString("ent_app_version", BuildConfig.VERSION_NAME);
        if(fbClicked)
        {
            mbundle.putString("ent_signup_type","2");
        }
        else
        {
            mbundle.putString("ent_signup_type","1");
        }
        Intent intent = new Intent(Register_Page.this, ConfirmNumber.class);
        intent.putExtras(mbundle);
        startActivity(intent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        finish();
    }

    /**
     * implementing on focuschange method
     *
     * @param v        id view
     * @param hasFocus focus
     */

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        switch (v.getId()) {
            case R.id.et_reg_fnm:

                if (!hasFocus) {
                    if (register_firstname.getText().toString().length() < 1) {
                        firstnamelayout.setError(getResources().getString(R.string.first_name_mandatory));
                        firstnamelayout.setErrorEnabled(true);
                    }

                    else {
                        firstnamelayout.setErrorEnabled(false);
                    }
                }
                break;

            case R.id.et_reg_refrl:


                break;

            case R.id.et_reg_paswrd:
                if (!hasFocus) {
                    if (register_password.getText().toString().equals("")) {
                        passwordlayout.setError(getResources().getString(R.string.password_mandatory));
                        passwordlayout.setErrorEnabled(true);
                    } else if (!validator.passStatus(register_password.getText().toString().trim())) {
                        passwordlayout.setError(getResources().getString(R.string.passwordcontain));
                        passwordlayout.setErrorEnabled(true);
                    } else {
                        passwordlayout.setError(null);
                        passwordlayout.setErrorEnabled(false);
                    }
                }
                break;

            case R.id.et_reg_eml:

                if (!hasFocus) {
                    // user is done editing
                    if (emaillayout.getEditText().getText().toString().length() < 1) {
                        emaillayout.setError(getResources().getString(R.string.email_mandatory));
                        emaillayout.setErrorEnabled(true);
                    } else if (!validator.emailValidation(emaillayout.getEditText().getText().toString())) {
                        emaillayout.setError(getResources().getString(R.string.email_invalid));
                        emaillayout.setErrorEnabled(true);
                    } else {
                        emaillayout.setError(null);
                        emaillayout.setErrorEnabled(false);
                        if (Utility.isNetworkAvailable(Register_Page.this)) {
                            emailValidationRequest(register_email.getText().toString());

                        } else {
                            alerts.showNetworkAlert(Register_Page.this);
                        }
                    }

                }

                break;

            case R.id.et_reg_mobno:

                if (!hasFocus) {
                    if (mobile_numberlayout.getEditText().getText().toString().length() < 1) {
                        mobile_numberlayout.setError(getResources().getString(R.string.contact_mandatory));
                        mobile_numberlayout.setErrorEnabled(true);
                    } else {
                        mobile_numberlayout.setError(null);
                        mobile_numberlayout.setErrorEnabled(false);
                        if (Utility.isNetworkAvailable(Register_Page.this)) {
                            phNoValidationRequest(register_mobile.getText().toString());
                        } else {
                            alerts.showNetworkAlert(Register_Page.this);
                        }
                    }
                }

                break;
            default:
                break;

        }

    }

    private void referalCodeCheck() {
        pDialog = new ProgressDialog(this);
        if (pDialog != null) {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage(getResources().getString(R.string.referal_code));
            pDialog.show();
        } else {
            pDialog.setMessage(getResources().getString(R.string.referal_code));
            pDialog.show();
        }
        FormBody requestfomr = new FormBody.Builder()
                .add("ent_coupon", register_referal.getText().toString())
                .add("ent_lat", manager.getLATITUDE() + "")
                .add("ent_long", manager.getLONGITUDE() + "")
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "verifyCode", requestfomr, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                pDialog.dismiss();
                Validator_Pojo referalcode;
                if (result != null) {
                    referalcode = gson.fromJson(result, Validator_Pojo.class);
                    if (referalcode.getErrFlag().equals("0")) {
                        // alerts.problemLoadingAlert(Register_Page.this,getResources().getString(R.string.promo_coed_added_success));
                    } else {
                        alerts.problemLoadingAlert(Register_Page.this, referalcode.getErrMsg());

                        register_referal.setText("");
                    }
                }

            }

            @Override
            public void onError(String error) {
                pDialog.dismiss();
            }
        });

    }


    /**
     * service for emailvalidator
     *
     * @param emailId emailtext
     */

    private void emailValidationRequest(String emailId) {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.mail_validating));
        pDialog.show();

        FormBody requestBody = new FormBody.Builder()
                .add("ent_email", emailId)
                .add("ent_user_type", 2 + "")
                .add("ent_date_time", Utility.dateintwtfour())
                .build();
        Utility.printLog("mail validtion getParams: " + requestBody);


        OkHttp3Request.doOkHttp3Request(Variableconstant.EMAILVALIDATION, requestBody, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                pDialog.dismiss();

                if (result != null && !result.equals("")) {


                    Validator_Pojo emailValidator_pojo;
                    Gson gson = new Gson();
                    emailValidator_pojo = gson.fromJson(result, Validator_Pojo.class);
                    switch (emailValidator_pojo.getErrFlag()) {
                        case "1":

                            emailflag = false;
                            register_email.setText("");
                            String text = emailValidator_pojo.getErrMsg();
                            emaillayout.setError(text);
                            Toast.makeText(Register_Page.this, text, Toast.LENGTH_LONG).show();
                            register_email.requestFocus();
                            register_mobile.setError(null);
                            mobile_numberlayout.setError(null);
                            mobile_numberlayout.setErrorEnabled(false);

                            break;
                        case "0":
                            emailflag = true;
                            break;
                        default:
                            Toast.makeText(Register_Page.this, emailValidator_pojo.getErrMsg(), Toast.LENGTH_LONG).show();
                            break;
                    }

                    // updateMasterStatusHandler(result);
                }
            }

            @Override
            public void onError(String error) {
                if (pDialog != null) {
                    pDialog.dismiss();
                    // pDialog = null;
                }
                Toast.makeText(Register_Page.this, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
            }
        });

    }//End for emailValidation

    /**
     * <h2>phNoValidationRequest</h2>
     * <p2>
     *     validating the mobile number if already present or not
     * service for PhoneValidator
     * @param phNo phone number</p2>
     */
    private void phNoValidationRequest(String phNo) {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.phone_validating));
        pDialog.show();

        FormBody requestBody = new FormBody.Builder()
                .add("ent_phone", phNo)
                .build();

        // .add("ent_user_type", "2")
        OkHttp3Request.doOkHttp3Request(Variableconstant.PHONENOVALIDATION, requestBody, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                pDialog.dismiss();
                if (result != null && !result.equals("")) {
                    try {
                        Validator_Pojo phoneNumberValidator_pojo;
                        Gson gson = new Gson();
                        phoneNumberValidator_pojo = gson.fromJson(result, Validator_Pojo.class);
                        switch (phoneNumberValidator_pojo.getErrFlag()) {
                            case "1":
                                String text = phoneNumberValidator_pojo.getErrMsg();
                                register_mobile.setText("");
                                mobile_numberlayout.setError(text);
                                phoneflag = false;
                                Toast.makeText(Register_Page.this, text, Toast.LENGTH_LONG).show();
                                register_mobile.requestFocus();

                                break;
                            case "0":
                                phoneflag = true;
                                if (facebookclickedflag) {
                                    if (Utility.isNetworkAvailable(Register_Page.this))
                                        checkForSmsPermission();
                                    else
                                        Utility.ShowAlert(getResources().getString(R.string.network_alert_message), Register_Page.this);
                                    overridePendingTransition(R.anim.anim_two, R.anim.anim_one);
                                }
                                break;
                            default:
                                Toast.makeText(Register_Page.this, phoneNumberValidator_pojo.getErrMsg(), Toast.LENGTH_LONG).show();
                                break;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(String error) {
                if (pDialog != null) {
                    pDialog.dismiss();
                    //  pDialog = null;
                }

                Toast.makeText(Register_Page.this, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });


    }//End for mobileValidator

    /**
     * <h2>selectimage</h2>
     *<p>alert builder to show to the option for the user to choose </p>
     */
    private void selectimage() {
        clearOrCreateDir();
        if (isPPSelected) {
            options = getResources().getStringArray(R.array.takeremovefoto);
        } else {
            options = getResources().getStringArray(R.array.takefoto);

        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.addprofilefoto));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.takephoto))) {

                    takePicFromCamera();

                } else if (options[item].equals(getResources().getString(R.string.choosefromglry))) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, GALLERY_PIC);
                    overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);


                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                } else if (options[item].equals(getResources().getString(R.string.removephoto))) {
                    register_profile_imv.setImageDrawable(null);
                    register_profile_imv.setImageDrawable(getResources().getDrawable(R.drawable.register_profile_default_image));
                    isPPSelected = false;
                }
            }
        });
        builder.show();

    }


    /**
     * we are getting option to pick the image
     */


    private void takePicFromCamera() {

        try {

            state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state))
                newFile = new File(Environment.getExternalStorageDirectory()+"/"+ Variableconstant.PARENT_FOLDER+"/Profile_Pictures/",takenNewImage);
            else
                newFile = new File(this.getFilesDir()+"/"+ Variableconstant.PARENT_FOLDER+"/Profile_Pictures/",takenNewImage);
            if(Build.VERSION.SDK_INT>=N)
                newProfileImageUri = FileProvider.getUriForFile(this,BuildConfig.APPLICATION_ID + ".provider",newFile);
            else
                newProfileImageUri = Uri.fromFile(newFile);

           /* if (Environment.MEDIA_MOUNTED.equals(state)) {

                newFile = new File(Environment.getExternalStorageDirectory() + "/" + Variableconstant.PARENT_FOLDER + "/Media/Images/Profile_Pictures", takenNewImage);
                newProfileImageUri = Uri.fromFile(newFile);
            } else {
                newFile = new File(getFilesDir() + "/" + Variableconstant.PARENT_FOLDER + "/Media/Images/CropImages/", takenNewImage);
                newProfileImageUri = Uri.fromFile(newFile);
            }*/

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, newProfileImageUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, CAMERA_PIC);
        } catch (ActivityNotFoundException e)
        {
            e.printStackTrace();
        }
    }


    /**
     * croping the image which u choose from
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)    //result code to check is the result is ok or not
        {
            return;
        }
        Bitmap bitmap;

        switch (requestCode) {
            case CAMERA_PIC:
                startCropImage();
                break;

            case GALLERY_PIC:
                try {

                    state = Environment.getExternalStorageState();

                    if (Environment.MEDIA_MOUNTED.equals(state)) {
                        newFile = new File(Environment.getExternalStorageDirectory() + "/" + Variableconstant.PARENT_FOLDER + "/Profile_Pictures/", takenNewImage);
                    } else {
                        newFile = new File(getFilesDir() + "/" + Variableconstant.PARENT_FOLDER + "/Profile_Pictures/", takenNewImage);
                    }

                    InputStream inputStream = getContentResolver().openInputStream(data.getData());

                    FileOutputStream fileOutputStream = new FileOutputStream(newFile);

                    Utility.copyStream(inputStream, fileOutputStream);

                    fileOutputStream.close();
                    inputStream.close();
                    newProfileImageUri = Uri.fromFile(newFile);
                    startCropImage();
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;

            case CROP_IMAGE:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {
                    Utility.printLog("profile fragment CROP_IMAGE file path is null: " + newFile.getPath());
                } else {
                    isPPSelected = true;    // profile pic now set
                    newProfileImageUri = Uri.fromFile(newFile);

                    uploadToAmazon(newFile);


                    try {


                        bitmap = BitmapFactory.decodeFile(newFile.getPath());

                        Bitmap bitmap1 = bitmap.copy(Bitmap.Config.ARGB_8888, true);

                        int w = register_profile_imv.getWidth(), h = register_profile_imv.getHeight();

                        Bitmap roundBitmap = Utility.getRoundedCroppedBitmap(bitmap1, w);

                        register_profile_imv.setImageBitmap(roundBitmap);


                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    break;
                }
        }
    }

    /**
     * <h1>startCropImage</h1>
     * this method crops the images taken from the camera or the gallery
     */

    private void startCropImage() {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, newFile.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        startActivityForResult(intent, CROP_IMAGE);
    }


    /**
     * to clear the directory if directory has been already created otherwise create a new directory
     */
    private void clearOrCreateDir() {
        try {

            state = Environment.getExternalStorageState();
            File cropImagesDir;
            File[] cropImagesDirectory;

            if (Environment.MEDIA_MOUNTED.equals(state)) {


                cropImagesDir = new File(Environment.getExternalStorageDirectory() + "/" + Variableconstant.PARENT_FOLDER + "/CropImages");
                profilePicsDir = new File(Environment.getExternalStorageDirectory() + "/" + Variableconstant.PARENT_FOLDER + "/Profile_Pictures");

            } else {

                cropImagesDir = new File(getFilesDir() + "/" + Variableconstant.PARENT_FOLDER + "/CropImages");
                profilePicsDir = new File(getFilesDir() + "/" + Variableconstant.PARENT_FOLDER + "/Profile_Pictures");

            }

            if (!cropImagesDir.isDirectory()) {
                cropImagesDir.mkdirs();
            } else {
                cropImagesDirectory = cropImagesDir.listFiles();

                if (cropImagesDirectory.length > 0) {
                    for (File aCropImagesDirectory : cropImagesDirectory) {
                        aCropImagesDirectory.delete();
                    }
                } else {
                    Utility.printLog("profile fragment CropImages Dir empty  or null: " + cropImagesDirectory.length);
                }
            }

            if (!profilePicsDir.exists() && !profilePicsDir.isDirectory()) {
                profilePicsDir.mkdirs();
            } else {
                File[] profilePicsDirectory = profilePicsDir.listFiles();

                if (profilePicsDirectory.length > 0) {
                    for (File aProfilePicsDirectory : profilePicsDirectory) {
                        aProfilePicsDirectory.delete();
                    }
                } else {
                    Utility.printLog("profile fragment profilePicsDir empty  or null: " + profilePicsDirectory.length);
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void setListener() {
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                                        int flagDrawableResID) {

                country_code.setText(dialCode);
                mCountryFlagImageView.setImageResource(flagDrawableResID);
                mCountryPicker.dismiss();
            }
        });
        country_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountryPicker.show(getSupportFragmentManager(), getResources().getString(R.string.Countrypicker));
            }
        });
        getUserCountryInfo();
    }


    private void getUserCountryInfo() {
        Country country = mCountryPicker.getUserCountryInfo(this);
        mCountryFlagImageView.setImageResource(country.getFlag());
        country_code.setText(country.getDialCode());
    }


    private boolean validateFields() {
        if (register_firstname.getText().toString().isEmpty() || register_firstname.getText().toString().equals("")) {
            firstnamelayout.setError(getResources().getString(R.string.first_name_mandatory));
            firstnamelayout.setErrorEnabled(true);
            return false;
        } else if (register_mobile.getText().toString().isEmpty()) {
            mobile_numberlayout.setError(getResources().getString(R.string.contact_mandatory));
            mobile_numberlayout.setErrorEnabled(true);
            return false;
        } else if (register_email.getText().toString().isEmpty()) {
            emaillayout.setError(getResources().getString(R.string.email_mandatory));
            emaillayout.setErrorEnabled(true);
            return false;
        } else if (register_password.getText().toString().isEmpty()) {
            passwordlayout.setError(getResources().getString(R.string.password_mandatory));
            passwordlayout.setErrorEnabled(true);
            return false;
        } else if (!register_check_selectr.isChecked()) {
            Toast.makeText(Register_Page.this, getResources().getString(R.string.accept_tnc), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Bitmap doInBackground(String... URL) {

            String imageURL = URL[0];

            Bitmap bitmap = null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // Set the bitmap into ImageView

            creatFilePath(result);

        }
    }

    /**
     * <h2>checkPermission</h2>
     * <p>checking for the permission for camera and file storage at run time for
     * build version more than 22
     * @param isFbClicked checking if facebook is clicked or not</p>
     */
    private void checkPermission(boolean isFbClicked, boolean isProfilePicSelected)
    {

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList = new ArrayList<>();
            myPermissionConstantsArrayList.clear();
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);

            if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE)) {
                //TODO: if all permissions are already granted

                if(isProfilePicSelected)
                {

                    Drawable myDrawable = getResources().getDrawable(R.drawable.register_profile_default_image);
                    Bitmap anImage = ((BitmapDrawable) myDrawable).getBitmap();
                    creatFilePath(anImage);
                    if(pDialog!=null)
                    {
                        pDialog.setMessage(getResources().getString(R.string.wait));
                        pDialog.show();
                    }
                }
                else
                {
                    if(!isFbClicked)
                        selectimage();
                    else
                        new DownloadImage().execute(fb_pic);
                }


            }
        } else
        {
            if(isProfilePicSelected)
            {
                Drawable myDrawable = getResources().getDrawable(R.drawable.register_profile_default_image);
                Bitmap anImage = ((BitmapDrawable) myDrawable).getBitmap();
                creatFilePath(anImage);
                if(pDialog!=null)
                {
                    pDialog.setMessage(getResources().getString(R.string.wait));
                    pDialog.show();
                }
            }
            else
            {
                if(!isFbClicked)
                    selectimage();
                else
                    new DownloadImage().execute(fb_pic);
            }

        }

    }

    /**
     * predefined method to check run time permissions list call back
     *
     * @param requestCode   request code
     * @param permissions:  contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        boolean isDenine = false;
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine) {
                    //TODO: if permissions denied by user, recall it
                    Snackbar.
                            make(coordinatorLayout, getResources().getString(R.string.permissiondenied) + " " + REQUEST_CODE_PERMISSION_MULTIPLE, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {
                   /* if(!fbClicked)
                        selectimage();
                    else
                        new DownloadImage().execute(fb_pic);*/

                    if(isProfilePicSelected)
                    {
                        Drawable myDrawable = getResources().getDrawable(R.drawable.register_profile_default_image);
                        Bitmap anImage = ((BitmapDrawable) myDrawable).getBitmap();
                        creatFilePath(anImage);
                        if(pDialog!=null)
                        {
                            pDialog.setMessage(getResources().getString(R.string.wait));
                            pDialog.show();
                        }
                    }
                    else
                    {
                        if(!fbClicked)
                            selectimage();
                        else
                            new DownloadImage().execute(fb_pic);
                    }

                    //TODO: if permissions granted by user, move forward
                }
                break;

            case REQUEST_CODE_SMS_PERMISSION:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine) {
                    //TODO: if permissions denied by user, recall it

                    alertDialogTORecallOrGo();
                    /*Snackbar.
                            make(coordinatorLayout, getResources().getString(R.string.permissiondenied) + " " + REQUEST_CODE_PERMISSION_MULTIPLE, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();*/
                } else {

                    pDialog = new ProgressDialog(this);
                    pDialog.setMessage(getString(R.string.generatingotp));
                    pDialog.show();
                    otpVerficationcode();

                    //TODO: if permissions granted by user, move forward
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private void alertDialogTORecallOrGo()
    {
        Log.d("TAG","smsReceived");

        AlertDialog.Builder builder = new AlertDialog.Builder(Register_Page.this);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        builder.setCancelable(false);
        builder.setIcon(getResources().getDrawable(R.drawable.ic_launcher));
        builder.setTitle(getResources().getString(R.string.smsPerminssionDenid));
        builder.setMessage(getResources().getString(R.string.smsPerminssion));
        builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {

                dialog.dismiss();
                pDialog = new ProgressDialog(Register_Page.this);
                pDialog.setMessage(getString(R.string.generatingotp));
                pDialog.show();
                otpVerficationcode();

            }
        });

        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkForSmsPermission();
                dialog.dismiss();
            }
        });
        builder.show();

    }

    /**
     * uploadimage to amazon
     * @param image profile Image to upload
     */
    private void uploadToAmazon(File image)
    {

        upload.Upload_data(Variableconstant.Amazonbucket, image, new UploadAmazonS3.Upload_CallBack() {
            @Override
            public void sucess(String sucess)
            {
                uplodedImage = sucess;

            }

            @Override
            public void error(String errormsg) {

            }
        });
    }

    public void checkForSmsPermission()
    {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList = new ArrayList<>();
            myPermissionConstantsArrayList.clear();
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_SMS);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_RECEIVE_SMS);
            if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList, REQUEST_CODE_SMS_PERMISSION)) {
                //TODO: if all permissions are already granted

               //
                pDialog = new ProgressDialog(this);
                pDialog.setMessage(getString(R.string.generatingotp));
                pDialog.show();
                otpVerficationcode();

            }
        } else
        {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage(getString(R.string.generatingotp));
            pDialog.show();
            otpVerficationcode();
        }
    }

}
