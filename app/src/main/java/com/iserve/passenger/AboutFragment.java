package com.iserve.passenger;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.utility.Utility;
import com.utility.Variableconstant;

/**
 * Created by embed on 05/05/16.
 *
 */
public class AboutFragment extends Fragment implements View.OnClickListener
{

    ImageView tolbarhomelogo;
    TextView private_Tv,Weblink_Tv,rate_in_google_play_Tv,legal_Tv,toolbarhometxv,tveditbarhometxv,
            youraddress,yourlocation,version,like_in_facebook_Tv;
    RelativeLayout rate_in_google_play_Rl,legal_Rl,like_in_facebook_Rl,imageframelayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * <p>inflating the view</p>
     * @param inflater using ifaltr we inflate our layout
     * @param container this contain viewgroup
     * @param savedInstanceState save the bundle instance
     * @return it return view
     */

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.aboutfragment,container,false);
        intilizeVariable(view);
        return view;
    }



    /**
     * <p>Intiliazing view elements</p>
     * @param view initialize the parameter with the view
     */
    void intilizeVariable(View view)
    {

        Typeface regularfont = Typeface.createFromAsset(getActivity().getAssets(), Variableconstant.regularfont);

        private_Tv= (TextView) view.findViewById(R.id.private_Tv);
        Weblink_Tv= (TextView) view.findViewById(R.id.Weblink_Tv);
        rate_in_google_play_Tv= (TextView) view.findViewById(R.id.rate_in_google_play_Tv);
        like_in_facebook_Tv= (TextView) view.findViewById(R.id.like_in_facebook_Tv);
        legal_Tv= (TextView) view.findViewById(R.id.legal_Tv);
        rate_in_google_play_Rl= (RelativeLayout) view.findViewById(R.id.rate_in_google_play_Rl);
        legal_Rl= (RelativeLayout) view.findViewById(R.id.legal_Rl);
        like_in_facebook_Rl= (RelativeLayout) view.findViewById(R.id.like_in_facebook_Rl);
        toolbarhometxv = (TextView) getActivity().findViewById(R.id.toolbarhometxv);
        yourlocation = (TextView) getActivity().findViewById(R.id.yourlocation);
        youraddress = (TextView) getActivity().findViewById(R.id.youraddress);
        imageframelayout = (RelativeLayout) getActivity().findViewById(R.id.imageframelayout);
        imageframelayout.setVisibility(View.GONE);
        version = (TextView) view.findViewById(R.id.version);
        toolbarhometxv.setVisibility(View.VISIBLE);
        toolbarhometxv.setText(getResources().getString(R.string.about));
        tolbarhomelogo = (ImageView) getActivity().findViewById(R.id.tolbarhomelogo);
        tveditbarhometxv = (TextView) getActivity().findViewById(R.id.tveditbarhometxv);
        youraddress.setVisibility(View.GONE);
        yourlocation.setVisibility(View.GONE);
        tveditbarhometxv.setVisibility(View.GONE);
        tolbarhomelogo.setVisibility(View.GONE);
        rate_in_google_play_Rl.setOnClickListener(this);
        legal_Rl.setOnClickListener(this);
        like_in_facebook_Rl.setOnClickListener(this);
        Weblink_Tv.setOnClickListener(this);
        typeface(regularfont);
        String appversion = "V "+BuildConfig.VERSION_NAME;
        version.setText(appversion);
    }

    private void typeface(Typeface regularfont)
    {
        private_Tv.setTypeface(regularfont);
        Weblink_Tv.setTypeface(regularfont);
        rate_in_google_play_Tv.setTypeface(regularfont);
        like_in_facebook_Tv.setTypeface(regularfont);
        legal_Tv.setTypeface(regularfont);
        toolbarhometxv.setTypeface(regularfont);
        version.setTypeface(regularfont);
    }

    /**
     * <p>overriding onclick method</p>
     * @see View.OnClickListener
     * @param v hear is view using this we compare the id's of the view
     */

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.Weblink_Tv:
                if (Utility.isNetworkAvailable(getActivity()))
                {
                    String url = "http://www.iserve.ind.in/";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
                else
                {
                    Utility.ShowAlert(getActivity().getResources().getString(R.string.nointernet), getActivity());
                }
                break;
            case R.id.rate_in_google_play_Rl:
                if (Utility.isNetworkAvailable(getActivity()))
                {

                    Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    try {
                        startActivity(goToMarket);
                    } catch (ActivityNotFoundException e)
                    {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
                    }
                }
                else
                {
                    Utility.ShowAlert(getActivity().getResources().getString(R.string.nointernet), getActivity());

                }
                break;
            case R.id.like_in_facebook_Rl:
                if (Utility.isNetworkAvailable(getActivity()))
                {
                    String url = "https://www.facebook.com/IServe-1801457180122864/";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);

                }
                else
                {
                    Utility.ShowAlert(getActivity().getResources().getString(R.string.nointernet), getActivity());
                }
                break;
            default:
                Intent intent=new Intent(getActivity(), Terms_Conditions.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
                break;

        }

    }
}
