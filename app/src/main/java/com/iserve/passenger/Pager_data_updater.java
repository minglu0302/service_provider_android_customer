package com.iserve.passenger;

import org.json.JSONObject;

/**
 * Created by embed on 18/11/16.
 */
public interface Pager_data_updater
{
    void onUpdated(JSONObject jsonObject);

    void onError();
}
