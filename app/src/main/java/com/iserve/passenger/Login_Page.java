package com.iserve.passenger;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.pojo.Language_Type;
import com.pojo.LoginActivity_pojo;
import com.pojo.Validator_Pojo;
import org.json.JSONObject;
import java.util.Arrays;
import com.utility.Alerts;
import com.utility.OkHttp3Request;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Validator;
import com.utility.Variableconstant;
import okhttp3.FormBody;


/**
 * <h>Login_Page</h>
 * <p>this class hold the user interface for Login
 * it also contain facebook login</p>
 *@since  8/7/16.
 *
 */
public class Login_Page extends AppCompatActivity implements View.OnClickListener
{
    Alerts alerts;
    FrameLayout fframelayt;
    EditText passwordedittext,emailedittext;
    TextView loginbutton,signinortextv,actionbartext,forgotpass_Tv,forgotemail,signin_facebook;
    Dialog forgorpassdialog;
    ProgressDialog pDialog;
    Bundle forfacebooksavedInstanceState = null;
    Gson gson;
    SessionManager manager;
    LoginActivity_pojo loginpojo;
    Validator_Pojo validatorpojo;
    TextInputLayout emtxtlayout,passtxtlayout;
    String fb_emailId,Fb_firstName,fb_lastName,fb_birthDay,fb_gender,fb_pic,fbid;
    CallbackManager fbCallbackManager;
    Validator validator;
    Typeface litefont,regularfont;
    Language_Type lantype_pojo;
    String ent_signup_type;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        forfacebooksavedInstanceState = savedInstanceState;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        FacebookSdk.sdkInitialize(Login_Page.this);
        fbCallbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.signin_layout);
        initialize();
        typeface();
        loginFacebookSdk();
    }

    /**
     *<h2>initialize</h2>
     * <p>here all the Ui Component of the Class is initialize
     * for the User</p>
     */
    private void initialize()
    {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.Loading));
        pDialog.setCancelable(false);
        gson = new Gson();
        manager = new SessionManager(this);
        validator = new Validator();
        alerts = new Alerts(this);
        lantype_pojo = new Language_Type();
        litefont = Typeface.createFromAsset(this.getAssets(),Variableconstant.lightfont);
        regularfont = Typeface.createFromAsset(this.getAssets(),Variableconstant.regularfont);
        signin_facebook = (TextView) findViewById(R.id.signin_facebook);
        passwordedittext = (EditText) findViewById(R.id.passwordedittext);
        emailedittext = (EditText) findViewById(R.id.emailedittext);
        loginbutton = (TextView) findViewById(R.id.loginbutton);
        signinortextv = (TextView) findViewById(R.id.signinortextv);
        forgotpass_Tv = (TextView) findViewById(R.id.forgotpass_Tv);
        emtxtlayout = (TextInputLayout) findViewById(R.id.emtxtlayout);
        passtxtlayout = (TextInputLayout) findViewById(R.id.passtxtlayout);
        emtxtlayout.setHint(getResources().getString(R.string.smallemail));
        passtxtlayout.setHint(getResources().getString(R.string.smallpassword));
        passtxtlayout.setHintAnimationEnabled(true);
        emtxtlayout.setHintAnimationEnabled(true);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toobar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        fframelayt.setVisibility(View.VISIBLE);
        actionbartext = (TextView) findViewById(R.id.toolbarhometxv);
        actionbartext.setVisibility(View.VISIBLE);
        actionbartext.setText(getResources().getString(R.string.logininaction));
        signin_facebook.setOnClickListener(this);
        loginbutton.setOnClickListener(this);
        forgotpass_Tv.setOnClickListener(this);

    }

   /*
   * defining the font styling */

    private void typeface()
    {
        actionbartext.setTypeface(regularfont);
        loginbutton.setTypeface(regularfont);
        passtxtlayout.setTypeface(litefont);
        emtxtlayout.setTypeface(litefont);
        passwordedittext.setTypeface(litefont);
        emailedittext.setTypeface(litefont);
        forgotpass_Tv.setTypeface(litefont);
        signin_facebook.setTypeface(litefont);

    }

    /**
     * <h>LoginFacebookSdk</h>
     * <p>
     *     This method is being called from onCreate method. this method is called
     *     when user click on LoginWithFacebook button. it contains three method onSuccess,
     *     onCancel and onError. if login will be successfull then success method will be
     *     called and in that method we obtain user all details like id, email, name, profile pic
     *     etc. onCancel method will be called if user click on facebook with login button and
     *     suddenly click back button. onError method will be called if any problem occurs like
     *     internet issue.
     * </p>
     */
    private void loginFacebookSdk()
    {
        LoginManager.getInstance().registerCallback(fbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d("LoginActivity", response.toString());

                        if (response.getError() != null) {
                            // handle error
                        } else {
                            fbid = object.optString("id");
                            fb_emailId = object.optString("email");
                            fb_gender = object.optString("gender");
                            fb_birthDay = object.optString("birthday");
                            Fb_firstName = object.optString("first_name");
                            fb_lastName = object.optString("last_name");
                            fb_pic = "https://graph.facebook.com/" + fbid + "/picture?width=1000&height=1000";
                            if (fb_emailId != null && !fb_emailId.equals(""))
                            {
                                ent_signup_type = "2";
                                Signinservice(true);
                            }
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday,first_name,last_name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.facebookloginfailed), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(Login_Page.this,""+error,Toast.LENGTH_SHORT).show();
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        finish();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.forgotpass_Tv:

                forgotpassword();
                break;
            case R.id.signin_facebook:
                if (Utility.isNetworkAvailable(Login_Page.this))
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
                else
                    Utility.ShowAlert(getResources().getString(R.string.nointernet),Login_Page.this);
                break;
            case R.id.loginbutton:


                emtxtlayout.setErrorEnabled(false);
                passtxtlayout.setErrorEnabled(false);


                if(validateemailphone())
                {
                    emtxtlayout.setErrorEnabled(false);
                    if (passtxtlayout.getEditText().getText().toString().equals("")) {
                        passtxtlayout.setError(getResources().getString(R.string.password_mandatory));
                        passtxtlayout.setErrorEnabled(true);
                    }
                    else
                    {
                        passtxtlayout.setErrorEnabled(false);
                        if(Utility.isNetworkAvailable(Login_Page.this))
                        {
                            ent_signup_type = "1";
                            Signinservice(false);
                        }
                        else{
                            alerts.showNetworkAlert(Login_Page.this);

                        }
                    }
                }
                else
                {
                    emtxtlayout.setErrorEnabled(true);
                }


                break;
            default:
        }
    }

    /**
     * <h2>forgotpassword</h2>
     * <p>opening the dialog for the forgot password to choose from email or mobile number</p>
     */

    private void forgotpassword()
    {

        forgorpassdialog = new Dialog(this);
        forgorpassdialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        forgorpassdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgorpassdialog.setContentView(R.layout.forgotpassdialogue);

        TextView forgotemail = (TextView) forgorpassdialog.findViewById(R.id.forgotemail);
        TextView forgotmobile = (TextView) forgorpassdialog.findViewById(R.id.forgotmobile);

        forgotemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgorpassdialog.dismiss();
                forgotpasswordwithemail();
            }
        });
        forgotmobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login_Page.this,ForgotPwdActivity.class);
                overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
                startActivity(intent);
                forgorpassdialog.dismiss();
            }
        });


        forgorpassdialog.show();

    }

    /**
     * <h2>forgotpasswordwithemail</h2>
     * <p>calling Server Api service of forgot password using email id</p>
     */
    private void forgotpasswordwithemail()
    {
        Button okButton,cancelbutton;
        forgorpassdialog = new Dialog(this);
        forgorpassdialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        forgorpassdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgorpassdialog.setContentView(R.layout.forgotpasswordemaildialog);
        forgotemail = (TextView) forgorpassdialog.findViewById(R.id.forgotemail);
        okButton = (Button) forgorpassdialog.findViewById(R.id.okButton);
        cancelbutton = (Button) forgorpassdialog.findViewById(R.id.cancelbutton);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                boolean flag1 = true, flag2 = true;

                if (forgotemail.getText().toString().equals("")) {
                    forgotemail.setError(getResources().getString(R.string.email_mandatory));
                    flag1 = false;
                } else {
                    if (!validator.emailId_status(forgotemail.getText().toString())) {
                        forgotemail.setError(getResources().getString(R.string.email_invalid));
                        flag2 = false;
                    }
                }
                if (flag1 && flag2) {

                    if (Utility.isNetworkAvailable(Login_Page.this)) {


                        forgotPassService(forgotemail.getText().toString());

                    } else {
                        alerts.showNetworkAlert(Login_Page.this);

                    }
                }
            }
        });
        cancelbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgorpassdialog.dismiss();
            }
        });
        forgorpassdialog.show();
    }

    /**
     * <h2>forgotPassService</h2>
     * <p>calling Server forgotpassword  api</p>
     * forgot password service
     * @param s emailid
     */

    private void forgotPassService(String s)
    {
        pDialog.setMessage(getString(R.string.sending));
        pDialog.show();

        FormBody requestform = new FormBody.Builder()
                .add("ent_email",s)
                .add("ent_user_type","2")
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.FORGOTPASS, requestform, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {

                if(pDialog.isShowing())
                {
                    pDialog.dismiss();
                }
                if(!result.equals(""))
                {
                    validatorpojo = gson.fromJson(result,Validator_Pojo.class);

                    if (validatorpojo.getErrNum().equals("66") && validatorpojo.getErrFlag().equals("1")) {
                        forgotemail.setError(validatorpojo.getErrMsg());
                        pDialog.dismiss();
                    } else if (validatorpojo.getErrFlag().equals("0")) {
                        Toast.makeText(Login_Page.this, validatorpojo.getErrMsg(), Toast.LENGTH_LONG).show();
                        pDialog.dismiss();
                        forgorpassdialog.dismiss();
                    } else {
                        forgotemail.setError(null);
                        pDialog.dismiss();
                        Toast.makeText(Login_Page.this, validatorpojo.getErrMsg(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(Login_Page.this, getString(R.string.something_wentwrong), Toast.LENGTH_LONG).show();


                }
            }

            @Override
            public void onError(String error)
            {
                if(pDialog.isShowing())
                {
                    pDialog.dismiss();
                }
                Toast.makeText(Login_Page.this, error, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * <h2>Signinservice</h2>
     * <p>Here i am calling the Server SlaveLogin Api using OkHttp3Request
     * and handelling the responce of the Server API </p>
     * @see OkHttp3Request
     * @param isFbClikd if the Facebook Icon is clicked then the ent_password is facebook id,and ent_email is fb_emailId
     *                  if Facebook Icon is not clicked then the ent_password is user password,and ent_email is User EmailID
     */
    private void Signinservice(final boolean isFbClikd)
    {
        pDialog.setMessage(getString(R.string.logind));
        pDialog.show();
        String email,password;
        if(isFbClikd)
        {
            email = fb_emailId;
            password = fbid;
        }
        else
        {
            email = emailedittext.getText().toString().trim();
            password = passwordedittext.getText().toString().trim();
        }
        FormBody requestform = new FormBody.Builder()
                .add("ent_email",email)
                .add("ent_password",password)
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_push_token",manager.getRegistrationId())
                .add("ent_app_version", BuildConfig.VERSION_NAME)
                .add("ent_dev_os", Build.VERSION.RELEASE)
                .add("ent_dev_model", Build.MODEL)
                .add("ent_manf", Build.BRAND)
                .add("ent_device_type","2")
                .add("ent_signup_type",ent_signup_type)
                .add("ent_date_time",Utility.dateintwtfour())
                .build();

        okio.Buffer sink = new okio.Buffer();
        try {
            requestform.writeTo(sink);
            byte[] barry =sink.readByteArray();
            String req = new String(barry);
            Log.i("loging","request "+req);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3Request.doOkHttp3Request(Variableconstant.SIGNIN_URL, requestform, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {

                signinsucces(result,isFbClikd);
                Log.i("sinin","success "+result);
            }
            @Override
            public void onError(String error)
            {
                pDialog.dismiss();
                Utility.printLog("HomeFrag JSON DATA Error" + error);

            }
        });

    }

    /**
     * <h2>signinsucces</h2>
     * <p>getting the responce of the Server SlaveLogin Api</p>
     * @param result slaveLogin Responce in the Form of String
     * @param isFbClikd Facebook is clicked and user is not Logged in then redirecting to
     *                  @see Register_Page
     */
    private void signinsucces(String result, boolean isFbClikd)
    {
        pDialog.dismiss();

        loginpojo = gson.fromJson(result,LoginActivity_pojo.class);

        if(loginpojo.getErrFlag().equals("0"))
        {

            manager.setIsLogin(true);
            manager.setSession(loginpojo.getToken());
            manager.SetChannel(loginpojo.getChn());
            manager.storecoustomerEmail(loginpojo.getEmail());
            manager.setProfilePic(loginpojo.getProfilePic());
            manager.setCustomerId(loginpojo.getCustId());
            manager.setCoupon(loginpojo.getCoupon());
            manager.setCustomerFnme(loginpojo.getFname());
            manager.setCustomerLnme(loginpojo.getLname());
            manager.setOldpassword(passwordedittext.getText().toString());
            manager.getSession();
            manager.setStripeKey(loginpojo.getStripe_pub_key());
            lantype_pojo.setLanguages(loginpojo.getLanguages());
            String languagetype = gson.toJson(lantype_pojo,Language_Type.class);
            manager.setLanguage(languagetype);
            Intent intent = new Intent(Login_Page.this, MenuActivity.class);
            startActivity(intent);
            finish();
        }
        else
        {
            if(isFbClikd && loginpojo.getErrNum().equals("120"))
            {
                Intent intent = new Intent(this, Register_Page.class);
                intent.putExtra("fbEmail", fb_emailId);
                intent.putExtra("fb_frstname",Fb_firstName);
                intent.putExtra("fb_lastname",fb_lastName);
                intent.putExtra("fb_pic",fb_pic);
                intent.putExtra("fb_id",fbid);
                startActivity(intent);
                finish();
            }
            Toast.makeText(this,loginpojo.getErrMsg(),Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * <h2>validateemailphone</h2>
     * <p>validate email or mobile number foramte</p>
     * @return true if vali
     */

    private boolean validateemailphone()
    {
        boolean emailphoneflag = false;
        if(!emailedittext.getText().toString().trim().equals("")) {

            if (Character.isDigit(emailedittext.getText().toString().charAt(0))) {
                if (!validator.contac_Status(emailedittext.getText().toString().trim())) {
                    emtxtlayout.setError(getResources().getString(R.string.mobile_invalid));
                    emtxtlayout.setErrorEnabled(true);
                    emailphoneflag = false;
                } else {
                    emtxtlayout.setError(null);
                    emtxtlayout.setErrorEnabled(false);
                    emailphoneflag = true;
                }
            } else if (Character.isLetter(emailedittext.getText().toString().charAt(0))) {

                if (!validator.emailId_status(emailedittext.getText().toString().trim())) {
                    emtxtlayout.setError(getResources().getString(R.string.email_invalid));
                    emtxtlayout.setErrorEnabled(true);
                    emailphoneflag = false;

                } else {
                    emtxtlayout.setError(null);
                    emtxtlayout.setErrorEnabled(false);
                    emailphoneflag = true;
                }
            }
        }
        else{

            emtxtlayout.setError(getResources().getString(R.string.enteremailorphone));
            emtxtlayout.setErrorEnabled(true);
            emailphoneflag = false;
        }
        return emailphoneflag;
    }


}
