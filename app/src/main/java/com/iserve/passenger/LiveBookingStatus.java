package com.iserve.passenger;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adapter.CancelAdapter;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.pojo.Cancellation_java;
import com.pojo.CancelresponceData;
import com.pojo.LiveBookinResponce;
import com.pojo.PubNub_pojo;
import com.pojo.TimeForNearestPojo;
import com.squareup.picasso.Picasso;
import com.utility.Alerts;
import com.utility.AppPermissionsRunTime;
import com.utility.CircleTransform;
import com.utility.MyNetworkChangeListner;
import com.utility.NetworkChangeReceiver;
import com.utility.NotificationUtils;
import com.utility.OkHttp3Request;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.FormBody;
/**
 * Created by embed on 6/8/16.
 *
 */
public class LiveBookingStatus extends AppCompatActivity implements View.OnClickListener{//, DeviceListener, ConnectionListener
    SupportMapFragment mGoogleMap;
    CoordinatorLayout coordinatorLayout;
    SessionManager manager;
    Gson gson;
    LiveBookinResponce liveBookinResponce;
    EditText text;
    CancelAdapter cancelAdapter;
    JSONObject jsonObject;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 130;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> mPermissionConstantsArrayList;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList;
    Marker proMarker, ownMArker;
    GoogleMap mMap = null;
    RelativeLayout rrmain, rrmap, iv_staus;
    String apptlat, apptlng, bid,  status, pid, email;
    private String statcode;
    long timecunsumdsecond = 0;
    Timer jobTimr;
    TimerTask task;
    Handler mHandler;
    TextView tvonestatus, tvtwostatus, tvthreestatus, tvfourstatus, tvjobtimer, tv_provider_name, tv_provider_profile,
            btcall, toolbarhometxv, tveditbarhometxv, btmesg, tveta, tvmin, tvproviderjob;
    ImageView ivproviderimage,ivdrivermarker;
    ListView listcancel;
    Toolbar toolbar;
    Dialog indialog;
    FrameLayout fframelayt;
    RatingBar invoice_driver_rating;
    ProgressDialog pDialog;
    String TAG = "LiveBookingStatus";
    private CountDownTimer timerforrequest = null;
    private boolean timerreqflagstop = false;
    private boolean firsttimeETAgot = true;
    private boolean isOpenLivebooking = true;
    private double apptdislat,apptdislng;
    private   LatLng prolatLng;
    private   LatLng ownlatLng;
    private  CancelresponceData candata;
    private  String JobLAt = "";
    private String JobLNG = "";
    private boolean callbuttn = true;
    private String phoneNo = "";
    boolean timerflag,timerflagatstart;
    private boolean singlecall = true;
    private String etatime = "";
    private String propic = "";
    private Typeface regulrtxt;
    public int selectedpostion = -1;
    private static final String KILOMETER = "Kilometers";
    private static final String METER = "meters";
    private static final String NAUTICAL_MILES = "nauticalMiles";
    private static final double LN2 = 0.6931471805599453;
    private static final int ZOOM_MAX = 21;
    private Location mCurrentLoc,mPreviousLoc;
    Utility utility;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.livebookingstatus);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        isOpenLivebooking = true;
        Variableconstant.isLIVBOKINOPN = true;
        mStartSocketListner();
        NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();
        networkChangeReceiver.setMyNetworkChangeListner(new MyNetworkChangeListner() {
            @Override
            public void onNetworkStateChanges(boolean nwStatus) {
                Utility.printLog("PK Network Status MainActvty "+nwStatus);
                if(nwStatus)
                {
                    if(isOpenLivebooking)
                    {
                        emitHeartbeat("1");
                        callserviceapptdetls();
                    }
                }
            }
        });
        manager = new SessionManager(this);
        if (getIntent().getExtras() != null) {
            bid = getIntent().getStringExtra("bid");
            statcode = getIntent().getStringExtra("statcode");
            if(statcode.equals("5"))
            {
                apptlat = getIntent().getStringExtra("apptlat");
                apptlng = getIntent().getStringExtra("apptlng");
                manager.setJOBLATI(apptlat);
                manager.setJOBLONGI(apptlng);
            }
            status = getIntent().getStringExtra("status");
            Log.d(TAG, "statecode" + statcode);
        }
        Log.d(TAG,"APPTLATLONG "+apptlat+" longi "+apptlng);
        initialize();
    }
    private void initialize() {

         jsonObject= new JSONObject();
        regulrtxt = Typeface.createFromAsset(getAssets(), Variableconstant.regularfont);
        Typeface litetxt = Typeface.createFromAsset(getAssets(), Variableconstant.lightfont);
        pDialog = new ProgressDialog(this);
        gson = new Gson();
        utility = new Utility();
        mHandler = new Handler();
        jobTimr = new Timer();
        pDialog.setMessage(getResources().getString(R.string.wait));
        pDialog.setCancelable(false);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        mGoogleMap = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mGoogleMap);
        ivdrivermarker = (ImageView) findViewById(R.id.ivdrivermarker);
        tveta = (TextView) findViewById(R.id.tveta);
        tvmin = (TextView) findViewById(R.id.tvmin);
        rrmain = (RelativeLayout) findViewById(R.id.rrmain);
        iv_staus = (RelativeLayout) findViewById(R.id.iv_staus);
        tvonestatus = (TextView) findViewById(R.id.tvonestatus);
        tvtwostatus = (TextView) findViewById(R.id.tvtwostatus);
        tvthreestatus = (TextView) findViewById(R.id.tvthreestatus);
        tvfourstatus = (TextView) findViewById(R.id.tvfourstatus);
        tvjobtimer = (TextView) findViewById(R.id.tvjobtimer);
        ivproviderimage = (ImageView) findViewById(R.id.ivproviderimage);
        tv_provider_name = (TextView) findViewById(R.id.tv_provider_name);
        invoice_driver_rating = (RatingBar) findViewById(R.id.invoice_driver_rating);
        tv_provider_profile = (TextView) findViewById(R.id.tv_provider_profile);
        rrmap = (RelativeLayout) findViewById(R.id.rrmap);
        btmesg = (TextView) findViewById(R.id.btmesg);
        btcall = (TextView) findViewById(R.id.btcall);
        tvproviderjob = (TextView) findViewById(R.id.tvproviderjob);
        if(statcode.equals("2"))
        {
            tvproviderjob.setText(getResources().getString(R.string.providerbeonthaway));
        }
        btmesg.setOnClickListener(this);
        btcall.setOnClickListener(this);
        tv_provider_profile.setOnClickListener(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tveditbarhometxv = (TextView) findViewById(R.id.tveditbarhometxv);
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        fframelayt.setVisibility(View.VISIBLE);
        toolbarhometxv = (TextView) findViewById(R.id.toolbarhometxv);
        tveditbarhometxv.setText(getResources().getString(R.string.cancel));
        tveditbarhometxv.setTextColor(Color.BLACK);
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        String bookingid = getResources().getString(R.string.bookingid)+" "+bid;
        toolbarhometxv.setText(bookingid);
        toolbarhometxv.setVisibility(View.VISIBLE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        callserviceapptdetls();
        typeface();
        canceloncleick();
    }

    private void typeface()
    {
        tvonestatus.setTypeface(regulrtxt);
        tvproviderjob.setTypeface(regulrtxt);
        tvjobtimer.setTypeface(regulrtxt);
        tvfourstatus.setTypeface(regulrtxt);
        tvtwostatus.setTypeface(regulrtxt);
        tvonestatus.setTypeface(regulrtxt);
        tvthreestatus.setTypeface(regulrtxt);
        tv_provider_name.setTypeface(regulrtxt);
        tv_provider_profile.setTypeface(regulrtxt);
        btcall.setTypeface(regulrtxt);
        btmesg.setTypeface(regulrtxt);
        tveditbarhometxv.setTypeface(regulrtxt);
        toolbarhometxv.setTypeface(regulrtxt);
    }

    private void canceloncleick() {
        tveditbarhometxv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(Utility.isNetworkAvailable(LiveBookingStatus.this))
                {
                    callCancelBookingReason();
                }
                else
                {
                    Toast.makeText(LiveBookingStatus.this,getResources().getString(R.string.network_alert_message),Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
    private void callCancelBookingReason()
    {

        pDialog.setMessage(getResources().getString(R.string.Loading));
        pDialog.show();
        try {
            jsonObject.put("ent_lan",Variableconstant.selectedlanid+"");
            jsonObject.put("ent_user_type","2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utility.doJsonRequest(Variableconstant.service_url + "getCancellationReson", jsonObject, new Utility.JsonRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                pDialog.dismiss();
                Log.d(TAG,"CANCELRESPONCE "+result);
                calCancelResponce(result);

            }
            @Override
            public void onError(String error) {
                pDialog.dismiss();
            }
        });
    }
    private void calCancelResponce(String result)
    {
        if(result!=null)
        {
             candata = gson.fromJson(result,CancelresponceData.class);
            if(candata.getErrFlag().equals("0"))
            {
                selectedpostion = -1;
                final String[] reasontext = new String[1];
                indialog = new Dialog(LiveBookingStatus.this);
                indialog.setCanceledOnTouchOutside(true);
                indialog.setCancelable(true);
                indialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                indialog.setContentView(R.layout.cancel_dialog);
                indialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                indialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                listcancel = (ListView) indialog.findViewById(R.id.listcancel);
                TextView tvreason = (TextView) indialog.findViewById(R.id.tvreason);
                TextView tvletusknw = (TextView) indialog.findViewById(R.id.tvletusknw);
                TextView submitcancel = (TextView) indialog.findViewById(R.id.submitcancel);
                tvletusknw.setTypeface(regulrtxt);
                tvreason.setTypeface(regulrtxt);
                submitcancel.setTypeface(regulrtxt);
                cancelAdapter = new CancelAdapter(LiveBookingStatus.this,candata.getGetdata());
                listcancel.setAdapter(cancelAdapter);
                listcancel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selectedpostion = position;
                        cancelAdapter.notifyDataSetChanged();
                        reasontext[0] = candata.getGetdata().get(position);
                    }
                });
                submitcancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        if(reasontext[0]!=null)
                        {
                            cancelapptservice(reasontext[0]);
                            indialog.dismiss();
                        }
                        else
                        {
                            Toast.makeText(LiveBookingStatus.this, "Please Provide Valid Reason", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                indialog.show();
            }
        }
    }
    private void cancelapptservice(final String reason) {
        if (Utility.isNetworkAvailable(this)) {
            pDialog.show();
            FormBody requestBody=new FormBody.Builder()
                    .add("ent_sess_token", manager.getSession())
                    .add("ent_dev_id", manager.getDevice_Id())
                    .add("ent_bid", bid)
                    .add("ent_reason", reason)
                    .add("ent_date_time", Utility.dateintwtfour())
                    .build();
            okio.Buffer sink = new okio.Buffer();
            try {
                requestBody.writeTo(sink);
                byte[] barry = sink.readByteArray();
                String req = new String(barry);
                Log.d(TAG, "requestcancel " + req);
            } catch (Exception e) {
                e.printStackTrace();
            }
            OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "cancelAppointment", requestBody, new OkHttp3Request.JsonRequestCallback() {
                @Override
                public void onSuccess(String result) {
                    pDialog.dismiss();
                    cancelResponce(result,reason);
                    Log.d(TAG, "ALIresponcecancel " + result);
                }
                @Override
                public void onError(String error) {
                    pDialog.dismiss();
                }
            });

        } else {

            Toast.makeText(this,getResources().getString(R.string.nointernet),Toast.LENGTH_SHORT).show();
        }
    }
    private void cancelResponce(String result, String reason) {

        Cancellation_java cancellationJava = gson.fromJson(result, Cancellation_java.class);
        if (cancellationJava.getErrFlag().equals("0"))
        {
            sendcancelresons(bid,Utility.dateintwtfour(),reason);
            AlertDialog.Builder builder = new AlertDialog.Builder(LiveBookingStatus.this);
            builder.setTitle(getResources().getString(R.string.cancel));
            builder.setCancelable(false);
            builder.setMessage(cancellationJava.getErrMsg());
            builder.setPositiveButton(this.getResources().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(LiveBookingStatus.this, MenuActivity.class);
                    manager.setProAccept(false);
                    manager.setProOntheWay(false);
                    startActivity(intent);
                    finish();
                    dialog.dismiss();
                }
            });
            builder.show();
        }
    }
    private void sendcancelresons(String bid, String dateintwtfour, String reason) {
        try
        {

            jsonObject.put("cid", Integer.parseInt(manager.getCustomerId()));
            jsonObject.put("bid", bid);
            jsonObject.put("dt",dateintwtfour);
            jsonObject.put("can_reason",reason);
            Log.d(TAG, "CancelbookingeObj: " + jsonObject);
            ApplicationController.getInstance().emit("cancelBooking",jsonObject);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
    public void timerViewmethod(String s)
    {
        switch (s)
        {
            case "2":
                tveditbarhometxv.setVisibility(View.VISIBLE);
                toolbarhometxv.setVisibility(View.VISIBLE);
                fframelayt.setVisibility(View.VISIBLE);
                canceloncleick();
                break;
            case "5":
                mapmethod();
                tveditbarhometxv.setVisibility(View.VISIBLE);
                rrmap.setVisibility(View.VISIBLE);
                rrmain.setVisibility(View.GONE);
                tvproviderjob.setText("");
                break;
            case "6":
                tvtwostatus.setBackgroundResource(R.drawable.circular_blueshap);
                tvtwostatus.setTextColor(Color.WHITE);
                tvthreestatus.setBackgroundResource(R.drawable.circular_blueshap);
                tvthreestatus.setTextColor(Color.WHITE);
                tvfourstatus.setBackgroundResource(R.drawable.circular_blueshap);
                iv_staus.setBackgroundResource(R.drawable.status_booking_fourth_circle);
                tvfourstatus.setTextColor(Color.WHITE);
                rrmap.setVisibility(View.GONE);
                rrmain.setVisibility(View.VISIBLE);
                tveditbarhometxv.setVisibility(View.GONE);
                break;
            case "7":
                tvtwostatus.setBackgroundResource(R.drawable.circular_blueshap);
                tvtwostatus.setTextColor(Color.WHITE);
                tvthreestatus.setBackgroundResource(R.drawable.circular_blueshap);
                tvthreestatus.setTextColor(Color.WHITE);
                tvfourstatus.setBackgroundResource(R.drawable.circular_blueshap);
                iv_staus.setBackgroundResource(R.drawable.status_booking_fifth_circle);
                tvfourstatus.setTextColor(Color.WHITE);
                tveditbarhometxv.setVisibility(View.GONE);
                new DialogActivityEx(LiveBookingStatus.this,bid,pDialog,"NotBokinFrag");
                break;
            case "15":
               break;
            case "16":
                break;
            case "21":
                tvtwostatus.setBackgroundResource(R.drawable.circular_blueshap);
                tvtwostatus.setTextColor(Color.WHITE);
                tvthreestatus.setBackgroundResource(R.drawable.circular_blueshap);
                tvthreestatus.setTextColor(Color.WHITE);
                iv_staus.setBackgroundResource(R.drawable.status_booking_third_circle);
                tvjobtimer.setText(getResources().getString(R.string.providerarived));
                tveditbarhometxv.setVisibility(View.VISIBLE);
                fframelayt.setVisibility(View.VISIBLE);
                toolbarhometxv.setVisibility(View.VISIBLE);
                tvproviderjob.setText("");
                canceloncleick();
                rrmap.setVisibility(View.GONE);
                rrmain.setVisibility(View.VISIBLE);
                break;
            case "22":
                tvtwostatus.setBackgroundResource(R.drawable.circular_blueshap);
                tvtwostatus.setTextColor(Color.WHITE);
                tvthreestatus.setBackgroundResource(R.drawable.circular_blueshap);
                tvthreestatus.setTextColor(Color.WHITE);
                tvfourstatus.setBackgroundResource(R.drawable.circular_blueshap);
                iv_staus.setBackgroundResource(R.drawable.status_booking_fifth_circle);
                tvfourstatus.setTextColor(Color.WHITE);
                tvproviderjob.setText(getResources().getString(R.string.jobcompleted));
                tvjobtimer.setText(getResources().getString(R.string.invoicepinding));
                jobTimr.cancel();
                jobTimr.purge();
                break;
        }
    }
    private void callserviceapptdetls() {
        if (Utility.isNetworkAvailable(this))
        {
            if(pDialog!=null)
            {
                pDialog.show();
            }
            FormBody requestform = new FormBody.Builder()
                    .add("ent_sess_token", manager.getSession())
                    .add("ent_dev_id", manager.getDevice_Id())
                    .add("ent_bid", bid)
                    .add("ent_inv", "0")
                    .add("ent_date_time", Utility.dateintwtfour())
                    .build();
            okio.Buffer sink = new okio.Buffer();
            try {
                requestform.writeTo(sink);
                byte[] barry = sink.readByteArray();
                String req = new String(barry);
                Log.d(TAG, "request " + req);
            } catch (Exception e) {
                e.printStackTrace();
            }
            OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "getAppointmentDetails", requestform, new OkHttp3Request.JsonRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(pDialog!=null)
                    {
                        pDialog.dismiss();
                    }
                    apptdetlsResponce(result);
                    Log.d(TAG, "ALIresponceliveget " + result);
                }
                @Override
                public void onError(String error) {
                    if(pDialog!=null)
                    {
                        pDialog.dismiss();
                    }
                }
            });
        } else {

            Toast.makeText(this,getResources().getString(R.string.nointernet),Toast.LENGTH_SHORT).show();
        }
    }
    private void apptdetlsResponce(String result)
    {
        liveBookinResponce = gson.fromJson(result, LiveBookinResponce.class);
        if (liveBookinResponce.getErrNum().equals("21") && liveBookinResponce.getErrFlag().equals("0")) {

            statcode = liveBookinResponce.getStatus();
            apptlat = liveBookinResponce.getApptLat();
            apptlng = liveBookinResponce.getApptLong();
            pid = liveBookinResponce.getPid();
            propic = liveBookinResponce.getpPic();
            timerViewmethod(statcode);
            Picasso.with(this).load(propic)
                    .centerCrop().transform(new CircleTransform())
                    .resize(ivproviderimage.getWidth(), ivproviderimage.getHeight())
                    .placeholder(R.drawable.register_profile_default_image)
                    .into(ivproviderimage);
            String name = liveBookinResponce.getfName() + " " + liveBookinResponce.getlName();
            tv_provider_name.setText(name);
            float rating = Float.parseFloat(liveBookinResponce.getRating());
            invoice_driver_rating.setRating(rating);
            invoice_driver_rating.setIsIndicator(true);
            email = liveBookinResponce.getEmail();
            phoneNo = liveBookinResponce.getMobile();
            if (statcode.equals("5")) {
                sendMessageForLiveBooking(pid, apptlat, apptlng);
            }
            if(!statcode.equals("7"))
            {
                if (liveBookinResponce.getTimer_status().equals("1"))
                {
                    if(singlecall)
                    {

                        singlecall = false;
                        tvproviderjob.setText(getResources().getString(R.string.timer_started));
                        jobTimr.cancel();
                        jobTimr.purge();
                        jobtimercall(liveBookinResponce);
                    }

                }
                else if(liveBookinResponce.getTimer_status().equals("2"))
                {
                    singlecall = true;
                    tvproviderjob.setText(getResources().getString(R.string.timer_paused));
                    if(!liveBookinResponce.getTimer().equals(""))
                    {
                        if(jobTimr !=null)
                        {
                            jobTimr.cancel();
                            jobTimr.purge();
                        }
                        tvjobtimer.setText(convertToRequiredFormat(liveBookinResponce.getTimer()));
                        jobTimr.cancel();
                        jobTimr.purge();
                        calltimervalue();
                        timerflag = true;
                    }
                    else
                    {
                        if(jobTimr !=null)
                        {
                            jobTimr.cancel();
                            jobTimr.purge();
                            calltimervalue();
                        }
                        else
                        {
                            calltimervalue();
                        }
                    }
                }
            }
        }
    }
    private void calltimervalue()
    {
        if(!tvjobtimer.getText().toString().equals("Provider Arrived")&& !tvjobtimer.getText().toString().equals("Provider")
                &&!tvjobtimer.getText().toString().equals("Invoice Raised") &&!tvjobtimer.getText().toString().equals(""))
        {
            String split[]= tvjobtimer.getText().toString().split(":");
            String newValue= Utility.dateintwtfour().split(" ")[0]+" "+split[0].trim()+":"+split[1].trim()+":"+split[2].trim();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss", Locale.getDefault());
            try {
                Date date = sdf.parse(newValue);
                manager.setSavedTime(date.getTime());

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        singlecall = true;
        timerreqflagstop = false;
        firsttimeETAgot = true;
        isOpenLivebooking = true;
        Variableconstant.isLIVBOKINOPN = true;
        if(Utility.isNetworkAvailable(this))
        {
            emitHeartbeat("1");
        }
        else
        {
            Alerts alerts  = new Alerts(this);
            alerts.showNetworkAlert(this);
        }
    }
    public void emitHeartbeat(String status) {
        jsonObject = new JSONObject();
        try {
            jsonObject.put("status", status);
            jsonObject.put("cid", manager.getCustomerId());
            ApplicationController.getInstance().emit("CustomerStatus", jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void cancelrequest(String alert)
    {

        showcancelpopup(alert);
    }
    private void Startpublishwithtimer(final String pid)
    {
        runOnUiThread(new TimerTask() {
            @Override
            public void run() {
                timerforrequest = new CountDownTimer(4000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished)
                    {
                        if(timerreqflagstop)
                        {
                            timerforrequest.cancel();
                        }

                    }

                    @Override
                    public void onFinish()
                    {

                        sendMessageForLiveBooking(pid, apptlat, apptlng);

                    }
                }.start();
            }
        });
    }

    /**
     * <h2>sendMessageForLiveBooking</h2>
     * <p>
     *     sendig request to socket on UpdateCustomer channel
     * @param pid provider Id
     * @param apptlat appointment latitude
     * @param apptlng appointment longitude
     * </p>
     */
    private void sendMessageForLiveBooking(String pid, String apptlat, String apptlng) {

        try {
            jsonObject.put("btype", "3");
            jsonObject.put("pid", pid);
            jsonObject.put("email", manager.getCoustomerEmail());
            jsonObject.put("lat", apptlat);
            jsonObject.put("long", apptlng);
            ApplicationController.getInstance().emit("UpdateCustomer", jsonObject);

        } catch (JSONException e)
        {
            e.printStackTrace();

        }
    }
    private void jobtimercall(LiveBookinResponce responce) {
        if (responce.getTimer().equals("0"))
        {
            /*timerflagatstart = false;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date date;
            try {

                date = sdf.parse(responce.getTimer_start_time());
                long finaltime = (System.currentTimeMillis() - date.getTime());
                if(finaltime<0)
                {
                    finaltime = 0;
                    finaltime = finaltime+1000;
                    jobTimer(tvjobtimer, finaltime);
                }
                else
                {
                    jobTimer(tvjobtimer, finaltime);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }*/
            timerflagatstart = false;
            setTimerText(responce);

        } else {
           /* calstatimandnotzero(responce);
            timerflagatstart = true;*/
            setTimerText(liveBookinResponce);
            timerflagatstart = true;
        }
    }
    private void calstatimandnotzero(LiveBookinResponce responce)
    {
        timerflagatstart = true;
        String dateInStrin =  convertToRequiredFormat(responce.getTimer());
        String dateInString = Utility.dateintwtfour().split(" ")[0]+" "+dateInStrin;
        Date date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD hh:mm:ss", Locale.getDefault());
        try {
            Date range2 = sdf.parse("0000-00-00 00:00:00");
            if(manager.getSavedTime()==0)
            {
                date = sdf.parse(dateInString);
                long finaltime = (date.getTime() - range2.getTime());
                jobTimer(tvjobtimer, finaltime);
            }
            else
            {
                if(timerflag)
                {
                    timerflag = false;
                    SimpleDateFormat formatt = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss", Locale.getDefault());
                    Date date2 = formatt.parse(responce.getTimer_start_time());
                    long difftime = (System.currentTimeMillis() - date2.getTime());
                    long fultim = difftime + manager.getSavedTime();
                    Date date1 = new Date(fultim);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss", Locale.getDefault());
                    String formatted = formatter.format(date1);
                    date = sdf.parse(formatted);
                    long finaltime = (date.getTime() - range2.getTime());
                    jobTimer(tvjobtimer, finaltime);

                }
                else
                {
                    long fulltime  = System.currentTimeMillis()-manager.getSavedSystemTime();
                    long fultim = fulltime + manager.getSavedTime();
                    Date date1 = new Date(fultim);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss", Locale.getDefault());
                    String formatted = formatter.format(date1);
                    date = sdf.parse(formatted);
                    long finaltime = (date.getTime() - range2.getTime());
                    jobTimer(tvjobtimer, finaltime);
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();

        }
    }
    private void jobTimer(final TextView tvjobtimer, long timecunsumedsecond)
    {
        timecunsumdsecond = timecunsumedsecond;
        jobTimr = new Timer();
        task = new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                      /*//  timecunsumdsecond = timecunsumdsecond + 1;
                        tvjobtimer.setText(getDurationString(timecunsumdsecond));
                        if (timecunsumdsecond > 0)
                            timecunsumdsecond += 1000;
                        else {
                            tvjobtimer.setText(getResources().getString(R.string.provider));
                        }*/
                        timecunsumdsecond = timecunsumdsecond + 1;
                        tvjobtimer.setText(formatSeconds(timecunsumdsecond));
                        if (timecunsumdsecond > 0)
                        {

                        }
                        //  timecunsumdsecond += 1000;
                        else {
                            tvjobtimer.setText(getResources().getString(R.string.provider));
                        }
                    }
                });
            }
        };
        jobTimr.scheduleAtFixedRate(task, 0, 1000);
    }
    private String getDurationString(long milliseconds) {
        long second = (milliseconds / 1000) % 60;
        long minutes = (milliseconds / (1000 * 60)) % 60;
        long hours = (milliseconds / (1000 * 60 * 60)) % 24;
        return twoDigitString(hours) + " : " + twoDigitString(minutes) + " : " + twoDigitString(second);
    }
    private String twoDigitString(long number) {

        if (number == 0) {
            return "00";
        }
        if (number / 10 == 0) {
            return "0" + number;
        }
        return String.valueOf(number);
    }
    private String convertToRequiredFormat(String dateString)
    {

        String dateInString = dateString;
        String timehr;
        int timehour = Integer.parseInt(dateInString) / 3600;
        if (timehour > 10) {
            timehr = String.valueOf(timehour);
        } else {
            timehr = "0" + String.valueOf(timehour);
        }
        String timemn;
        int timemin = (Integer.parseInt(dateInString)%3600) / 60;

        if(timemin==60)
        {
            timemn = "00";
        }
        else if(timemin>60)
        {
            timemin = timemin-60;
            if (timemin > 10) {
                timemn = String.valueOf(timemin);
            } else {
                timemn = "0" + String.valueOf(timemin);
            }
        }
        else if(timemin<60 && timemin>=10)
        {
            timemn = String.valueOf(timemin);
        }
        else
        {
            timemn = "0" + String.valueOf(timemin);
        }
        String timesec;
        int timsec = (Integer.parseInt(dateInString)%3600 )% 60;
        if(timsec<10)
        {
            timesec = "0" + String.valueOf(timsec);
        }
        else
        {
            timesec = String.valueOf(timsec);
        }
        dateInString = (timehr + ":" + timemn + ":" + timesec);
        return dateInString;
    }
    private void mapmethod() {
        if (mGoogleMap != null) {
            mGoogleMap.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    loadMap(googleMap);
                    mMap = googleMap;
                    if(!manager.getPUSHMSGFORMAP().equals(""))
                    {
                        Log.i(TAG,"TAGISASMES "+manager.getPUSHMSGFORMAP()+" code "+manager.getPUSHACTIONMAP()+" bid "+
                                manager.getPUSHBIDMAP()+" prolat "+manager.getPUSHLATLNGMAP());
                        Intent pushNotification = new Intent(getApplicationContext(), Notification_Handler.class);
                        pushNotification.putExtra("message", manager.getPUSHMSGFORMAP());
                        pushNotification.putExtra("statcode", manager.getPUSHACTIONMAP());
                        pushNotification.putExtra("bid", manager.getPUSHBIDMAP());
                        pushNotification.putExtra("prolat", manager.getPUSHLATLNGMAP());
                        pushNotification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(pushNotification);
                        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                        notificationUtils.playNotificationSound();
                        manager.setPUSHMSGFORMAP("");
                    }

                }
            });
        } else {
            Toast.makeText(this, getResources().getString(R.string.couldnotloadmap), Toast.LENGTH_SHORT).show();
        }
    }
    private void loadMap(GoogleMap mgoogleMap) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE);
            return;
        }

        mgoogleMap.setMyLocationEnabled(false);
        double curlatlng[] = new double[2];
        curlatlng[0] = Double.parseDouble(apptlat);
        curlatlng[1] = Double.parseDouble(apptlng);
        CameraUpdate center= CameraUpdateFactory.newLatLng(new LatLng(curlatlng[0], curlatlng[1]));
        CameraUpdate zoom= CameraUpdateFactory.zoomTo(11.0f);
        mgoogleMap.moveCamera(center);
        mgoogleMap.animateCamera(zoom);

    }
    private void markingDeliverers()//String pid, String email,
    {
        final Double jobtalitude = Double.parseDouble(JobLAt);
        final Double joblongitute = Double.parseDouble(JobLNG);
        prolatLng = new LatLng(jobtalitude, joblongitute);
        ownlatLng = new LatLng(Double.parseDouble(apptlat), Double.parseDouble(apptlng));
        if (mMap != null)
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                       /* mMap.clear();
                        int width = getResources().getDisplayMetrics().widthPixels;
                        int height = getResources().getDisplayMetrics().heightPixels;*/
                    if(proMarker!=null)
                    {
                        proMarker.setPosition(prolatLng);
                    }
                    else {
                        try {
                            BitmapCustomMarker bitmapCustomMarker = new BitmapCustomMarker(LiveBookingStatus.this, etatime);
                            proMarker = mMap.addMarker(new MarkerOptions()
                                    .position(prolatLng)
                                    .icon(BitmapDescriptorFactory.fromBitmap(bitmapCustomMarker.createBitmap()))
                            );
                            ownMArker = mMap.addMarker(new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.add_new_address_red_pin_icon))
                                    .position(ownlatLng));
                        } catch (IllegalArgumentException e) {

                            e.printStackTrace();
                        }
                        updatebound(prolatLng,ownlatLng,mMap);
                    }
                  //  UpdateDriverLocation_DriverOnTheWay(JobLAt,JobLNG);
                }
            });
        }
        if(firsttimeETAgot)
        {
            String[] params=new String[4];
            apptdislat = jobtalitude;
            apptdislng = joblongitute;
            if(jobtalitude!=0 && joblongitute!=0)
            {
                params[0]=String.valueOf(apptlat);
                params[1]=String.valueOf(apptlng);
                params[2]=JobLAt;
                params[3]=JobLNG;
                new getETA().execute(params);
            }
            firsttimeETAgot = false;
        }
        else if(distance(apptdislat,apptdislng,jobtalitude,joblongitute,METER)>100)
        {
            apptdislat = jobtalitude;
            apptdislng = joblongitute;
            String[] params=new String[4];
            if(jobtalitude!=0 && joblongitute!=0)
            {
                params[0]=String.valueOf(apptlat);
                params[1]=String.valueOf(apptlng);
                params[2]=JobLAt;
                params[3]=JobLNG;
                new getETA().execute(params);
            }
        }
    }

    private void updatebound(LatLng prolatLng, LatLng ownlatLng, GoogleMap mMap)
    {
        final LatLngBounds bounds = new LatLngBounds.Builder()
                .include(prolatLng)
                .include(ownlatLng)
                .build();

        this.mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LiveBookingStatus.this.mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,200));
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btcall:
                callbuttn = true;
                callmsgmethod(callbuttn);
                break;
            case R.id.btmesg:
                //callbuttn = false;
                // callmethod();
              //  callmsgmethod(callbuttn);
                Intent intent = new Intent(LiveBookingStatus.this,ChattingActivity.class);
                intent.putExtra("bid",bid);
                intent.putExtra("proimg",propic);
                intent.putExtra("providername",tv_provider_name.getText().toString());
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
                startActivity(intent);
                break;
            case R.id.tv_provider_profile:
                Variableconstant.showProfile = true;
                Variableconstant.comingfromlaterbooking = false;
                Intent intnent = new Intent(this, MasterDetails.class);
                intnent.putExtra("PID", pid);
                startActivity(intnent);
                break;
        }
    }
    private void callmsgmethod(boolean callmsg) {
        if (callbuttn) {
            if (!phoneNo.equals("")) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + phoneNo));
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
            else
            {
                Toast.makeText(this, getResources().getString(R.string.driver_phone_num_not_available), Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Uri smsToUri = Uri.parse("smsto:" +phoneNo);
            Intent intent = new Intent(
                    Intent.ACTION_SENDTO, smsToUri);
            intent.putExtra("sms_body", "");
            startActivity(intent);
        }
    }
    /**
     * <h2>onRequestPermissionsResult</h2>
     * predefined method to check run time permissions list call back
     * @param requestCode this is the code
     * @param permissions: contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        boolean isDenine = false;
        switch (requestCode)
        {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                int i = 0;
                for (; i < grantResults.length; i++)
                {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                    {
                        isDenine = true;
                        break;
                    }
                }
                if (isDenine)
                {
                    //TODO: if permissions denied by user, recall it

                    AppPermissionsRunTime.checkPermission(this, mPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE);
                }
                else
                {

                    callmsgmethod(callbuttn);
                    //TODO: if permissions granted by user, move forward
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(LiveBookingStatus.this, MenuActivity.class);
        Variableconstant.comgfrmConfirmscrn = true;
        startActivity(intent);
        finish();
        if(timerforrequest!=null)
        {
            timerreqflagstop = true;
            timerforrequest.cancel();
        }
    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        isOpenLivebooking = false;
        Variableconstant.isLIVBOKINOPN = false;
        callpausedestroytimer();
    }
    private class  getETA extends AsyncTask<String, Void, String>
    {
        TimeForNearestPojo timeresponse;
        @Override
        protected String doInBackground(String... params) {

            String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+params[0]+","+params[1]+"&destinations="+params[2]+","+params[3];
            String stringResponse= Utility.callhttpRequest(url);
            if(stringResponse!=null)
            {
                Gson gson=new Gson();
                timeresponse=gson.fromJson(stringResponse, TimeForNearestPojo.class);
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result)
        {

            super.onPostExecute(result);

            if(timeresponse!=null)
            {
                Utility.printLog("getetastatus" + timeresponse.getStatus());

                if(timeresponse.getStatus().equals("OK"))
                {
                    Utility.printLog("Test ETA DIstance: "+timeresponse.getRows().get(0).getElements().get(0).getDuration().getText());

                    try
                    {
                        String etaTime=timeresponse.getRows().get(0).getElements().get(0).getDuration().getText();
                        etaTime = etaTime.replaceAll("[^\\d.]", "");
                        etaTime.replace(" ", "");
                        etatime = etaTime;
                        if (mMap != null)
                        {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    mMap.clear();
                                    int width = getResources().getDisplayMetrics().widthPixels;
                                    int height = getResources().getDisplayMetrics().heightPixels;

                                    try {

                                        BitmapCustomMarker bitmapCustomMarker = new BitmapCustomMarker(LiveBookingStatus.this,etatime);

                                        proMarker = mMap.addMarker(new MarkerOptions()
                                                .position(prolatLng)
                                                .icon(BitmapDescriptorFactory.fromBitmap(bitmapCustomMarker.createBitmap()))
                                        );

                                        ownMArker = mMap.addMarker(new MarkerOptions()
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.add_new_address_red_pin_icon))
                                                .position(ownlatLng));
                                    } catch (IllegalArgumentException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
            }
        }
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        if(pDialog!=null )
        {
            pDialog.dismiss();
            pDialog.cancel();
        }
        if(indialog!=null)
        {
            pDialog.dismiss();
            pDialog.cancel();
        }
        isOpenLivebooking = false;
        Variableconstant.isLIVBOKINOPN = false;
        callpausedestroytimer();
    }
    /**
     * <h2>callpausedestroytimer</h2>
     * <p2>
     *      at this method we are saving the systemtime and the time on the screen
     * </p2>
     */
    private void callpausedestroytimer()
    {
        if(timerflagatstart) {
            if (!tvjobtimer.getText().toString().equals("Provider Arrived") && !tvjobtimer.getText().toString().equals("Provider")
                    && !tvjobtimer.getText().toString().equals(getResources().getString(R.string.invoicepinding)) && !tvjobtimer.getText().toString().equals("")) {
                String splittext[] = tvjobtimer.getText().toString().split(":");
                String newValue = Utility.dateintwtfour().split(" ")[0] + " " + splittext[0].trim() + ":" + splittext[1].trim() + ":" + splittext[2].trim();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss", Locale.getDefault());
                try {
                    Date date = sdf.parse(newValue);
                    manager.setSavedTime(date.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long savesystmtime = System.currentTimeMillis();
                manager.setSavedSystemTime(savesystmtime);
            }
        }
    }

    /**
     * <h2>showcancelpopup</h2>
     * showing popup when cancel buttom is clicked to cancel the bookings
     * @param alert reason, why he wants to cancel the bookings
     */
    private void showcancelpopup(String alert)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(LiveBookingStatus.this);
        builder.setTitle(getResources().getString(R.string.providercanceld));
        builder.setCancelable(false);
        builder.setMessage(alert);
        builder.setPositiveButton(LiveBookingStatus.this.getResources().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {

                Intent intent = new Intent(LiveBookingStatus.this,MenuActivity.class);
                startActivity(intent);
                finish();
                dialog.dismiss();
            }
        });
        Dialog dialogg = builder.create();
        dialogg.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        dialogg.show();
    }
    public static double distance(double lat1, double lng1, double lat2, double lng2, String unit)
    {
        double earthRadius = 3958.75; // miles (or 6371.0 kilometers)
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = earthRadius * c ;
        if (KILOMETER.equals(unit))
// Kilometer
        {
            dist = dist * 1.609344;
        }
        else if (NAUTICAL_MILES.equals(unit))
// Nautical Miles
        {
            dist = dist * 0.8684;
        }
        else if(METER.equals(unit))
// meter
        {
            dist = dist * 1609.344;
        }
        return dist;
    }
    /*Latlong bount */
    public int getBoundsZoomLevel(LatLngBounds bounds, int mapWidthPx, int mapHeightPx){

        LatLng ne = bounds.northeast;
        LatLng sw = bounds.southwest;

        double latFraction = (latRad(ne.latitude) - latRad(sw.latitude)) / Math.PI;

        double lngDiff = ne.longitude - sw.longitude;
        double lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        int WORLD_PX_HEIGHT = 256;
        double latZoom = zoom(mapHeightPx, WORLD_PX_HEIGHT, latFraction);
        int WORLD_PX_WIDTH = 256;
        double lngZoom = zoom(mapWidthPx, WORLD_PX_WIDTH, lngFraction);

        int result = Math.min((int)latZoom, (int)lngZoom);
        return Math.min(result, ZOOM_MAX);
    }
    private double latRad(double lat) {
        double sin = Math.sin(lat * Math.PI / 180);
        double radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }
    private double zoom(int mapPx, int worldPx, double fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / LN2);
    }

    /**
     * <h2>UpdateDriverLocation_DriverOnTheWay</h2>
     * <p>
     * @param latitude provider latitude
     * @param longitude provider longitude
     * </p>
     */
    private void UpdateDriverLocation_DriverOnTheWay(String latitude,String longitude) {
        double driver_current_latitude = Double.parseDouble(latitude);
        double driver_cuttent_longitude = Double.parseDouble(longitude);

        /********************************************************************************************************/
        Location driverLocation = new Location("starting_point");
        driverLocation.setLatitude(driver_current_latitude);
        driverLocation.setLongitude(driver_cuttent_longitude);
        mCurrentLoc = driverLocation;
        if (mPreviousLoc == null) {
            mPreviousLoc = driverLocation;
        }
        final float bearing = mPreviousLoc.bearingTo(mCurrentLoc);
        if (proMarker != null) {
            if (proMarker.getPosition()!= null) {
                proMarker.setPosition(new LatLng(driverLocation.getLatitude(), driverLocation.getLongitude()));
                proMarker.setAnchor(0.5f, 0.5f);
                final Handler handler = new Handler();
                final long start = SystemClock.uptimeMillis();
                Projection proj = mMap.getProjection();
                Point startPoint = proj.toScreenLocation(new LatLng(mPreviousLoc.getLatitude(), mPreviousLoc.getLongitude()));
                final LatLng startLatLng = proj.fromScreenLocation(startPoint);
                final long duration = 500;
                final Interpolator interpolator = new LinearInterpolator();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        long elapsed = SystemClock.uptimeMillis() - start;
                        float t = interpolator.getInterpolation((float) elapsed
                                / duration);
                        double lng = t * mCurrentLoc.getLongitude() + (1 - t) * startLatLng.longitude;
                        double lat = t * mCurrentLoc.getLatitude() + (1 - t) * startLatLng.latitude;
                        proMarker.setPosition(new LatLng(lat, lng));
                        proMarker.setAnchor(0.5f, 0.5f);
                        if (t < 1.0) {
                            // Post again 16ms later.
                            handler.postDelayed(this, 16);
                        }
                    }
                });
            }
        }
        mPreviousLoc = mCurrentLoc;
        /********************************************************************************************************/
        LatLng latLng = new LatLng(driver_current_latitude, driver_cuttent_longitude);

        if (mMap != null)
        {
            LatLngBounds bounds = LatLngBounds.builder()
                    .include(ownMArker.getPosition())
                    .include(proMarker.getPosition()).build();
            CameraPosition cp = new CameraPosition.Builder()
                    .bearing(30)
                    .target(latLng)
                    //.zoom(getBoundsZoomLevel(bounds, rrmap.getMeasuredWidth(), rrmap.getMeasuredHeight()))
                    .zoom(17f)
                    .build();
            CameraUpdate cu = CameraUpdateFactory.newCameraPosition(cp);
            mMap.animateCamera(cu);

        }
          //  mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

    }

    /**
     * <h2>mStartSocketListner</h2>
     * <p>
     *     listen all the data on UpdateCustomer  and  CustomerStatus channel
     * </p>
     */
    public void mStartSocketListner()
    {
        ApplicationController.initInstance(new SockEmitter(this) {
            @Override
            public void execute(String socket_event, final JSONObject jsonObject) throws JSONException
            {
                if(socket_event.equals("UpdateCustomer"))
                {
                    Log.d(TAG, "ALIresponceupdateinLive " + jsonObject);
                    PubNub_pojo pubNubPojo = gson.fromJson(jsonObject.toString(), PubNub_pojo.class);
                    pid = pubNubPojo.getMsg().getTrack().getPid();
                    email = pubNubPojo.getMsg().getTrack().getE();
                    JobLAt = pubNubPojo.getMsg().getTrack().getLt();
                    JobLNG = pubNubPojo.getMsg().getTrack().getLg();
                    if(statcode.equals("5"))
                    {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    markingDeliverers();
                                    Startpublishwithtimer(pid);
                                  //  markingDeliverers(JobLAt, JobLNG, apptlat, apptlng);
                                }
                            });
                    }
                }
                else if(socket_event.equals("CustomerStatus"))
                {
                    Log.d(TAG, "ALIresponcelivegetat " + jsonObject);
                    try {
                        statcode = jsonObject.getString("st");
                        switch (statcode) {
                            case "10":
                                final String alert = jsonObject.getString("alert");
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        cancelrequest(alert);
                                    }
                                });

                                break;
                            case "22":
                                runOnUiThread(new TimerTask() {
                                    @Override
                                    public void run() {
                                        timerViewmethod(statcode);
                                    }
                                });
                                break;
                            default:
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        callserviceapptdetls();
                                    }
                                });

                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setTimerText(LiveBookinResponce response) {
        long diff=0;
        String[] Fulltime=response.getTimer_start_time().split(" ");
        String[] date=Fulltime[0].split("-");
        String[] time=Fulltime[1].split(":");
        Date timerstarttime = new Date();
        Calendar cal = Calendar.getInstance();
        //cal.add(Calendar.);
        timerstarttime.setDate(Integer.parseInt(date[2]));
        timerstarttime.setMonth(Integer.parseInt(date[1])-1);
        timerstarttime.setYear(Integer.parseInt(date[0])-1900);
        timerstarttime.setHours(Integer.parseInt(time[0]));
        timerstarttime.setMinutes(Integer.parseInt(time[1]));
        timerstarttime.setSeconds(Integer.parseInt(time[2]));
        Date currentDate=new Date();
        diff=(currentDate.getTime()-timerstarttime.getTime())/1000;
        long timerTime = Long.parseLong(response.getTimer());
        jobTimer(tvjobtimer,timerTime+diff);
    }
    public static String formatSeconds(long timeInSeconds)
    {
        int hours = (int)timeInSeconds / 3600;
        int secondsLeft = (int)timeInSeconds - hours * 3600;
        int minutes = secondsLeft / 60;
        int seconds = secondsLeft - minutes * 60;

        String formattedTime = "";
        if (hours < 10)
            formattedTime += "0";
        formattedTime += hours + ":";

        if (minutes < 10)
            formattedTime += "0";
        formattedTime += minutes + ":";

        if (seconds < 10)
            formattedTime += "0";
        formattedTime += seconds ;

        return formattedTime;
    }
}

