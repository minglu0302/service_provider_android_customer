package com.iserve.passenger;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.pojo.GetCard_pojo;
import com.pojo.card_info_pojo;
import com.utility.Alerts;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by embed on 14/12/15.
 * PaymentFragment fragement show the saved card and info of card
 */
public class PaymentFragment extends Fragment implements View.OnClickListener,AdapterView.OnItemClickListener
{
    private View view;
    TextView toolbarhometxv,tveditbarhometxv,youraddress,yourlocation,text_add_card,poweredby;

    ImageView tolbarhomelogo;
    private ListView card_list;
    private CustomListViewAdapter adapter;
    List<card_info_pojo> rowItems;
    private RelativeLayout add_cc_bt;
    SessionManager manager;
    ProgressDialog pDialog;
    Resources resources;
    GetCard_pojo response;
    JSONObject jsonObject;

    private Typeface regularfont;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.payment_fragment, container, false);
        card_list = (ListView) view.findViewById(R.id.cards_list_view);
        LayoutInflater footerinflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View fotter_view = footerinflater.inflate(R.layout.add_card_fotter, null);
        card_list.addFooterView(fotter_view);
        add_cc_bt = (RelativeLayout) fotter_view.findViewById(R.id.add_card_rel_fotter);
        text_add_card = (TextView) fotter_view.findViewById(R.id.text_add_card);
        regularfont = Typeface.createFromAsset(getActivity().getAssets(),Variableconstant.regularfont);
        initialize();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(Utility.isNetworkAvailable(getActivity())){
            adapter = new CustomListViewAdapter(getActivity(), R.layout.card_list_row, rowItems);
            card_list.setAdapter(adapter);

            add_cc_bt.setOnClickListener(this);
            card_list.setOnItemClickListener(this);
            cllGetAllCards();
        }
        else{
            Alerts alerts=new Alerts(getActivity());
            alerts.showNetworkAlert(getActivity());

        }
    }

    private void initialize() {

        jsonObject = new JSONObject();
        manager = new SessionManager(getActivity());
        toolbarhometxv = (TextView) getActivity().findViewById(R.id.toolbarhometxv);
        yourlocation = (TextView) getActivity().findViewById(R.id.yourlocation);
        youraddress = (TextView) getActivity().findViewById(R.id.youraddress);
        poweredby = (TextView) view.findViewById(R.id.poweredby);
        toolbarhometxv.setVisibility(View.VISIBLE);
        toolbarhometxv.setText(getResources().getString(R.string.Payment));
        tolbarhomelogo = (ImageView) getActivity().findViewById(R.id.tolbarhomelogo);
        tolbarhomelogo.setVisibility(View.GONE);
        tveditbarhometxv = (TextView) getActivity().findViewById(R.id.tveditbarhometxv);
        RelativeLayout imageframelayout = (RelativeLayout) getActivity().findViewById(R.id.imageframelayout);
        imageframelayout.setVisibility(View.GONE);
        tveditbarhometxv.setVisibility(View.GONE);
        youraddress.setVisibility(View.GONE);
        yourlocation.setVisibility(View.GONE);
        text_add_card.setTypeface(regularfont);
        toolbarhometxv.setTypeface(regularfont);
        poweredby.setTypeface(regularfont);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage(getResources().getString(R.string.Loading));
        pDialog.setCancelable(false);
        resources = getActivity().getResources();
        rowItems = new ArrayList<>();
        adapter = new CustomListViewAdapter(getActivity(), R.layout.card_list_row, rowItems);
        card_list.setAdapter(adapter);
        add_cc_bt.setOnClickListener(this);
        card_list.setOnItemClickListener(this);
    }

    public void cllGetAllCards()
    {
        Utility.printLog(" in side get card service ");
        pDialog.show();
        rowItems.clear();



        try {
            jsonObject.put("ent_sess_token", manager.getSession());
            jsonObject.put("ent_dev_id",manager.getDevice_Id());
            jsonObject.put("ent_cust_id", manager.getCustomerId());
            jsonObject.put("ent_date_time", Utility.dateintwtfour());
            Log.i("TAG ","THEDATAIS "+jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utility.doJsonRequest(Variableconstant.GETCARD, jsonObject, new Utility.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("The Response get card " + result);
                callGetCardServiceResponse(result);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getActivity(), resources.getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                if (pDialog != null) {
                    pDialog.cancel();
                    pDialog.dismiss();
                }
            }
        });

    }


    public void callGetCardServiceResponse(String Response) {

        try {
            Gson gson = new Gson();
            response = gson.fromJson(Response, GetCard_pojo.class);
            if (response.getErrNum().equals("52") && response.getErrFlag().equals("0")) {
                if(response.getCards().size()>0)
                {
                    rowItems.clear();
                    for (int i = 0; i < response.getCards().size(); i++) {
                        Bitmap bitmap = Utility.setCreditCardLogo(response.getCards().get(i).getType(),getActivity());

                        card_info_pojo item = new card_info_pojo(bitmap, response.getCards().get(i).getLast4(), response.getCards().get(i).getExp_month(),
                                response.getCards().get(i).getExp_year(), response.getCards().get(i).getId());//id
                        rowItems.add(item);
                    }
                    adapter = new CustomListViewAdapter(getActivity(),
                            R.layout.card_list_row, rowItems);
                    card_list.setAdapter(adapter);
                }

            } else {
                Toast.makeText(getActivity(), response.getErrMsg(), Toast.LENGTH_LONG).show();

            }
            if (pDialog != null) {
                pDialog.cancel();
                pDialog.dismiss();
            }
        }
        catch (Exception e) {
            if (pDialog != null) {
                pDialog.cancel();
                pDialog.dismiss();
            }
            e.printStackTrace();
            Utility.printLog("" + e);
        }

    }

    /*private Bitmap setCreditCardLogo(String cardMethod)
    {
        Bitmap anImage;
        Drawable myDrawable;
        switch (cardMethod)
        {

            case "Visa":
                myDrawable = getResources().getDrawable(R.drawable.visa);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            case "MasterCard":
                myDrawable = getResources().getDrawable(R.drawable.master_card);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            case "American Express":
                myDrawable = getResources().getDrawable(R.drawable.amex);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            case "Discover":
                myDrawable = getResources().getDrawable(R.drawable.amex_back);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
            default:
                myDrawable = getResources().getDrawable(R.drawable.cc_back);
                anImage =((BitmapDrawable) myDrawable).getBitmap();
                break;
        }
        return anImage;
    }*/

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.add_card_rel_fotter) {
            Intent intent = new Intent(getActivity(), TestSignup_Payment.class);
            intent.putExtra("coming_From","payment_Fragment");
            startActivity(intent);

            getActivity().overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        card_info_pojo row_details =(card_info_pojo)card_list.getItemAtPosition(position);

        Utility.printLog("Card count: "+response.getCards().size());


        String expDate=row_details.getExp_month();

        if(expDate.length()==1)
        {
            expDate="0"+expDate;
        }
        expDate=expDate+"/"+row_details.getExp_year();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        row_details.getCard_image().compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        Intent intent=new Intent(getActivity(), DeleteCardActivity.class);
        intent.putExtra("NUM",row_details.getCard_numb());
        intent.putExtra("EXP",expDate);
        intent.putExtra("IMG",byteArray);
        intent.putExtra("ID",row_details.getCard_id());
        intent.putExtra("COUNT",response.getCards().size());
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.anim_two, R.anim.anim_one);

    }

    class CustomListViewAdapter extends ArrayAdapter<card_info_pojo> {

        Context context;
        public CustomListViewAdapter(Context context, int resourceId, List<card_info_pojo> items) {
            super(context, resourceId, items);
            this.context = context;
        }


        private class ViewHolder {
            ImageView card_image;
            TextView card_numb;
            RelativeLayout change_card_relative;

        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            final card_info_pojo rowItem = getItem(position);

            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {

                convertView = mInflater.inflate(R.layout.card_list_row, parent,false);
                holder = new ViewHolder();

                holder.card_numb = (TextView) convertView.findViewById(R.id.card_numb_row_change);
                holder.card_image = (ImageView) convertView.findViewById(R.id.card_img_row_change);
                holder.change_card_relative = (RelativeLayout) convertView.findViewById(R.id.change_card_relative);
                convertView.setTag(holder);


            } else
                holder = (ViewHolder) convertView.getTag();
            holder.card_image.setImageBitmap(rowItem.getCard_image());
            holder.card_numb.setText(rowItem.getCard_numb());
            return convertView;
        }
    }
}
