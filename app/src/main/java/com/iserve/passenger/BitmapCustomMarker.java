package com.iserve.passenger;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by embed on 15/9/16.
 *
 */
class BitmapCustomMarker
{
    private String sname;
    static int i=1;
    Bitmap bitmap;
    Context context;
    BitmapCustomMarker(Context context,String sname)
    {
        this.context=context;
        this.sname=sname;
    }
    Bitmap createBitmap()
    {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout=inflater.inflate(R.layout.markerlayoutlivetracking, null);
        RelativeLayout lyBitmap= (RelativeLayout) layout.findViewById(R.id.lyBitmap);
        TextView tveta= (TextView) layout.findViewById(R.id.tveta);
        tveta.setText(sname);
        lyBitmap.setDrawingCacheEnabled(true);
        lyBitmap.buildDrawingCache();
        lyBitmap.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        lyBitmap.layout(0, 0, lyBitmap.getMeasuredWidth(), lyBitmap.getMeasuredHeight());
        bitmap =lyBitmap.getDrawingCache();
        return bitmap;
    }

}
