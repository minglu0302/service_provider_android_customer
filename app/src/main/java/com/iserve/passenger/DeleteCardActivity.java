package com.iserve.passenger;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.pojo.Validator_Pojo;
import com.utility.OkHttp3Request;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import okhttp3.FormBody;


/**
 * <h>DeleteCardActivity</h>
 * <p>Deleting the card from the app and Database also using server Api</p>
 */

public class DeleteCardActivity extends AppCompatActivity implements OnClickListener
{
	private TextView card_numb_text,exp_text,actionbartext,delete;
	private ImageView card_img;
	private String id;
	SessionManager manager;
	ProgressDialog dialogL;
	FrameLayout fframelayt;
	private Typeface regularfont;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.delete_card);
		manager = new SessionManager(this);
		intializeVariables();
		regularfont = Typeface.createFromAsset(getAssets(),Variableconstant.regularfont);
		Bundle bundle=getIntent().getExtras();
		card_numb_text.setText(bundle.getString("NUM"));
		exp_text.setText(bundle.getString("EXP"));
		byte[] byteArray = bundle.getByteArray("IMG");
		id=bundle.getString("ID");
		if(byteArray!=null)
			card_img.setImageBitmap(BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length));

	}

	private void intializeVariables() {
		// TODO Auto-generated method stub
		card_numb_text=(TextView)findViewById(R.id.card_numb_delete);
		exp_text=(TextView)findViewById(R.id.exp_date_delete);
		delete=(TextView) findViewById(R.id.delete_card_btn);
		card_img=(ImageView)findViewById(R.id.card_img_delete);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setTitle("");
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
		fframelayt.setVisibility(View.VISIBLE);
		actionbartext = (TextView) findViewById(R.id.toolbarhometxv);
		actionbartext.setVisibility(View.VISIBLE);
		actionbartext.setText(getResources().getString(R.string.delete_card));
		delete.setTypeface(regularfont);
		actionbartext.setTypeface(regularfont);
		delete.setOnClickListener(this);
		
	}
	
	
	public void BackgroundDeleteCard()
	{
		dialogL = new ProgressDialog(DeleteCardActivity.this);
		dialogL.setMessage(getResources().getString(R.string.delete_card));
		dialogL.show();
		Utility.printLog(" CARDREQUEST " ,id+ " custid "+manager.getCustomerId()+" = "+manager.getSession() +" :: "+manager.getDevice_Id());
		FormBody requestBody=new FormBody.Builder()
				.add("ent_sess_token",manager.getSession())
				.add("ent_dev_id",manager.getDevice_Id())
				.add("ent_cc_id", id)
				.add("ent_cust_id", manager.getCustomerId())
				.add("ent_date_time", Utility.dateintwtfour())
				.build();
		OkHttp3Request.doOkHttp3Request(Variableconstant.REMOVECARD, requestBody, new OkHttp3Request.JsonRequestCallback() {
		@Override
		public void onSuccess(String result) {
			Utility.printLog(" CARDREQUESTresponse " + result);
			dialogL.dismiss();

			try {
				Gson gson = new Gson();
				Validator_Pojo response = gson.fromJson(result, Validator_Pojo.class);
				Utility.printLog("error no get card" + response.getErrNum());
				if(response.getErrFlag().equals("0"))
				{
					Toast.makeText(DeleteCardActivity.this,getResources().getString(R.string.card_removed), Toast.LENGTH_SHORT).show();
					Intent intent=new Intent();
					finish();
					overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
				}
				else
				{
					Utility.ShowAlert(response.getErrMsg(),DeleteCardActivity.this);
				}
			}
			catch (Exception e) {
				if (dialogL != null) {
					dialogL.cancel();
					dialogL.dismiss();
				}
				e.printStackTrace();
				Utility.printLog("" + e);
			}
		}
		@Override
		public void onError(String error) {

			Utility.printLog("error in add card " + error);
			dialogL.dismiss();
		}
	});
	}
	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.delete_card_btn)
		{
			BackgroundDeleteCard();
		}
	}
	@Override
	public void onBackPressed() 
	{
		finish();
		overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
	}
}
