package com.iserve.passenger;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import com.utility.*;
/**
 * <h>Splash_Screen</h>
 * <P>
 *    User InterFace for the user to Login and Register
 * </P>
 * @author 3Embed.
 * @since   7-7-16
 * @version  1.0.15
 */

public class Splash_Screen extends Activity implements View.OnClickListener, LocationNotifier {

    private TextView tv_splsh_login,tv_splsh_regstr,setalarm;
    private LocationUtil networkUtilObj;
    private RelativeLayout rll_splash_hold_ll;
    public static double[] latiLongi = new double[2];
    Alerts alerts;
    private SessionManager manager;
    private boolean isScktRspnce = false;
    private int count = 0;
    private Timer myTimer_publish;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 120;
    private String currentVersion,scheduleday;
    private boolean updateflag = false;
    private ProgressBar progressBar;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList;
    private int hour,min;
    private int reqestcode = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        setContentView(R.layout.activity_splash_screen);
        manager = new SessionManager(this);
        alerts = new Alerts(this);
        Utility.statusbar(this);
        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        initialize();
        chckForPermission();
        getPushToken();
        manager.setDevice_Id(android_id);
        socketUpdtCstmrRspnce();
    }

    /*
     * initializing the views of the Activity
     */

    private void initialize()
    {
        tv_splsh_login = (TextView) findViewById(R.id.tv_splsh_login);
        tv_splsh_regstr = (TextView) findViewById(R.id.tv_splsh_regstr);
        rll_splash_hold_ll = (RelativeLayout) findViewById(R.id.rll_splash_hold_ll);
        progressBar = (ProgressBar) findViewById(R.id.progressBar_splsh);
        tv_splsh_login.setOnClickListener(this);
        tv_splsh_regstr.setOnClickListener(this);
        setalarm = (TextView) findViewById(R.id.setalarm);
        setalarm.setOnClickListener(this);
        progressBar.setVisibility(View.VISIBLE);
        //calendrset();


    }

    /**
     * <h2>getPushToken</h2>
     * <p>
     * getting the MyFirebaseInstanceIDService's token
     * i.e pushtoken and saving that token in session maneger for further use
     * @see MyFirebaseInstanceIDService
     * </p>
     */

    private void getPushToken() {

        if (checkPlayServices())
        {

            Intent intent = new Intent(this, MyFirebaseInstanceIDService.class);
            startService(intent);
            String tokenis =  FirebaseInstanceId.getInstance().getToken();
            if(tokenis!=null)
            {
                manager.setRegistrationId(tokenis);
            }
        }else
        {
            alerts.alert(this,getResources().getString(R.string.splash_playServicenotfound));
        }
    }

    /**
     * <h2>chckForPermission</h2>
     * <p>checking the permission at runtime if app version is greater or equal to 23
     * 23 refer to Marshmallow
     * checking permission for Location and phone State</p>
     * @see AppPermissionsRunTime
     */

    private void chckForPermission()
    {
        if (Build.VERSION.SDK_INT >= 23)
        {


            myPermissionConstantsArrayList = new ArrayList<>();

            //myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_PHONE_STATE);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_ACCESS_FINE_LOCATION);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_ACCESS_COARSE_LOCATION);

            if(AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE))
            {
                networkUtilObj = new LocationUtil(Splash_Screen.this,this);
                if (!networkUtilObj.isGoogleAPIConnected())
                {
                    networkUtilObj.connectGoogleApiClient();

                }
            }
        }
        else
        {
            networkUtilObj = new LocationUtil(this, this);
            if (!networkUtilObj.isGoogleAPIConnected())
            {
                networkUtilObj.connectGoogleApiClient();
            }
        }
    }

    /**
     * <h2>socketUpdtCstmrRspnce</h2>
     * <p>here we are getting UpadateCustomer channel responce
     * we are setting this responce in sessionmaneger for further use , and making boolean flag
     * isScktRspnce to true;
     * @see ApplicationController
     * @see SockEmitter
     * this is an abstract class
     * have an abstract method <h2>execute</h2></p>
     *
     */

    private void socketUpdtCstmrRspnce()
    {
        ApplicationController.initInstance(new SockEmitter(this) {
            @Override
            public void execute(String socket_event, JSONObject jsonObject) throws JSONException
            {
                if(socket_event.equals("UpdateCustomer"))
                {

                    Log.d("UpdateCustomer ", jsonObject.toString());

                    manager.setSocketRes(jsonObject.toString());
                    isScktRspnce =true;
                }
            }
        });
    }

    /**
     * predefined method to check run time permissions list call back
     * @param requestCode request code
     * @param permissions: contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        boolean isDenine = false;
        switch (requestCode)
        {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                int i = 0;
                for (; i < grantResults.length; i++)
                {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                    {
                        isDenine = true;
                        break;
                    }

                }
                if (isDenine)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(getResources().getString(R.string.ohuhavedenied)+" "+myPermissionConstantsArrayList.get(i)+" "+getResources().getString(R.string.pleaseexitapptogivepermission))
                            .setTitle(getResources().getString(R.string.permissionrejected));
                    builder.setPositiveButton(getResources().getString(R.string.exitapp), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else
                {
                    networkUtilObj = new LocationUtil(this, this);
                    if (!networkUtilObj.isGoogleAPIConnected())
                    {
                        networkUtilObj.connectGoogleApiClient();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }
    private boolean checkPlayServices()
    {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else
                {

                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    public void updatedInfo(String info)
    {

    }

    @Override
    public void locationUpdates(Location location)
    {
        latiLongi[0] = location.getLatitude();
        latiLongi[1]= location.getLongitude();
        manager.setLATITUDE(latiLongi[0]+"");
        manager.setLONGITUDE(latiLongi[1]+"");

    }

    @Override
    public void locationFailed(String message) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        currentVersion = BuildConfig.VERSION_NAME;
        String newVersion = "";
       new GetVersionCode().execute(newVersion);

        if (networkUtilObj != null && !networkUtilObj.isGoogleAPIConnected())
        {
            networkUtilObj.connectGoogleApiClient();
        }

        if(manager.isLogin())
        {

            startPublishingWithTimer();
        }

    }



    /**
     * <h2>runUithread</h2>
     * <p>checking the user is already LoggedIn
     * For User Not Already LoggedIn it will Show Login and Register view
     * For User Already LoggedIn it will check the LatLong and responce From the Socket
     * </p>
     * running ui thread with animation
     */
    public void runUithread()
    {
        Thread timer=new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    sleep(1000);

                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                finally {
                    if (updateflag)
                    {
                        if (manager.isLogin()) {

                                    if (latiLongi[0] == 0.0 && latiLongi[1] == 0.0 || !isScktRspnce)//!isScktRspnce
                                    {

                                        runUithread();
                                    } else
                                        {


                                        if(count==0)
                                        {
                                            count++;
                                            runOnUiThread(new Runnable() {
                                                public void run() {
                                                    progressBar.setVisibility(View.GONE);

                                                }
                                            });
                                            Intent intent = new Intent(Splash_Screen.this, MenuActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }

                        } else {

                            if (latiLongi[0] == 0.0 && latiLongi[1] == 0.0) {
                                runUithread();
                            } else {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        progressBar.setVisibility(View.GONE);
                                        rll_splash_hold_ll.setVisibility(View.VISIBLE);


                                    }
                                });

                            }
                        }
                    }


                }
            }
        };
        timer.start();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.tv_splsh_login:
                    tv_splsh_login.setSelected(false);
                    tv_splsh_regstr.setSelected(false);
                    Intent intent = new Intent(this, Login_Page.class);
                    startActivity(intent);
                break;
            case R.id.tv_splsh_regstr:
                    tv_splsh_login.setSelected(true);
                    tv_splsh_regstr.setSelected(true);
                    Intent rintent = new Intent(this, Register_Page.class);
                    startActivity(rintent);
                break;
            case R.id.setalarm:

                break;
            default:
        }
    }

    /**
     * <h2>startPublishingWithTimer</h2>
     * <p>publishing LatLng on to the Socket by calling Method sendMessage in every 2 second</p>
     */
    private void startPublishingWithTimer()
    {

        if(myTimer_publish!= null)
        {
            Utility.printLog("Timer already started");
            return;
        }
        myTimer_publish = new Timer();
        TimerTask myTimerTask_publish = new TimerTask() {
            @Override
            public void run() {
                if (latiLongi[0] == 0.0 && latiLongi[1] == 0.0) {
                    Utility.printLog("startPublishingWithTimer getServerChannel no location");
                } else
                    {
                    sendMessage(latiLongi[0], latiLongi[1]);
                }
            }
        };
        myTimer_publish.schedule(myTimerTask_publish, 0, 2000);
    }

    /**
     * <h2>sendMessage</h2>
     * <p>
     * sending JSONObject to the socket Channel "UpdateCustomer"
     * @param latitude current latitude
     * @param logitude current longitude
     * </p>

     */

    private void sendMessage(double latitude,double logitude){

        JSONObject sendJson = new JSONObject();
        try{

            sendJson.put("email",manager.getCoustomerEmail());
            sendJson.put("lat",latitude);
            sendJson.put("long",logitude);
            sendJson.put("btype",3);
            ApplicationController.getInstance().emit("UpdateCustomer", sendJson);
        }catch(JSONException e){
            e.printStackTrace();
        }

    }


    @Override
    protected void onStop() {
        super.onStop();

    }
    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if(networkUtilObj!=null)
        {
            networkUtilObj.disconnectGoogleApiClient();
        }

        try {
            if(myTimer_publish!=null)
            {
                myTimer_publish.cancel();
                myTimer_publish = null;

            }
        }catch (Exception e){

            e.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(networkUtilObj!=null)
        {
            networkUtilObj.disconnectGoogleApiClient();
        }

        try {
            if(myTimer_publish!=null)
            {
                myTimer_publish.cancel();
                myTimer_publish = null;

            }
        }catch (Exception e){

            e.printStackTrace();
        }

    }

    private class GetVersionCode extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... args) {

            String newVersion = args[0];
            Log.i("TAG","PRINTg "+newVersion);
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + Splash_Screen.this.getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();

                return newVersion;
            } catch (Exception e)
            {

                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion)
        {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (!currentVersion.equals(onlineVersion)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(Splash_Screen.this);
                    builder.setTitle(getResources().getString(R.string.app_name));
                    builder.setCancelable(false);
                    builder.setMessage(getResources().getString(R.string.newupdateavailable));
                    builder.setPositiveButton(Splash_Screen.this.getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which)
                        {


                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                            }


                            finish();
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton(Splash_Screen.this.getResources().getString(R.string.notnow), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateflag = true;

                            if(Utility.isNetworkAvailable(Splash_Screen.this))
                            {
                                runUithread();
                            }
                            else
                            {
                                alerts.showNetworkAlert(Splash_Screen.this);
                            }


                            dialog.dismiss();
                        }
                    });

                    AlertDialog dialogg = builder.create();
                    dialogg.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                    try {
                        dialogg.show();
                    } catch(Exception e){
                       e.printStackTrace();
                    }
                }
                else
                {
                    updateflag = true;
                    if(Utility.isNetworkAvailable(Splash_Screen.this))
                    {
                        runUithread();
                    }
                    else
                    {
                        alerts.showNetworkAlert(Splash_Screen.this);
                    }
                }
            }
            else
            {
                updateflag = true;
                if(Utility.isNetworkAvailable(Splash_Screen.this))
                {
                    runUithread();
                }
                else
                {
                    alerts.showNetworkAlert(Splash_Screen.this);
                }
            }
        }


    }


}
