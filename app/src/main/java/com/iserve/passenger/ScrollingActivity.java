package com.iserve.passenger;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.adapter.SpinerAdaptor;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pojo.AllServiceDtl;
import com.pojo.Cancellation_java;
import com.pojo.DataService;
import com.pojo.GetCard_pojo;
import com.pojo.LiveBookingPojo;
import com.pojo.PromoCodepojo;
import com.pojo.PubNubType;
import com.pojo.PubNub_pojo;
import com.pojo.ServiceGroup;
import com.pojo.ServiceJson;
import com.squareup.picasso.Picasso;
import com.utility.Alerts;
import com.utility.AppPermissionsRunTime;
import com.utility.CircleTransform;
import com.utility.LocationUtil;
import com.utility.MyFirebaseInstanceIDService;
import com.utility.OkHttp3Request;
import com.utility.Scaler;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import com.utility.WaveDrawable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.CropImage;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.FormBody;

import static android.os.Build.VERSION_CODES.N;
import static java.security.AccessController.getContext;

public class ScrollingActivity extends AppCompatActivity implements View.OnClickListener
{
    Toolbar toolbar;
    ProgressDialog pDialog;
    Dialog dialog;
    SessionManager manager;
    UploadAmazonS3 upload;
    Alerts alerts;
    TextView doctorname,doctorlike,providerdistance,confirmbooktv,apptaddress,apptcngaddtxv,appbutton,tvhourlyfee,
            tvhourlymount,tvtotlamount,tvminimumfeeamount,card_no_Tv,paymentbuton,addaddgroup,tvdisamount
            ,tvgrouptotl,grouptotl,tvserviceamount;
    ImageView doc_pic,iv_payment,cancel_booking;
    RatingBar invoice_driver_rating;
    EditText calc_txt_Prise,jobdtltxt;
    RelativeLayout relativminifare,relativdisfare,choose_payment_screen,rrwaveout,rrloutbooking,relativeo_ut,maincontn_scrol,rrlscrollmain,rlmaincontotal,relativservfare;
    Spinner spinerhourlyquant;
    Button payment_cancel,payment_card,payment_cash;
    ArrayList<String> jobIds = new ArrayList<>();
    ArrayList<ServiceJson>rowservice = new ArrayList<>();
    JSONArray jsonArray;
    LiveBookingPojo liveBookingPojo;
    AllServiceDtl allservicedtlpojo;
    AppBarLayout appBarLayout;
    WaveDrawable waveDrawable;
    LinearInterpolator interpolator;
    private boolean apptcancel = false;
    CardView cardaddgroup;
    LinearLayout lloutjob,MainContainerservice;
    int count = 0;
    int buttonclicked = 0;
    int callpopupcount = 0;
    double servicetotal = 0.0;
    boolean totalhavetopay  = true;
    double totalamntpay = 0.00;
    ImageView glv = null;
    ImageView glvdlt = null;
    AlertDialog dialogg;
    Gson gson;
    String pid,typeid,rating_1,ful_name,rewiev,imgurl,DISTANCE,soltid,currencysymbl,eTime,sTime,bid = "";
    String payment_type = 1+"";
    static  String cardId = "";
    double total;
    private int counter=0;
    private CountDownTimer countDownTimer;
    private String state;
    private String landmark = "";
    public static File mFileTemp;
    private File newFile;
    private File jobpicDirectry;
    private  File[] jobPicFiles;
    private String takenNewImage="";
    private Uri newProfileImageUri;
    private final int REQUEST_CODE_GALLERY      = 0x2;
    private final int REQUEST_CODE_TAKE_PICTURE = 0x3;
    private final int REQUEST_CODE_CROP_IMAGE   = 0x4;
    private final int addressrequestcode   = 0x14;
    public static int groupcounter ;
    double size[];
    String SelectedCard="",selectedpromotype = "";
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 126;
    private final int REQUEST_CODE_PERMISSION_CALENDR = 133;
    boolean isComingFromoncreat = false;
    boolean pIctureTaken = false;
    String TAG = "ScrollingActivity";
    double hourlyfee = 0.0;
    //private Socket socket;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    PromoCodepojo promoresponce;
    double promodiscount = 0.0;
    double discountpercent = 0.0;
    Typeface regularfont,lightbold;
    private CoordinatorLayout coordinatorLayout;
    private boolean allowforback = true;
    private boolean activityIsRunin;
    private Socket socket;
    {
        try {
            socket = IO.socket(Variableconstant.SOCKET_PATH);
        } catch (URISyntaxException e) {
        //    Log.d("errormsg123", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_scrolling2);
        manager = new SessionManager(this);
        gson = new Gson();
        pDialog = new ProgressDialog(this);
        jsonArray = new JSONArray();
        pDialog.setCancelable(false);
        currencysymbl = getResources().getString(R.string.currencySymbol);
        groupcounter = 0;
        socket.connect();
        socket.on("CustomerStatus", handleIncomingMessages);
        socket.on("UpdateCustomer", handleIncomingCustomer);
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null)
        {
            pid = bundle.getString("PID");
            typeid = bundle.getString("TYPEID");
            rating_1 = bundle.getString("RATING");
            ful_name = bundle.getString("NAME");
            rewiev = bundle.getString("REVIEWS");
            imgurl = bundle.getString("PIC");
            DISTANCE = bundle.getString("DISTANCE");
            soltid = bundle.getString("SID");
            eTime = bundle.getString("eTime");
            sTime = bundle.getString("sTime");

            assert sTime != null;
            if(!sTime.equals(""))
            {
                long stm = Long.parseLong(sTime);
                String datefor = getDate(stm,"yyyy-MM-dd hh:mm:ss a");
            //    Log.d(TAG,"STIME "+sTime+" timeformate "+datefor + " long "+ stm);
            }

        }


        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis()+""+ Variableconstant.TEMP_PHOTO_FILE_NAME);
        }
        else
        {
            mFileTemp = new File(getFilesDir(),System.currentTimeMillis()+""+ Variableconstant.TEMP_PHOTO_FILE_NAME);
        }

        initilize();
    }

    @Override
    protected void onStart() {
        super.onStart();
        activityIsRunin = true;
    }

    private void initilize()
    {
        size = Scaler.getScalingFactor(this);
        alerts = new Alerts(ScrollingActivity.this);
        regularfont = Typeface.createFromAsset(this.getAssets(), Variableconstant.regularfont);
        lightbold = Typeface.createFromAsset(this.getAssets(), Variableconstant.SemiBoldFont);
        upload =UploadAmazonS3.getInstance(ScrollingActivity.this, Variableconstant.Amazoncognitoid);
        isComingFromoncreat = true;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        invoice_driver_rating = (RatingBar) findViewById(R.id.invoice_driver_rating);
        doc_pic = (ImageView) findViewById(R.id.doc_pic);
        doctorname = (TextView) findViewById(R.id.doctorname);
        doctorlike = (TextView) findViewById(R.id.doctorlike);
        providerdistance = (TextView) findViewById(R.id.providerdistance);
        tvdisamount = (TextView) findViewById(R.id.tvdisamount);
        grouptotl = (TextView) findViewById(R.id.grouptotl);
        tvgrouptotl = (TextView) findViewById(R.id.tvgrouptotl);
        tvserviceamount = (TextView) findViewById(R.id.tvserviceamount);
        relativservfare = (RelativeLayout) findViewById(R.id.relativservfare);
        rlmaincontotal = (RelativeLayout) findViewById(R.id.rlmaincontotal);
        relativeo_ut = (RelativeLayout) findViewById(R.id.relativeo_ut);
        MainContainerservice = (LinearLayout) findViewById(R.id.MainContainerservice);
        addaddgroup = (TextView) findViewById(R.id.addaddgroup);
        cardaddgroup = (CardView) findViewById(R.id.cardaddgroup);
        waveDrawable = new WaveDrawable(ContextCompat.getColor(this,  R.color.whitee), 450);
        relativminifare = (RelativeLayout) findViewById(R.id.relativminifare);
        relativdisfare = (RelativeLayout) findViewById(R.id.relativdisfare);
        spinerhourlyquant = (Spinner) findViewById(R.id.spinerhourlyquant);
        tvhourlyfee = (TextView) findViewById(R.id.tvhourlyfee);
        tvhourlymount = (TextView) findViewById(R.id.tvhourlymount);
        tvtotlamount = (TextView) findViewById(R.id.tvtotlamount);
        tvminimumfeeamount = (TextView) findViewById(R.id.tvminimumfeeamount);
        lloutjob = (LinearLayout) findViewById(R.id.lloutjob);
        iv_payment = (ImageView) findViewById(R.id.iv_payment);
        card_no_Tv = (TextView) findViewById(R.id.card_no_Tv);
        paymentbuton = (TextView) findViewById(R.id.paymentbuton);
        choose_payment_screen = (RelativeLayout) findViewById(R.id.choose_payment_screen);
        payment_cancel = (Button) findViewById(R.id.payment_cancel);
        payment_card = (Button) findViewById(R.id.payment_card);
        payment_cash = (Button) findViewById(R.id.payment_cash);
        rrwaveout = (RelativeLayout) findViewById(R.id.rrwaveout);
        cancel_booking = (ImageView) findViewById(R.id.cancel_booking);
        appbutton = (TextView) findViewById(R.id.appbutton);
        calc_txt_Prise = (EditText) findViewById(R.id.calc_txt_Prise);
        apptaddress = (TextView) findViewById(R.id.apptaddress);
        apptcngaddtxv = (TextView) findViewById(R.id.apptcngaddtxv);
        jobdtltxt = (EditText) findViewById(R.id.jobdtltxt);
        rrloutbooking= (RelativeLayout) findViewById(R.id.rrloutbooking);
        maincontn_scrol = (RelativeLayout) findViewById(R.id.maincontn_scrol);
        rrlscrollmain = (RelativeLayout) findViewById(R.id.rrlscrollmain);
        confirmbooktv = (TextView) findViewById(R.id.confirmbooktv);
        relativdisfare.setVisibility(View.GONE);
        jobIds.clear();
        manager.setJobIds(jobIds);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        assert collapsingToolbarLayout != null;
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
         appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    String drname = ful_name;
                    collapsingToolbarLayout.setTitle(drname);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle("");
                    isShow = false;
                }
            }
        });

          /*for calling couponcode */

        calc_txt_Prise.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(calc_txt_Prise.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    Backgrounddoctorcheckpromocode(calc_txt_Prise.getText().toString());

                    return true;
                }
                return false;
            }
        });
        String name[] = ful_name.split(" ");
        String confirmandbook = "Confirm and Book "+name[0];
        confirmbooktv.setText(confirmandbook);
        confirmbooktv.setOnClickListener(this);
        apptcngaddtxv.setOnClickListener(this);
        appbutton.setOnClickListener(this);
        cancel_booking.setOnClickListener(this);
        paymentbuton.setOnClickListener(this);
        payment_cancel.setOnClickListener(this);
        payment_card.setOnClickListener(this);
        payment_cash.setOnClickListener(this);
        addaddgroup.setOnClickListener(this);
        if(payment_type.equals("1"))
        {
            card_no_Tv.setText(getResources().getString(R.string.cash));
        }

        /*google play service*/

        //Checking play service is available or not
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        //if play service is not available
        if(ConnectionResult.SUCCESS != resultCode) {
            //If play service is supported but not installed
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                //Displaying message that play service is not installed
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());

                //If play service is not supported
                //Displaying an error message
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }

            //If play service is available
        } else {

            Intent intent = new Intent(this, MyFirebaseInstanceIDService.class);
            startService(intent);
            //String tokenis =  FirebaseInstanceId.getInstance().getToken();
        }
        /*End of googleplay service*/

        double width2 = size[0]*60;
        double height2 = size[1]*60;

        String url = imgurl;

        if(!url.equals(""))
        {
            Picasso.with(this).load(imgurl).transform(new CircleTransform())
                    .centerCrop()
                    .resize((int)width2,(int)height2)
                    .into(doc_pic);
        }



        doctorname.setText(ful_name);
        doctorlike.setText(rewiev);
        providerdistance.setText(DISTANCE);
        float ratin = Float.parseFloat(rating_1);
        invoice_driver_rating.setRating(ratin);
        invoice_driver_rating.setIsIndicator(true);


        apptaddress.setText(manager.getSavedAddress());


        PubNub_pojo pubNubPoj = gson.fromJson(manager.getPUNUB_RES(),PubNub_pojo.class);

        String ftype;

        for(int i =0; i<pubNubPoj.getMsg().getTypes().size(); i++)
        {

            if(manager.getServiceProviderNAme().equals(pubNubPoj.getMsg().getTypes().get(i).getFtype())
                    && typeid.equals(pubNubPoj.getMsg().getTypes().get(i).getType_id()))
            {

                ftype = pubNubPoj.getMsg().getTypes().get(i).getFtype();
                dynamicViewtype(ftype,pubNubPoj.getMsg().getTypes().get(i));
            }
        }

        if(Variableconstant.now_servicegroup.equals("1"))
        {
            cardaddgroup.setVisibility(View.VISIBLE);

            if(Utility.isNetworkAvailable(this))
            {
                if(pDialog!=null)
                {
                    pDialog.show();
                }
                callservicedtl();
            }
            else
            {
                alerts.showNetworkAlert(this);
            }


        }
        else
        {
            cardaddgroup.setVisibility(View.GONE);
        }

        addNewImageView();
        typeface();
    }

    private void getDefaultCard()
    {
        if(pDialog!=null)
        {
            pDialog.show();
        }
        Utility.printLog(" in side get card service ");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ent_sess_token", manager.getSession());
            jsonObject.put("ent_dev_id",manager.getDevice_Id());
            jsonObject.put("ent_cust_id", manager.getCustomerId());
            jsonObject.put("ent_date_time", Utility.dateintwtfour());
            Log.i("TAG ","THEDATAIS "+jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utility.doJsonRequest(Variableconstant.GETCARD, jsonObject, new Utility.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("The Response get card " + result);
                defaltCardServiceResponse(result);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(ScrollingActivity.this, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                if (pDialog != null) {
                    pDialog.cancel();
                    pDialog.dismiss();
                }
            }
        });
    }

    private void defaltCardServiceResponse(String result)
    {
        try {
            // Gson gson = new Gson();
            GetCard_pojo response = gson.fromJson(result, GetCard_pojo.class);
            if (response.getErrNum().equals("52") && response.getErrFlag().equals("0")) {
                if(response.getCards().size()==1)
                {

                    if(response.getDef().equals(response.getCards().get(0).getId()))
                    {
                        Bitmap cardbitmap =Utility.setCreditCardLogo(response.getCards().get(0).getType(),ScrollingActivity.this);

                        String cardNo = response.getCards().get(0).getLast4();
                        cardId = response.getCards().get(0).getId();

                        String card = "**** **** **** " + " " + cardNo;
                        card_no_Tv.setText(card);
                        SelectedCard=cardId;
                        payment_type = "2";
                        iv_payment.setImageBitmap(cardbitmap);

                    }
                }
                else
                {
                    choose_payment_screen.setVisibility(View.GONE);
                    Intent cardsIntent = new Intent(ScrollingActivity.this, ChangeCardActivity.class);
                    startActivityForResult(cardsIntent, 1);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
                    getSupportActionBar().show();
                }

            } else {
                Toast.makeText(ScrollingActivity.this, response.getErrMsg(), Toast.LENGTH_LONG).show();

            }
            if (pDialog != null) {
                pDialog.cancel();
                pDialog.dismiss();
            }
        }
        catch (Exception e) {
            if (pDialog != null) {
                pDialog.cancel();
                pDialog.dismiss();
            }
            e.printStackTrace();
            Utility.printLog("" + e);
        }
    }

    private void typeface() {

            TextView apptaddtxv  = (TextView) findViewById(R.id.apptaddtxv);
            TextView tvaddaddgroup = (TextView) findViewById(R.id.tvaddaddgroup);
            TextView promocodetv = (TextView) findViewById(R.id.promocodetv);
            TextView chosepaymnt = (TextView) findViewById(R.id.chosepaymnt);
            TextView jobdetails = (TextView) findViewById(R.id.jobdetails);
            TextView addphoto = (TextView) findViewById(R.id.addphoto);
            TextView tvfeestimate = (TextView) findViewById(R.id.tvfeestimate);
            TextView tvminimumfee = (TextView) findViewById(R.id.tvminimumfee);
            TextView tvtotlfare = (TextView) findViewById(R.id.tvtotlfare);
            TextView tvdisfare = (TextView) findViewById(R.id.tvdisfare);
            apptaddress.setTypeface(regularfont);
            apptcngaddtxv.setTypeface(regularfont);
            addaddgroup.setTypeface(regularfont);
            tvaddaddgroup.setTypeface(regularfont);
            apptaddtxv.setTypeface(regularfont);
            promocodetv.setTypeface(regularfont);
            calc_txt_Prise.setTypeface(regularfont);
            appbutton.setTypeface(regularfont);
            chosepaymnt.setTypeface(regularfont);
            card_no_Tv.setTypeface(regularfont);
            paymentbuton.setTypeface(regularfont);
            jobdetails.setTypeface(regularfont);
            jobdtltxt.setTypeface(regularfont);
            addphoto.setTypeface(regularfont);
            tvfeestimate.setTypeface(regularfont);
            tvminimumfee.setTypeface(regularfont);
            tvminimumfeeamount.setTypeface(regularfont);
            tvhourlyfee.setTypeface(regularfont);
            tvhourlymount.setTypeface(regularfont);
            tvdisfare.setTypeface(regularfont);
            tvdisamount.setTypeface(regularfont);
            tvtotlfare.setTypeface(lightbold);
            tvtotlamount.setTypeface(lightbold);
            confirmbooktv.setTypeface(regularfont);
            payment_cancel.setTypeface(regularfont);
            payment_card.setTypeface(regularfont);
            payment_cash.setTypeface(regularfont);
            doctorname.setTypeface(regularfont);

        cancel_booking.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v)
            {

                if(bid!=null && !bid.equals(""))
                {
                    cancelapptservice("");
                }

                return false;
            }
        });

    }

    private void cancelapptservice(String reason)
    {
        if (Utility.isNetworkAvailable(this))
        {
            if(pDialog!=null)
            {
                pDialog.show();
            }



            FormBody requestBody=new FormBody.Builder()
                    .add("ent_sess_token", manager.getSession())
                    .add("ent_dev_id", manager.getDevice_Id())
                    .add("ent_bid", bid)
                    .add("ent_reason", reason)
                    .add("ent_date_time", Utility.dateintwtfour())
                    .build();
            okio.Buffer sink = new okio.Buffer();

            try {
                requestBody.writeTo(sink);
                byte[] barry = sink.readByteArray();
                String req = new String(barry);
              //  Log.d(TAG, "requestcancel " + req);
            } catch (Exception e) {
                e.printStackTrace();
            }

            OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "cancelAppointment", requestBody, new OkHttp3Request.JsonRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(pDialog!=null)
                    {
                        pDialog.dismiss();
                    }

                    cancelResponce(result);
               //     Log.d(TAG, "ALIresponcecancel " + result);
                }

                @Override
                public void onError(String error)
                {
                    if(pDialog!=null)
                    {
                        pDialog.dismiss();
                    }

                }
            });

        } else {

            Toast.makeText(this,getResources().getString(R.string.nointernet),Toast.LENGTH_SHORT).show();
        }
    }

    private void cancelResponce(String result) {

        Cancellation_java cancellationJava = gson.fromJson(result, Cancellation_java.class);

        if (cancellationJava.getErrFlag().equals("0"))// && cancellationJava.getErrNum().equals("43")
        {
            rrloutbooking.setVisibility(View.GONE);
            maincontn_scrol.setVisibility(View.VISIBLE);
            rrlscrollmain.setVisibility(View.VISIBLE);
            waveDrawable.stopAnimation();
            apptcancel = true;
            appBarLayout.setVisibility(View.VISIBLE);
            counter=0;
            makefree(pid);
            countDownTimer.cancel();
        //    Log.d(TAG,"CANCLECONTEXT "+getContext());
            if(activityIsRunin)
            {
                callAlertDialog("Booking cancelled");
            }
        }

    }

    private void callAlertDialog(String message)
    {
       new AlertDialog.Builder(ScrollingActivity.this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(""+message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        Intent intent = new Intent(ScrollingActivity.this, MenuActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        dialog.dismiss();

                    }
                })

                .setIcon(R.drawable.ic_launcher)
                .show();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            if(!allowforback)
                cancelapptservice("");
            else
                onBackPressed();

        }
        return super.onKeyDown(keyCode, event);
    }

    private void callservicedtl()
    {


        FormBody requestform = new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_catid",typeid)
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "getAllServices", requestform, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {

                if(pDialog!=null)
                {
                    pDialog.dismiss();
                }

             //   Log.d(TAG,"groupservices "+result);
                allservicedtlpojo = gson.fromJson(result,AllServiceDtl.class);
                if(allservicedtlpojo.getErrFlag().equals("0") && allservicedtlpojo.getErrNum().equals("21"))
                {
                    gettingroup();
                    if(allservicedtlpojo.getData().size()>0)
                    {
                        for(int i = 0;i<allservicedtlpojo.getData().size();i++)
                        {
                            if(allservicedtlpojo.getData().get(i).getCmand().equals("1"))
                            {
                                groupcounter++;
                            }
                        }
                    }
                }else if(("1").equals(allservicedtlpojo.getErrFlag()) && (("83").equals(allservicedtlpojo.getErrNum())||("7").equals(allservicedtlpojo.getErrNum()) ))
                {
                    Toast.makeText(ScrollingActivity.this,allservicedtlpojo.getErrMsg(),Toast.LENGTH_SHORT).show();
                    Utility.setMAnagerWithBID(ScrollingActivity.this,manager);
                }
                else
                {
                    Toast.makeText(ScrollingActivity.this,allservicedtlpojo.getErrMsg(),Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onError(String error)
            {
                if(pDialog!=null)
                {
                    pDialog.dismiss();
                }

            }
        });


    }


    private void addNewImageView()
    {

        glv = null;
        glvdlt = null;
        if(count < 4)
        {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            final View inflaterv = inflater.inflate(R.layout.testimageclasssingle, lloutjob,false);
            final ImageView imageview = (ImageView) inflaterv.findViewById(R.id.imageview);
            ImageView ivdelete = (ImageView) inflaterv.findViewById(R.id.ivdelete);

            glv = imageview;
            imageview.setTag(glv);
            glvdlt = ivdelete;
            imageview.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        // Marshmallow+
                        myPermissionConstantsArrayList = new ArrayList<>();
                        myPermissionConstantsArrayList.clear();

                        myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
                        myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
                        myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);

                        if (AppPermissionsRunTime.checkPermission(ScrollingActivity.this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE)) {
                            //TODO: if all permissions are already granted

                            callDialogue();

                        }
                    } else {
                        callDialogue();
                    }
                }
            });

            ivdelete.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    lloutjob.removeView(inflaterv);
                    count--;
                    if(count==3)
                    {
                        addNewImageView();
                    }

                }
            });
            lloutjob.addView(inflaterv);
        }

    }

    private void dynamicViewtype(String ftype,final PubNubType pubNubType)
    {
        switch (ftype)
        {
            case  "Hourly":


                ArrayList<String> range = new ArrayList<>();
                SpinerAdaptor spinerAdaptor= new SpinerAdaptor(this,range);
                total = Double.parseDouble(pubNubType.getPrice_min());
                hourlyfee = Double.parseDouble(pubNubType.getPrice_min());

                spinerhourlyquant.setAdapter(spinerAdaptor);

                //   spinerhourlyquant.setSelection(Integer.parseInt(temp.getqUantity()), false);
                spinerhourlyquant.setSelection(0, false);

                relativeo_ut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        spinerhourlyquant.performClick();
                    }
                });

                spinerhourlyquant.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                    {

                        spinerhourlyquant.setSelection(position);


                          //  total = Double.parseDouble(pubNubType.getPrice_min())*(position*60);
                            total = Double.parseDouble(pubNubType.getPrice_min())*(position+1);
                            // tvhourlymount.setText("₹ " + form.format(total));
                            String hourly = currencysymbl +" "+total;
                            tvhourlymount.setText(hourly);
                            switch (selectedpromotype) {
                                case "Percent":
                                    if (promodiscount == 0.0) {
                                        double fee = Double.parseDouble(pubNubType.getVisit_fees()) + total+servicetotal;
                                        String totalfee = currencysymbl + " " + fee;
                                        tvtotlamount.setText(totalfee);
                                    } else {
                                        double fee = Double.parseDouble(pubNubType.getVisit_fees()) + total+servicetotal; //-promodiscount;

                                        double discountis = (discountpercent / 100) * fee;
                                        promodiscount = discountis;
                                        double totalafterdiscount = fee - (discountis);
                                        String totalfee = currencysymbl + " " + totalafterdiscount;
                                        tvtotlamount.setText(totalfee);
                                        @SuppressLint("DefaultLocale") String stringdiscount = String.format("%.2f", discountis);
                                        tvdisamount.setText(currencysymbl + " " + stringdiscount);
                                    }
                                    break;
                                case "Fixed":
                                    if (promodiscount == 0.0) {
                                        double fee = Double.parseDouble(pubNubType.getVisit_fees()) + total+servicetotal;
                                        String totalfee = currencysymbl + " " + fee;
                                        tvtotlamount.setText(totalfee);
                                    } else {
                                        double fee = Double.parseDouble(pubNubType.getVisit_fees()) + total+servicetotal; //-promodiscount;

                                        double totalafterdiscount = fee - (promodiscount);
                                        String totalfee = currencysymbl + " " + totalafterdiscount;
                                        tvtotlamount.setText(totalfee);
                                        @SuppressLint("DefaultLocale") String stringdiscount = String.format("%.2f", promodiscount);
                                        tvdisamount.setText(currencysymbl + " " + stringdiscount);
                                    }
                                    break;
                                default:
                                    double fee = Double.parseDouble(pubNubType.getVisit_fees()) + total+servicetotal;
                                    String totalfee = currencysymbl + " " + fee;
                                    tvtotlamount.setText(totalfee);
                                    break;
                            }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                String minifee = currencysymbl +" "+pubNubType.getVisit_fees();
                tvminimumfeeamount.setText(minifee);
                String fullstring = "Hourly @ "+currencysymbl+" "+total;
                tvhourlyfee.setText(fullstring);
                String hourlyamt = currencysymbl+ " "+total;
                tvhourlymount.setText(hourlyamt);

                if(promodiscount!=0.0)
                {
                    double fee = Double.parseDouble(pubNubType.getVisit_fees())+total+servicetotal-promodiscount;
                    String totalfee = currencysymbl+" "+fee;
                    tvtotlamount.setText(totalfee);
                }
                else
                {
                    double fee = Double.parseDouble(pubNubType.getVisit_fees())+total+servicetotal;
                    String totalfee = currencysymbl+" "+fee;
                    tvtotlamount.setText(totalfee);
                }

                break;
            case "Fixed":
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(callpopupcount==2)
            callpopupscreen();
        emitHeartbeat("1");
        if(Variableconstant.Service_group)
        {
            Variableconstant.Service_group = false;
            gettingroup();
        }

    }

    public void emitHeartbeat(String status)
    {

        JSONObject obj = new JSONObject();

        try
        {
            obj.put("status",status);
            obj.put("cid", Integer.parseInt(manager.getCustomerId()));

        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        socket.emit("CustomerStatus", obj);
    }

    private void gettingroup()
    {

        jobIds = manager.getJobIds();

        if(allservicedtlpojo!=null)
        {
            MainContainerservice.removeAllViews();
            rowservice.clear();
            servicetotal = 0.0;
            for (int i = 0; i < allservicedtlpojo.getData().size(); i++) {
                for (int j = 0; j < allservicedtlpojo.getData().get(i).getServices().size(); j++) {
                    if (jobIds != null && jobIds.contains(allservicedtlpojo.getData().get(i).getServices().get(j).getSid().trim())) {

                        ServiceGroup serviceGroup  = allservicedtlpojo.getData().get(i).getServices().get(j);
                        ServiceJson serviceJson;
                        TextView group_name, group_price;
                        LayoutInflater inflatervehicle = LayoutInflater.from(this);
                        View inflatedLayout = inflatervehicle.inflate(R.layout.addgroupsinglelayout, null, false);
                        group_name = (TextView) inflatedLayout.findViewById(R.id.group_name);
                        group_price = (TextView) inflatedLayout.findViewById(R.id.group_price);
                        group_name.setText(serviceGroup.getSname());
                        String price = getResources().getString(R.string.currencySymbol) +" "+
                                serviceGroup.getFixed_price();
                        group_price.setText(price);

                        serviceJson = new ServiceJson(serviceGroup.getSname(),
                                serviceGroup.getSid(),
                                serviceGroup.getFixed_price());

                        MainContainerservice.addView(inflatedLayout);
                        rowservice.add(serviceJson);

                    }
                }

            }
        }
        if(rowservice.size()>0)
        {
            for(ServiceJson serviceJson : rowservice)
            {
                JSONObject jsonitem = new JSONObject();
                try {
                    jsonitem.put("sname",serviceJson.getSname());
                    jsonitem.put("sid",serviceJson.getSid());
                    jsonitem.put("sprice",serviceJson.getSprice());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArray.put(jsonitem);
                servicetotal =servicetotal+ Double.parseDouble(serviceJson.getSprice());
            }

            if(totalhavetopay)
            {
                totalhavetopay = false;
                if(!tvtotlamount.getText().toString().trim().equals(""))
                {
                    totalamntpay = Double.parseDouble(tvtotlamount.getText().toString().split(" ")[1]);
                }

            }
            rlmaincontotal.setVisibility(View.VISIBLE);
            relativservfare.setVisibility(View.VISIBLE);

            @SuppressLint("DefaultLocale") String totalserviceamt = String.format("%.2f", servicetotal);

            String totalservice = currencysymbl+" "+totalserviceamt;
            grouptotl.setText(totalservice);
            tvserviceamount.setText(totalservice);

            String total = currencysymbl+" "+(totalamntpay+servicetotal);
            tvtotlamount.setText(total);
            addaddgroup.setText(getResources().getString(R.string.editservice));
        }
        else
        {
            rlmaincontotal.setVisibility(View.GONE);
            relativservfare.setVisibility(View.GONE);
            if(!tvtotlamount.getText().toString().trim().equals(""))
            {
                String total = currencysymbl+" "+(Double.parseDouble(tvtotlamount.getText().toString().split(" ")[1])+servicetotal);
                tvtotlamount.setText(total);
            }
            else {
                String total = currencysymbl+" "+servicetotal;
                tvtotlamount.setText(total);
            }
            addaddgroup.setText(getResources().getString(R.string.addservice));
        }
    }

    @Override
    public void onClick(View v)
    {

        switch (v.getId())
        {

            case R.id.addaddgroup:
                viewServiceDtl();
                break;

            case R.id.appbutton:

                if(appbutton.getText().toString().equals(getResources().getString(R.string.apply)))
                {
                    if (Utility.isNetworkAvailable(this)) {
                        if (!calc_txt_Prise.getText().toString().trim().equals("")) {

                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(calc_txt_Prise.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                            Backgrounddoctorcheckpromocode(calc_txt_Prise.getText().toString());
                        } else {
                            Utility.ShowAlert("Please Enter the Promo Code", this);
                        }
                    } else {
                        alerts.showNetworkAlert(this);
                    }
                }
                else if(appbutton.getText().toString().equals(getResources().getString(R.string.clearpromocode)))
                {
                    appbutton.setText(getResources().getString(R.string.apply));
                    String splittxt[] = tvtotlamount.getText().toString().split(" ");
                    relativdisfare.setVisibility(View.GONE);
                    double amount = Double.parseDouble(splittxt[1])+promodiscount;
                    tvtotlamount.setText(currencysymbl+ " "+amount);
                    promodiscount = 0.0;
                    calc_txt_Prise.setText("");
                    selectedpromotype = "";
                }

                break;
            case R.id.confirmbooktv:

                if(buttonclicked ==0)
                {
                    buttonclicked++;
                    confirmbooktv.setBackgroundColor(getResources().getColor(R.color.bluelogincolor));

                    if(Utility.isNetworkAvailable(this))
                    {
                        if(!apptaddress.getText().toString().trim().equals(""))
                        {
                            if(pDialog!=null)
                            {
                                pDialog.setMessage(getString(R.string.booking));
                                pDialog.show();
                            }
                            if(Variableconstant.ent_forlaterbooking==4)
                            {

                                sendrequestessage(manager.getJOBLATI(), manager.getJOBLONGI(), Variableconstant.ent_forlaterbooking, Variableconstant.ent_dt);
                            }
                            else
                            {
                                sendrequestessage(manager.getJOBLATI(), manager.getJOBLONGI(), Variableconstant.ent_btype, Utility.dateintwtfour());
                            }

                        }
                        else
                        {
                            Toast.makeText(this,"Please Enter Address",Toast.LENGTH_SHORT).show();
                            buttonclicked = 0;
                            confirmbooktv.setBackgroundColor(getResources().getColor(R.color.actionbar_color));

                        }


                    }
                    else
                    {
                        alerts.showNetworkAlert(this);
                        buttonclicked = 0;
                        confirmbooktv.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
                    }
                }
                break;

            case R.id.apptcngaddtxv:
                Intent intnt = new Intent(this,Address_Confirm.class);
                startActivityForResult(intnt,addressrequestcode);
                break;
            case R.id.paymentbuton:
                choose_payment_screen.setVisibility(View.VISIBLE);
                appBarLayout.setVisibility(View.GONE);
                break;

            case R.id.payment_cash:
                choose_payment_screen.setVisibility(View.GONE);
                appBarLayout.setVisibility(View.VISIBLE);
                payment_type = "1";
                iv_payment.setImageResource(R.drawable.confirmation_cash_icon);
                break;

            case R.id.payment_cancel:
                choose_payment_screen.setVisibility(View.GONE);
                appBarLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.payment_card:
                getDefaultCard();
                choose_payment_screen.setVisibility(View.GONE);
                appBarLayout.setVisibility(View.VISIBLE);
              /*  choose_payment_screen.setVisibility(View.GONE);
                Intent cardsIntent = new Intent(ScrollingActivity.this, ChangeCardActivity.class);
                startActivityForResult(cardsIntent, 1);
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
                appBarLayout.setVisibility(View.VISIBLE);*/
                break;
        }
    }
    private void viewServiceDtl()
    {
        if(allservicedtlpojo!=null && allservicedtlpojo.getData().size()>0)
        {
            Intent intent = new Intent(this,ServiceGroupDetials.class);
            intent.putExtra("DATA",allservicedtlpojo.getData());
            startActivity(intent);
        }
    }
    private void Backgrounddoctorcheckpromocode(String s)
    {
        if(pDialog!=null) {

            pDialog.setMessage("Checking promocode");
            pDialog.show();
        }
        FormBody requestBody=new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_coupon", s)
                .add("ent_lat", manager.getJOBLATI())
                .add("ent_long", manager.getJOBLONGI())
                .add("ent_date_time", Utility.dateintwtfour())
                .build();
        OkHttp3Request.doOkHttp3Request(Variableconstant.CHECKCOUPON, requestBody, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                if(pDialog!=null) {
                    pDialog.dismiss();
                }

                try {
                    Gson gson = new Gson();
                    promoresponce = gson.fromJson(result, PromoCodepojo.class);

                    if(promoresponce.getErrFlag().equals("0"))
                    {
                        appbutton.setText(getResources().getString(R.string.clearpromocode));
                        String splittxt[] = tvtotlamount.getText().toString().split(" ");
                        switch (promoresponce.getDtype()) {
                            case "Percent":
                                relativdisfare.setVisibility(View.VISIBLE);
                                selectedpromotype = "Percent";
                                double discountamtinper = Double.parseDouble(promoresponce.getDis());
                                discountpercent = discountamtinper;
                                double discountis = (discountamtinper / 100) * Double.parseDouble(splittxt[1]);
                                promodiscount = discountis;
                                double discount = Double.parseDouble(splittxt[1]) - (discountis);
                                tvtotlamount.setText(currencysymbl + " " + discount);
                                @SuppressLint("DefaultLocale") String stringdiscount = String.format("%.2f", discountis);
                                tvdisamount.setText(currencysymbl + " " + stringdiscount);
                                break;
                            case "Fixed":
                                relativdisfare.setVisibility(View.VISIBLE);
                                selectedpromotype = "Fixed";
                                promodiscount = Double.parseDouble(promoresponce.getDis());
                                double discountfied = Double.parseDouble(splittxt[1]) - (promodiscount);
                                tvtotlamount.setText(currencysymbl + " " + discountfied);
                                @SuppressLint("DefaultLocale") String stringdiscountamt = String.format("%.2f", promodiscount);
                                tvdisamount.setText(currencysymbl + " " + stringdiscountamt);

                                break;
                            default:
                                payment_type = "1";
                                break;
                        }
                        alerts.promoAlert(ScrollingActivity.this, promoresponce.getErrMsg());
                    }
                    else if(("1").equals(promoresponce.getErrFlag()) && (("83").equals(promoresponce.getErrNum())||("7").equals(promoresponce.getErrNum()) ))
                    {

                        Utility.setMAnagerWithBID(ScrollingActivity.this,manager);
                    }
                    else
                    {
                        Toast.makeText(ScrollingActivity.this,promoresponce.getErrMsg(),Toast.LENGTH_SHORT).show();
                    }

                    if (pDialog != null) {
                        pDialog.cancel();
                        pDialog.dismiss();
                    }
                }
                catch (Exception e) {
                    if (pDialog != null) {
                        pDialog.cancel();
                        pDialog.dismiss();
                    }
                    e.printStackTrace();
                    Utility.printLog("" + e);
                }
            }

            @Override
            public void onError(String error) {

                Utility.printLog("error in add card " + error);
                if(pDialog!=null)
                {
                    pDialog.dismiss();
                }

            }
        });

    }
    private void callDialogue()
    {
        clearOrCreateDir();

        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(ScrollingActivity.this);

        // Setting Dialog Message
        alertDialog2.setMessage(getResources().getString(R.string.selecto_photo));

        // Setting Positive "Yes" Btn
        alertDialog2.setPositiveButton(getResources().getString(R.string.gallery),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        openGallery();
                    }
                });

        // Setting Negative "NO" Btn
        alertDialog2.setNegativeButton(getResources().getString(R.string.camera),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                });

        // Showing Alert Dialog
        alertDialog2.show();
    }

    private void openGallery()
    {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }
    private void takePicture()
    {
        try
        {
            state = Environment.getExternalStorageState();
            takenNewImage = System.currentTimeMillis()+".png";
            if (Environment.MEDIA_MOUNTED.equals(state))
                newFile = new File(jobpicDirectry,takenNewImage);
            else
                newFile = new File(getFilesDir()+"/"+ Variableconstant.PARENT_FOLDER+"/Job_Pics/",takenNewImage);
            if(Build.VERSION.SDK_INT>=N)
                newProfileImageUri = FileProvider.getUriForFile(this,BuildConfig.APPLICATION_ID + ".provider",newFile);
            else
                newProfileImageUri = Uri.fromFile(newFile);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, newProfileImageUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent,REQUEST_CODE_TAKE_PICTURE);
        }
        catch (ActivityNotFoundException e)
        {
            Utility.printLog("profile fragment cannot take picture: " + e);
        }

    }

    private void liveBooking()
    {

        String jObPics;
        if(pIctureTaken)
        {
            jobPicFiles = jobpicDirectry.listFiles();
            jObPics = jobPicFiles.length+"";
        }
        else
        {
            jObPics = "";
        }
        String pricetopay;

        if(!tvtotlamount.getText().toString().trim().equals(""))
        {
            String splittxt[] = tvtotlamount.getText().toString().split(" ");
            pricetopay = splittxt[1];
        }
        else
        {
            pricetopay = "";
        }

        String proId;
        if(Variableconstant.ent_btype==3)
        {
            proId = "";
        }
        else
        {
            proId=pid;
        }
        String solt;

        if(Variableconstant.comingfromlaterbooking)
        {
            solt = soltid;
        }
        else
        {
            solt = "";
        }

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("ent_sess_token", manager.getSession());
            jsonObj.put("ent_dev_id",manager.getDevice_Id());
            jsonObj.put("ent_custid",manager.getCustomerId());
            jsonObj.put("ent_btype", Variableconstant.ent_btype);
            jsonObj.put("ent_proid",proId);
            jsonObj.put("ent_slot_id",solt);
            jsonObj.put("ent_dtype",1);
            jsonObj.put("ent_a1", apptaddress.getText().toString().trim());
            jsonObj.put("ent_a2",landmark+"");
            jsonObj.put("ent_lat",manager.getJOBLATI()+"");
            jsonObj.put("ent_long",manager.getJOBLONGI()+"");
            jsonObj.put("ent_cat_id",typeid);
            jsonObj.put("ent_card_id",cardId);
            jsonObj.put("ent_job_imgs",jObPics);
            jsonObj.put("ent_job_details",jobdtltxt.getText().toString());
            jsonObj.put("ent_pymt",payment_type);
            jsonObj.put("ent_pymt",payment_type);
            jsonObj.put("ent_coupon",calc_txt_Prise.getText().toString().trim());
            jsonObj.put("ent_hourly_amt",hourlyfee);
            jsonObj.put("ent_services",jsonArray);
            jsonObj.put("ent_cat_name",manager.getServiceProvdr());
            jsonObj.put("ent_total",pricetopay);
            jsonObj.put("ent_date_time", Utility.dateintwtfour());
         //   Log.d(TAG,"ScrollLIVErequest "+jsonObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utility.doJsonRequest(Variableconstant.LIVEBOOKING, jsonObj, new Utility.JsonRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
             //   Log.d(TAG, "Scrollingresponcelive " + result);
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                masterdtlsucces(result);
            }
            @Override
            public void onError(String error) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
            }
        });
    }

    private void masterdtlsucces(String result)
    {
        try
        {
            if(result!=null) {
                liveBookingPojo = gson.fromJson(result, LiveBookingPojo.class);
                if (liveBookingPojo != null) {
                    if (liveBookingPojo.getErrFlag().equals("0")) {
                        bid = liveBookingPojo.getBid();
                        if (Variableconstant.ent_btype == 3) {
                            rrloutbooking.setVisibility(View.VISIBLE);
                            maincontn_scrol.setVisibility(View.GONE);
                            rrlscrollmain.setVisibility(View.GONE);
                            appBarLayout.setVisibility(View.GONE);

                            /*
                             * Checking the android version to avoid the deprecation problem..
                             * <h1>Note :</h1>
                             * {@code setBackgroundDrawable()} this method is deprecated method after  android JELLY_BEAN version .
                             * to avoid the deprecation problem first checking the SDK version then applying the method according
                             * to the required SDK version method.*/
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                rrwaveout.setBackgroundDrawable(waveDrawable);
                            } else {
                                rrwaveout.setBackground(waveDrawable);
                            }

                            /*
                             * Defining a LinearInterpolator animation object for doing animation.
                             * and passing that object to the Wave animator class.
                             * @see LinearInterpolator
                             * @see WaveDrawable */
                            interpolator = new LinearInterpolator();
                            waveDrawable.setWaveInterpolator(interpolator);
                            waveDrawable.startAnimation();
                            liveBookingPojo.getBid();
                            allowforback = false;
                            countDownTimer = new CountDownTimer(30000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    if (counter == 0) {
                                        counter++;
                                        sendMessageForLiveBooking(liveBookingPojo.getBid());
                                    }
                                }
                                public void onFinish() {
                                    rrloutbooking.setVisibility(View.GONE);
                                    maincontn_scrol.setVisibility(View.VISIBLE);
                                    rrlscrollmain.setVisibility(View.VISIBLE);
                                    waveDrawable.stopAnimation();
                                    appBarLayout.setVisibility(View.VISIBLE);
                                    counter = 0;
                                    makefree(pid);
                                    if(activityIsRunin)
                                    {
                                        callAlertDialog(doctorname.getText().toString() + " is currently busy");
                                    }
                                }
                            }.start();
                        } else if (Variableconstant.ent_btype != 3)
                        {
                            sendMessageForLaterBooking(liveBookingPojo.getBid());
                        }
                        if (pIctureTaken) {
                            state = Environment.getExternalStorageState();
                            jobPicFiles = jobpicDirectry.listFiles();
                            boolean isavailable;
                            for (int j = 0; j < jobPicFiles.length; j++) {
                                File newRenamefile;
                                if (Environment.MEDIA_MOUNTED.equals(state)) {


                                    File f = new File(jobpicDirectry + "/" + jobPicFiles[j].getName());
                                    newRenamefile = new File(jobpicDirectry, liveBookingPojo.getBid() + "_" + j + ".png");
                                    isavailable = f.renameTo(newRenamefile);
                                    if (isavailable) {

                                        uploadToAmazon(newRenamefile);
                                    }
                                } else {

                                    newRenamefile = new File(jobpicDirectry, liveBookingPojo.getBid() + "_" + j + ".png");
                                    isavailable = jobpicDirectry.renameTo(newRenamefile);
                                    if (isavailable) {
                                        uploadToAmazon(newRenamefile);
                                    }
                                }
                            }
                        }
                    } else if (("1").equals(liveBookingPojo.getErrFlag()) && (("83").equals(liveBookingPojo.getErrNum())||("7").equals(liveBookingPojo.getErrNum()) ))
                    {
                        Utility.setMAnagerWithBID(ScrollingActivity.this,manager);


                    } else {
                        allowforback = true;
                        Toast.makeText(this, liveBookingPojo.getErrMsg(), Toast.LENGTH_SHORT).show();
                        buttonclicked = 0;
                        confirmbooktv.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
                        if(activityIsRunin)
                        {
                            callAlertDialog("We cant book now, come latter");
                        }
                    }
                }
                else
                Toast.makeText(this, "We cant book now, come latter", Toast.LENGTH_SHORT).show();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    private void makefree(String pid)
    {
        JSONObject sendText = new JSONObject();
        try{
            sendText.put("proid", pid);
          //  Log.d("makefree", " chanel " + sendText.toString() +"pro index "+pid);
            ApplicationController.getInstance().emit("makefree",sendText);
        }catch(JSONException e)
        {
            e.printStackTrace();
        }
    }
    private void sendMessageForLaterBooking(String bid)
    {
        JSONObject sendText = new JSONObject();
        try{
            sendText.put("cid",manager.getCustomerId());
            sendText.put("bid",bid);
            sendText.put("proid", pid);
            sendText.put("stime",sTime);
            sendText.put("etime",eTime);
            socket.emit("LaterBooking", sendText);
        }catch(JSONException e)
        {

            e.printStackTrace();
        }
        Variableconstant.comgfrmConfirmscrn = true;
        jobIds.clear();
        manager.setJobIds(jobIds);
        if(activityIsRunin)
        {
            callAlertDialog("Thank you your appointment is sent to provider");
        }
      //  checkforPermission();

    }

    private void checkforPermission()
    {
        if(Build.VERSION.SDK_INT>=23)
        {
            myPermissionConstantsArrayList = new ArrayList<>();
            myPermissionConstantsArrayList.clear();

            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_CALENDAR);
            myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSSION_READ_CALENDAR);


            if (AppPermissionsRunTime.checkPermission(ScrollingActivity.this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_CALENDR)) {
                //TODO: if all permissions are already granted

                createReminder();

            }
        } else {
            createReminder();
        }
    }

    private void sendMessageForLiveBooking(String bid)
    {

        JSONObject sendText = new JSONObject();
        try{
            sendText.put("cid",manager.getCustomerId());
            sendText.put("bid",bid);
            sendText.put("proid", pid);
            sendText.put("btype", Variableconstant.ent_btype);
            sendText.put("dt", Utility.dateintwtfour());
            socket.emit("LiveBooking", sendText);
        }catch(JSONException e)
        {

            e.printStackTrace();
        }

    }
    private void sendrequestessage(String latitude, String logitude, int btype, String datetime)
    {
        JSONObject sendText = new JSONObject();
        try
        {
            sendText.put("email",manager.getCoustomerEmail());
            sendText.put("lat",latitude);
            sendText.put("long",logitude);
            sendText.put("btype",btype);
            sendText.put("dt",datetime);
            socket.emit("UpdateCustomer", sendText);
          //  Log.d(TAG,"MATCHEDIDPIDUpdateCustomer "+sendText);
        }catch(JSONException e)
        {
            e.printStackTrace();
        }
    }
    private Emitter.Listener handleIncomingCustomer = new Emitter.Listener() {
        @Override
        public void call(Object... args)
        {

            JSONObject data = (JSONObject)args[0];
            manager.setSocketRes(data.toString());
            PubNub_pojo nubPojo = gson.fromJson(data.toString(),PubNub_pojo.class);
            boolean isfound = false;
         //   Log.d(TAG,"MATCHEDIDPID "+pid+" socket msg "+data);
            for(int i= 0;i<nubPojo.getMsg().getMasArr().size();i++)
            {
                for(int j=0;j<nubPojo.getMsg().getMasArr().get(i).getMas().size();j++)
                {
                    if(pid.equals(nubPojo.getMsg().getMasArr().get(i).getMas().get(j).getPid()))
                    {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    calllivebookingmethod();
                           //         Log.d(TAG,"MATCHEDID "+pid);
                                }
                            });

                        isfound = true;
                        break;
                    }
                }
                if(isfound)
                {
                    break;
                }
            }
            if(!isfound)
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(activityIsRunin)
                        {
                            if(pDialog!=null)
                            {
                                pDialog.dismiss();
                                pDialog.cancel();
                            }
                            callAlertDialog(getResources().getString(R.string.sorrythisproviderisnotavailablenow));
                        }

                        manager.setSocketRes("");
                    }
                });

            }
        }
    };
    private Emitter.Listener  handleIncomingMessages = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            ScrollingActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  //  Log.d(TAG, "Scrolllivebookingresponce " + args[0].toString());
                    JSONObject data = (JSONObject) args[0];
                    try {
                        String bStatus = data.getString("st");
                        sendmessageBookingAck(bStatus);

                        /*
                         * check for pro rejection and increment the pro iNdex
                         */
                        if (bStatus.equals("3"))
                        {
                            jobIds.clear();
                            rrloutbooking.setVisibility(View.GONE);
                            maincontn_scrol.setVisibility(View.VISIBLE);
                            rrlscrollmain.setVisibility(View.VISIBLE);
                            waveDrawable.stopAnimation();
                            counter=0;
                            countDownTimer.cancel();
                            manager.setJobIds(jobIds);
                           if(activityIsRunin)
                           {
                               callAlertDialog("Provider rejected the Booking !");
                           }

                        }
                        else if(bStatus.equals("2"))
                        {
                            rrloutbooking.setVisibility(View.GONE);
                            maincontn_scrol.setVisibility(View.VISIBLE);
                            rrlscrollmain.setVisibility(View.VISIBLE);
                            waveDrawable.stopAnimation();
                            counter=0;
                            countDownTimer.cancel();
                            if(!apptcancel)
                            {
                                if(callpopupcount==0)
                                {
                                    callpopupscreen();
                                }

                            }
                        }

                    } catch (JSONException e)
                    {
                        e.printStackTrace();

                    }
                }
            });
        }
    };
    private void calllivebookingmethod()
    {
            if(groupcounter>0)
            {
                int temp = 0;
                for (DataService dataService: allservicedtlpojo.getData())
                {
                    boolean isManSelctd = false;
                    if("1".equals(dataService.getCmand()))
                    {
                        for (ServiceGroup serviceGroup: dataService.getServices())
                        {
                            boolean isIdFound = false;
                            for(String currentItemId :  jobIds)
                            {
                                if(currentItemId.equals(serviceGroup.getSid()))
                                {
                                    isIdFound = true;
                                    break;
                                }
                            }
                            if(isIdFound)
                            {
                                isManSelctd = true;
                                break;
                            }
                        }

                        if(isManSelctd)
                        {
                            temp++;
                        }
                        else
                        {
                            //TODO: show ur toast
                            if(pDialog!=null)
                            {
                                pDialog.dismiss();
                            }
                            Toast.makeText(this,"Please select mandatory services",Toast.LENGTH_SHORT).show();
                            buttonclicked = 0;
                            confirmbooktv.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
                            break;
                        }
                    }
                }
                if(groupcounter == temp)
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            liveBooking();
                        }
                    });

                }
            }
            else
            {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        liveBooking();
                    }
                });
            }
    }
    private void callpopupscreen()
    {
        callpopupcount = 1;
        AlertDialog.Builder builder = new AlertDialog.Builder(ScrollingActivity.this);
        builder.setTitle(getResources().getString(R.string.thanku));
        builder.setCancelable(false);
        String fname = manager.getCustomerFnme().substring(0, 1).toUpperCase() + manager.getCustomerFnme().substring(1);
        String message = getResources().getString(R.string.hey)+" \""+fname+"\" "+getResources().getString(R.string.yourjobrequest)
                +" \""+doctorname.getText().toString().trim()+"\" "+getResources().getString(R.string.hewillbeonthewayshortly);
        builder.setMessage(message);
        builder.setPositiveButton(ScrollingActivity.this.getResources().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                jobIds.clear();
                manager.setJobIds(jobIds);
                Variableconstant.comgfrmConfirmscrn = true;
                Intent intent = new Intent(ScrollingActivity.this,MenuActivity.class);
                startActivity(intent);
                finish();
                dialog.dismiss();
            }
        });
        dialogg = builder.create();
        dialogg.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        try {
            dialogg.show();
        } catch(Exception e){
         e.printStackTrace();
        }
    }
    private void sendmessageBookingAck(String bStatus)
    {
        JSONObject sendText = new JSONObject();
        try{
            sendText.put("bStatus",bStatus);
            socket.emit("LiveBookingAck", sendText);
        }catch(JSONException e)
        {
            e.printStackTrace();
        }
    }
    private void uploadToAmazon(File image)
    {
        upload.Upload_data(Variableconstant.AmazonJobbucket, image, new UploadAmazonS3.Upload_CallBack() {
            @Override
            public void sucess(String sucess) {
            }
            @Override
            public void error(String errormsg) {
            }
        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode != RESULT_OK)
        {
            return;
        }
        // Bitmap bitmap;
        switch (requestCode)
        {
            case addressrequestcode:
                if(data!=null)
                {
                    String aRea = data.getExtras().getString("area");
                    landmark = data.getExtras().getString("landmark");
                    assert aRea != null;
                    if(!aRea.equals(""))
                    {
                        String fulladd =  data.getExtras().getString("area")+"\n"+data.getExtras().getString("city")+
                                "\n"+data.getExtras().getString("addrestype");
                        apptaddress.setText(fulladd);
                    }
                    else {
                        String fulladd =  data.getExtras().getString("city")+
                                "\n"+data.getExtras().getString("addrestype");
                        apptaddress.setText(fulladd);
                    }
                    manager.setJOBLATI(data.getExtras().getString("Latitud"));
                    manager.setJOBLONGI(data.getExtras().getString("Longitud"));
                }
                break;

            case REQUEST_CODE_GALLERY:
                try {
                    state = Environment.getExternalStorageState();
                    takenNewImage = System.currentTimeMillis()+".png";

                    if (Environment.MEDIA_MOUNTED.equals(state)) {
                        newFile = new File(jobpicDirectry, takenNewImage);
                    } else {
                        newFile = new File(getFilesDir() + "/" + Variableconstant.PARENT_FOLDER+ "/Job_Pics/", takenNewImage);
                    }
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(newFile);
                    Utility.copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                    newProfileImageUri = Uri.fromFile(newFile);
                    startCropImage();
                } catch (Exception e) {
                    Utility.printLog("SingUp_page error is as follows" + e);
                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:

                    startCropImage();

                break;
            case REQUEST_CODE_CROP_IMAGE:

                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                Log.d(TAG, "path fileOutputStream " + path);
                if(path == null)
                {
                    return;
                }
                isComingFromoncreat = false;
                pIctureTaken = true;
                Picasso.with(this).load(Uri.fromFile(newFile))
                        .skipMemoryCache()
                        .resize(glv.getWidth(), glv.getHeight())
                        .centerCrop()
                        .into(glv);
                glv.setClickable(false);
                glvdlt.setVisibility(View.VISIBLE);

                count++;
                addNewImageView();
                break;
        }
        if (requestCode == 1) {

            if(data.getExtras()!=null) {


                String cardNo = data.getStringExtra("cardNo");
                cardId = data.getStringExtra("cardID");

                Bitmap cardbitmap = data.getParcelableExtra("cardImage");
                String cardis = "**** **** **** " + " " + cardNo;
                card_no_Tv.setText(cardis);
                SelectedCard=cardId;
                payment_type = "2";
                iv_payment.setImageBitmap(cardbitmap);

                boolean changeDefault=false;

                while(!changeDefault)
                {
                    if(Utility.isNetworkAvailable(ScrollingActivity.this))
                    {
                       // new BackgrounddoctorChangecard().execute();
                        changeDefault=true;
                    }
                    else
                    {
                        Utility.ShowAlert("Check the Internet Connect",ScrollingActivity.this);
                    }
                }
            }
        }

    }

    private void startCropImage()
    {
        Intent intent = new Intent(ScrollingActivity.this,CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH,newFile.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }
      /*creat or delete directory*/
    private void clearOrCreateDir()
    {
        try
        {
            state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state))
            {
                jobpicDirectry = new File(Environment.getExternalStorageDirectory()+"/"+ Variableconstant.PARENT_FOLDER+"/Job_Pics");
                Utility.printLog("in if condotion profilepicsDir di" + jobpicDirectry);
            }
            else
            {
                jobpicDirectry = new File(this.getFilesDir()+"/"+ Variableconstant.PARENT_FOLDER+"/Job_Pics");
                Utility.printLog("in else condotion profilepicsDir di" + jobpicDirectry);
            }
            if(!jobpicDirectry.exists() && !jobpicDirectry.isDirectory())
            {
                jobpicDirectry.mkdirs();
                Utility.printLog("profile fragment profilePicsDir is created:" + jobpicDirectry);
            }
            else
            {
                if(isComingFromoncreat)
                {
                    File[] jobPicsDirectory = jobpicDirectry.listFiles();

                    if(jobPicsDirectory.length > 0)
                    {
                        for (File aJobPicsDirectory : jobPicsDirectory) {
                            aJobPicsDirectory.delete();
                        }
                    }
                    else
                    {
                        Utility.printLog("profile fragment profilePicsDir empty  or null: " + jobPicsDirectory.length);
                    }
                }

            }
        }
        catch (Exception e)
        {
            Utility.printLog("profile fragment Error while creating newfile:" + e);
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if(dialogg!=null)
        {
            dialogg.dismiss();
            dialogg.cancel();
        }
        if(pDialog!=null)
        {
            pDialog.dismiss();
            pDialog.cancel();
        }
        if(callpopupcount==1)
            callpopupcount++;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*if(!allowforback)
        {*/
            overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
            finish();
        //}
    }
    private long getOffsetValue(String localTime)
    {
        long res1;
        String trimedOffset = localTime.trim();
        String operator = trimedOffset.charAt(0) + "";
        if (operator.equals("+")) {
            trimedOffset = trimedOffset.replace("+", "");
        } else {
            trimedOffset = trimedOffset.replace("-", "");
        }
        String min = trimedOffset.substring(2);
        String hour = trimedOffset.substring(0, 2);
        System.out.println("ALi" +
                "\t" + trimedOffset
                + "\t" + operator
                + "\t" + trimedOffset
                + "\t" + hour
                + "\t" + min
        );

        int hh = Integer.parseInt(hour.trim());
        int mm = Integer.parseInt(min.trim());

        if (operator.equals("+")) {
            res1 = (((hh * 60) + mm) * 60);
        } else {
            res1 = (((hh * 60) + mm) * 60) * (-1);
        }
        return res1;
    }
    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.US);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityIsRunin = false;
    }

    /**
     * <p>Create Reminder</p>
     * create reminder for the later booking
     */

    public void createReminder()
    {
        ContentValues event = new ContentValues();
        event.put(CalendarContract.Events.CALENDAR_ID,3);
        java.util.TimeZone timeZone = java.util.TimeZone.getDefault();
        event.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
        event.put(CalendarContract.Events.TITLE, getResources().getString(R.string.app_name));
        event.put(CalendarContract.Events.ALLOWED_REMINDERS, true);
        event.put(CalendarContract.Events.DESCRIPTION, "You have an appointment for "+manager.getServiceProvdr());
        event.put(CalendarContract.Events.EVENT_LOCATION, apptaddress.getText().toString().trim());
        Log.d(TAG, "calendrset: "+bid+" "+sTime+" "+System.currentTimeMillis());
        event.put(CalendarContract.Events.DTSTART, ((Long.parseLong(sTime.trim())*1000)-timeZone.getRawOffset()));
        event.put(CalendarContract.Events.DTEND, (Long.parseLong(sTime.trim())*1000)+3600000-timeZone.getRawOffset());
        event.put(CalendarContract.Events.ALL_DAY, 0);
        event.put(CalendarContract.Events.HAS_ALARM, 1);



        Uri baseUri;
        if (Build.VERSION.SDK_INT >= 8) {
            baseUri = Uri.parse("content://com.android.calendar/events");
        } else {
            baseUri = Uri.parse("content://calendar/events");
        }

      /*  Uri extendedPropUri = ExtendedProperties.CONTENT_URI;
        extendedPropUri = extendedPropUri.buildUpon()
                .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER,"true")
                .appendQueryParameter(Calendars.ACCOUNT_NAME, this.accountName)
                .appendQueryParameter(Calendars.ACCOUNT_TYPE, this.accountType).build();


        ContentValues extraDataValues = new ContentValues();
        extraDataValues.put(ExtendedProperties.EVENT_ID, eventId);
        extraDataValues.put(ExtendedProperties.NAME, key);
        extraDataValues.put(ExtendedProperties.VALUE, value);

        context.getContentResolver().insert(extendedPropUri, extraDataValues);*/

        Uri uri=getContentResolver().insert(baseUri, event);
        long eventID = Long.parseLong(uri.getLastPathSegment());

        if (Build.VERSION.SDK_INT >= 8) {
            baseUri = Uri.parse("content://com.android.calendar/reminders");
        } else {
            baseUri = Uri.parse("content://calendar/reminders");
        }
        event = new ContentValues();

        event.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
        event.put(CalendarContract.Reminders.EVENT_ID,  eventID);
        event.put(CalendarContract.Reminders.MINUTES, 30);
       // Uri reminderUri=getContentResolver().insert(baseUri, event);
        String bidEvent=bid+","+eventID;
        //Getting bid and event id from sharedPreference
        Gson gson = new Gson();
        String json = manager.getBIDEvent();
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        ArrayList<String> arrayList = gson.fromJson(json,type);
        if(arrayList==null){
            arrayList=new ArrayList<>();
        }
        arrayList.add(bidEvent);
        String bidEventJson = gson.toJson(arrayList);
        manager.setBIDEvent(bidEventJson);
        Log.d(TAG,"calendretEvant "+manager.getBIDEvent());




    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode)
        {
            //

            case REQUEST_CODE_PERMISSION_CALENDR:

                boolean isDenine = false;
                int i = 0;
                for (; i < grantResults.length; i++)
                {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                    {
                        isDenine = true;
                        break;
                    }

                }
                if (isDenine)
                {
                    Snackbar.
                            make(coordinatorLayout, getResources().getString(R.string.permissiondenied) + " " + REQUEST_CODE_PERMISSION_CALENDR, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else
                {
                    createReminder();
                }

                /*if (requestPermision(grantResults))
                {

                    Snackbar.
                            make(coordinatorLayout, getResources().getString(R.string.permissiondenied) + " " + REQUEST_CODE_PERMISSION_CALENDR, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else
                {

                }*/
                break;
            case REQUEST_CODE_PERMISSION_MULTIPLE:

               // if (isDenine)
                if (requestPermision(grantResults))
                {

                    Snackbar.
                            make(coordinatorLayout, getResources().getString(R.string.permissiondenied) + " " + REQUEST_CODE_PERMISSION_MULTIPLE, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else
                {
                    callDialogue();
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private boolean requestPermision(int[] grantResults)
    {
        boolean isDenine = false;
        int i = 0;
        for (; i < grantResults.length; i++)
        {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
            {
                isDenine = true;
                break;
            }

        }
        return isDenine;
    }
}
