package com.iserve.passenger;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.adapter.ServiceDetailsAdpater;
import com.google.gson.Gson;
import com.pojo.ServiceGroup;
import com.utility.SessionManager;
import com.utility.Variableconstant;
import java.util.ArrayList;

/**
 * Created by embed on 26/9/16.
 *
 */
public class ServiceDetials extends AppCompatActivity
{
    public ArrayList<ServiceGroup> serviceGroups = new ArrayList<>();
    String cmul;
    ArrayList<String> jobIds = new ArrayList<>();
    boolean isMandetory;
    ListView servicedtlst;
    FrameLayout fframelayt;
    TextView toolbarhometxv,tvaddhome;
    Toolbar toolbar;
    String TAG = "ServiceDetials";
    Gson gson;
    SessionManager manager;
    int position;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.servicedetaillist);
        gson = new Gson();
        manager = new SessionManager(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        servicedtlst = (ListView) findViewById(R.id.servicedtlst);

        tvaddhome = (TextView) findViewById(R.id.tvaddhome);
        toolbarhometxv = (TextView) findViewById(R.id.toolbarhometxv);
        Typeface regularfont = Typeface.createFromAsset(getAssets(), Variableconstant.regularfont);
        tvaddhome.setTypeface(regularfont);
        toolbarhometxv.setTypeface(regularfont);
        tvaddhome.setText(getResources().getString(R.string.Add));
        toolbarhometxv.setText(getResources().getString(R.string.addservic));
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        fframelayt.setVisibility(View.VISIBLE);
        toolbarhometxv.setVisibility(View.VISIBLE);
        tvaddhome.setVisibility(View.VISIBLE);

        if(getIntent().getExtras()!=null)
        {
            serviceGroups = (ArrayList<ServiceGroup>)getIntent().getSerializableExtra("DATA");
            cmul = getIntent().getStringExtra("DATAMULTI");
            position = getIntent().getIntExtra("PSITION",0);
            isMandetory = getIntent().getBooleanExtra("Mandetory",false);
            ServiceDetailsAdpater groupAdaptr = new ServiceDetailsAdpater(this,serviceGroups,cmul);
            servicedtlst.setAdapter(groupAdaptr);
        }

        tvaddhome.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int counter = 0;
                for(ServiceGroup serviceGroupItem : serviceGroups)
                {
                    if(serviceGroupItem.isServicegroupflag())
                    {
                        counter++;
                        if(!jobIds.contains(serviceGroupItem.getSid()))
                        {
                            jobIds.add(serviceGroupItem.getSid());
                        }
                    }
                    else
                    {
                        if(jobIds.contains(serviceGroupItem.getSid()))
                        {
                            jobIds.remove(serviceGroupItem.getSid());
                        }
                    }
                }

                if(isMandetory && counter <=0)
                {
                    Toast.makeText(ServiceDetials.this,getResources().getString(R.string.pleaseaddmandetorydata),Toast.LENGTH_SHORT).show();
                    return;
                }

                    manager.setJobIds(jobIds);
                finish();
            }






          /*  manager.setJOBSID("");
            for(int i=0;i<serviceGroups.size();i++)
            {
                Log.i("Servicedtl ","flagi "+serviceGroups.get(i).isServicegroupflag());
                if(isMandetory)
                {
                    if(serviceGroups.get(i).isServicegroupflag())
                    {
                        jobSidList.add(serviceGroups.get(i).getSid());
                    }
                    else
                    {
                        if(jobSidList.size()==0)
                        {

                        }

                    }
                }
                else
                {
                    if(serviceGroups.get(i).isServicegroupflag())
                    {
                        jobSidList.add(serviceGroups.get(i).getSid());
                    }
                }
                //Log.i(TAG,"checkedmathd "+serviceGroups.get(i).isServicegroupflag());
            }
            String jsonText = gson.toJson(jobSidList);
            manager.setJOBSID(jsonText);
*/


        //}
    });
}

    @Override
    protected void onResume()
    {
        super.onResume();

        jobIds = manager.getJobIds();

        for(int i =0; i< serviceGroups.size(); i++)
        {
            if(jobIds != null && jobIds.contains(serviceGroups.get(i).getSid().trim()))
            {
                serviceGroups.get(i).setServicegroupflag(true);
            }
        }

    }

}
