package com.iserve.passenger;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.adapter.Address_Adaptor;
import com.google.gson.Gson;
import com.pojo.Saving_Address;
import com.pojo.Validator_Pojo;
import com.utility.OkHttp3Request;
import com.utility.SessionManager;
import com.utility.Variableconstant;
import java.util.ArrayList;

import okhttp3.FormBody;

/**
 * Created by embed on 30/8/16.
 *
 */
public class AddressFragment extends Fragment
{
    ImageView tolbarhomelogo;
    String TAG =  "AddressFragment";
    ListView addresslist;
    TextView addaddress,noadressadded;
    private ArrayList<Saving_Address> saving_addresses = new ArrayList<>();
    RelativeLayout imageframelayout;
    Address_Adaptor address_adaptor;
    ProgressDialog pdialog;
    SessionManager manager;
    Gson gson;
    Validator_Pojo apojo;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.addresslayout,container,false);
        intilizeVariable(view);
        manager = new SessionManager(getActivity());
        gson = new Gson();
        pdialog = new ProgressDialog(getActivity());
        pdialog.setMessage(getResources().getString(R.string.wait));
        pdialog.setCancelable(false);
        return view;
    }
    private void intilizeVariable(View view)
    {
        Typeface regularfont = Typeface.createFromAsset(getActivity().getAssets(), Variableconstant.regularfont);
        addresslist = (ListView) view.findViewById(R.id.addresslist);
        addaddress = (TextView) view.findViewById(R.id.addaddress);
        noadressadded = (TextView) view.findViewById(R.id.noadressadded);
        TextView toolbarhometxv = (TextView) getActivity().findViewById(R.id.toolbarhometxv);
        TextView yourlocation = (TextView) getActivity().findViewById(R.id.yourlocation);
        TextView youraddress = (TextView) getActivity().findViewById(R.id.youraddress);
        TextView tveditbarhometxv = (TextView) getActivity().findViewById(R.id.tveditbarhometxv);
        tolbarhomelogo = (ImageView) getActivity().findViewById(R.id.tolbarhomelogo);
        imageframelayout = (RelativeLayout) getActivity().findViewById(R.id.imageframelayout);
        imageframelayout.setVisibility(View.GONE);
        toolbarhometxv.setVisibility(View.VISIBLE);
        toolbarhometxv.setText(getResources().getString(R.string.manageaddress));
        tolbarhomelogo.setVisibility(View.GONE);
        tveditbarhometxv.setVisibility(View.GONE);
        youraddress.setVisibility(View.GONE);
        yourlocation.setVisibility(View.GONE);
        addaddress.setTypeface(regularfont);
        noadressadded.setTypeface(regularfont);
        toolbarhometxv.setTypeface(regularfont);
        address_adaptor = new Address_Adaptor(getActivity(), saving_addresses, AddressFragment.this);
        addaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),Current_Location.class);
                startActivity(intent);
            }
        });
        addresslist.setAdapter(address_adaptor);
    }
    @Override
    public void onResume() {
        super.onResume();
        callgetAddress();
    }
    private void callgetAddress()
    {
        pdialog.show();
        FormBody request = new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_cust_id",manager.getCustomerId())
                .build();
        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "GetCustomerAddress", request, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                pdialog.dismiss();
                callresponceMethod(result);
            }
            @Override
            public void onError(String error) {
                pdialog.dismiss();
            }
        });
    }
    private void callresponceMethod(String result)
    {
        apojo = gson.fromJson(result,Validator_Pojo.class);
        if(apojo!=null && apojo.getErrNum().equals("113") && apojo.getErrFlag().equals("0"))
        {
            saving_addresses.clear();
            if(apojo.getAddlist().size()>0) {

                noadressadded.setVisibility(View.GONE);
                saving_addresses.addAll(apojo.getAddlist());
                address_adaptor.notifyDataSetChanged();
            }
            else
            {
                noadressadded.setVisibility(View.VISIBLE);
            }
        }
    }
    /**
     * Delete the Address from the DataBase
     * @param positiondelete integer type delete the address at a particular position
     */
    public void deletefromdb(final int positiondelete)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.deleteaddress));
        builder.setMessage(getResources().getString(R.string.doyouwishtodelete));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                deleteaddress(saving_addresses.get(positiondelete).getAddressid());
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void deleteaddress(String addressid) {
        pdialog.show();
        final FormBody request = new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_cust_id",manager.getCustomerId())
                .add("ent_addressid",addressid)
                .build();
        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "DeleteCustomerAddress", request, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                pdialog.dismiss();
                if(result!=null)
                {
                    apojo = gson.fromJson(result,Validator_Pojo.class);
                    if(apojo!=null && apojo.getErrNum().equals("112") && apojo.getErrFlag().equals("0"))
                    {
                        saving_addresses.clear();
                        if(apojo.getAddlist().size()>0) {

                            noadressadded.setVisibility(View.GONE);
                            saving_addresses.addAll(apojo.getAddlist());
                            address_adaptor.notifyDataSetChanged();
                            address_adaptor.notifyDataSetChanged();
                        }
                        else
                        {
                            noadressadded.setVisibility(View.VISIBLE);
                            address_adaptor.notifyDataSetChanged();
                        }
                    }
                }
            }
            @Override
            public void onError(String error) {
                pdialog.dismiss();
            }
        });
    }
}
