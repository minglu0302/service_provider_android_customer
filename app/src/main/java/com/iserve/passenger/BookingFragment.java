package com.iserve.passenger;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adapter.BookingFrag_Adaptr;
import com.google.gson.Gson;
import com.pojo.BookingHistoryPojo;
import com.pojo.ContainRow_Item;
import com.pojo.OnGoing_Booking;
import com.pojo.Past_Booking;
import com.utility.OkHttp3Request;
import com.utility.Scaler;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import okhttp3.FormBody;
/**
 * <h>BookingFragment</h>
 * Created by embed on 02/08/16.
 *
 */
public class BookingFragment extends Fragment
{
    TextView toolbarhometxv,tveditbarhometxv,yourlocation,youraddress,nobooking;
    ImageView tolbarhomelogo;
    BookingFrag_Adaptr adapter;
    RecyclerView historylist;
    SessionManager manager;
    ProgressDialog pDialog;
    Gson gson;
    JSONObject jsonObject;
    BookingHistoryPojo bookingHistoryPojo;
    ArrayList<ContainRow_Item> rowItems = new ArrayList<>();
    ArrayList<OnGoing_Booking> ongoingbookings;
    ArrayList<Past_Booking> pastbookings;
    private int currentPage = 0;
    private boolean loading = true;
    String currencySybmol = "";
    double size[];
    String TAG = "BookingFragment";
    Typeface regulrtxt,litetxt;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager mLayoutManager;
    RelativeLayout imageframelayout;
    private String statecode,bookid,alertcode;
    private CountDownTimer timer = null;
    Utility utility;
    Context mcontext;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.bookingfragment,container,false);
        manager = new SessionManager(getActivity());
        jsonObject = new JSONObject();
        intilizeVariable(view);
        Variableconstant.comgfrmConfirmscrn = false;
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mcontext = getActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        ApplicationController.initInstance(new SockEmitter(getActivity()) {
            @Override
            public void execute(String socket_event, JSONObject jsonObject) throws JSONException
            {
                if(socket_event.equals("CustomerStatus"))
                {
                    try {
                        Log.d(TAG,"BOOKINGHISTRY "+jsonObject.toString());
                        statecode = jsonObject.getString("st");
                        bookid = jsonObject.getString("bid");
                        alertcode = jsonObject.getString("alert");
                        timerViewmethod(statecode,bookid,alertcode);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    private void intilizeVariable(View view)
    {

        regulrtxt = Typeface.createFromAsset(getActivity().getAssets(), Variableconstant.regularfont);
        litetxt = Typeface.createFromAsset(getActivity().getAssets(), Variableconstant.lightfont);
        currencySybmol = getResources().getString(R.string.currencySymbol);
        size = Scaler.getScalingFactor(getActivity());
        gson = new Gson();
        utility = new Utility();
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage(getResources().getString(R.string.wait));
        tolbarhomelogo = (ImageView) getActivity().findViewById(R.id.tolbarhomelogo);
        toolbarhometxv = (TextView) getActivity().findViewById(R.id.toolbarhometxv);
        yourlocation = (TextView) getActivity().findViewById(R.id.yourlocation);
        youraddress = (TextView) getActivity().findViewById(R.id.youraddress);
        imageframelayout = (RelativeLayout) getActivity().findViewById(R.id.imageframelayout);
        imageframelayout.setVisibility(View.GONE);
        nobooking = (TextView) view.findViewById(R.id.nobooking);
        toolbarhometxv.setText(getResources().getString(R.string.bookingistory));
        toolbarhometxv.setVisibility(View.VISIBLE);
        tolbarhomelogo.setVisibility(View.GONE);
        tveditbarhometxv = (TextView) getActivity().findViewById(R.id.tveditbarhometxv);
        tveditbarhometxv.setVisibility(View.GONE);
        youraddress.setVisibility(View.GONE);
        yourlocation.setVisibility(View.GONE);
        historylist = (RecyclerView) view.findViewById(R.id.historylist);
        adapter = new BookingFrag_Adaptr(getActivity(),rowItems,BookingFragment.this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        historylist.setLayoutManager(mLayoutManager);
        historylist.setItemAnimator(new DefaultItemAnimator());
        historylist.setAdapter(adapter);
        historylist.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0)
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            currentPage++;
                            getAppointmentDetails(currentPage);
                        }
                    }
                }
            }
        });
        typeface();
    }
    private void typeface()
    {
        toolbarhometxv.setTypeface(regulrtxt);
        nobooking.setTypeface(litetxt);
    }
    @Override
    public void onResume() {
        super.onResume();
        if(timer!=null)
        {
            timer.cancel();
        }
        if(!Variableconstant.isLIVBOKINOPN)
        {
            manager.setPUSHMSGFORMAP("");
        }
        currentPage = 0;
        loading = true;
        ongoingbookings = new ArrayList<>();
        pastbookings = new ArrayList<>();
        getAppointmentDetails(currentPage);
        emitHeartbeat("1");
    }
    public void emitHeartbeat(String status) {
        try
        {
            jsonObject.put("status", status);
            jsonObject.put("cid", manager.getCustomerId());
            ApplicationController.getInstance().emit("CustomerStatus", jsonObject);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void timerViewmethod(String statcode, final String bookid, final String alertcode)
    {
        switch (statcode)
        {
            case "5":
            case "6":
            case "15":
            case "16":
            case "21":
            case "22":
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getAppointmentDetails(0);
                    }
                });
                break;
            case "7":
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        calpopupDetails(bookid,"7");
                    }
                });
                break;
            case "10":
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                       calCanceldialog(alertcode,bookid);
                    }
                });
                break;
        }
    }

    private void calCanceldialog(String alertcode, String bookid)
    {
        new AlertDialog.Builder(getActivity())
                .setTitle("Pro cancellation reason")
                .setMessage(alertcode)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        getAppointmentDetails(0);
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_launcher)
                .show();
        Utility.deleteEventFromCalender(bookid,manager,getActivity());
    }

    private void calpopupDetails(final String bookid, final String status) {
        new AlertDialog.Builder(getActivity())
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage("Booking completed,have a look at the invoice")
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                       Intent intent = new Intent(getActivity(), LiveBookingStatus.class);
                        intent.putExtra("bid", bookid);
                        intent.putExtra("statcode", status);
                        intent.putExtra("status", "Invoice Raised");
                        startActivity(intent);
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_launcher)
                .show();
    }
    private void getAppointmentDetails(final int cPage)
    {
        if(Utility.isNetworkAvailable(getActivity()))
        {
            Utility.printLog("Requestpara",manager.getSession()+" deviceid "+manager.getDevice_Id()+" index "+cPage+" date "+Utility.dateintwtfour());
            pDialog.show();
            FormBody requestform =  new FormBody.Builder()
                    .add("ent_sess_token", manager.getSession())
                    .add("ent_dev_id", manager.getDevice_Id())
                    .add("ent_page_index",cPage+"")
                    .add("ent_date_time", Utility.dateintwtfour())
                    .build();

            OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "getSlaveAppts", requestform, new OkHttp3Request.JsonRequestCallback() {
                @Override
                public void onSuccess(String result) {

                    pDialog.dismiss();
                    Log.d("TAG","HIBOOKNGRES "+result);
                    apptatusResponce(result,cPage);
                }
                @Override
                public void onError(String error) {
                    pDialog.dismiss();
                }
            });
        }
        else
        {
            if(getActivity()!=null)
            Toast.makeText(mcontext,getResources().getString(R.string.network_alert_message),Toast.LENGTH_SHORT).show();
        }
    }
    private void apptatusResponce(String result,int cpag)
    {
        bookingHistoryPojo = gson.fromJson(result,BookingHistoryPojo.class);
        if(bookingHistoryPojo.getErrFlag().equals("0"))
        {
            if(cpag==0)
            {
                pastbookings.clear();
            }
            pastbookings.addAll(bookingHistoryPojo.getPast_appts());
            rowItems.clear();
            if(bookingHistoryPojo.getOngoing_appts().size()>0)
            {
                for (int i=0 ;i<bookingHistoryPojo.getOngoing_appts().size();i++)
                {
                    OnGoing_Booking onGoing_booking=bookingHistoryPojo.getOngoing_appts().get(i);
                    ContainRow_Item convertrowitem = new ContainRow_Item(onGoing_booking.getApntDt(),onGoing_booking.getpPic(),onGoing_booking.getEmail(),
                            onGoing_booking.getStatus(),onGoing_booking.getFname(),onGoing_booking.getLname(),onGoing_booking.getDeg(),
                            onGoing_booking.getPhone(),onGoing_booking.getApntTime(),onGoing_booking.getApntDate(),onGoing_booking.getApptLat(),onGoing_booking.getApptLong(),
                            onGoing_booking.getBid(),onGoing_booking.getCat_name(),onGoing_booking.getAddrLine1(),onGoing_booking.getAddrLine2(),onGoing_booking.getNotes(),
                            onGoing_booking.getStar_rating(),onGoing_booking.getBookType(),onGoing_booking.getStatCode(),onGoing_booking.getAmount(),onGoing_booking.getCancelAmount(),onGoing_booking.getDt(),
                            null,onGoing_booking.getPid(),"","","");

                    if(i==0)
                    {
                        convertrowitem.setFirst(true);
                    }
                    rowItems.add(convertrowitem);
                }
            }
            if(pastbookings.size()>0)
            {
                for (int i=0 ;i<pastbookings.size();i++)
                {
                    Past_Booking past_booking=pastbookings.get(i);
                    ContainRow_Item containRowItem = new ContainRow_Item(past_booking.getApntDt(),past_booking.getpPic(),past_booking.getEmail(),
                            past_booking.getStatus(),past_booking.getFname(),past_booking.getLname(),past_booking.getDeg(),
                            past_booking.getPhone(),past_booking.getApntTime(),past_booking.getApntDate(),past_booking.getApptLat(),past_booking.getApptLong(),
                            past_booking.getBid(),past_booking.getCat_name(),past_booking.getAddrLine1(),past_booking.getAddrLine2(),past_booking.getNotes(),
                            past_booking.getStar_rating(),past_booking.getBookType(),past_booking.getStatCode(),past_booking.getAmount(),past_booking.getCancelAmount(),past_booking.getDt(),
                            past_booking.getFdata(),past_booking.getPid(),past_booking.getCancel_reason(),past_booking.getDispute_msg(),past_booking.getDisputed());
                    if(i==0)
                    {
                        containRowItem.setFirst(true);
                    }
                    rowItems.add(containRowItem);
                }
            }
            adapter.notifyDataSetChanged();
        }
        if(rowItems.size()>0)
        {
            nobooking.setVisibility(View.GONE);
        }
        else
        {
            nobooking.setVisibility(View.VISIBLE);
        }
    }
    public void itemclickedmethod(ContainRow_Item postion)
    {
        Intent intent;
        switch (postion.getStatCode()) {
            case "2":
            case "5":
            case "6":
            case "21":
            case "22":
                manager.setProprofilePic(postion.getpPic());
                intent = new Intent(getActivity(), LiveBookingStatus.class);
                intent.putExtra("apptlat", postion.getApptLat());
                intent.putExtra("apptlng", postion.getApptLong());
                intent.putExtra("bid", postion.getBid());
                intent.putExtra("statcode", postion.getStatCode());
                intent.putExtra("status", postion.getStatus());
                startActivity(intent);
                break;
            case "4":
            case "7":
            case "9":
            case "10":
                manager.setProprofilePic(postion.getpPic());
                new DialogActivityEx(getActivity(),postion.getBid(),pDialog,"BookingFrag");
                break;
            default:
                Toast.makeText(getActivity(), "" + postion.getStatus(), Toast.LENGTH_SHORT).show();
                break;
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        if(timer!=null)
        {
            timer.cancel();
        }
    }
}
