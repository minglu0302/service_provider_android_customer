package com.iserve.passenger;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.pojo.InvoiceResponce;
import com.pojo.Validator_Pojo;
import com.squareup.picasso.Picasso;
import com.utility.CircleTransform;
import com.utility.OkHttp3Request;
import com.utility.Scaler;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import okhttp3.FormBody;


/**
 * <h>DialogActivityEx</h>
 * <p>this show the invoice and receipt</p>
 * Created by embed on 7/4/17.
 *
 */

class DialogActivityEx extends Dialog
{

    String TAG = "DialogActivityEx";
    Context context;
    SessionManager manager;
    private InvoiceResponce invoiceresp;
    private ProgressDialog progressDialog = null;
    private String bid;
    Gson gson;
    private Dialog indialog,receiptdialog;
    double size[];
    private RatingBar invoice_rating;
    private String currencySybmol,cominFrm;
    private Typeface regulrtxt,litetxt;
    private String reviewString = "";
    private String invoicetxt = "";
    private EditText etext;
    private ProgressDialog pDialog ;

    DialogActivityEx(@NonNull Context context, String bid, ProgressDialog progressDialog, String cominFrm)
    {
        super(context);
        this.context = context;
        this.bid = bid;
        this.progressDialog = progressDialog;
        this.cominFrm = cominFrm;
        getApptDtls();
    }
    private void getApptDtls()
    {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getResources().getString(R.string.wait));
        pDialog.setCancelable(false);
        regulrtxt = Typeface.createFromAsset(context.getAssets(),Variableconstant.regularfont);
        litetxt = Typeface.createFromAsset(context.getAssets(),Variableconstant.lightfont);
        manager = new SessionManager(context);
        currencySybmol = context.getResources().getString(R.string.currencySymbol);
        gson = new Gson();

        if (Utility.isNetworkAvailable(context))
        {
           /* if(progressDialog!=null)
            {
                progressDialog.show();
            }*/
            if(pDialog!=null)
            {
                pDialog.show();
            }
            FormBody requestform = new FormBody.Builder()
                    .add("ent_sess_token", manager.getSession())
                    .add("ent_dev_id", manager.getDevice_Id())
                    .add("ent_bid", bid)
                    .add("ent_inv", "1")
                    .add("ent_date_time", Utility.dateintwtfour())
                    .build();
            OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "getAppointmentDetails", requestform, new OkHttp3Request.JsonRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(pDialog!=null)
                    {
                        pDialog.dismiss();
                    }
                   /* if(progressDialog!=null)
                    {
                        progressDialog.dismiss();
                    }*/
                    Log.d(TAG, "ALIresponceInvoice " + result);
                    apptdetlsinvResponce(result);
                }
                @Override
                public void onError(String error)
                {
                    if(pDialog!=null)
                    {
                        pDialog.dismiss();
                    }
                    /*if(progressDialog!=null)
                    {
                        progressDialog.dismiss();
                    }*/
                }
            });
        } else {
            Toast.makeText(context,context.getResources().getString(R.string.nointernet),Toast.LENGTH_SHORT).show();
        }
    }
    private void apptdetlsinvResponce(String result)
    {
        size = Scaler.getScalingFactor(context);
        final Utility utility = new Utility();
        invoiceresp = gson.fromJson(result, InvoiceResponce.class);
        if (invoiceresp.getErrFlag().equals("0") && invoiceresp.getErrNum().equals("21"))
        {
            indialog = new Dialog(context);
            indialog.setCanceledOnTouchOutside(true);
            indialog.setCancelable(false);
            indialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            indialog.setContentView(R.layout.invoice_popup);
            indialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            indialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            ImageView driverImage = (ImageView) indialog.findViewById(R.id.driverImage);
            double width2 = size[0] * 60;
            double height2 = size[1] * 60;
            Picasso.with(context).load(invoiceresp.getpPic()).centerCrop()
                    .transform(new CircleTransform())
                    .resize((int) width2, (int) height2)
                    .into(driverImage);
            invoice_rating = (RatingBar) indialog.findViewById(R.id.invoice_rating);

            final TextView pickupaddresstv, rideDate, driverName, providertype, amount, receipt, invoice_review, submitButton, needHelp,lastRide
                    ,pickDate,txt_rating,tvdisputedcancel,tvdisputedcancelrsn;
            RelativeLayout rlreviewtxt,rlcanceldispute,rl_rate_journey;

            pickupaddresstv = (TextView) indialog.findViewById(R.id.pickupaddresstv);
            rideDate = (TextView) indialog.findViewById(R.id.rideDate);
            driverName = (TextView) indialog.findViewById(R.id.driverName);
            providertype = (TextView) indialog.findViewById(R.id.providertype);
            amount = (TextView) indialog.findViewById(R.id.amount);
            receipt = (TextView) indialog.findViewById(R.id.receipt);
            invoice_review = (TextView) indialog.findViewById(R.id.invoice_review);
            submitButton = (TextView) indialog.findViewById(R.id.submitButton);
            needHelp = (TextView) indialog.findViewById(R.id.needHelp);
            lastRide = (TextView) indialog.findViewById(R.id.lastRide);
            pickDate = (TextView) indialog.findViewById(R.id.pickDate);
            txt_rating = (TextView) indialog.findViewById(R.id.txt_rating);
            tvdisputedcancel = (TextView) indialog.findViewById(R.id.tvdisputedcancel);
            tvdisputedcancelrsn = (TextView) indialog.findViewById(R.id.tvdisputedcancelrsn);
            rlreviewtxt = (RelativeLayout) indialog.findViewById(R.id.rlreviewtxt);
            rlcanceldispute = (RelativeLayout) indialog.findViewById(R.id.rlcanceldispute);
            rl_rate_journey = (RelativeLayout) indialog.findViewById(R.id.rl_rate_journey);
            pickupaddresstv.setText(invoiceresp.getAddr1());
            rideDate.setText(invoiceresp.getApptDate());
            String name = invoiceresp.getfName() + " " + invoiceresp.getlName();
            driverName.setText(name);
            providertype.setText(invoiceresp.getCat_name());

            if(invoiceresp.getCancel_reason().equals(""))
                utility.setAmtOnRecept(invoiceresp.getFdata().getTotal_pro(),amount,currencySybmol);
            else
                utility.setAmtOnRecept(invoiceresp.getCancel_amount(),amount,currencySybmol);
            needHelp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!invoiceresp.getHelpLink().equals(""))
                    {
                        showDisputeAlert();
                    }
                }
            });
            txt_rating.setTypeface(regulrtxt);
            needHelp.setTypeface(regulrtxt);
            invoicetypeface(lastRide,rideDate,pickDate,pickupaddresstv,receipt,driverName,providertype,amount,submitButton);
            receipt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    double width = size[0] * 70;
                    double height = size[1] * 50;
                    receiptdialog = new Dialog(context);
                    receiptdialog.setCanceledOnTouchOutside(true);
                    receiptdialog.setCancelable(true);
                    receiptdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    receiptdialog.setContentView(R.layout.receipt_layout);
                    receiptdialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    receiptdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    ImageView ivsignature = (ImageView) receiptdialog.findViewById(R.id.ivsignature);
                    Button submitButton = (Button) receiptdialog.findViewById(R.id.submitButton);
                    TextView timeFee = (TextView) receiptdialog.findViewById(R.id.timeFee);
                    TextView tvmaterialamount = (TextView) receiptdialog.findViewById(R.id.tvmaterialamount);
                    TextView minFare = (TextView) receiptdialog.findViewById(R.id.minFare);
                    TextView miscfee = (TextView) receiptdialog.findViewById(R.id.miscfee);
                    TextView subtotalfee = (TextView) receiptdialog.findViewById(R.id.subtotalfee);
                    TextView prodrdiscnt = (TextView) receiptdialog.findViewById(R.id.prodrdiscnt);
                    TextView tvcoupondisamount = (TextView) receiptdialog.findViewById(R.id.tvcoupondisamount);
                    TextView totalamt = (TextView) receiptdialog.findViewById(R.id.totalamt);
                    TextView bookingId = (TextView) receiptdialog.findViewById(R.id.bookingId);
                    TextView  payment_type = (TextView) receiptdialog.findViewById(R.id.payment_type);
                    TextView  tvinvoicenote = (TextView) receiptdialog.findViewById(R.id.tvinvoicenote);
                    TextView  inVoiceNote = (TextView) receiptdialog.findViewById(R.id.inVoiceNote);
                    LinearLayout  invoice_note = (LinearLayout) receiptdialog.findViewById(R.id.invoice_note);

                    submitButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            receiptdialog.dismiss();
                        }
                    });

                    if(!invoiceresp.getFdata().getPayment_type().equals("1"))
                    {

                        payment_type.setText(context.getResources().getString(R.string.card));
                    }
                    else
                    {
                        ImageView paymentimage = (ImageView) receiptdialog.findViewById(R.id.paymentimage);
                        payment_type.setText(context.getResources().getString(R.string.cash));
                        paymentimage.setVisibility(View.GONE);
                    }

                    utility.setAmtOnRecept(invoiceresp.getFdata().getTime_fees(),timeFee,currencySybmol);
                    utility.setAmtOnRecept(invoiceresp.getFdata().getMat_fees(),tvmaterialamount,currencySybmol);

                    /*if(invoiceresp.getCancel_amount().equals(""))
                    {
                        utility.setAmtOnRecept(invoiceresp.getFdata().getVisit_fees(),minFare,currencySybmol);
                    }
                    else
                    {
                        utility.setAmtOnRecept("",minFare,currencySybmol);
                    }*/

                    utility.setAmtOnRecept(invoiceresp.getFdata().getVisit_fees(),minFare,currencySybmol);
                    utility.setAmtOnRecept(invoiceresp.getFdata().getMisc_fees(),miscfee,currencySybmol);

                    utility.setAmtOnRecept(invoiceresp.getFdata().getSub_total_cust(),subtotalfee,currencySybmol);

                    utility.setAmtOnRecept(invoiceresp.getFdata().getPro_disc(),prodrdiscnt,currencySybmol);

                    utility.setAmtOnRecept(invoiceresp.getFdata().getCoupon_discount(),tvcoupondisamount,currencySybmol);

                    if(invoiceresp.getCancel_reason().equals(""))
                        utility.setAmtOnRecept(invoiceresp.getFdata().getTotal_pro(),totalamt,currencySybmol);
                    else
                        utility.setAmtOnRecept(invoiceresp.getCancel_amount(),totalamt,currencySybmol);


                    String bid = "BID: " + invoiceresp.getBid();
                    bookingId.setText(bid);
                    if(!invoiceresp.getFdata().getPro_note().equals(""))
                    {
                        invoice_note.setVisibility(View.VISIBLE);
                        tvinvoicenote.setText(invoiceresp.getFdata().getPro_note());
                    }
                    else
                    {
                        invoice_note.setVisibility(View.GONE);
                    }
                    if(!invoiceresp.getFdata().getSign_url().equals(""))
                    {
                        Picasso.with(context).load(invoiceresp.getFdata().getSign_url())
                                .centerCrop()
                                .resize((int) width, (int) height)
                                .into(ivsignature);
                    }
                    if(invoiceresp.getServices().size()>0)
                    {

                        RelativeLayout rlservices = (RelativeLayout) receiptdialog.findViewById(R.id.rlservices);
                        TextView  tvservices = (TextView) receiptdialog.findViewById(R.id.tvservices);
                        LinearLayout llservicesare = (LinearLayout) receiptdialog.findViewById(R.id.llservicesare);
                        rlservices.setVisibility(View.VISIBLE);
                        tvservices.setTypeface(litetxt);
                        for(int i=0; i<invoiceresp.getServices().size();i++)
                        {
                            TextView group_name, group_price;
                            LayoutInflater inflatervehicle = LayoutInflater.from(context);
                            View inflatedLayout = inflatervehicle.inflate(R.layout.singleservice_layout, llservicesare, false);
                            group_name = (TextView) inflatedLayout.findViewById(R.id.group_name);
                            group_price = (TextView) inflatedLayout.findViewById(R.id.group_price);
                            group_name.setTypeface(litetxt);
                            group_price.setTypeface(litetxt);
                            group_name.setText(invoiceresp.getServices().get(i).getSname());
                            String price =currencySybmol+" "+ Utility.doubleformate(invoiceresp.getServices().get(i).getSprice());
                            group_price.setText(price);
                            llservicesare.addView(inflatedLayout);
                        }
                    }
                    receipttypeface(receiptdialog,minFare,timeFee,tvmaterialamount,miscfee,subtotalfee,prodrdiscnt,
                            tvcoupondisamount,totalamt,payment_type,submitButton,tvinvoicenote,inVoiceNote);

                    receiptdialog.show();
                }
            });
            if(cominFrm.equals("BookingFrag"))
            {
                rlreviewtxt.setVisibility(View.GONE);
                needHelp.setVisibility(View.GONE);
                invoice_rating.setRating(Float.parseFloat(invoiceresp.getRating()));
                txt_rating.setText(context.getResources().getString(R.string.yourrating));
                submitButton.setText(context.getResources().getString(R.string.done));
                invoice_rating.setIsIndicator(true);
                if(invoiceresp.getDisputed().equals("1") || !invoiceresp.getCancel_reason().equals(""))
                {
                    rl_rate_journey.setVisibility(View.GONE);
                    rlcanceldispute.setVisibility(View.VISIBLE);
                    if(invoiceresp.getDisputed().equals("1"))
                    {
                        tvdisputedcancel.setText(context.getResources().getString(R.string.bookingDisputed));
                        tvdisputedcancelrsn.setText(invoiceresp.getDispute_msg());
                    }
                    else
                    {
                        tvdisputedcancel.setText(context.getResources().getString(R.string.providercanceld));
                        tvdisputedcancelrsn.setText(invoiceresp.getCancel_reason());
                    }
                }
                else
                {
                    rl_rate_journey.setVisibility(View.VISIBLE);
                    rlcanceldispute.setVisibility(View.GONE);
                }
            }else
            {
                invoice_review.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        reviewAlert(invoice_review);
                    }
                });
            }
            submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if(submitButton.getText().toString().trim().equals(context.getResources().getString(R.string.done)))
                        indialog.dismiss();
                    else
                    {

                        updateSlaveReview(invoice_rating.getRating());
                        indialog.dismiss();
                    }
                }
            });
            indialog.show();
        }
    }
    private void reviewAlert(final TextView invoice_review)
    {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.leave_comment_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle(context.getResources().getString(R.string.leaveCommentTitle));
        final EditText text = (EditText) dialog.findViewById(R.id.user_dispute_text);
        TextView backButton= (TextView) dialog.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button submit = (Button) dialog.findViewById(R.id.submit);
        if(!invoicetxt.equals(""))
        {
            text.setText(invoicetxt);
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Utility.printLog("user msg=" + text.getText().toString().trim());

                reviewString=text.getText().toString();
                invoicetxt = text.getText().toString();

                if(!invoicetxt.equals(""))
                {
                    invoice_review.setText(context.getResources().getString(R.string.editinvoice));
                    invoice_review.setTextColor(context.getResources().getColor(R.color.actionbar_color));

                }
                else
                {
                    invoice_review.setText(context.getResources().getString(R.string.write_review));

                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void updateSlaveReview(float rating)
    {

        if (Utility.isNetworkAvailable(context)) {
            pDialog.show();
            FormBody requestform = new FormBody.Builder()
                    .add("ent_sess_token", manager.getSession())
                    .add("ent_dev_id", manager.getDevice_Id())
                    .add("ent_bid", bid)
                    .add("ent_rating_num", rating + "")
                    .add("ent_review_msg", reviewString)
                    .add("ent_date_time", Utility.dateintwtfour())
                    .build();
            okio.Buffer sink = new okio.Buffer();
            try {
                requestform.writeTo(sink);
                byte[] barry = sink.readByteArray();
                String req = new String(barry);
                Log.d(TAG, "request " + req);
            } catch (Exception e) {
                e.printStackTrace();
            }

            OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "updateSlaveReview", requestform, new OkHttp3Request.JsonRequestCallback() {
                @Override
                public void onSuccess(String result) {

                    pDialog.dismiss();
                    reviewResponce(result);
                    Log.d(TAG, "ALIresponceInvoice " + result);
                }

                @Override
                public void onError(String error) {
                    pDialog.dismiss();
                }
            });

        } else {

            Toast.makeText(context,context.getResources().getString(R.string.network_alert_message),Toast.LENGTH_SHORT).show();
        }
    }
    private void reviewResponce(String result)
    {
        Log.d(TAG, "reviewResponce: "+result+" comingFrom "+cominFrm);
        Validator_Pojo reviewpojo = gson.fromJson(result, Validator_Pojo.class);
        if (reviewpojo.getErrFlag().equals("0"))
        {
            if(cominFrm.equals("MenuActivity"))
            {
                invoicetxt = "";
                manager.setSavedTime(0);
                manager.setSavedSystemTime(0);
            }
            else
            {
                invoicetxt = "";
                Intent intnt = new Intent(context, MenuActivity.class);
                intnt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                manager.setSavedTime(0);
                manager.setSavedSystemTime(0);
                context.startActivity(intnt);
            }
        }
        else
        {
            Toast.makeText(context,reviewpojo.getErrMsg(),Toast.LENGTH_SHORT).show();
        }
    }
    private void showDisputeAlert()
    {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        etext = (EditText) dialog.findViewById(R.id.user_dispute_text);
        TextView backButton = (TextView) dialog.findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
        Button submit = (Button) dialog.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Utility.printLog("user msg=" + etext.getText().toString().trim());
                if (etext.getText().toString().trim().equals(""))
                {
                    Utility.ShowAlert(context.getResources().getString(R.string.provide_valid_reason_for_dispute), context);
                } else
                {
                    if(Utility.isNetworkAvailable(context))
                    {
                        dialog.dismiss();
                        getDisputeDetails(etext.getText().toString().trim());
                    }
                    else
                    {
                        Toast.makeText(context,context.getResources().getString(R.string.network_alert_message),Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        dialog.show();
    }

    /**
     * <h2>getDisputeDetails</h2>
     * <p>
     *     dispute the booking
     * @param disputemsg
     * </p>
     */
    private void getDisputeDetails(String disputemsg)
    {
        pDialog.setMessage(context.getResources().getString(R.string.wait));
        pDialog.show();
        FormBody requestform = new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_bid",bid)
                .add("ent_dispute_msg",disputemsg)
                .add("ent_date_time", Utility.dateintwtfour())
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "reportDispute", requestform, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {


                pDialog.dismiss();
                if(result!=null)
                {
                    Validator_Pojo vpojo = gson.fromJson(result,Validator_Pojo.class);
                    if(vpojo.getErrFlag().equals("0"))
                    {
                        if(cominFrm.equals("MenuActivity"))
                        {
                            invoicetxt = "";
                            manager.setSavedTime(0);
                            manager.setSavedSystemTime(0);
                            indialog.dismiss();
                        }
                        else
                        {
                            invoicetxt = "";
                            Intent intnt = new Intent(context, MenuActivity.class);
                            intnt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            manager.setSavedTime(0);
                            manager.setSavedSystemTime(0);
                            indialog.dismiss();
                            context.startActivity(intnt);
                        }
                    }
                    else
                    {
                        Toast.makeText(context,vpojo.getErrMsg(),Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onError(String error) {
                pDialog.dismiss();
            }
        });
    }
    /*
    * Setting typeFace for the invoice texes*/
    private void invoicetypeface(TextView lastRide, TextView rideDate, TextView pickDate, TextView pickupaddresstv,
                                 TextView receipt, TextView driverName, TextView providertype, TextView amount,
                                 TextView submitButton)
    {
        lastRide.setTypeface(regulrtxt);
        rideDate.setTypeface(regulrtxt);
        pickDate.setTypeface(regulrtxt);
        pickupaddresstv.setTypeface(regulrtxt);
        receipt.setTypeface(regulrtxt);
        driverName.setTypeface(regulrtxt);
        providertype.setTypeface(regulrtxt);
        amount.setTypeface(regulrtxt);
        submitButton.setTypeface(regulrtxt);
    }

    /**
     *
     * @param receiptdialog this the view
     * @param minFare minimum fare txtview
     * @param timeFee time fare txtview
     * @param tvmaterialamount meaterial fare txtview
     * @param miscfee mislenious fare txtview
     * @param subtotalfee subtotal of all above fare
     * @param prodrdiscnt discount by provider
     * @param tvcoupondisamount coupon discount
     * @param totalamt total amount after substracting between subtotal and other discount
     * @param payment_type cash or card payment
     * @param submitButton submit
     */
    private void receipttypeface(Dialog receiptdialog, TextView minFare, TextView timeFee, TextView tvmaterialamount,
                                 TextView miscfee, TextView subtotalfee, TextView prodrdiscnt, TextView tvcoupondisamount,
                                 TextView totalamt, TextView payment_type, Button submitButton, TextView tvinvoicenote,
                                 TextView inVoiceNote)
    {
        TextView tvsignature = (TextView) receiptdialog.findViewById(R.id.tvsignature);
        TextView invoice_min_fare_txt = (TextView) receiptdialog.findViewById(R.id.invoice_min_fare_txt);
        TextView tvtimefee = (TextView) receiptdialog.findViewById(R.id.tvtimefee);
        TextView tvmaterialfee = (TextView) receiptdialog.findViewById(R.id.tvmaterialfee);
        TextView tvmiscfee = (TextView) receiptdialog.findViewById(R.id.tvmiscfee);
        TextView subtotatal = (TextView) receiptdialog.findViewById(R.id.subtotatal);
        TextView tvprodrdiscnt = (TextView) receiptdialog.findViewById(R.id.tvprodrdiscnt);
        TextView tvcoupondis = (TextView) receiptdialog.findViewById(R.id.tvcoupondis);
        TextView tvtotal = (TextView) receiptdialog.findViewById(R.id.tvtotal);
        TextView paymentmethod = (TextView) receiptdialog.findViewById(R.id.paymentmethod);
        minFare.setTypeface(litetxt);
        timeFee.setTypeface(litetxt);
        tvmaterialamount.setTypeface(litetxt);
        miscfee.setTypeface(litetxt);
        payment_type.setTypeface(litetxt);
        paymentmethod.setTypeface(litetxt);
        tvsignature.setTypeface(litetxt);
        inVoiceNote.setTypeface(litetxt);
        tvinvoicenote.setTypeface(litetxt);
        invoice_min_fare_txt.setTypeface(litetxt);
        tvtimefee.setTypeface(litetxt);
        tvmiscfee.setTypeface(litetxt);
        tvmaterialfee.setTypeface(litetxt);
        subtotalfee.setTypeface(regulrtxt);
        subtotatal.setTypeface(regulrtxt);
        prodrdiscnt.setTypeface(regulrtxt);
        tvprodrdiscnt.setTypeface(regulrtxt);
        tvcoupondisamount.setTypeface(regulrtxt);
        tvcoupondis.setTypeface(regulrtxt);
        totalamt.setTypeface(regulrtxt);
        tvtotal.setTypeface(regulrtxt);
        submitButton.setTypeface(regulrtxt);
    }

}
