package com.iserve.passenger;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.pojo.MasterDetails_Slots;
import com.pojo.Master_Details;
import com.squareup.picasso.Picasso;
import com.utility.CircleTransform;
import com.utility.OkHttp3Request;
import com.utility.Scaler;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import okhttp3.FormBody;


/**
 * Created by embed on 15/4/16.
 *
 */
public class MasterDetails extends AppCompatActivity implements View.OnClickListener
{

    Toolbar toolbar;
    TextView confirmbooktv,doctorname,doctorlike,providerdistance,aboutmetv,tvareaofexpert,languagetv,
            docmore,norewies,nojobimage,choseslots,chosedate,title,subtitle;//drdetaileducation,
    ImageView doc_pic,calenderimage;
    RatingBar invoice_driver_rating;
    RelativeLayout rrljobimage,rrljobreview,rrlbook;//,rrleducation,rrlabout;
    CardView cardslots;//cardtime,cardeducation,

    HorizontalScrollView horizontaljobimageView,horizontalslotView;
    LinearLayout MainContainerjob,reviewllout,MainContainerslot;
    ProgressDialog pDialog;
    SessionManager manager;
    Gson gson;
    String pid,typeId,DISTANCE,amtpermin,distancefrom;
    Master_Details masterDetails;
    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;
    double size[];
    double pricepermin;
    int imgcount;
    ArrayList<String>arrayList = new ArrayList<>();

    String timeSlots = "";
    String slots = "";
    String isclicked=1+"";
    String sId = "";
    String sprice = "";
    String eTime = "";
    String sTime = "";
    String TAG ="MasterDetails";
    MasterDetails_Slots docdetslots;
    MasterDetails_Slots doctorDetails_slots;
    Typeface bold,regular,semibold;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        size= Scaler.getScalingFactor(this);
        doctorDetails_slots = new MasterDetails_Slots();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if(getIntent().getExtras()!=null)
        {
            if(Variableconstant.showProfile)
            {
                pid = getIntent().getStringExtra("PID");

            }
            else
            {
                pid = getIntent().getStringExtra("PID");
                typeId = getIntent().getStringExtra("SELECTED_ID");
                distancefrom = getIntent().getStringExtra("DISTANCE");
                amtpermin = getIntent().getStringExtra("AMT");
            }
        }


        initialize();

    }

    private void initialize()
    {

        gson = new Gson();
        manager = new SessionManager(this);
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        NumberFormat nf_out = NumberFormat.getNumberInstance(Locale.US);
        nf_out.setMaximumFractionDigits(2);
        nf_out.setGroupingUsed(false);

        if(distancefrom!=null && !distancefrom.equals(""))
        {
            String faremount=nf_out.format(Double.parseDouble(distancefrom)/1000);
            double kilometerDistance = Double.parseDouble(faremount);//1.6
            @SuppressLint("DefaultLocale") String kilometer = String.format("%.2f", kilometerDistance);
            DISTANCE = kilometer + " " + getResources().getString(R.string.distance);

        }




        regular  = Typeface.createFromAsset(getAssets(), Variableconstant.regularfont);
        bold= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold_0.ttf");
        semibold= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold_0.ttf");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layoutact);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_baract);
        nojobimage = (TextView) findViewById(R.id.nojobimage);
        norewies = (TextView) findViewById(R.id.norewies);
        docmore = (TextView) findViewById(R.id.docmore);
        chosedate = (TextView) findViewById(R.id.chosedate);
        choseslots = (TextView) findViewById(R.id.choseslots);
        confirmbooktv = (TextView) findViewById(R.id.confirmbooktv);
        doctorname = (TextView) findViewById(R.id.doctorname);
        doctorlike = (TextView) findViewById(R.id.doctorlike);
        title = (TextView) findViewById(R.id.title);
        subtitle = (TextView) findViewById(R.id.subtitle);
        providerdistance = (TextView) findViewById(R.id.providerdistance);
        doc_pic = (ImageView) findViewById(R.id.doc_pic);
        calenderimage = (ImageView) findViewById(R.id.calenderimage);
        invoice_driver_rating = (RatingBar) findViewById(R.id.invoice_driver_rating);
        aboutmetv = (TextView) findViewById(R.id.aboutmetv);
        tvareaofexpert = (TextView) findViewById(R.id.tvareaofexpert);
        languagetv = (TextView) findViewById(R.id.languagetv);
        rrljobimage = (RelativeLayout) findViewById(R.id.rrljobimage);
        rrljobreview = (RelativeLayout) findViewById(R.id.rrljobreview);
        rrlbook = (RelativeLayout) findViewById(R.id.rrlbook);
        cardslots = (CardView) findViewById(R.id.cardslots);
        horizontaljobimageView = (HorizontalScrollView) findViewById(R.id.horizontaljobimageView);
        horizontalslotView = (HorizontalScrollView) findViewById(R.id.horizontalslotView);
        MainContainerjob = (LinearLayout) findViewById(R.id.MainContainerjob);
        MainContainerslot = (LinearLayout) findViewById(R.id.MainContainerslot);
        reviewllout = (LinearLayout) findViewById(R.id.reviewllout);
        confirmbooktv.setOnClickListener(this);
        choseslots.setTypeface(regular);
        chosedate.setTypeface(semibold);
        calenderimage.setOnClickListener(this);
        choseslots.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        providerdistance.setText(DISTANCE);
        workingmethod();
        typeface();
    }

    private void typeface()
    {
        TextView avilableslots = (TextView) findViewById(R.id.avilableslots);
        TextView chosepayment = (TextView) findViewById(R.id.chosepayment);
        TextView experttv = (TextView) findViewById(R.id.experttv);
        TextView langtv = (TextView) findViewById(R.id.langtv);
        TextView tvjobimage = (TextView) findViewById(R.id.tvjobimage);
        TextView tvreviews = (TextView) findViewById(R.id.tvreviews);
        aboutmetv.setTypeface(regular);
        tvareaofexpert.setTypeface(regular);
        languagetv.setTypeface(regular);
        nojobimage.setTypeface(regular);
        tvreviews.setTypeface(regular);
        norewies.setTypeface(regular);
        docmore.setTypeface(regular);
        tvjobimage.setTypeface(regular);
        langtv.setTypeface(regular);
        experttv.setTypeface(regular);
        chosepayment.setTypeface(regular);
        avilableslots.setTypeface(regular);
        confirmbooktv.setTypeface(regular);
        doctorname.setTypeface(regular);
        doctorlike.setTypeface(regular);
        providerdistance.setTypeface(regular);
        doctorlike.setTypeface(regular);
        providerdistance.setTypeface(regular);
    }

    private void workingmethod()
    {
        if(Variableconstant.comingfromlaterbooking)
        {
            cardslots.setVisibility(View.VISIBLE);
            timeSlots = Variableconstant.ent_SlotDate;
            chosedate.setText(Variableconstant.ent_txtValue);
        }
        else
        {
            cardslots.setVisibility(View.GONE);
            timeSlots = "";
        }

        Log.i(TAG,"TimingSlots "+timeSlots);
        getMasterDetails();

    }


    private void getMasterDetails()
    {
        pDialog.setMessage(getString(R.string.wait));
        pDialog.show();

        String slottime;

        if(!timeSlots.equals(""))
        {
            slottime = timeSlots;
        }
        else
        {
            slottime = "";
        }

        Log.i(TAG,"masterreq "+manager.getSession()+" divid "+manager.getDevice_Id()+" pid "+pid+" slotid "+slottime+" datetime "+Utility.dateintwtfour());

        FormBody requestbody = new FormBody.Builder()
                .add("ent_sess_token", manager.getSession())
                .add("ent_dev_id", manager.getDevice_Id())
                .add("ent_pro_id",pid)
                .add("ent_slot_date",slottime)
                .add("ent_date_time", Utility.dateintwtfour())
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "getMasterDetails", requestbody, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {

                if (pDialog != null) {
                    pDialog.dismiss();
                }
                Log.i("MASTERDETAIL", "masterresponce " + result);
                masterdtlsucces(result);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(MasterDetails.this, error, Toast.LENGTH_LONG).show();
                if (pDialog != null) {
                    pDialog.dismiss();
                }
            }
        });


    }

    private void masterdtlsucces(String result) {
        final double rheight = (25) * size[1];
        final double rwidth = (25) * size[0];
        masterDetails = gson.fromJson(result, Master_Details.class);

        if (masterDetails != null)
        {
            if (masterDetails.getErrFlag().equals("0") && masterDetails.getErrNum().equals("21"))
            {

                if (!masterDetails.getpPic().equals("")) {
                    Picasso.with(this).load(masterDetails.getpPic())
                            .transform(new CircleTransform()).centerCrop()
                            .resize(doc_pic.getWidth(), doc_pic.getHeight())
                            .into(doc_pic);
                }
                String name = masterDetails.getfName() + " " + masterDetails.getlName();
                doctorname.setText(name);

                float rating = Float.parseFloat(masterDetails.getRating());
                invoice_driver_rating.setRating(rating);
                invoice_driver_rating.setIsIndicator(true);
               /* if (masterDetails.getTotalRev().equals("")) {

                    doctorlike.setText(getResources().getString(R.string.noreviews));
                } else {
                    String rev = masterDetails.getTotalRev() + " " + getResources().getString(R.string.reviewssmal);
                    doctorlike.setText(rev);
                }*/

                if(amtpermin!=null && !amtpermin.equals(""))
                {
                    pricepermin = Double.parseDouble(amtpermin);
                  //  pricepermin = Double.parseDouble(amtpermin)*60;
                }


                aboutmetv.setText(masterDetails.getAbout());
                tvareaofexpert.setText(masterDetails.getExpertise());
                languagetv.setText(masterDetails.getLanguages());

                collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
                collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);

                appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                    boolean isShow = false;
                    int scrollRange = -1;

                    @Override
                    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                        if (scrollRange == -1) {
                            scrollRange = appBarLayout.getTotalScrollRange();
                        }
                        if (scrollRange + verticalOffset == 0)
                        {
                            String drname = doctorname.getText().toString();
                            Log.i(TAG,drname);

                            String s =getResources().getString(R.string.currencySymbol)+" "+ pricepermin+" / "+providerdistance.getText().toString();
                            //   collapsingToolbarLayout.setTitle(drname);
                            subtitle.setText(s);
                            title.setText(drname);
                            isShow = true;
                        } else if (isShow) {
                            collapsingToolbarLayout.setTitle("");
                            subtitle.setText("");
                            title.setText("");
                            isShow = false;
                        }
                    }
                });


                if (masterDetails.getJob_images().size()>0) {
                    nojobimage.setVisibility(View.GONE);

                    double widthimg = (130) * size[0];
                    double heightimg = (130) * size[1];


                    imgcount = masterDetails.getJob_images().size();
                    nojobimage.setVisibility(View.GONE);

                    for (int j = 0; j < imgcount; j++) {

                        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.doctor_image,MainContainerjob,false);
                        ImageView ivmainjob;
                        ivmainjob = (ImageView) view.findViewById(R.id.ivmainjob);
                        String rurl = masterDetails.getJob_images().get(j);
                        arrayList.add(rurl);
                        Log.i("MasterDetails", "imageurlmas " + rurl);
                        rurl = rurl.replace(" ", "%20");
                        if (!rurl.equals("")) {
                            Picasso.with(MasterDetails.this)
                                    .load(rurl)
                                    .resize((int) widthimg, (int) heightimg)
                                    .into(ivmainjob);
                        }

                        ivmainjob.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                                Intent intent = new Intent(MasterDetails.this,Image_Gallery.class);
                                intent.putStringArrayListExtra("Imagelist",arrayList);
                                startActivity(intent);
                            }
                        });


                        MainContainerjob.addView(view);

                    }
                } else {
                    nojobimage.setVisibility(View.VISIBLE);
                }

          /*  if(masterDetails.getReviews().size()>0)
            {
                rrljobreview.setVisibility(View.VISIBLE);
            }
            else
            {
                rrljobreview.setVisibility(View.GONE);
            }*/


                if(Variableconstant.comingfromlaterbooking)
                {
                    if(masterDetails.getSlots().size()>0)
                    {
                        doctorDetails_slots.setSlots(masterDetails.getSlots());
                        slots = gson.toJson(doctorDetails_slots, MasterDetails_Slots.class);
                        manager.setDoctorSlots(slots);
                        //End of saving

                        //calling below methods to get the timing slotes

                        slotestiming();
                    }
                    else
                    {
                        Toast.makeText(MasterDetails.this,"Sorry no appointment available today,choose another doctor or another day",Toast.LENGTH_SHORT).show();
                    }
                }
                if (Variableconstant.showProfile) {

                    rrlbook.setVisibility(View.GONE);
                } else {

                    rrlbook.setVisibility(View.VISIBLE);
                    String book = getResources().getString(R.string.book) + " " + masterDetails.getfName().toUpperCase();
                    confirmbooktv.setText(book);
                }


                if (masterDetails.getReviews().size() > 0) {
                    norewies.setVisibility(View.GONE);
                } else {
                    norewies.setVisibility(View.VISIBLE);
                }


                if (masterDetails.getReviews().size() > 2) {
                    norewies.setVisibility(View.GONE);
                    docmore.setVisibility(View.VISIBLE);

                    for (int i = 0; i < 2; i++) {

                        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.docreviewdtl, reviewllout,false);

                        TextView reviewname, revtext;
                        RatingBar doctreviewrat;
                        ImageView reviewimage;
                        reviewname = (TextView) view.findViewById(R.id.reviewname);
                        doctreviewrat = (RatingBar) view.findViewById(R.id.doctreviewrat);
                        revtext = (TextView) view.findViewById(R.id.revtext);
                        reviewimage = (ImageView) view.findViewById(R.id.reviewimage);
                        reviewname.setText(masterDetails.getReviews().get(i).getBy());
                        revtext.setText(masterDetails.getReviews().get(i).getReview());
                        float barrating = Float.parseFloat(masterDetails.getReviews().get(i).getRating());
                        doctreviewrat.setRating(barrating);
                        doctreviewrat.setIsIndicator(true);
                        String rurl = masterDetails.getReviews().get(i).getPatPic();
                        rurl = rurl.replace(" ", "%20");
                        if (!rurl.equals("")) {
                            Picasso.with(this).load(rurl).resize((int) rwidth, (int) rheight)
                                    .transform(new CircleTransform()).into(reviewimage);
                        }

                        reviewllout.addView(view);
                    }
                    docmore.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            docmore.setVisibility(View.GONE);
                            norewies.setVisibility(View.GONE);
                            reviewllout.removeAllViews();

                            for (int j = 0; j < masterDetails.getReviews().size(); j++) {

                                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                                View view = inflater.inflate(R.layout.docreviewdtl, reviewllout,false);

                                TextView reviewname, revtext;
                                RatingBar doctreviewrat;
                                ImageView reviewimage;
                                reviewname = (TextView) view.findViewById(R.id.reviewname);
                                revtext = (TextView) view.findViewById(R.id.revtext);
                                doctreviewrat = (RatingBar) view.findViewById(R.id.doctreviewrat);
                                reviewimage = (ImageView) view.findViewById(R.id.reviewimage);
                                reviewname.setText(masterDetails.getReviews().get(j).getBy());
                                revtext.setText(masterDetails.getReviews().get(j).getReview());
                                float barrating = Float.parseFloat(masterDetails.getReviews().get(j).getRating());
                                doctreviewrat.setRating(barrating);
                                doctreviewrat.setIsIndicator(true);
                                String rurl = masterDetails.getReviews().get(j).getPatPic();
                                rurl = rurl.replace(" ", "%20");
                                if (!rurl.equals("")) {
                                    Picasso.with(MasterDetails.this).load(rurl).resize((int) rwidth, (int) rheight)
                                            .transform(new CircleTransform()).into(reviewimage);
                                }

                                reviewllout.addView(view);

                            }
                        }
                    });
                } else {
                    docmore.setVisibility(View.GONE);
                    for (int i = 0; i < masterDetails.getReviews().size(); i++) {

                        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
                        View view = inflater.inflate(R.layout.docreviewdtl, reviewllout,false);
                        TextView reviewname, revtext;
                        ImageView reviewimage;
                        RatingBar doctreviewrat;
                        reviewname = (TextView) view.findViewById(R.id.reviewname);
                        revtext = (TextView) view.findViewById(R.id.revtext);
                        doctreviewrat = (RatingBar) view.findViewById(R.id.doctreviewrat);
                        reviewimage = (ImageView) view.findViewById(R.id.reviewimage);
                        reviewname.setText(masterDetails.getReviews().get(i).getBy());
                        revtext.setText(masterDetails.getReviews().get(i).getReview());
                        float barrating = Float.parseFloat(masterDetails.getReviews().get(i).getRating());
                        doctreviewrat.setRating(barrating);
                        doctreviewrat.setIsIndicator(true);
                        String rurl = masterDetails.getReviews().get(i).getPatPic();
                        rurl = rurl.replace(" ", "%20");
                        if (!rurl.equals("")) {
                            Picasso.with(this).load(rurl).resize((int) rwidth, (int) rheight)
                                    .transform(new CircleTransform()).into(reviewimage);
                        }

                        reviewllout.addView(view);

                    }
                }


            }
            else if(masterDetails.getErrNum().equals("7") || masterDetails.getErrNum().equals("83") && masterDetails.getErrFlag().equals("1") )
            {

              //  Utility.setManagerWithBID(MasterDetails.this,manager);
                Utility.setMAnagerWithBID(MasterDetails.this,manager);

            }
            else
            {
                Toast.makeText(MasterDetails.this, masterDetails.getErrMsg(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * get the timing for the slotes
     */
    private void slotestiming()
    {
        docdetslots = gson.fromJson(manager.getDoctorSlots(),MasterDetails_Slots.class);
        if(docdetslots.getSlots().size()>0)
        {
            MainContainerslot.removeAllViews();
            horizontalslotView.setVisibility(View.VISIBLE);
            cardslots.setVisibility(View.VISIBLE);
            for(int i=0;i<docdetslots.getSlots().size();i++)
            {

                TextView opentimetext;
                RelativeLayout mainrelout;
                View inflatedLayoutview = getLayoutInflater().inflate(R.layout.docslotes_xml, MainContainerslot,false);
                mainrelout = (RelativeLayout) inflatedLayoutview.findViewById(R.id.mainrelout);
                opentimetext = (TextView) inflatedLayoutview.findViewById(R.id.timingtxt);
                mainrelout.setTag(docdetslots.getSlots().get(i).getSid());
                MainContainerslot.addView(inflatedLayoutview);
                mainrelout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v1) {
                        isclicked = v1.getTag().toString();
                        Utility.printLog("clicked outside" + v1.getTag());
                        Utility.printLog("clicked outside" + v1.getId());
                        priView();
                    }
                });
                if (i==0)
                {
                    String timingis = docdetslots.getSlots().get(0).getFrom()+ '\n'+"    to"+'\n'+docdetslots.getSlots().get(0).getTo()+'\n'+"  "+getResources().getString(R.string.currencySymbol)+docdetslots.getSlots().get(0).getSlotPrice();
                    opentimetext.setText(timingis);
                    mainrelout.setBackgroundResource(R.drawable.doc_slots_sel);
                    opentimetext.setTextColor(getResources().getColor(R.color.colorAccent));
                    docslotinfo(docdetslots.getSlots().get(0).getSid(),docdetslots.getSlots().get(0).getSlotPrice()
                            ,docdetslots.getSlots().get(0).getEtime(),docdetslots.getSlots().get(0).getStime());
                }
                else
                {
                    String timingis = docdetslots.getSlots().get(i).getFrom()+ '\n'+"    to"+'\n'+docdetslots.getSlots().get(i).getTo()+'\n'+ "  "+getResources().getString(R.string.currencySymbol)+docdetslots.getSlots().get(0).getSlotPrice();
                    opentimetext.setText(timingis);
                    mainrelout.setBackgroundResource(R.drawable.doc_slots_unsel);
                    opentimetext.setTextColor(getResources().getColor(R.color.gray));
                }

            }
        }
        else
        {
            horizontalslotView.setVisibility(View.GONE);
            cardslots.setVisibility(View.GONE);
        }

    }//End of slotestiming

    /**
     * this method is for selecting or unselecting
     */

    private void priView()
    {
        for(int j =0;j<MainContainerslot.getChildCount();j++)
        {
            View tempView=  MainContainerslot.getChildAt(j);
            TextView textView = (TextView) tempView.findViewById(R.id.timingtxt);
            RelativeLayout mainrelout = (RelativeLayout) tempView.findViewById(R.id.mainrelout);
            try
            {
                if(isclicked.equals(docdetslots.getSlots().get(j).getSid()))
                {
                    String timingis = docdetslots.getSlots().get(j).getFrom()+ '\n'+"    to"+'\n'+docdetslots.getSlots().get(j).getTo()+'\n'+"  "+getResources().getString(R.string.currencySymbol)+docdetslots.getSlots().get(j).getSlotPrice();

                    textView.setText(timingis);
                    mainrelout.setBackgroundResource(R.drawable.doc_slots_sel);
                    textView.setTextColor(getResources().getColor(R.color.colorAccent));
                    docslotinfo(docdetslots.getSlots().get(j).getSid(),
                            docdetslots.getSlots().get(j).getSlotPrice(),
                            docdetslots.getSlots().get(j).getEtime(),docdetslots.getSlots().get(j).getStime());
                }else
                {
                    String timingis = docdetslots.getSlots().get(j).getFrom()+ '\n'+"    to"+'\n'+docdetslots.getSlots().get(j).getTo()+'\n'+"  "+getResources().getString(R.string.currencySymbol)+docdetslots.getSlots().get(j).getSlotPrice();
                    textView.setText(timingis);
                    mainrelout.setBackgroundResource(R.drawable.doc_slots_unsel);
                    textView.setTextColor(getResources().getColor(R.color.gray));
                }

            }catch (Exception e)
            {
                Utility.printLog(" Exception" + e);
            }
        }

    }//End of preview


    private void docslotinfo( String sid, String slotPrice, String etime, String stime)//String timing,
    {

        sId = sid;
        sprice = slotPrice;
        eTime = etime;
        sTime = stime;

    }//End of docslotinfo

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Variableconstant.backpressforlist = true;
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        finish();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.confirmbooktv:
                Intent intent = new Intent(MasterDetails.this, ScrollingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("PID",pid);
                bundle.putString("TYPEID",typeId);
                bundle.putString("NAME",doctorname.getText().toString());
                bundle.putString("REVIEWS",doctorlike.getText().toString());
                bundle.putString("PIC",masterDetails.getpPic());
                bundle.putString("RATING",masterDetails.getRating());
                bundle.putString("DISTANCE",DISTANCE);
                bundle.putString("SID",sId);
                bundle.putString("eTime",eTime);
                bundle.putString("sTime",sTime);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.choseslots:
                //  openDate_Picker(MasterDetails.this, chosedate);
                break;
            case R.id.calenderimage:
                //  openDate_Picker(MasterDetails.this, chosedate);
                break;


        }
    }


}
