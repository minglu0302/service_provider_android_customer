package com.iserve.passenger;

import android.app.Application;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
import com.utility.Utility;
import com.utility.Variableconstant;
import io.fabric.sdk.android.Fabric;
import org.json.JSONObject;
import java.net.URISyntaxException;
import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * @author 3embed
 * <h>ApplicationController</h>
 * <p>
 *     this class create the object of socket, which is single reference throughout applications
 * </p>
 * Created by embed on 10/8/16.
 *
 */
public class ApplicationController extends Application
{
    private static Socket mSocket;
    private static ApplicationController mInstance;
    private static SockEmitter handler;
    private static String TAG = "ApplicationController";
    @Override
    public void onCreate()
    {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;
        initializeSocket();
    }
    public static synchronized ApplicationController getInstance()
    {
        return mInstance;
    }
    public static void initializeSocket()
    {
        if (mSocket == null)
        {
            synchronized (ApplicationController.class)
            {
                if (mSocket == null)
                {
                    try
                    {
                        mSocket = IO.socket(Variableconstant.SOCKET_PATH);

                        connectSocket();

                        initializeListeners();

                    }
                    catch (URISyntaxException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    public static void connectSocket()
    {
        if (mSocket != null && (!mSocket.connected()))
        {
            synchronized (ApplicationController.class)
            {
                if (mSocket != null  && (!mSocket.connected()))
                {
                    mSocket.connect();
                }
            }
        }
    }
    protected void emit(String eventName, JSONObject obj)
    {
        if(mSocket.connected())
        {
            mSocket.emit(eventName, obj, new Ack() {
                @Override
                public void call(Object... args) {

                }
            });
        }
    }
    private static void initializeListeners()
    {
        if(mSocket != null)
        {
            mSocket.on(SocketEvents.getHeartbeat.value,CustomerStatus);
            mSocket.on(SocketEvents.getUpdateCustomer.value,UpdateCustomer);
            mSocket.on(SocketEvents.getMessage.value,Message);
        }
    }
    private static Emitter.Listener UpdateCustomer = new Emitter.Listener()
    {
        @Override
        public void call(Object... args)
        {
            try
            {
                JSONObject data = new JSONObject(args[0].toString());
                handler.handleObject(SocketEvents.getUpdateCustomer.value,data);

                Log.d(TAG, "JSON MessageStatusUpdate Response: " + data);
            }
            catch (Exception e)
            {
                Log.d(TAG, "JSONException MessageStatusUpdate: " + e);
            }
        }
    };
    private static Emitter.Listener CustomerStatus = new Emitter.Listener()
    {
        @Override
        public void call(Object... args)
        {
            Log.d(TAG,"Emitter Listener connect");
            JSONObject data;
            try {
                data = new JSONObject(args[0].toString());
                Log.d(TAG, "JSON message_chat_: " + data);

                handler.handleObject(SocketEvents.getHeartbeat.value,data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private static Emitter.Listener Message = new Emitter.Listener()
    {
        @Override
        public void call(Object... args)
        {
            try
            {
                JSONObject data = new JSONObject(args[0].toString());
                handler.handleObject(SocketEvents.getMessage.value,data);
                Log.d(TAG, "JSON MessageStatusUpdate Response: " + data);
            }
            catch (Exception e)
            {
                Log.d(TAG, "JSONException MessageStatusUpdate: " + e);
            }
        }
    };
    public static void initInstance(SockEmitter responseHandler)
    {
        handler = responseHandler;
    }
}
