package com.iserve.passenger;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.gson.Gson;
import com.pojo.NotReviewedAppt;
import com.pojo.PubNubMas;
import com.pojo.PubNubMasArr;
import com.pojo.PubNubType;
import com.pojo.PubNub_pojo;
import com.pojo.TimeForNearestPojo;
import com.squareup.picasso.Picasso;
import com.utility.Alerts;
import com.utility.CircleTransform;
import com.utility.GeocodingResponse;
import com.utility.LocationNotifier;
import com.utility.LocationUtil;
import com.utility.OkHttp3Request;
import com.utility.Scaler;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import static android.view.View.VISIBLE;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;
import static java.lang.Math.*;

/**
 * Created by embed on 14/12/15.
 *
 */
public class HomeFragment extends Fragment implements View.OnClickListener, LocationNotifier,FragmentManager.OnBackStackChangedListener {
    private String TAG = "HomeFragment";
    private TextView homejobaddress,yourlocation,youraddress,tveta,tvmin;
    private ImageView  tolbarhomelogo;
    private LinearLayout iserve_type;
    private RelativeLayout relative_all_car_types,rrlmapmain,rlhomeboknw,rlhomebokltr,rlempty,homeiservrlv;
    private RelativeLayout timeRl,rlorbooklatter,rrlmaintab;
    private String delivererId = 1 + "";
    private String isclicked = 1 + "";
    private String latlongforETA = "";
    private String  scheduleday;
    private int doubleclick;
    private boolean firstTime = true;
    private GoogleMap mMap = null;
    private double curlatlng[] = new double[2];
    private double currentLatitude, currentLongitude;
    private SessionManager manager;
    private Timer myTimer_publish;
    private LocationUtil locationUtil;
    private Timer myTimer;
    private boolean listviewclicked = false;
    private boolean isfirsttimeclicked = false;
    private boolean IsreturnFromSearch = false;
    private boolean isaddresslocked = false;
    private boolean isFirsttimelatlong = true;
    private boolean isFirsttimeETA = true;
    private boolean isProAvailable=false;
    private boolean isFirstTime = true;
    private double latownETA;
    private double longownETA;
    private Gson gson;
    private PubNub_pojo nubPojo;
    private double height;
    private double width;
    private double size[];
    private Marker driverMarker;
    private Geocoder geocoder;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 99;
    private Typeface addresstext;
    private Typeface semibold;
    private Typeface italicfont;
    private Typeface regulrtxt;
    private ProgressBar progress;
    private Context  mcontext;
    private TextView orderTV;
    private ImageView action_map;
    private ImageView action_list;
    private ViewPager viewPager;
    private SampleFragmentPagerAdapter sfpa;
    private TabLayout tabLayout;
    private boolean isAdapter_added=false;
    private int data_visible_point;
    private int initial_Adapter_size;
    private ProgressDialog progressDialog,pdialog;
    private NotReviewedAppt notReviewedAppt;
    private ArrayList<String> allcartypes = new ArrayList<>();
    private HashMap<String, ArrayList<String>> markers;
    private ArrayList<PubNubType> types;
    private ArrayList<Pager_data_updater> listerner_list;
    private ArrayList<String> alltidare = new ArrayList<>();
    private ArrayList<String>savetime = new ArrayList<>();
    private ArrayList<String>valuinmarker = new ArrayList<>();
    private Socket socket;
    {
        try {
            socket = IO.socket(Variableconstant.SOCKET_PATH);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.homefragment_layout, container, false);
        generalmethodToInit();
        initialze(view);
        mapmethod();
        socket.connect();
        socket.on("UpdateCustomer", handlIncomingMessages);
        socket.on("CustomerStatus", handleCustomerStatus);
        alltidare.clear();
        return view;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mcontext = getActivity();
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    Emitter.Listener handleCustomerStatus = new Emitter.Listener() {
        @Override
        public void call(Object... args)
        {
           // String alertcode;
            JSONObject data = (JSONObject) args[0];
            try {
                if(data.getString("st").equals("10"))
                {
                    final String alertcode = data.getString("alert");

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            calCanceldialog(alertcode);
                        }
                    });
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    };

    private void calCanceldialog(String finalAlertcode)
    {
        new AlertDialog.Builder(getActivity())
                .setTitle("Pro cancellation reason")
                .setMessage(finalAlertcode)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                       // getAppointmentDetails(0);
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_launcher)
                .show();
    }

    Emitter.Listener handlIncomingMessages = new Emitter.Listener() {
        @Override
        public void call(final Object... args)
        {
            if(mcontext!=null)
            {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        Log.d("UpdateCustomer", "responce " + data );
                        if(!listviewclicked)
                        {
                                preview(data.toString());
                        }
                        else
                        {
                                nubPojo = gson.fromJson(data.toString(), PubNub_pojo.class);
                                types = nubPojo.getMsg().getTypes();
                                if(types.size()>0)
                                {
                                    rrlmaintab.setVisibility(View.VISIBLE);
                                    rlempty.setVisibility(View.GONE);

                                    if(initial_Adapter_size==0)
                                    {
                                        initial_Adapter_size=types.size();
                                    }

                                    if(initial_Adapter_size!=types.size())
                                    {
                                        isAdapter_added=false;
                                    }

                                    if(!isAdapter_added)
                                    {
                                        setadatper();
                                    }else
                                    {
                                        Pager_data_updater updater=listerner_list.get(data_visible_point);
                                        if(pdialog!=null)
                                        {
                                            pdialog.dismiss();
                                        }
                                        if(updater!=null)
                                        {
                                            updater.onUpdated(data);
                                        }
                                    }
                                }
                                else
                                {
                                    rlempty.setVisibility(VISIBLE);
                                    rrlmaintab.setVisibility(View.GONE);
                                    if(pdialog!=null)
                                    {
                                        pdialog.dismiss();
                                        pdialog.cancel();
                                    }
                                }
                        }
                    }
                });
            }
        }
    };
    private void setadatper()
    {
        listerner_list.clear();
        sfpa.clearFragment();
        for(int i =0;i<types.size();i++)
        {
            PageFragment pageFragment=PageFragment.newInstance(nubPojo.getMsg().getMasArr().get(i).getMas(),nubPojo.getMsg().getMasArr().get(i).getTid(),
                    types.get(i).getFtype(),types.get(i).getIsGroupContain()
                   );
            listerner_list.add(pageFragment.getListener());
            sfpa.addFragment(pageFragment,types.get(i).getCat_name());

            viewPager.setAdapter(sfpa);
            sfpa.notifyDataSetChanged();
        }
        tabLayout.setupWithViewPager(viewPager);
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(regulrtxt);
                }
            }
        }
        if(pdialog!=null)
        {
            pdialog.dismiss();
            pdialog.cancel();
        }
        isAdapter_added=true;
    }
    @Override
    public void onStop() {
        super.onStop();
    }
    private void openAutocompleteActivity() {
        try {

            LatLngBounds bound = Utility.setBound(currentLatitude,currentLongitude);
           /* LatLngBounds bound = new LatLngBounds(
                    new LatLng(12.746658, 77.563477),
                    new LatLng(13.241763, 77.654114));*/
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setBoundsBias(bound)
                    .build(getActivity());
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {

            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Log.e(TAG, message);
            Toast.makeText(mcontext, message, Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    public void updatedInfo(String info) {

    }
    @Override
    public void locationUpdates(Location location) {
        curlatlng[0] = location.getLatitude();
        curlatlng[1] = location.getLongitude();
        manager.setLATITUDE(curlatlng[0] + "");
        manager.setLONGITUDE(curlatlng[1] + "");
    }
    @Override
    public void locationFailed(String message) {}
    private void mapmethod() {
        SupportMapFragment fm = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.googleMap);
        fm.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap)
            {
                mMap = googleMap;
                if (ActivityCompat.checkSelfPermission(mcontext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mcontext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                int value = (int)Math.round(5 * size[0]);
                int value1 = (int)Math.round(5 * size[1]);
                googleMap.setPadding(0, value, value1, 0);
                googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                    @Override
                    public void onCameraChange(CameraPosition arg0) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                IsreturnFromSearch = false;
                            }
                        }, 5000);
                    }
                });
                if(curlatlng[0]==0.0 && curlatlng[1]==0.0)
                {
                    LatLng latLng;
                    if(Splash_Screen.latiLongi[0]==0.0&& Splash_Screen.latiLongi[1]==0){

                        latLng = new LatLng(Double.parseDouble(manager.getLATITUDE()),Double.parseDouble(manager.getLONGITUDE()));

                    }else{
                        latLng = new LatLng(Splash_Screen.latiLongi[0], Splash_Screen.latiLongi[1]);

                    }
                    updatCameraInfo(latLng,googleMap);
                }
                else
                {
                    LatLng latLng1 = new LatLng(curlatlng[0], curlatlng[1]);
                    updatCameraInfo(latLng1,googleMap);
                }
            }
        });
    }//End Of Map Method
    /**
     * <h2>updatCameraInfo</h2>
     * <p>updating the camera to the desired locations</p>
     * @param latLng latitude and langotude object
     * @param googleMap GoogleMap object
     */
    private void updatCameraInfo(LatLng latLng, GoogleMap googleMap)
    {
        CameraUpdate center= CameraUpdateFactory.newLatLng(latLng);
        CameraUpdate zoom= CameraUpdateFactory.zoomTo(16.0f);
        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);
    }
    private void initialze(View view)
    {
        markers = new HashMap<>();
        types = new ArrayList<>();
        TextView chosedate = (TextView) view.findViewById(R.id.chosedate);
        TextView choseslots = (TextView) view.findViewById(R.id.choseslots);
        TextView tvmasterbokltr = (TextView) view.findViewById(R.id.tvmasterbokltr);
        TextView tvwearenotavailable = (TextView) view.findViewById(R.id.tvwearenotavailable);
        ImageView calenderimage = (ImageView) view.findViewById(R.id.calenderimage);
        timeRl = (RelativeLayout)view.findViewById(R.id.timeRl);
        rlempty = (RelativeLayout)view.findViewById(R.id.rlempty);
        rrlmaintab = (RelativeLayout)view.findViewById(R.id.rrlmaintab);
        rlhomeboknw = (RelativeLayout)view.findViewById(R.id.rlhomeboknw);
        rlhomebokltr = (RelativeLayout)view.findViewById(R.id.rlhomebokltr);
        homeiservrlv = (RelativeLayout)view.findViewById(R.id.homeiservrlv);
        rlorbooklatter = (RelativeLayout)view.findViewById(R.id.rlorbooklatter);
        RelativeLayout imageframelayout = (RelativeLayout) getActivity().findViewById(R.id.imageframelayout);
        viewPager = (ViewPager)view.findViewById(R.id.pager);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorHeight(5);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.actionbar_color));
        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        sfpa = new SampleFragmentPagerAdapter(getFragmentManager());
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position)
            {
                data_visible_point=position;
                if(nubPojo.getMsg().getMasArr().get(position).getMas().size()>0)
                {
                    String proavilablenw = getResources().getString(R.string.providerwhoareavailable);
                    orderTV.setText(proavilablenw);
                }
                else
                {
                    String proavilablenw =getResources().getString(R.string.noprovideravailablenow);
                    orderTV.setText(proavilablenw);
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        rrlmaintab.setVisibility(View.GONE);
        rrlmapmain = (RelativeLayout) view.findViewById(R.id.rrlmapmain);
        orderTV = (TextView) view.findViewById(R.id.orderTV);
        RelativeLayout searchrrlout = (RelativeLayout) view.findViewById(R.id.searchrrlout);
        tveta = (TextView) view.findViewById(R.id.tveta);
        tvmin = (TextView) view.findViewById(R.id.tvmin);
        progress = (ProgressBar) view.findViewById(R.id.progress);
        TextView homejoblocation = (TextView) view.findViewById(R.id.homejoblocation);
        homejobaddress = (TextView) view.findViewById(R.id.homejobaddress);
        TextView homeboknw = (TextView) view.findViewById(R.id.homeboknw);
        TextView homebokltr = (TextView) view.findViewById(R.id.homebokltr);
        TextView toolbarhometxv = (TextView) getActivity().findViewById(R.id.toolbarhometxv);
        ImageView midpointmarker = (ImageView) view.findViewById(R.id.midpointmarker);
        ImageView homecurrentlociv = (ImageView) view.findViewById(R.id.homecurrentlociv);
        ImageView home_location = (ImageView) view.findViewById(R.id.home_location);
        imageframelayout.setVisibility(View.VISIBLE);
        toolbarhometxv.setVisibility(View.GONE);
        tolbarhomelogo = (ImageView) getActivity().findViewById(R.id.tolbarhomelogo);
        youraddress = (TextView) getActivity().findViewById(R.id.youraddress);
        yourlocation = (TextView) getActivity().findViewById(R.id.yourlocation);
        TextView tveditbarhometxv = (TextView) getActivity().findViewById(R.id.tveditbarhometxv);
        tveditbarhometxv.setVisibility(View.GONE);
        tolbarhomelogo.setVisibility(View.VISIBLE);
        iserve_type = (LinearLayout) view.findViewById(R.id.iserve_type);
        relative_all_car_types = (RelativeLayout) view.findViewById(R.id.relative_all_car_types);
        RelativeLayout rllistmapicon = (RelativeLayout) getActivity().findViewById(R.id.rllistmapicon);
        action_map = (ImageView) getActivity().findViewById(R.id.action_map);
        action_list = (ImageView) getActivity().findViewById(R.id.action_list);
        rllistmapicon.setVisibility(VISIBLE);
        action_map.setSelected(true);
        action_list.setSelected(false);
        progress.setVisibility(VISIBLE);
        homeboknw.setOnClickListener(this);
        homebokltr.setOnClickListener(this);
        midpointmarker.setOnClickListener(this);
        homecurrentlociv.setOnClickListener(this);
        home_location.setOnClickListener(this);
        searchrrlout.setOnClickListener(this);
        tvmasterbokltr.setOnClickListener(this);
        choseslots.setOnClickListener(this);
        calenderimage.setOnClickListener(this);
        action_map.setOnClickListener(this);
        action_list.setOnClickListener(this);
        getActivity().getFragmentManager().addOnBackStackChangedListener(this);
        homejobaddress.setTypeface(regulrtxt);
        youraddress.setTypeface(regulrtxt);
        homejoblocation.setTypeface(addresstext);
        yourlocation.setTypeface(addresstext);
        homeboknw.setTypeface(regulrtxt);
        homebokltr.setTypeface(regulrtxt);
        tveta.setTypeface(regulrtxt);
        tvmin.setTypeface(regulrtxt);
        choseslots.setTypeface(regulrtxt);
        chosedate.setTypeface(semibold);
        tvwearenotavailable.setTypeface(italicfont);
        orderTV.setTypeface(regulrtxt);
        if(!manager.getPUNUB_RES().equals(""))
        {
            alltidare.clear();
            addvehicles("oncreate");
        }
    }
    private void sendrequestessage(double latitude, double logitude, int btype, String datetime)
    {
        JSONObject sendText = new JSONObject();
        try
        {
            sendText.put("email",manager.getCoustomerEmail());
            sendText.put("lat",latitude);
            sendText.put("long",logitude);
            sendText.put("btype",btype);
            sendText.put("dt",datetime);
            socket.emit("UpdateCustomer", sendText);
        }catch(JSONException e)
        {
            e.printStackTrace();
        }
    }
    @Override
    public void onClick(View v)
    {
        Intent intent;
        switch (v.getId())
        {

            case R.id.tvmasterbokltr:


                if(!homejobaddress.getText().toString().trim().equals(""))
                {
                    openDate_Picker(getActivity());
                }
                else
                {
                    Snackbar.make(getView(), getResources().getString(R.string.pleasewaitwearegintuaaddress), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

                break;
            case R.id.homeboknw:

                if(!homejobaddress.getText().toString().trim().equals(""))
                {
                    if(isProAvailable)
                    {
                        Variableconstant.onresumeflag = false;
                        Variableconstant.comingfromlaterbooking = false;
                        intent= new Intent(mcontext,Confirmation_Screen.class);
                        manager.setSavedAddress(homejobaddress.getText().toString());
                        Variableconstant.ent_btype = 1;
                        manager.setJOBLATI(String.valueOf(currentLatitude));
                        manager.setJOBLONGI(String.valueOf(currentLongitude));
                        intent.putExtra("SELECTED_ID", isclicked);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(mcontext,getResources().getString(R.string.noprovideravailable),Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {

                    Snackbar.make(getView(), getResources().getString(R.string.pleasewaitwearegintuaaddress), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                break;
            case R.id.homebokltr:

                if(!homejobaddress.getText().toString().trim().equals(""))
                {

                    openDate_Picker(getActivity());

                }
                else
                {
                    Snackbar.make(getView(), getResources().getString(R.string.pleasewaitwearegintuaaddress), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                break;

            case R.id.midpointmarker:

                break;
            case R.id.homecurrentlociv:


                if(curlatlng[0]==0.0 && curlatlng[1]==0.0)
                {
                    LatLng latLng;
                    if(Splash_Screen.latiLongi[0]==0.0&& Splash_Screen.latiLongi[1]==0){

                        latLng = new LatLng(Double.parseDouble(manager.getLATITUDE()),Double.parseDouble(manager.getLONGITUDE()));

                    }else{
                        latLng = new LatLng(Splash_Screen.latiLongi[0], Splash_Screen.latiLongi[1]);

                    }
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));

                }
                else
                {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(curlatlng[0], curlatlng[1]), 17.0f));

                }
                break;
            case R.id.home_location:
                openAutocompleteActivity();
                break;
            case R.id.searchrrlout:
                openAutocompleteActivity();
                break;
            case R.id.action_map:
                if(Utility.isNetworkAvailable(mcontext))
                {
                    listviewclicked = false;
                    action_map.setSelected(true);
                    action_list.setSelected(false);
                    if(nubPojo!=null)
                    {
                        listviewselected();
                    }
                }

                break;
            case R.id.action_list:
                if(types.size()>0)
                {
                    if(Utility.isNetworkAvailable(mcontext))
                    {
                        if(pdialog!=null)
                        {
                            pdialog.show();
                        }
                        listviewclicked = true;
                        action_list.setSelected(true);
                        action_map.setSelected(false);
                        if(nubPojo!=null)
                        {
                            listviewselected();
                        }
                        else
                        {
                            Toast.makeText(mcontext,getResources().getString(R.string.wait),Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Alerts alerts  = new Alerts(mcontext);
                        alerts.showNetworkAlert(mcontext);
                    }
                }
                else
                {
                    Toast.makeText(mcontext,getResources().getString(R.string.wait),Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
    @Override
    public void onResume()
    {
        super.onResume();
        emitHeartbeat("1");
        myTimer.cancel();
        myTimer.purge();
        myTimer = new Timer();
        isFirsttimelatlong = true;
        isFirsttimeETA = true;
        if(!Variableconstant.isLIVBOKINOPN)
        {
            manager.setPUSHMSGFORMAP("");
        }
        Variableconstant.ent_btype = 3;
        Variableconstant.ent_forlaterbooking=-1;
        if(Utility.isNetworkAvailable(mcontext))
        {
            getAllOngoingBookings();
        }
        else
        {
            Toast.makeText(mcontext,getResources().getString(R.string.nointernet),Toast.LENGTH_SHORT).show();
        }
        isFirstTime = true;
        Variableconstant.onresumeflag = true;
        Variableconstant.showProfile = false;
        Variableconstant.comingfromlaterbooking = false;
        Variableconstant.ent_SlotDate = "";
        manager.setProAccept(false);
        locationUtil.connectGoogleApiClient();
        if(action_map.isSelected())
        {
            action_map.setSelected(true);
            action_list.setSelected(false);
            rrlmapmain.setVisibility(View.VISIBLE);
            tolbarhomelogo.setVisibility(View.VISIBLE);
            yourlocation.setVisibility(View.GONE);
            youraddress.setVisibility(View.GONE);
            rrlmaintab.setVisibility(View.GONE);
            listviewclicked = false;
            rlempty.setVisibility(View.GONE);
        }
       else if(action_list.isSelected())
        {
            listviewclicked = true;
            action_list.setSelected(true);
            action_map.setSelected(false);
            yourlocation.setVisibility(View.VISIBLE);
            youraddress.setVisibility(View.VISIBLE);
            rrlmapmain.setVisibility(View.GONE);
            tolbarhomelogo.setVisibility(View.GONE);
            rrlmaintab.setVisibility(View.VISIBLE);
        }
        rlempty.setVisibility(View.GONE);
        timeRl.setVisibility(View.GONE);
        rlorbooklatter.setVisibility(View.VISIBLE);
        orderTV.setVisibility(View.VISIBLE);
        Log.i(TAG,"sessiontoken "+manager.getSession());
        Log.i(TAG,"sessiondevice "+manager.getDevice_Id());
        addressTimer();
        myTimer_publish = null;
        startPublishingWithTimer();
    }//End OF OnResume
    private void getAllOngoingBookings()
    {
        if(progressDialog!=null)
        {
            progressDialog.setMessage(getResources().getString(R.string.wait));
            progressDialog.show();
        }
        FormBody request = new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_cust_id",manager.getCustomerId())
                .add("ent_date_time",Utility.dateintwtfour())
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "getAllOngoingBookings", request, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                if(progressDialog!=null)
                {
                    progressDialog.dismiss();
                }
                Utility.printLog("getAllOngoingBookings : ",result);

                if(result!=null)
                {
                    notReviewedAppt = gson.fromJson(result,NotReviewedAppt.class);
                    if(notReviewedAppt!=null)
                    {
                        if(notReviewedAppt.getErrFlag().equals("0") && notReviewedAppt.getErrNum().equals("31"))
                        {
                            if(notReviewedAppt.getNotReviewedAppts().size()>0)
                            {

                                callreviewmethod(notReviewedAppt.getNotReviewedAppts());
                            }
                        }
                        else if(notReviewedAppt.getErrFlag().equals("1") && notReviewedAppt.getErrNum().equals("7") ||notReviewedAppt.getErrNum().equals("83"))
                        {
                            emitHeartbeat("0");
                            socket.disconnect();
                            socket.close();
                            socket.off();
                            Utility.setMAnagerWithBID(mcontext,manager);


                        }
                    }
                }
            }
            @Override
            public void onError(String error)
            {
                if(progressDialog!=null)
                {
                    progressDialog.dismiss();
                }
            }
        });
    }
    public void emitHeartbeat(String status)
    {
        JSONObject obj = new JSONObject();
        try
        {
            obj.put("status",status);
            obj.put("cid", manager.getCustomerId());

        } catch (JSONException e)
        {
            e.printStackTrace();
        }
         socket.emit("CustomerStatus", obj);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == -1) {
                IsreturnFromSearch = true;
                final Place place = PlaceAutocomplete.getPlace(mcontext, data);
                String placefull =place.getName()+", "+ place.getAddress();
                homejobaddress.setText(placefull);
                youraddress.setText(placefull);
                currentLatitude = place.getLatLng().latitude;
                currentLongitude = place.getLatLng().longitude;
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(place.getLatLng().latitude, place.getLatLng().longitude), 17.0f));
                listviewselected();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(mcontext, data);
                Log.d(TAG, "Error: Status = " + status.toString());
            } else if (resultCode == 0)
            {
                IsreturnFromSearch= false;
            }
        }
    }

    /**
     * publishing passenger channel for every 5 second
     */
    public void startPublishingWithTimer()
    {

        TimerTask myTimerTask_publish;
        if(myTimer_publish!= null)
        {
            Log.d(TAG, "Timer already started");
            return;
        }
        myTimer_publish = new Timer();
        myTimerTask_publish = new TimerTask()
        {
            @Override
            public void run()
            {
                if(currentLatitude!=0.0 || currentLongitude!=0.0)
                    sendrequestessage(currentLatitude, currentLongitude,Variableconstant.ent_btype,Variableconstant.ent_dt);//Variableconstant.ent_forlaterbooking
            }
        };
        myTimer_publish.schedule(myTimerTask_publish, 0, 4000);
    }
    private void preview(String pubnubmsg)
    {

        String url;
        if (!pubnubmsg.equals("") && pubnubmsg != null) {
            iserve_type.removeAllViews();
            progress.setVisibility(View.GONE);
            if(!Variableconstant.comingfromlaterbooking)
            {
                manager.setSocketRes(pubnubmsg);
            }
            nubPojo = gson.fromJson(manager.getPUNUB_RES(),PubNub_pojo.class);
            if (nubPojo != null)
            {
                try
                {
                    mMap.clear();
                    valuinmarker.clear();
                    markers.clear();
                } catch (Exception e)
                {

                    e.printStackTrace();
                }

                if (nubPojo.getMsg().getTypes() != null)
                {

                    if (nubPojo.getMsg().getTypes().size() == 0)
                    {
                        relative_all_car_types.setVisibility(View.GONE);
                        Toast.makeText(mcontext, " "+getResources().getString(R.string.wearenotavailable), Toast.LENGTH_SHORT).show();
                        tveta.setText(getResources().getString(R.string.no));
                        tvmin.setText(getResources().getString(R.string.provider));
                        rlhomeboknw.setVisibility(View.GONE);
                        rlhomebokltr.setVisibility(View.GONE);
                        homeiservrlv.setVisibility(View.GONE);

                    } else {

                        types = nubPojo.getMsg().getTypes();
                        relative_all_car_types.setVisibility(View.VISIBLE);
                        rlhomeboknw.setVisibility(View.VISIBLE);
                        rlhomebokltr.setVisibility(View.VISIBLE);
                        homeiservrlv.setVisibility(View.VISIBLE);
                        /*
                         * forEta
                         */
                        String destinationlatlong= "";
                        alltidare.clear();
                        if(nubPojo.getMsg().getMasArr().size()>0)
                        {
                            for(int i=0;i<nubPojo.getMsg().getMasArr().size();i++)
                            {

                                if(nubPojo.getMsg().getMasArr().get(i).getMas().size()>0)
                                {
                                    if(isFirsttimelatlong)
                                    {
                                        latownETA = currentLatitude;
                                        longownETA = currentLongitude;
                                        destinationlatlong =  addLatlongForEta(true,nubPojo.getMsg().getMasArr().get(i).getMas(),destinationlatlong);

                                    }
                                    else
                                    {
                                        destinationlatlong =  addLatlongForEta(false,nubPojo.getMsg().getMasArr().get(i).getMas(),destinationlatlong);

                                    }
                                    alltidare.add(nubPojo.getMsg().getMasArr().get(i).getTid());

                                }

                            }
                            isFirsttimelatlong = false;
                        }
                        if(isFirsttimeETA)
                        {
                            if(!destinationlatlong.equals(""))
                            {

                                String timeUrl="https://maps.googleapis.com/maps/api/distancematrix/json?origins="+String.valueOf(currentLatitude)+","+String.valueOf(currentLongitude)
                                        +"&"+"destinations="+destinationlatlong+"&mode=driving"+"&"+"key="+Variableconstant.SERVERKEY;
                                new getETA().execute(timeUrl);
                            }
                            isFirsttimeETA = false;
                        }
                        else
                        {
                            if(!destinationlatlong.equals("") && !latlongforETA.equals(destinationlatlong) || latownETA!=currentLatitude && longownETA!=currentLongitude)
                            {

                                latlongforETA = destinationlatlong;
                                latownETA = currentLatitude;
                                longownETA = currentLongitude;
                                String timeUrl="https://maps.googleapis.com/maps/api/distancematrix/json?origins="+String.valueOf(currentLatitude)+","+String.valueOf(currentLongitude)
                                        +"&"+"destinations="+latlongforETA+"&mode=driving"+"&"+"key="+Variableconstant.SERVERKEY;
                                new getETA().execute(timeUrl);
                            }
                        }
                        //EAT DONE
                        addvehicles("pubnub");
                    }
                }
            }
        }

        for (int j = 0; j < iserve_type.getChildCount(); j++)
        {

            View tempView = iserve_type.getChildAt(j);
            ImageView tempimg = (ImageView) tempView.findViewById(R.id.provider_image);
            if(!allcartypes.contains(isclicked)){
                isclicked=nubPojo.getMsg().getTypes().get(0).getType_id();
                delivererId=nubPojo.getMsg().getTypes().get(0).getType_id();
            }

            try {

                if (isclicked.equals(nubPojo.getMsg().getTypes().get(j).getType_id()))
                {
                    url = Variableconstant.pics_path + nubPojo.getMsg().getTypes().get(j).getSelImg();
                    TextView tempdist = (TextView) tempView.findViewById(R.id.distance);
                    TextView tempTypeName = (TextView) tempView.findViewById(R.id.provider_name);
                    tempTypeName.setText(nubPojo.getMsg().getTypes().get(j).getCat_name());

                    if(!listviewclicked)
                    {
                        Variableconstant.now_servicegroup = nubPojo.getMsg().getTypes().get(j).getIsGroupContain();
                        manager.setServiceProvdr(nubPojo.getMsg().getTypes().get(j).getCat_name());
                        manager.setServiceProviderNAme(nubPojo.getMsg().getTypes().get(j).getFtype());
                    }
                    int tempcolr = getColor(mcontext, R.color.actionbar_color);
                    tempTypeName.setTextColor(tempcolr);
                    if (nubPojo.getMsg().getMasArr().get(j).getMas().size() > 0)
                    {

                        if(nubPojo.getMsg().getMasArr().get(j).getTid().equals(isclicked))
                        {

                            isProAvailable=true;
                            isfirsttimeclicked = true;

                            if(alltidare.contains(nubPojo.getMsg().getMasArr().get(j).getTid()))
                            {

                                int index = alltidare.indexOf(nubPojo.getMsg().getMasArr().get(j).getTid());
                                if(savetime.size()==alltidare.size())
                                {
                                    tvmin.setText(getResources().getString(R.string.minute));
                                    tveta.setText(savetime.get(index));
                                }
                            }

                            for (int p = 0; p < nubPojo.getMsg().getMasArr().get(j).getMas().size(); p++) {

                                ArrayList<PubNubMas>punbunMas = nubPojo.getMsg().getMasArr().get(j).getMas();
                                ploatMapDataDistnce(punbunMas,punbunMas.get(p),tempdist);
                            }
                        }
                    }
                    else
                        noProviderAvailble(tvmin,tveta,tempdist);

                    if (doubleclick == 2)
                    {
                        doubleclick = 1;
                        showDialoag(nubPojo.getMsg().getTypes().get(j).getVisit_fees(), nubPojo.getMsg().getTypes().get(j).getPrice_min()
                                );//,nubPojo.getMsg().getTypes().get(j).getBase_fees()
                    }

                }
                else {
                    url = Variableconstant.pics_path + nubPojo.getMsg().getTypes().get(j).getUnSelImg();
                    TextView tempdist = (TextView) tempView.findViewById(R.id.distance);
                    TextView tempTypeName = (TextView) tempView.findViewById(R.id.provider_name);
                    tempTypeName.setText(nubPojo.getMsg().getTypes().get(j).getCat_name());

                    if (nubPojo.getMsg().getMasArr().get(j).getMas().size() > 0)
                    {
                        if(!nubPojo.getMsg().getMasArr().get(j).getMas().get(0).getD().equals(""))
                        setDistanceInKmFormate(nubPojo.getMsg().getMasArr().get(j).getMas().get(0).getD(),tempdist);
                    }
                    else
                    {
                        tempdist.setText(getResources().getString(R.string.noprovider));
                        tempdist.setTextColor(Color.RED);
                    }

                    int tempcolr = getColor(mcontext, R.color.gray);
                    tempTypeName.setTextColor(tempcolr);



                }
                url = url.replace(" ", "%20");

                if (!url.equals("")) {
                    Picasso.with(mcontext).load(url)
                            .resize((int) width, (int) height)
                            .transform(new CircleTransform())
                            .into(tempimg);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void ploatMapDataDistnce(ArrayList<PubNubMas> punbunMas, PubNubMas pubNubMas, TextView tempdist)
    {
        PubNubMasArr pubNubMasArr = new PubNubMasArr();
        pubNubMasArr.setMas(punbunMas);
        if(!pubNubMas.getD().equals(""))
            setDistanceInKmFormate(pubNubMas.getD(),tempdist);

        valuinmarker = new ArrayList<>();
        valuinmarker.add(pubNubMas.getPid());
        valuinmarker.add(pubNubMas.getAmt());
        valuinmarker.add(pubNubMas.getD());
        markingDeliverers(pubNubMas.getLt(),
                pubNubMas.getLg(),
                pubNubMas.getI(),
                valuinmarker);
    }
    private void setDistanceInKmFormate(String pubNubMas, TextView tempdist)
    {
        NumberFormat nf_out = NumberFormat.getNumberInstance(Locale.US);
        nf_out.setMaximumFractionDigits(2);
        nf_out.setGroupingUsed(false);
        String faremount=nf_out.format(Double.parseDouble(pubNubMas)/1000);
        double kilometerDistance = Double.parseDouble(faremount);//*1.6
        @SuppressLint("DefaultLocale") String kilometer = String.format("%.2f", kilometerDistance);
        String kilometr = kilometer + " " + getResources().getString(R.string.distance);
        tempdist.setText(kilometr);
        tempdist.setTextColor(Color.BLACK);
    }

    private String addLatlongForEta(boolean b, ArrayList<PubNubMas> mas, String destinationlatlong)
    {
        String returnlatlng;
        if(destinationlatlong.equals(""))
        {
            destinationlatlong = mas.get(0).getLt()+","+ mas.get(0).getLg();
            returnlatlng = destinationlatlong;
        }

        else
        {
            destinationlatlong = destinationlatlong+"|"+mas.get(0).getLt()+","+ mas.get(0).getLg();
            returnlatlng = destinationlatlong;
        }

        if(b)
        {
            latlongforETA = destinationlatlong;
            returnlatlng = latlongforETA;
        }

        return  returnlatlng;
    }
    /**
     * adding view to horizantal scroll view
     * A string is passing to this method from which it is calling, if it coming from on create method then
     * oncreate string is passed to set first vehicle is in onstate.
     * @param cmingfrom it indicate from where you are adding vehical
     */
    void addvehicles(String cmingfrom)
    {
        if(mcontext!=null)
        {
            allcartypes.clear();

            for (int i = 0; i < nubPojo.getMsg().getTypes().size(); i++)
            {
                TextView provider_name,distance;
                ImageView provider_image;
                LayoutInflater inflatervehicle = LayoutInflater.from(mcontext);
                View inflatedLayout = inflatervehicle.inflate(R.layout.single_provider, iserve_type, false);
                distance = (TextView) inflatedLayout.findViewById(R.id.distance);
                provider_name = (TextView) inflatedLayout.findViewById(R.id.provider_name);
                provider_image = (ImageView) inflatedLayout.findViewById(R.id.provider_image);
                RelativeLayout provider_Rl = (RelativeLayout) inflatedLayout.findViewById(R.id.provider_Rl);
                final RelativeLayout rl_proforlatrboking = (RelativeLayout) inflatedLayout.findViewById(R.id.rl_proforlatrboking);
                /*
                 * setting tag to each type
                 */
                provider_Rl.setTag(nubPojo.getMsg().getTypes().get(i).getType_id());
                allcartypes.add(nubPojo.getMsg().getTypes().get(i).getType_id());
                rl_proforlatrboking.setTag(i);
                /*
                 * add view to linear layout in scroll view
                 */

                iserve_type.addView(inflatedLayout);
                /*
                 * setting type name and distance
                 */
                provider_name.setText(nubPojo.getMsg().getTypes().get(i).getCat_name());
                /*
                 * handling on click of deliverer types
                 */
                provider_Rl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v1)
                    {


                            if (delivererId.equals(v1.getTag().toString()))
                            {
                                doubleclick = 2;
                            }
                            else
                            {
                                doubleclick = 1;
                            }
                        isclicked = v1.getTag().toString();
                        delivererId = v1.getTag().toString();
                        Variableconstant.pro_forlatrboking = (int)rl_proforlatrboking.getTag();
                        Log.d(TAG,"TAGSELECTED "+rl_proforlatrboking.getTag());
                        mMap.clear();
                        valuinmarker.clear();
                        preview("");
                    }
                });
                if(firstTime){

                    for (int j=0;j<nubPojo.getMsg().getTypes().size();j++)
                    {
                        String   url = Variableconstant.pics_path + nubPojo.getMsg().getTypes().get(j).getSelImg();
                        try {
                            url = url.replace(" ", "%20");
                            if (!url.equals("")) {
                                Picasso.with(mcontext).load(url)
                                        .resize((int) width, (int) height)
                                        .transform(new CircleTransform())
                                        .into(provider_image);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String   url1 = Variableconstant.pics_path + nubPojo.getMsg().getTypes().get(j).getUnSelImg();
                        try {
                            url1 = url1.replace(" ", "%20");
                            if (!url1.equals("")) {
                                Picasso.with(mcontext).load(url1)
                                        .resize((int) width, (int) height)
                                        .transform(new CircleTransform())
                                        .into(provider_image);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    firstTime=false;
                }
                if (cmingfrom.equals("pubnub"))
                {
                    if(!isfirsttimeclicked)
                    {

                        String url;
                        if (i == 0) {
                            url = Variableconstant.pics_path + nubPojo.getMsg().getTypes().get(i).getSelImg();
                            int tempcol = getColor(mcontext, R.color.actionbar_color);
                            provider_name.setTextColor(tempcol);
                            if(!listviewclicked) {
                                Variableconstant.now_servicegroup = nubPojo.getMsg().getTypes().get(0).getIsGroupContain();
                                manager.setServiceProvdr(nubPojo.getMsg().getTypes().get(0).getCat_name());
                                manager.setServiceProviderNAme(nubPojo.getMsg().getTypes().get(0).getFtype());
                            }
                            if (nubPojo.getMsg().getMasArr().get(0).getMas().size() > 0)
                            {
                                isProAvailable=true;
                                for (int p = 0; p < nubPojo.getMsg().getMasArr().get(0).getMas().size(); p++)
                                {

                                    if(alltidare.contains(nubPojo.getMsg().getMasArr().get(0).getTid()))
                                    {
                                        int index = alltidare.indexOf(nubPojo.getMsg().getMasArr().get(0).getTid());
                                        if(savetime.size()==alltidare.size())
                                        {
                                            tvmin.setText(getResources().getString(R.string.minute));
                                            tveta.setText(savetime.get(index));
                                        }
                                    }
                                    ArrayList<PubNubMas>punbunMas = nubPojo.getMsg().getMasArr().get(0).getMas();
                                    ploatMapDataDistnce(punbunMas,punbunMas.get(p),distance);
                                }
                            } else
                                noProviderAvailble(tvmin,tveta,distance);

                        } else {
                            url = Variableconstant.pics_path + nubPojo.getMsg().getTypes().get(i).getUnSelImg();
                            int tempcol = getColor(mcontext, R.color.gray);
                            provider_name.setTextColor(tempcol);
                            if (nubPojo.getMsg().getMasArr().get(i).getMas().size() > 0)
                            {
                                if(!nubPojo.getMsg().getMasArr().get(i).getMas().get(0).getD().equals(""))
                                    setDistanceInKmFormate(nubPojo.getMsg().getMasArr().get(i).getMas().get(0).getD(),distance);
                            }
                            else
                            {
                                distance.setText(getResources().getString(R.string.noprovider));
                                distance.setTextColor(Color.RED);
                            }

                        }
                        try {
                            url = url.replace(" ", "%20");
                            if (!url.equals("")) {
                                Picasso.with(mcontext).load(url)
                                        .resize((int) width, (int) height)
                                        .transform(new CircleTransform())
                                        .into(provider_image);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if(cmingfrom.equals("oncreate"))
                preview(manager.getPUNUB_RES());

        }
    }
    private void noProviderAvailble(TextView tvmin, TextView tveta, TextView distance)
    {
        isProAvailable=false;
        distance.setText(getResources().getString(R.string.noprovider));
        distance.setTextColor(Color.RED);
        tveta.setText(getResources().getString(R.string.no));
        tvmin.setText(getResources().getString(R.string.provider));
        tvmin.setTextSize(8);
        tveta.setTextSize(8);
    }
    /**
     *
     * @param visit_fees this is the visit fee of provider
     * @param price_min this is the minimum fee of provider
     */

    private void showDialoag(String visit_fees, String price_min)//, String base_fees
    {


        Dialog dialog=new Dialog(mcontext);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.car_details);

        RelativeLayout rlproinfo = (RelativeLayout) dialog.findViewById(R.id.rlproinfo);
        TextView tvetimevalu = (TextView) dialog.findViewById(R.id.tvetimevalu);
        TextView timevalu = (TextView) dialog.findViewById(R.id.timevalu);
        TextView tvvisitfeeis = (TextView) dialog.findViewById(R.id.tvvisitfeeis);
        TextView visitfeeis = (TextView) dialog.findViewById(R.id.visitfeeis);
        TextView tvpriceperhr = (TextView) dialog.findViewById(R.id.tvpriceperh);
        TextView priceperhr = (TextView) dialog.findViewById(R.id.priceperh);
        priceperhr.setTypeface(regulrtxt);
        tvpriceperhr.setTypeface(regulrtxt);
        visitfeeis.setTypeface(regulrtxt);
        tvvisitfeeis.setTypeface(regulrtxt);
        timevalu.setTypeface(regulrtxt);
        tvetimevalu.setTypeface(regulrtxt);
        dialog.show();
        rlproinfo.setVisibility(VISIBLE);
        Animation animation   =    AnimationUtils.loadAnimation(mcontext, R.anim.upanim);// abc_slide_in_top
        rlproinfo.startAnimation(animation);
        if(tveta.getText().toString().equals(getResources().getString(R.string.no)))
            timevalu.setText("--");
        else
        {
            String timeval = tveta.getText().toString() + " min";
            timevalu.setText(timeval);
        }

        String visitfee = mcontext.getResources().getString(R.string.currencySymbol)+" "+visit_fees;
      //  double pricepermin = Double.parseDouble(price_min)*60;
        double pricepermin = Double.parseDouble(price_min);
        String priceperh = mcontext.getResources().getString(R.string.currencySymbol)+" "+pricepermin;
        visitfeeis.setText(visitfee);
        priceperhr.setText(priceperh);
    }
    /**
     *  marking deliverer icon on map
     * @param lat driver latitude
     * @param lng driver longitude
     * @param mapicon image of the map
     * @param valuinmarker provider id
    // * @param amoun provider amount
     */
    public void markingDeliverers(String lat, String lng, String mapicon, final ArrayList<String>valuinmarker)//, String dist, final String pid,String amoun
    {

        final View marker_view = ((LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
        ImageView image = (ImageView) marker_view.findViewById(R.id.image);

        if(mcontext!=null) {
            if (mMap != null)
            {
                double width = size[0] * 50;
                double height = size[1] * 50;
                LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                try {
                    driverMarker = mMap.addMarker(new MarkerOptions().position(latLng));
                    if (!mapicon.equals("")) {

                        Picasso.with(mcontext).load(mapicon)
                                .resize((int) width, (int) height)
                                .centerCrop()
                                .transform(new CircleTransform())
                                .into(image, new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {

                                        try {
                                            driverMarker.setIcon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(mcontext, marker_view)));
                                            markers.put(driverMarker.getId(), valuinmarker);
                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onError()
                                    {
                                        driverMarker.setIcon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(mcontext,marker_view)));
                                        markers.put(driverMarker.getId(), valuinmarker);
                                    }
                                });
                    } else {
                        driverMarker.setIcon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(mcontext, marker_view)));

                        markers.put(driverMarker.getId(), valuinmarker);
                    }
                 //   Log.d(TAG, "markingDeliverers: "+driverMarker.getId());
                    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            Variableconstant.onresumeflag = false;
                            if (!homejobaddress.getText().toString().trim().equals("")) {
                                manager.setSavedAddress(homejobaddress.getText().toString());
                                Intent intent = new Intent(mcontext, MasterDetails.class);
                                Variableconstant.ent_btype = 3;
                                Log.d(TAG, "onMarkerClick: "+markers.get(driverMarker.getId()).get(0)+
                                " TAGID "+driverMarker.getId()+" getID "+marker.getId());
                                intent.putExtra("SELECTED_ID", isclicked);
                                intent.putExtra("PID", markers.get(driverMarker.getId()).get(0));
                                intent.putExtra("AMT", markers.get(driverMarker.getId()).get(1));
                                intent.putExtra("DISTANCE", markers.get(driverMarker.getId()).get(2));
                                Variableconstant.comingfromlaterbooking = false;
                                manager.setJOBLATI(String.valueOf(currentLatitude));
                                manager.setJOBLONGI(String.valueOf(currentLongitude));
                                startActivity(intent);
                            } else {
                                Snackbar.make(getView(), getResources().getString(R.string.pleasewaitwearegintuaaddress), Snackbar.LENGTH_LONG)
                                        .setAction("Action", null).show();

                            }
                            return false;
                        }
                    });
                } catch (IllegalArgumentException e) {

                    e.printStackTrace();
                }
            }
        }
    }
    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        if(context!=null)
        {
            if(context.getApplicationContext()!=null)
            {
                ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            }
        }
        view.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    @Override
    public void onBackStackChanged() {

    }
    //to get the address on the homescreen
    private class BackgroundGetAddress extends AsyncTask<String, Void, String>
    {
        List<Address> address;
        String lat,lng;
        @Override
        protected String doInBackground(String... params) {


            try {
                lat = params[0];
                lng = params[1];

                if(lat!=null && lng!=null)
                {
                    if(mcontext!=null)
                    {

                        geocoder=new Geocoder(mcontext);
                    }

                    if(geocoder!=null)
                    {
                        address=geocoder.getFromLocation(Double.parseDouble(params[0]), Double.parseDouble(params[1]), 1);

                    }
                }
            } catch (IOException e)
            {

                e.printStackTrace();

                callmethod();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result)
        {

            super.onPostExecute(result);
            if(address!=null && address.size()>0)
            {
                String fuladdress = address.get(0).getAddressLine(0)+", "+address.get(0).getAddressLine(1);
                homejobaddress.setText(fuladdress);
                youraddress.setText(fuladdress);
                manager.setSavedAddress(fuladdress);
            }

        }
    }

    private void callmethod()
    {
        new BackgroundGeocodingTask().execute();
    }

    private class BackgroundGeocodingTask extends AsyncTask<String, Void, String>
    {
        GeocodingResponse response;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            String url="https://maps.googleapis.com/maps/api/geocode/json?latlng="+currentLatitude+","+currentLongitude+"&sensor=false&key="+ Variableconstant.SERVERKEY;
            Utility.printLog("Geocoding url: " + url);

            String stringResponse= Utility.callhttpRequest(url);

            if(stringResponse!=null)
            {
                response=gson.fromJson(stringResponse, GeocodingResponse.class);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            if(response!=null)
            {
                if(response.getStatus().equals("OK"))
                {
                    homejobaddress.setText(response.getResults().get(0).getFormatted_address());
                    youraddress.setText(response.getResults().get(0).getFormatted_address());
                    String short_address[]=response.getResults().get(0).getFormatted_address().split(",");
                    String temp_address=null;
                    if(short_address.length==1)
                    {

                        if(short_address[0]!=null)
                        {
                            temp_address=short_address[0];
                            if(short_address.length==2)
                            {

                                if(short_address[1]!=null)
                                {
                                    temp_address=temp_address+", "+short_address[1];


                                    if(short_address.length==3)
                                    {

                                        if(short_address[2]!=null)
                                        {
                                            temp_address=temp_address+", "+short_address[2];
                                        }
                                    }

                                }
                            }
                        }
                        Utility.printLog("VariableConstants.area_name: " + temp_address);
                    }
                }
            }
        }
    }
    public void listviewselected()
    {
        if(listviewclicked)
        {
            yourlocation.setVisibility(View.VISIBLE);
            youraddress.setVisibility(View.VISIBLE);
            rrlmapmain.setVisibility(View.GONE);
            tolbarhomelogo.setVisibility(View.GONE);
            rrlmaintab.setVisibility(View.VISIBLE);

            if(currentLatitude!=0.0 && currentLongitude!=0.0)
            {
                manager.setJOBLATI(String.valueOf(currentLatitude));
                manager.setJOBLONGI(String.valueOf(currentLongitude));
            }
            else {
                manager.setJOBLATI(String.valueOf(curlatlng[0]));
                manager.setJOBLONGI(String.valueOf(curlatlng[1]));
            }
        listviewclicked = true;
            if(!youraddress.getText().toString().equals(""))
            {
                manager.setSavedAddress(youraddress.getText().toString());
            }
            else if(!homejobaddress.getText().toString().equals(""))
            {
                manager.setSavedAddress(homejobaddress.getText().toString());
            }


        if(nubPojo.getMsg().getTypes().size()>0)
            if(nubPojo.getMsg().getMasArr().get(0).getMas().size()>0)
            {

                String proavilablenw = getResources().getString(R.string.providerwhoareavailable);
                orderTV.setText(proavilablenw);
            }
            else if(nubPojo.getMsg().getMasArr().get(0).getMas().size()<0)
            {
                String proavilablenw =getResources().getString(R.string.noprovideravailablenow);//noprovideravailablenow
                orderTV.setText(proavilablenw);
            }
        Variableconstant.ent_btype = 3;
        Variableconstant.ent_forlaterbooking = 3;
        Variableconstant.ent_dt = Utility.dateintwtfour();
        Variableconstant.comingfromlaterbooking = false;
        youraddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAutocompleteActivity();
            }
        });
        yourlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAutocompleteActivity();
            }
        });
        }
        else
        {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 17.0f));
            rrlmapmain.setVisibility(View.VISIBLE);
            tolbarhomelogo.setVisibility(View.VISIBLE);
            yourlocation.setVisibility(View.GONE);
            youraddress.setVisibility(View.GONE);
            rrlmaintab.setVisibility(View.GONE);
            listviewclicked = false;
            rlempty.setVisibility(View.GONE);
            Variableconstant.ent_btype = 3;
            Variableconstant.ent_forlaterbooking = 3;
            Variableconstant.ent_dt = Utility.dateintwtfour();
            Variableconstant.comingfromlaterbooking = false;
            timeRl.setVisibility(View.GONE);
            rlorbooklatter.setVisibility(View.VISIBLE);
            orderTV.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        locationUtil.disconnectGoogleApiClient();
        myTimer_publish.cancel();
        myTimer_publish.purge();
        if(myTimer!=null)
        {
            myTimer.purge();
            myTimer.cancel();
        }
        if(pdialog!=null)
        {
            pdialog.dismiss();
            pdialog.cancel();
        }
        if(progressDialog!=null)
        {
            progressDialog.dismiss();
            progressDialog.cancel();
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Variableconstant.onresumeflag = false;
        Variableconstant.forParticularBooking = true;
        socket.disconnect();
        socket.close();
        socket.off();
        if(myTimer_publish!=null)
        {
            myTimer_publish.cancel();
            myTimer_publish.purge();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        socket.disconnect();
        socket.close();
        socket.off();
    }

    private class  getETA extends AsyncTask<String, Void, String>
    {
        TimeForNearestPojo timeresponse;


        @Override
        protected String doInBackground(String... params)
        {
            final String[] result = new String[1];
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(params[0])
                    .build();
            Response response;
            try
            {
                response = client.newCall(request).execute();
                result[0] =response.body().string();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                Utility.printLog("result for exception "+e);
            }
            return result[0];
        }
        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            if (result != null)
            {
                Gson gson = new Gson();
                timeresponse = gson.fromJson(result, TimeForNearestPojo.class);

                if (timeresponse != null) {


                    if (timeresponse.getStatus().equals("OK")) {
                        try {
                            savetime.clear();
                            for(int i=0;i<timeresponse.getRows().size();i++)
                            {
                                for(int j=0;j<timeresponse.getRows().get(i).getElements().size();j++)
                                {
                                    String etaTime = timeresponse.getRows().get(i).getElements().get(j).getDuration().getText();
                                    etaTime = etaTime.replaceAll("[^\\d.]", "").trim();
                                    savetime.add(etaTime);
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

    }
    /*Review Update */
    private void callreviewmethod(ArrayList<String> notReviewedAppts)
    {
        new DialogActivityEx(mcontext, notReviewedAppts.get(0),progressDialog,"MenuActivity");

    }//ReviewUpdate
   public int getColor (Context mContext, int id)
    {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return mContext.getColor(id);
        }
        else
        {
           return getResources().getColor(id);
        }
    }
    private void generalmethodToInit() {

        Variableconstant.comingfromlaterbooking = false;
        Variableconstant.ent_btype = 3;
        Variableconstant.ent_forlaterbooking = 3;
        Variableconstant.ent_dt = Utility.dateintwtfour();
        Variableconstant.cmngFrmsrvlst = false;
        getActivity().overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        locationUtil = new LocationUtil(getActivity(), this);
        locationUtil.connectGoogleApiClient();
        manager = new SessionManager(mcontext);
        gson = new Gson();
        listerner_list=new ArrayList<>();
        nubPojo = gson.fromJson(manager.getPUNUB_RES(), PubNub_pojo.class);
        size = Scaler.getScalingFactor(mcontext);
        height = (48) * size[1];
        width = (50) * size[0];
        if(!manager.getLATITUDE().equals("") && !manager.getLONGITUDE().equals(""))
        {
            curlatlng[0] = Double.parseDouble(manager.getLATITUDE());
            curlatlng[1] = Double.parseDouble(manager.getLONGITUDE());
        }
        currentLatitude = 0.0;
        currentLongitude = 0.0;
        myTimer = new Timer();
        progressDialog = new ProgressDialog(mcontext);
        pdialog = new ProgressDialog(mcontext);
        pdialog.setMessage(mcontext.getResources().getString(R.string.wait));
        pdialog.setCancelable(false);
        regulrtxt = Typeface.createFromAsset(mcontext.getAssets(), Variableconstant.regularfont);
        addresstext= Typeface.createFromAsset(mcontext.getAssets(), Variableconstant.BoldFont);
        semibold= Typeface.createFromAsset(mcontext.getAssets(), Variableconstant.SemiBoldFont);
        italicfont= Typeface.createFromAsset(mcontext.getAssets(), Variableconstant.italicfont);
    }
    private void addressTimer()
    {
        TimerTask myTimerTask = new TimerTask()
        {
            @Override
            public void run() {
                if (mcontext != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            VisibleRegion visibleRegion = mMap.getProjection()
                                    .getVisibleRegion();
                            Point x1 = mMap.getProjection().toScreenLocation(
                                    visibleRegion.farRight);
                            Point y = mMap.getProjection().toScreenLocation(
                                    visibleRegion.nearLeft);
                            Point centerPoint = new Point(x1.x / 2, y.y / 2);
                            LatLng centerFromPoint = mMap.getProjection().fromScreenLocation(centerPoint);
                            double lat  = centerFromPoint.latitude;
                            double lon = centerFromPoint.longitude;
                            if ((lat == currentLatitude && lon == currentLongitude)) {

                                Utility.printLog("Update Address: FALSE");
                            }
                            else
                            {
                                if (isFirstTime)
                                {

                                    new CountDownTimer(1000, 1000) {
                                        @Override
                                        public void onTick(long millisUntilFinished) {

                                        }

                                        @Override
                                        public void onFinish() {

                                            isFirstTime = false;
                                        }
                                    }.start();
                                }

                                if (lat != 0.0 && lon != 0.0) {
                                    if (!isFirstTime)
                                    {

                                        currentLatitude = lat;
                                        currentLongitude = lon;
                                        String[] params = new String[]{"" + lat, "" + lon};

                                        if (isAdded()) {
                                            if (!IsreturnFromSearch) {
                                                if (!isaddresslocked) {

                                                    new BackgroundGetAddress().execute(params);
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            }
        };
        myTimer.schedule(myTimerTask, 0, 4000);
    }


    /**
     * <h1>openDate_Picker</h1>
     * Method is used to open a date picker and Pict a date.
     * <p>
     *     This method open a Date Picker dialog by DatePickerDialog object.
     *     Hen set the Max time as Current Date by the help of the {@code fromDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime())}.
     *     Also listening the response From the DatePicker and setting the Date to the Given Edit text.
     * </p>
     * @param activity context
     */
    private void openDate_Picker(final FragmentActivity activity)
    {

        final Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newCalendar.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat dateFormate= new SimpleDateFormat("MM-dd-yyyy",Locale.getDefault());
                Variableconstant.ent_SlotDate = dateFormate.format(newCalendar.getTime());
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                scheduleday = dateFormatter.format(newCalendar.getTime());
                String title = DateUtils.formatDateTime(activity,
                        newCalendar.getTimeInMillis(),
                        DateUtils.FORMAT_SHOW_DATE
                                | DateUtils.FORMAT_SHOW_WEEKDAY
                                | DateUtils.FORMAT_SHOW_YEAR
                                | DateUtils.FORMAT_ABBREV_MONTH
                                | DateUtils.FORMAT_ABBREV_WEEKDAY);
                Utility.printLog("year " + year + " month  " + monthOfYear + " day " + dayOfMonth + " title "
                        + title+"schedule "+scheduleday+" dateis "+ Variableconstant.ent_SlotDate);
                calltimepicker();
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);// new Date().getTime()
        fromDatePickerDialog.show();

    }//End of selecting the date
    private void calltimepicker()
    {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(mcontext,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute)
                    {

                        if(hourOfDay<10)
                        {
                            if(minute<10)
                            {
                                Variableconstant.ent_dt = scheduleday+" "+("0"+hourOfDay + ":0" + minute+":00");
                            }
                            else
                            {
                                Variableconstant.ent_dt = scheduleday+" "+("0"+hourOfDay + ":" + minute+":00");
                            }

                        }
                        else
                        {
                            if(minute<10)
                            {
                                Variableconstant.ent_dt = scheduleday+" "+(hourOfDay + ":0" + minute+":00");
                            }
                            else
                            {
                                Variableconstant.ent_dt = scheduleday+" "+(hourOfDay + ":" + minute+":00");
                            }

                        }
                        intentForLater();
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }
    private void intentForLater() {
        Variableconstant.onresumeflag = false;
        listviewclicked = true;
        Intent intent = new Intent(mcontext,Later_Booking.class);
        Variableconstant.comingfromlaterbooking = true;
        Variableconstant.ent_btype = 2;
        Variableconstant.ent_forlaterbooking = 4;
        manager.setSavedAddress( homejobaddress.getText().toString());
        manager.setJOBLATI(String.valueOf(currentLatitude));
        manager.setJOBLONGI(String.valueOf(currentLongitude));
        intent.putExtra("SELECTED_ID", isclicked);
        intent.putExtra("currentLatitude",currentLatitude);
        intent.putExtra("currentLongitude",currentLongitude);
        startActivity(intent);
    }


}