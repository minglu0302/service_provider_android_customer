package com.iserve.passenger;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.gson.Gson;
import com.utility.GeocodingResponse;
import com.utility.LocationNotifier;
import com.utility.LocationUtil;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Current_Location extends AppCompatActivity implements
        LocationNotifier, GoogleMap.OnCameraMoveStartedListener,GoogleMap.OnCameraIdleListener
{

/*implements LocationListener*/

    private GoogleMap mMap;
    private LocationUtil networkUtilObj;
    double[] latLng = new double[2];

    private ImageView midpointmarker,search_location;
    TextView actionbartext, tvplace,butnconfirm,addresstxt;
    FrameLayout fframelayt;
    Timer myTimer;
    Context mcontext;
    private Geocoder geocoder;
    private boolean IsreturnFromSearch = false;
    private static final int searchPlaceRequestCode = 100;
    private static final int minDistanceToUpdate = 10;
    private boolean isFirstTime = false;
    Location old = new Location("OLD");
    private Handler addressHandler;
    private Runnable addressFinder;
    SessionManager manager;
    Typeface regularfont,lightfont;
    String TAG = "CurrentLocation";
    Gson gson;
    String fullAddress="";
    CardView cardview;
    Animation hide, visible;
    /**
     * *****************************************************************************************************************
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_location);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        gson = new Gson();
        regularfont = Typeface.createFromAsset(this.getAssets(), Variableconstant.regularfont);
        lightfont = Typeface.createFromAsset(this.getAssets(), Variableconstant.lightfont);
        isFirstTime = true;
        networkUtilObj = new LocationUtil(this, this);
        manager = new SessionManager(this);
        hide = AnimationUtils.loadAnimation(this,R.anim.up_moving_view_component);
        visible = AnimationUtils.loadAnimation(this,R.anim.down_moving_view_component);
        butnconfirm = (TextView) findViewById(R.id.butnconfirm);
        tvplace = (TextView) findViewById(R.id.tvplace);
        midpointmarker = (ImageView) findViewById(R.id.midpointmarker);
        search_location = (ImageView) findViewById(R.id.search_location);
        addresstxt = (TextView) findViewById(R.id.addresstxt);

        cardview = (CardView) findViewById(R.id.cardview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        fframelayt.setVisibility(View.VISIBLE);
        actionbartext = (TextView) findViewById(R.id.toolbarhometxv);
        actionbartext.setVisibility(View.VISIBLE);
        actionbartext.setText(getResources().getString(R.string.addnewaddress));
        tvplace.setTypeface(lightfont);
        addresstxt.setTypeface(lightfont);
        butnconfirm.setTypeface(regularfont);
        actionbartext.setTypeface(regularfont);
        mcontext = this.getApplication();
        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.googleMap);
        View locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.ABOVE);
        rlp.setMargins(0,getResources().getInteger(R.integer.maximumheight),30,0);
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                if (ActivityCompat.checkSelfPermission(Current_Location.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Current_Location.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);

                double[] latiLongi = new double[2];
                latiLongi[0] = Double.parseDouble(manager.getLATITUDE());
                latiLongi[1] = Double.parseDouble(manager.getLONGITUDE());
                if(latiLongi[0]!=0 &&latiLongi[1]!=0)
                {
                    initMapandAddress(latiLongi[0], latiLongi[1]);
                    LatLng latLng = new LatLng(latiLongi[0], latiLongi[1]);
                    CameraUpdate center= CameraUpdateFactory.newLatLng(latLng);
                    CameraUpdate zoom= CameraUpdateFactory.zoomTo(17.0f);
                    googleMap.moveCamera(center);
                    googleMap.animateCamera(zoom);
                }

                googleMap.setOnCameraChangeListener(new OnCameraChangeListener() {

                    @Override
                    public void onCameraChange(CameraPosition arg0) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                IsreturnFromSearch = false;
                            }
                        }, 5000);
                    }
                });

                googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                    @Override
                    public void onCameraMove() {

                    }
                });

                googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                    @Override
                    public void onCameraMoveStarted(int i) {
                        Log.d(TAG,"Camera StartMove ");
                        cardview.setAnimation(hide);
                        cardview.setVisibility(View.GONE);

                    }
                });

                googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                        Log.d(TAG,"Camera idle ");
                        cardview.setAnimation(visible);
                        cardview.setVisibility(View.VISIBLE);
                    }
                });

            }
        });

        butnconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!fullAddress.equals(""))
                {
                    Intent intnt = new Intent(Current_Location.this, Address_Save.class);
                    intnt.putExtra("fulladdress",addresstxt.getText().toString().trim());
                    intnt.putExtra("latitude",old.getLatitude());
                    intnt.putExtra("longitude",old.getLongitude());
                    startActivity(intnt);
                    finish();
                }
                else
                {
                    Toast.makeText(Current_Location.this,getResources().getString(R.string.pleasewaitwearegettinguaaddress),Toast.LENGTH_SHORT).show();
                }

            }
        });


        search_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    LatLngBounds bound = Utility.setBound(old.getLatitude(),old.getLongitude());
                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .setBoundsBias(bound)
                                    .build(Current_Location.this);
                    startActivityForResult(intent, searchPlaceRequestCode);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e)
                {
                    // TODO: Handle the error.
                    e.printStackTrace();
                }
            }
        });

    }
    @Override
    public void onCameraIdle()
    {
        String[] params=new String[]{""+old.getLatitude(),""+old.getLongitude()};
        new BackgroundGetAddress().execute(params);
    }

    @Override
    public void onCameraMoveStarted(int i)
    {

        Log.d(TAG,"PRINTISCAMERAMOVESTARTED ");
        IsreturnFromSearch = false;
    }
    @Override
    protected void onPause() {
        super.onPause();
        networkUtilObj.disconnectGoogleApiClient();
        if(myTimer!=null)
        {
            myTimer.cancel();
            myTimer.purge();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        networkUtilObj.connectGoogleApiClient();
        if(myTimer!=null)
        {
            myTimer.cancel();
            myTimer.purge();
            myTimer = new Timer();
        }
        else
        {
            myTimer = new Timer();
        }

        statMyTimer();
    }
    private void statMyTimer()
    {
        TimerTask myTimerTask = new TimerTask()
        {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        updateMapLocationAddress();

                    }
                });
            }

        };
        myTimer.schedule(myTimerTask, 0, 3000);
    }

    @Override
    public void updatedInfo(String info) {
        Utility.printLog("Location Info Data" + info);
    }

    @Override
    public void locationUpdates(Location location) {
        latLng[0] = location.getLatitude();
        latLng[1] = location.getLongitude();
        Log.d("GOOGLE MAP LOC lat", "=" + latLng[0]);
        Log.d("GOOGLE MAP LOC long", "=" + latLng[1]);
      //  Location old = new Location("OLD");
    }

    @Override
    public void locationFailed(String message) {
        Utility.printLog("Location Info Data" + message);
    }


    private void initMapandAddress(double newLat,double newLong){
        if(isFirstTime){
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(newLat, newLong), 17f));
            String[] params = new String[]{"" + newLat, "" + newLong};
            new BackgroundGetAddress().execute(params);
            isFirstTime = false;
        }

    }

    private void updateMapLocationAddress()
    {
        Utility.printLog("Calling Location...");
        VisibleRegion visibleRegion = mMap.getProjection().getVisibleRegion();

        Point x1 = mMap.getProjection().toScreenLocation(visibleRegion.farRight);
        Point y = mMap.getProjection().toScreenLocation(visibleRegion.nearLeft);
        Point centerPoint = new Point(x1.x / 2, y.y / 2);
        LatLng centerFromPoint = mMap.getProjection().fromScreenLocation(centerPoint);
        double lat = centerFromPoint.latitude;
        double lon = centerFromPoint.longitude;
        Utility.printLog("lat.........." + lat + ".........long....." + lon);
        Location newLoc = new Location("NEW");
        newLoc.setLatitude(lat);
        newLoc.setLongitude(lon);
        if(old.getLongitude()!= 0 && old.getLatitude()!= 0){
            double distance = newLoc.distanceTo(old);
            if ((lat == old.getLatitude() && lon == old.getLongitude())) {
                Utility.printLog("Update Address: FALSE");
            } else {
                old.setLatitude(lat);
                old.setLongitude(lon);
                if (midpointmarker.isShown()) {
                    Utility.printLog("Update Address: TRUE");
                    if(distance>minDistanceToUpdate) {
                        String[] params=new String[]{""+lat,""+lon};
                        if (!IsreturnFromSearch) {
                                new BackgroundGetAddress().execute(params);
                        }
                    }

                }
            }
        } else {
            old.setLatitude(lat);
            old.setLongitude(lon);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==searchPlaceRequestCode)
        {

            if (resultCode == RESULT_OK) {
                assert data != null;
                Place place = PlaceAutocomplete.getPlace(this, data);
                addresstxt.setText(place.getAddress());
                old.setLatitude(place.getLatLng().latitude);
                old.setLongitude(place.getLatLng().longitude);
                IsreturnFromSearch=true;
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(place.getLatLng().latitude, place.getLatLng().longitude), 17f));

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                assert data != null;
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED)
            {
                IsreturnFromSearch = false;
            }


        }

    }

    /*********************************************************************************************************************/

    class BackgroundGeocodingTask extends AsyncTask<String, Void, String> {
        String param[];
        GeocodingResponse response;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            Utility.printLog("bbb BackgroundGeocodingTask");
            param = params;

            String url = "http://maps.google.com/maps/api/geocode/json?latlng=" + params[0] + "," + params[1] + "&sensor=false";
            String stringResponse= Utility.callhttpRequest(url);

            if(stringResponse!=null)
            {
                response=gson.fromJson(stringResponse, GeocodingResponse.class);
            }

            return null;

        }

       /* *******************************************************************************************************************/

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            if (response != null) {

                if (response.getStatus().equals("OK") && response.getResults() != null && response.getResults().size() > 0) {

                    if (response.getResults().size() > 0 && !response.getResults().get(0).getFormatted_address().isEmpty())
                    {
                        Utility.printLog("formatted address at 1=" + response.getResults().get(0).getFormatted_address());
                        String address = response.getResults().get(0).getFormatted_address();

                        fullAddress = address;
                        addresstxt.setText(fullAddress);
                    }
                }
            }

        }
    }

    /*********************************************************************************************************************/

    class BackgroundGetAddress extends AsyncTask<String, Void, String>
    {
        List<Address> address=null;
        String lat,lng;
       String[] param;
        @Override
        protected String doInBackground(String... params) {
            param = params;

            try {
                Log.i("","LatLong test lat "+ params[0] );
                Log.i("","LatLong test lon "+ params[1] );
                lat = params[0];
                lng = params[1];

                if(lat!=null && lng!=null)
                {
                    if(Current_Location.this != null)
                    {

                        geocoder=new Geocoder(Current_Location.this);
                    }

                    if(geocoder!=null)
                    {
                        address=geocoder.getFromLocation(Double.parseDouble(params[0]), Double.parseDouble(params[1]), 1);

                    }
                }
            } catch (IOException e)
            {

                e.printStackTrace();
                Utility.printLog("address.get(0): INSIDE CATCH");

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result)
        {
            Log.i(TAG,"HIII "+address);

            super.onPostExecute(result);
            if(address!=null && address.size()>0)
            {

                fullAddress = address.get(0).getAddressLine(0)+","+address.get(0).getAddressLine(1)+","+address.get(0).getAddressLine(2);
                addresstxt.setText(fullAddress);

            }
            else
            {
                new BackgroundGeocodingTask().execute(param);
            }
        }
    }


    /*************************************************************************************************************************/

    /*************************************************************************************************************************/

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
    }
}


