package com.iserve.passenger;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.InputType;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.countrypic.Country;
import com.countrypic.CountryPicker;
import com.countrypic.CountryPickerListener;
import com.google.gson.Gson;
import com.pojo.Language_Type;
import com.pojo.MyProfile_pojo;
import com.pojo.Validator_Pojo;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.utility.AppPermissionsRunTime;
import com.utility.CircleTransform;
import com.utility.OkHttp3Request;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Validator;
import com.utility.Variableconstant;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import eu.janmuller.android.simplecropimage.CropImage;
import okhttp3.FormBody;
import static android.app.Activity.RESULT_OK;
import static android.os.Build.VERSION_CODES.N;

/**
 * Created by embed on 10/2/16.
 *
 */
public class ProfileFragment extends Fragment implements AdapterView.OnItemSelectedListener
{
    View view;
    UploadAmazonS3 upload;
    ImageView tolbarhomelogo;
    List<String> languageary = new ArrayList<>();
    private SessionManager manager;
    MyProfile_pojo myProfile_pojo;
    ProgressDialog pDialog=null,uDialog=null;
    Validator validator;
    TextView dname,logout,toolbarhometxv,tveditbarhometxv,pro_country_code,youraddress,yourlocation,tvchangepass;
    Spinner lang_spinner;
    TextInputLayout tIprofilefname,tIprofile_lname,tIprofile_email,tIprofilemobile_number,test;
    EditText eTprofil_fname,eTprofile_lname,eTprofile_email,eTprofile_mobile,ettest;
    RelativeLayout imageprorrlout,imageframelayout;
    ProgressBar progressBar;
    ImageView profileIv;
    private static final int CAMERA_PIC = 1, GALLERY_PIC = 2, CROP_IMAGE=3;
    private String state;
    private File newFile;
    private File profilePicsDir;
    private String takenNewImage="";
    private String replaceEmailAtRate="";
    private Uri newProfileImageUri;
    boolean imageflag=false;
    Language_Type lantype_pojo;
    private boolean isPPSelected = false;
    Gson gson ;
    JSONObject jsonObj;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 125;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList;

    private ImageView mCountryFlagImageView;
    private CountryPicker mCountryPicker;
    private String profileToDelete = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        if(view != null)
        {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try
        {
            view = inflater.inflate(R.layout.profile_fragment, container, false);
            gson = new Gson();
            jsonObj = new JSONObject();
            manager = new SessionManager(getActivity());
            validator = new Validator();
           // replaceEmailAtRate = manager.getCoustomerEmail().replace("@","1");
            replaceEmailAtRate = Variableconstant.PARENT_FOLDER+System.currentTimeMillis();
            upload =UploadAmazonS3.getInstance(getActivity(), Variableconstant.Amazoncognitoid);
            progressBar = (ProgressBar)view.findViewById(R.id.progressBar_splsh);
            initialize();
            setListener();

        } catch (InflateException e)
        {
            Utility.printLog("onCreateView  InflateException " + e);
        }
        setHasOptionsMenu(false);

        return view;
    }


    /* initialize all xml field*/
    private void initialize()
    {
        progressBar.setVisibility(View.VISIBLE);
        pDialog = new ProgressDialog(view.getContext());
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        lantype_pojo = new Language_Type();
        lang_spinner = (Spinner) view.findViewById(R.id.lang_spinner);
        tvchangepass = (TextView) view.findViewById(R.id.tvchangepass);
        toolbarhometxv = (TextView) getActivity().findViewById(R.id.toolbarhometxv);
        yourlocation = (TextView) getActivity().findViewById(R.id.yourlocation);
        youraddress = (TextView) getActivity().findViewById(R.id.youraddress);
        imageframelayout = (RelativeLayout) getActivity().findViewById(R.id.imageframelayout);
        imageframelayout.setVisibility(View.GONE);
        toolbarhometxv.setVisibility(View.VISIBLE);
        String profile = " "+getResources().getString(R.string.profile);
        toolbarhometxv.setText(profile);
        tveditbarhometxv = (TextView) getActivity().findViewById(R.id.tveditbarhometxv);
        tveditbarhometxv.setVisibility(View.VISIBLE);
        tveditbarhometxv.setText(getResources().getString(R.string.edit));
        toolbarhometxv.setTextColor(getResources().getColor(R.color.actionbar_color));
        tolbarhomelogo = (ImageView) getActivity().findViewById(R.id.tolbarhomelogo);
        youraddress.setVisibility(View.GONE);
        yourlocation.setVisibility(View.GONE);
        tolbarhomelogo.setVisibility(View.GONE);
        ettest= (EditText) view.findViewById(R.id.ettest);
        test = (TextInputLayout) view.findViewById(R.id.test);
        mCountryFlagImageView = (ImageView) view.findViewById(R.id.selected_country_flag_image_view);
        mCountryPicker = CountryPicker.newInstance("Select Country");
        profileIv = (ImageView)view.findViewById(R.id.profileIv);
        imageprorrlout = (RelativeLayout) view.findViewById(R.id.imageprorrlout);
        eTprofil_fname = (EditText)view.findViewById(R.id.eTprofil_fname);
        eTprofile_lname = (EditText)view.findViewById(R.id.eTprofile_lname);
        eTprofile_mobile = (EditText)view.findViewById(R.id.eTprofile_mobile);
        eTprofile_email= (EditText) view.findViewById(R.id.eTprofile_email);
        tIprofilefname = (TextInputLayout) view.findViewById(R.id.tIprofilefname);
        tIprofile_lname = (TextInputLayout) view.findViewById(R.id.tIprofile_lname);
        tIprofile_email = (TextInputLayout) view.findViewById(R.id.tIprofile_email);
        tIprofilemobile_number = (TextInputLayout) view.findViewById(R.id.tIprofilemobile_number);
        logout = (TextView) view.findViewById(R.id.logout);
        pro_country_code = (TextView) view.findViewById(R.id.pro_country_code);
        logout.setVisibility(View.VISIBLE);
        profileIv.setClickable(false);

        pro_country_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                AlertDialog dialog = builder.create();
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                builder.setTitle(getResources().getString(R.string.dialoglogout));
                builder.setMessage(getResources().getString(R.string.doyouwishtologout));
                builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                        loggingout();
                        dialog.dismiss();
                        dialog.cancel();
                    }
                });

                builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

        eTprofil_fname.setEnabled(false);
        eTprofile_lname.setEnabled(false);
        eTprofile_email.setEnabled(false);
        eTprofile_mobile.setEnabled(false);
        tIprofile_lname.setHintTextAppearance(R.style.QText);
        tIprofilefname.setHintTextAppearance(R.style.QText);
        tIprofile_email.setHintTextAppearance(R.style.QText);
        tIprofilemobile_number.setHintTextAppearance(R.style.QText);
        ettest.requestFocus();
        test.requestFocus();
        dname= (TextView) getActivity().findViewById(R.id.drawer_name);
        uDialog=new ProgressDialog(view.getContext());
        uDialog.setMessage(getResources().getString(R.string.updating));
        uDialog.setCancelable(false);
        profileService();
        tveditbarhometxv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tveditbarhometxv.getText().toString().equals(getActivity().getResources().getString(R.string.edit)))
                {

                    tveditbarhometxv.setText(getResources().getString(R.string.save));
                    tveditbarhometxv.setTextColor(Color.BLACK);
                    toolbarhometxv.setText(getResources().getString(R.string.editprofile));
                    logout.setVisibility(View.GONE);
                    ettest.setVisibility(View.GONE);
                    test.setVisibility(View.GONE);
                    eTprofil_fname.requestFocus();
                    tIprofilefname.requestFocus();
                    profileIv.setClickable(true);
                    profileIv.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v)
                        {

                            if (Build.VERSION.SDK_INT >= 23)
                            {

                                myPermissionConstantsArrayList = new ArrayList<>();
                                myPermissionConstantsArrayList.clear();
                                myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
                                myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
                                myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);
                                if(AppPermissionsRunTime.checkPermission(getActivity(), myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE))
                                {
                                    //TODO: if all permissions are already granted

                                    selectImage();

                                }
                            }
                            else
                            {
                                selectImage();
                            }
                        }
                    });

                    editprofile();
                }
                else if(tveditbarhometxv.getText().toString().equals(getActivity().getResources().getString(R.string.save)))
                {
                    String profile = " "+getResources().getString(R.string.profile);
                    toolbarhometxv.setText(profile);

                    ettest.requestFocus();
                    test.requestFocus();
                    profileIv.setClickable(false);
                    logout.setVisibility(View.VISIBLE);
                    saveprofile();
                }
            }
        });

        /*Change password */
        tvchangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),ForgotpassNewPassActivity.class);
                intent.putExtra("MOBILE",eTprofile_mobile.getText().toString().trim());
                intent.putExtra("ProfileData","Profile");
                startActivity(intent);
            }
        });

    }

    private void callspinradaptermethod()
    {

        Language_Type language_type = gson.fromJson(manager.getLanguage(),Language_Type.class);
        for(int i = 0; i<language_type.getLanguages().size(); i++)
        {
            languageary.add(language_type.getLanguages().get(i).getLan_name());
        }
        ArrayAdapter<String>dataAdapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,languageary);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lang_spinner.setAdapter(dataAdapter);
        lang_spinner.setSelection(Variableconstant.selectedlanid,false);

        lang_spinner.setOnItemSelectedListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void loggingout()
    {
        pDialog.setMessage(getResources().getString(R.string.loggingout));
        pDialog.show();

        FormBody requestbody = new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_user_type",2+"")
                .add("ent_date_time", Utility.dateintwtfour())
                .build();
        OkHttp3Request.doOkHttp3Request(Variableconstant.LOGOUTURL, requestbody, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result)
            {
                pDialog.dismiss();

                Validator_Pojo validator_pojo = gson.fromJson(result,Validator_Pojo.class);
                if(validator_pojo.getErrFlag().equals("0"))
                {
                    emitHeartbeat("0");

                    Utility.setMAnagerWithBID(getActivity(),manager);
                }
            }

            @Override
            public void onError(String error)
            {
                pDialog.dismiss();
            }
        });

    }

    public void emitHeartbeat(String status)
    {

        JSONObject obj = new JSONObject();

        try
        {
            obj.put("status",status);
            obj.put("cid", manager.getCustomerId());

        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        ApplicationController.getInstance().emit("CustomerStatus",obj);

    }

    private void editprofile()
    {

        eTprofil_fname.setEnabled(true);
        eTprofil_fname.setInputType(InputType.TYPE_CLASS_TEXT);
        eTprofil_fname.setFocusable(true);

        eTprofile_lname.setEnabled(true);
        eTprofile_lname.setInputType(InputType.TYPE_CLASS_TEXT);
        eTprofile_lname.setFocusable(true);

        tIprofile_lname.setHintEnabled(true);
        tIprofilefname.setHintEnabled(true);

        tIprofile_lname.setHintAnimationEnabled(true);
        tIprofilefname.setHintAnimationEnabled(true);

        tIprofilefname.setEnabled(true);
        tIprofile_lname.setEnabled(true);


    }

    /**
     * predefined method to check run time permissions list call back
     * @param requestCode requestcode comes from permission granted
     * @param permissions: contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        boolean isDenine = false;
        switch (requestCode)
        {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine)
                {
                    //TODO: if permissions denied by user, recall it
                    Snackbar.
                            make(getView(), getResources().getString(R.string.permissiondenied)+" "+REQUEST_CODE_PERMISSION_MULTIPLE, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else
                {
                    selectImage();


                    //TODO: if permissions granted by user, move forward
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }


    private void saveprofile()
    {
        boolean  flag=true;
        if(tIprofilefname.getEditText().getText().toString().equals(""))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.first_name_mandatory), Toast.LENGTH_LONG).show();
            tIprofilefname.setError(getResources().getString(R.string.first_name_mandatory));
            tIprofilefname.setErrorEnabled(true);
            flag=false;
        }



        else if(!eTprofile_lname.getText().toString().equals(""))
        {
            if(!validator.lastName_status(eTprofile_lname.getText().toString()))
            {
                flag=false;

                Toast.makeText(getActivity(), "Invalid Last Name", Toast.LENGTH_LONG).show();
            }
        }


        if (flag)
        {

            uDialog.show();

            eTprofile_lname.setEnabled(false);
            eTprofil_fname.setEnabled(false);
            tIprofilefname.setErrorEnabled(false);
            tIprofilefname.setError("");
            eTprofile_email.setEnabled(false);
            eTprofile_mobile.setEnabled(false);

            if(isPPSelected && newFile!=null)
            {
                uploadToAmazon(newFile);
            }
            else
            {
                updateprofile();
            }

            //updateprofile();


        }
    }

    /***************************************person_image***********************************************************/

    private void selectImage() {

        clearOrCreateDir();
        final CharSequence[] options = getResources().getStringArray(R.array.takeremovefoto);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.addprofilefoto));
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.takephoto)))//"Take Photo"
                {

                    takePicFromCamera();

                }
                else if (options[item].equals(getResources().getString(R.string.choosefromglry)))
                {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, GALLERY_PIC);
                    getActivity().overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);

                }
                else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
                else if (options[item].equals(getResources().getString(R.string.removephoto))) {
                    profileIv.setImageDrawable(null);
                    profileIv.setImageDrawable(getResources().getDrawable(R.drawable.register_profile_default_image));
                    clearOrCreateDir();
                    isPPSelected = true;
                    removeimage();
                }
            }
        });
        builder.show();
    }

    private void takePicFromCamera()
    {
        try
        {
            state = Environment.getExternalStorageState();
            takenNewImage = replaceEmailAtRate+".png";

            if (Environment.MEDIA_MOUNTED.equals(state))
                newFile = new File(Environment.getExternalStorageDirectory()+"/"+ Variableconstant.PARENT_FOLDER+"/Profile_Pictures/",takenNewImage);
            else
                newFile = new File(getActivity().getFilesDir()+"/"+ Variableconstant.PARENT_FOLDER+"/Profile_Pictures/",takenNewImage);
            if(Build.VERSION.SDK_INT>=N)
                newProfileImageUri = FileProvider.getUriForFile(getActivity(),BuildConfig.APPLICATION_ID + ".provider",newFile);
            else
                newProfileImageUri = Uri.fromFile(newFile);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, newProfileImageUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent,CAMERA_PIC);
        }
        catch (ActivityNotFoundException e)
        {
            e.printStackTrace();
        }
    }


    private void clearOrCreateDir()
    {
        try
        {

            state = Environment.getExternalStorageState();
            File cropImagesDir ;
            File[] cropImagesDirectory;


            if (Environment.MEDIA_MOUNTED.equals(state))
            {

                cropImagesDir = new File(Environment.getExternalStorageDirectory()+"/"+ Variableconstant.PARENT_FOLDER+"/CropImages");
                profilePicsDir = new File(Environment.getExternalStorageDirectory()+"/"+ Variableconstant.PARENT_FOLDER+"/Profile_Pictures");

            }
            else
            {

                cropImagesDir = new File(getActivity().getFilesDir()+"/"+ Variableconstant.PARENT_FOLDER+"/CropImages");
                profilePicsDir = new File(getActivity().getFilesDir()+"/"+ Variableconstant.PARENT_FOLDER+"/Profile_Pictures");

            }

            if(!cropImagesDir.isDirectory())
            {
                cropImagesDir.mkdirs();
            }
            else
            {
                cropImagesDirectory = cropImagesDir.listFiles();
                Utility.printLog("cropImageDir is=" + cropImagesDir);

                if(cropImagesDirectory.length > 0)
                {
                    for (File aCropImagesDirectory : cropImagesDirectory) {
                        aCropImagesDirectory.delete();
                    }
                }
                else
                {
                    Utility.printLog("profile fragment CropImages Dir empty  or null: " + cropImagesDirectory.length);
                }
            }

            if(!profilePicsDir.isDirectory())
            {
                profilePicsDir.mkdirs();

            }
            else
            {
                File[] profilePicsDirectory = profilePicsDir.listFiles();

                if(profilePicsDirectory.length > 0)
                {
                    for (File aProfilePicsDirectory : profilePicsDirectory) {
                        aProfilePicsDirectory.delete();
                    }
                }
                else
                {
                    Utility.printLog("profile fragment profilePicsDir empty  or null: " + profilePicsDirectory.length);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Utility.printLog("profile fragment ON ACTIVITY RESULT: " + "rqstCode" + requestCode + " rsltCode " + resultCode + " Intent data " + data + " getdata ");

        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK)    //result code to check is the result is ok or not
        {
            return;
        }
        //Bitmap bitmap;

        switch (requestCode) {
            case CAMERA_PIC:
                //  fileType = "image";
                Utility.printLog("profile fragment FilePAth in case CAMERA_PIC: " + newFile.getPath());
                startCropImage();
                break;

            case GALLERY_PIC:
                try {

                    state = Environment.getExternalStorageState();

                    takenNewImage = replaceEmailAtRate+".png";

                    if (Environment.MEDIA_MOUNTED.equals(state)) {
                        newFile = new File(Environment.getExternalStorageDirectory() + "/" + Variableconstant.PARENT_FOLDER+ "/Profile_Pictures/", takenNewImage);
                    } else {
                        newFile = new File(getActivity().getFilesDir() + "/" + Variableconstant.PARENT_FOLDER+ "/Profile_Pictures/", takenNewImage);
                    }

                    InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());

                    FileOutputStream fileOutputStream = new FileOutputStream(newFile);

                    Utility.copyStream(inputStream, fileOutputStream);

                    fileOutputStream.close();
                    inputStream.close();
                    newProfileImageUri = Uri.fromFile(newFile);
                    startCropImage();
                } catch (Exception e) {
                    Utility.printLog("SingUp_page error is as follows" + e);
                }
                break;

            case CROP_IMAGE:
                isPPSelected = true;
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {
                    Utility.printLog("profile fragment CROP_IMAGE file path is null: " + newFile.getPath());
                    //return;
                } else {
                    isPPSelected = true;
                    newProfileImageUri = Uri.fromFile(newFile);
                    try {



                        Picasso.with(getActivity()).load(Uri.fromFile(newFile))
                                .skipMemoryCache()
                                .resize(profileIv.getWidth(), profileIv.getHeight())
                                .centerCrop().transform(new CircleTransform())
                                .placeholder(R.drawable.register_profile_default_image).into(profileIv);
                        imageflag=true;



                    } catch (Exception e) {
                        Utility.printLog("profile fragment in CROP_IMAGE exception while copying file = " + e.toString());
                    }

                    break;

                }}}

    private void startCropImage()
    {
        Utility.printLog("profile fragment CROP IMAGE CALLED: " + newFile.getPath());
        Intent intent = new Intent(getActivity(),CropImage.class );
        intent.putExtra(CropImage.IMAGE_PATH,newFile.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        startActivityForResult(intent, CROP_IMAGE);
    }


    /**
     * uploadimage to amazon
     * @param image image to upload on amazonserver
     */
    private void uploadToAmazon(File image)
    {
        if(!profileToDelete.equals(""))
        {
            new deleteImage().execute();
        }

        progressBar.setVisibility(View.VISIBLE);
        upload.Upload_data(Variableconstant.Amazonbucket, image, new UploadAmazonS3.Upload_CallBack() {
            @Override
            public void sucess(String sucess) {

                String   url=("https://s3.amazonaws.com/"+ Variableconstant.Amazonbucket+"/"+replaceEmailAtRate+".png").replace(" ", "%20");

                profileToDelete = Variableconstant.AmazonProfileFolderName+"/"+replaceEmailAtRate+".png";

               // Log.i("TAGIS "," DELECTEup "+profileToDelete);
                manager.setProfilePic(sucess);
                Picasso.with(getActivity()).load(url)
                        .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)
                        .resize(MenuActivity.profilePicture.getWidth(), MenuActivity.profilePicture.getHeight())
                        .centerCrop().transform(new CircleTransform())
                        .skipMemoryCache()
                        .placeholder(R.drawable.register_profile_default_image)
                        .into(MenuActivity.profilePicture);


                progressBar.setVisibility(View.GONE);

              updateprofile();

            }

            @Override
            public void error(String errormsg) {
             //   Log.i("imageuploaded ", "error " + errormsg);
                isPPSelected = false;
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "could not upload image ", Toast.LENGTH_SHORT).show();


            }
        });
    }

    /********************************************************/
    public void profileService () {

        if (Utility.isNetworkAvailable(getActivity()))
        {

            try {
                jsonObj.put("ent_dev_id", manager.getDevice_Id());
                jsonObj.put("ent_sess_token",manager.getSession());
                jsonObj.put("ent_date_time", Utility.dateintwtfour());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Utility.doJsonRequest(Variableconstant.GETPROFILE, jsonObj, new Utility.JsonRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    Log.d("TAG","PROFILEPICRESULT "+result);
                    callProfileResponse(result);

                }

                @Override
                public void onError(String error) {

                }
            });

        }
    }

    /*******************************/

    private void callProfileResponse(String jsonResponse) {
        pDialog.dismiss();
        try {


            myProfile_pojo = gson.fromJson(jsonResponse, MyProfile_pojo.class);
            if (myProfile_pojo.getErrNum().equals("33") && myProfile_pojo.getErrFlag().equals("0"))
            {
                manager.setProfilePic(myProfile_pojo.getpPic());
                String fileuploded[] = manager.getProfilePic().split("/");
               // Log.i("TAGIS ","IMAGESARE "+fileuploded.length);
                if(fileuploded.length==6)
                    profileToDelete = Variableconstant.AmazonProfileFolderName+"/"+fileuploded[5];
              //  Log.i("TAGPROFILE ","IMAGESIZE "+fileuploded.length + " pic "+manager.getProfilePic());
                lantype_pojo.setLanguages(myProfile_pojo.getLanguages());
                String   url=manager.getProfilePic().replace(" ", "%20");
                String languagetype = gson.toJson(lantype_pojo,Language_Type.class);
                manager.setLanguage(languagetype);
                callspinradaptermethod();
                if(!url.equals(""))
                {

                    progressBar.setVisibility(View.VISIBLE);

                    Picasso.with(getActivity()).load(url)
                            //.memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)
                            .resize(profileIv.getWidth(), profileIv.getHeight())
                            .centerCrop().transform(new CircleTransform())
                            .placeholder(R.drawable.register_profile_default_image)
                            .into(profileIv);

                    Picasso.with(getActivity()).load(url)
                            .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE)
                            .resize(MenuActivity.profilePicture.getWidth(), MenuActivity.profilePicture.getHeight())
                            .centerCrop().transform(new CircleTransform())
                            .skipMemoryCache()
                            .placeholder(R.drawable.register_profile_default_image)
                            .into(MenuActivity.profilePicture);

                    progressBar.setVisibility(View.GONE);

                }




                String fname = myProfile_pojo.getfName().substring(0, 1).toUpperCase() + myProfile_pojo.getfName().substring(1);
                if(!myProfile_pojo.getlName().equals(""))
                {
                    String lname = myProfile_pojo.getlName().substring(0, 1).toUpperCase() + myProfile_pojo.getlName().substring(1);
                    eTprofile_lname.setText(lname);
                }
                else
                {
                    eTprofile_lname.setText("");
                }
                eTprofil_fname.setText(fname);
                String drawrname = eTprofil_fname.getText().toString()+" "+eTprofile_lname.getText().toString();
                MenuActivity.drawer_name.setText(drawrname);
                manager.setCustomerFnme(eTprofil_fname.getText().toString());

                manager.setCustomerLnme(eTprofile_lname.getText().toString());


                eTprofile_email.setText(myProfile_pojo.getEmail());
                eTprofile_mobile.setText(myProfile_pojo.getPhone());
                pro_country_code.setText(myProfile_pojo.getCountry_code());

            } else if (myProfile_pojo.getErrNum().equals("96") && myProfile_pojo.getErrFlag().equals("1")) {
                Toast.makeText(getActivity(), myProfile_pojo.getErrMsg(), Toast.LENGTH_LONG).show();
            }
            else if (myProfile_pojo.getErrNum().equals("94") && myProfile_pojo.getErrFlag().equals("1")) {
                Toast.makeText(getActivity(), myProfile_pojo.getErrMsg(), Toast.LENGTH_LONG).show();
            }else if(myProfile_pojo.getErrNum().equals("7") || myProfile_pojo.getErrNum().equals("83") && myProfile_pojo.getErrFlag().equals("1"))
            {
                Utility.setMAnagerWithBID(getActivity(),manager);
                Toast.makeText(getActivity(),myProfile_pojo.getErrMsg(),Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    ////////////////////////
    public void updateprofile()
    {

        if(Utility.isNetworkAvailable(getActivity()))
        {

         /*   Log.i("updateprofile","fname "+eTprofil_fname.getText().toString()+
                    " lastname "+eTprofile_lname.getText().toString()+""+" country "+pro_country_code.getText().toString()+
                    " email "+eTprofile_email.getText().toString()+" mobile "+eTprofile_mobile.getText().toString()+
                    " sessiontoken "+manager.getSession()+" deviceid "+manager.getDevice_Id()
                    +" cid "+manager.getCustomerId());*/


            String lname;
            if(eTprofile_lname.getText().toString().trim().equals(""))
            {
                lname = "";
            }
            else
            {
                lname = eTprofile_lname.getText().toString().trim();
            }
            FormBody requestBody = new FormBody.Builder()
                    .add("ent_sess_token", manager.getSession())
                    .add("ent_dev_id", manager.getDevice_Id())
                    .add("ent_first_name", eTprofil_fname.getText().toString())
                    .add("ent_last_name",lname)
                    .add("ent_country_code",pro_country_code.getText().toString())
                    .add("ent_email",eTprofile_email.getText().toString())
                    .add("ent_phone",eTprofile_mobile.getText().toString())
                    .add("ent_cid",manager.getCustomerId())
                    .add("ent_ppic",manager.getProfilePic())
                    .add("ent_date_time", Utility.dateintwtfour())
                    .build();


            OkHttp3Request.doOkHttp3Request(Variableconstant.UPDATEPROFILE, requestBody, new OkHttp3Request.JsonRequestCallback() {
                @Override
                public void onSuccess(String result) {
                    uDialog.dismiss();

                    callProfileupdateResponse(result);
                  //  Log.i("profile update","responce"+result);
                }

                @Override
                public void onError(String error) {
                    uDialog.dismiss();
                }
            });
        }



    }


    private void callProfileupdateResponse(String jsonResponse) {

        try
        {
            Gson gson = new Gson();

            ettest.requestFocus();
            test.requestFocus();

            myProfile_pojo = gson.fromJson(jsonResponse, MyProfile_pojo.class);
            if(myProfile_pojo.getErrNum().equals("54")&&myProfile_pojo.getErrFlag().equals("0")) {

                tveditbarhometxv.setText(getResources().getString(R.string.edit));
                eTprofile_mobile.setText(eTprofile_mobile.getText().toString());
                manager.setCustomerFnme(eTprofil_fname.getText().toString());
                manager.setCustomerLnme(eTprofile_lname.getText().toString());
                if(!manager.getCustomerFnme().trim().equals("")&&!manager.getCustomerLnme().trim().equals(""))
                {
                    String fname = manager.getCustomerFnme().substring(0, 1).toUpperCase() + manager.getCustomerFnme().substring(1);
                    String lname = manager.getCustomerLnme().substring(0, 1).toUpperCase() + manager.getCustomerLnme().substring(1);
                    String name = fname+" "+lname;
                    dname.setText(name);
                }
                else if(!manager.getCustomerFnme().trim().equals(""))
                {
                    String fname = manager.getCustomerFnme().substring(0, 1).toUpperCase() + manager.getCustomerFnme().substring(1);
                    dname.setText(fname);
                }
                eTprofil_fname.setEnabled(false);
                eTprofile_lname.setEnabled(false);
                eTprofile_mobile.setEnabled(false);


                Toast.makeText(getActivity(), myProfile_pojo.getErrMsg(), Toast.LENGTH_LONG).show();
            }
            else if(myProfile_pojo.getErrNum().equals("96")&&myProfile_pojo.getErrFlag().equals("1"))
            {
                Toast.makeText(getActivity(), myProfile_pojo.getErrMsg(), Toast.LENGTH_LONG).show();
            } else if(myProfile_pojo.getErrNum().equals("94")&&myProfile_pojo.getErrFlag().equals("1"))
            {
                Toast.makeText(getActivity(), myProfile_pojo.getErrMsg(), Toast.LENGTH_LONG).show();
            }
            else if(myProfile_pojo.getErrNum().equals("7") ||myProfile_pojo.getErrNum().equals("83") &&myProfile_pojo.getErrFlag().equals("1"))
            {


                Utility.setMAnagerWithBID(getActivity(),manager);

                Toast.makeText(getActivity(),myProfile_pojo.getErrMsg(),Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e)
        {
            Utility.printLog(e + "");
        }

    }


    private void setListener() {
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override public void onSelectCountry(String name, String code, String dialCode,
                                                  int flagDrawableResID) {

                pro_country_code.setText(dialCode);
                mCountryFlagImageView.setImageResource(flagDrawableResID);
                mCountryPicker.dismiss();
            }
        });

        getUserCountryInfo();
    }



    private void getUserCountryInfo() {
        Country country = mCountryPicker.getUserCountryInfo(getActivity());
        mCountryFlagImageView.setImageResource(country.getFlag());
        pro_country_code.setText(country.getDialCode());

    }

    @Override
    public void onPause() {
        super.onPause();
        if(pDialog!=null)
        {
            pDialog.dismiss();
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Variableconstant.selectedlanid = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



    public void removeimage()
    {
        Drawable myDrawable = getResources().getDrawable(R.drawable.register_profile_default_image);
        Bitmap anImage = ((BitmapDrawable) myDrawable).getBitmap();

        takenNewImage = replaceEmailAtRate + ".png";
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            newFile = new File(profilePicsDir, takenNewImage);
        } else {
            newFile = new File(getActivity().getFilesDir() + "/" + Variableconstant.PARENT_FOLDER + "/Profile_Pictures/", takenNewImage);
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(newFile);
            anImage.compress(Bitmap.CompressFormat.PNG, 100, fos);

            uploadToAmazon(newFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {

                if (fos != null) {
                    fos.flush();
                    fos.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private class deleteImage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params)
        {
            Log.d("TAGto ","DELECTE "+profileToDelete);
            upload.delete_Item("iserve",profileToDelete);

            return null;
        }
    }
}

