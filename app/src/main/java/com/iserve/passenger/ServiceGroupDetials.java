package com.iserve.passenger;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.adapter.ServiceGroupAdaptr;
import com.google.gson.Gson;
import com.pojo.DataService;
import com.utility.SessionManager;
import com.utility.Variableconstant;

import java.util.ArrayList;

/**
 * Created by embed on 24/9/16.
 *
 */
public class ServiceGroupDetials extends AppCompatActivity
{



    ListView servicelist;
    String TAG = "ServiceGroupDetials";
    TextView tveditbarhometxv,toolbarhometxv,tvaddhome;
    FrameLayout fframelayt;
    Toolbar toolbar;
    ArrayList<DataService> questions = new ArrayList<>();
    SessionManager manager;
    Gson gson;
    ServiceGroupAdaptr groupAdaptr;
    ArrayList<String> jobIds = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.servicegrouplist);
        manager = new SessionManager(this);
        gson = new Gson();
        servicelist = (ListView) findViewById(R.id.servicelist);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvaddhome = (TextView) findViewById(R.id.tvaddhome);
        toolbarhometxv = (TextView) findViewById(R.id.toolbarhometxv);
        tvaddhome.setText(getResources().getString(R.string.done));
        toolbarhometxv.setText(getResources().getString(R.string.addservic));
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        toolbarhometxv.setVisibility(View.VISIBLE);
        tvaddhome.setVisibility(View.VISIBLE);
        fframelayt.setVisibility(View.VISIBLE);
        tvaddhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Variableconstant.Service_group = true;
                finish();
            }
        });
        getSupportActionBar().setTitle("");

        if(getIntent().getExtras()!=null)
        {

            questions = (ArrayList<DataService>)getIntent().getSerializableExtra("DATA");
            Log.i(TAG,"questionlist "+questions.get(0).getGroup_name());
            groupAdaptr = new ServiceGroupAdaptr(this,questions);
            servicelist.setAdapter(groupAdaptr);

        }

        servicelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {


                Intent intent  = new Intent(ServiceGroupDetials.this,ServiceDetials.class);


                intent.putExtra("DATA",questions.get(position).getServices());
                intent.putExtra("DATAMULTI",questions.get(position).getCmult());
                intent.putExtra("Mandetory",questions.get(position).isMandatorydata());
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"checkedmathdman "+manager.getJobIds());

        jobIds = manager.getJobIds();

        for(DataService service: questions)
        {

            for(int i =0; i< service.getServices().size(); i++)
            {
                if(jobIds != null && jobIds.contains(service.getServices().get(i).getSid().trim()))
                {
                    service.getServices().get(i).setServicegroupflag(true);

                }
                else {
                    service.getServices().get(i).setServicegroupflag(false);
                }
            }
        }


        groupAdaptr.notifyDataSetChanged();

    }

}
