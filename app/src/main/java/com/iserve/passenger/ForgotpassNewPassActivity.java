package com.iserve.passenger;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.utility.Alerts;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rahul on 15/10/16.
 *
 */

public class ForgotpassNewPassActivity extends AppCompatActivity
{
    private String mobileNumber="",comingFrom;
    private TextView  tvHeader;
    Alerts alerts;
    ProgressDialog pDialog;
    Button submit;
    EditText etPassword,etConfirmPass,etoldpass;
    SessionManager manager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        setContentView(R.layout.set_new_password);
        pDialog = new ProgressDialog(this);
        alerts = new Alerts(this);
        manager = new SessionManager(this);
        intialize();
    }

    private void intialize()
    {
        mobileNumber= getIntent().getStringExtra("MOBILE");
        comingFrom = getIntent().getStringExtra("ProfileData");


         tvHeader= (TextView) findViewById(R.id.tvHeader);
        etConfirmPass= (EditText) findViewById(R.id.etConfirmPass);
        etPassword= (EditText) findViewById(R.id.etPassword);
        etoldpass= (EditText) findViewById(R.id.etoldpass);
         submit = (Button) findViewById(R.id.submit);
        FrameLayout fframelayt = (FrameLayout) findViewById(R.id.fframelayt);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(toolbar!=null)
        {
            toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
            getSupportActionBar().setTitle("");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }


        assert fframelayt != null;
        fframelayt.setVisibility(View.VISIBLE);

        TextView  actionbartext = (TextView) findViewById(R.id.toolbarhometxv);

        assert actionbartext != null;
        if(comingFrom.equals("Profile"))
        {

            actionbartext.setText(getResources().getString(R.string.changepassword));
            tvHeader.setText(getResources().getString(R.string.changeuapassword));
            etPassword.setVisibility(View.VISIBLE);
            etoldpass.setVisibility(View.VISIBLE);
        }
        else
        {
            actionbartext.setText(getResources().getString(R.string.forgotpassword));
            tvHeader.setText(getResources().getString(R.string.thankyouenternewpass));
            etPassword.setVisibility(View.VISIBLE);
            etoldpass.setVisibility(View.GONE);
        }

        actionbartext.setVisibility(View.VISIBLE);

        if(comingFrom.equals("Profile"))
        {
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!etoldpass.getText().toString().isEmpty())
                    {
                        if(etoldpass.getText().toString().equals(manager.getOldpassword()))
                        {
                            if(!etPassword.getText().toString().isEmpty())
                            {

                                if (!etConfirmPass.getText().toString().trim().isEmpty()) {

                                    if (etPassword.getText().toString().equals(etConfirmPass.getText().toString())) {
                                        submitPass();
                                    } else {
                                        alerts.problemLoadingAlert(ForgotpassNewPassActivity.this, getResources().getString(R.string.pas_no_match));
                                    }

                                } else {
                                    alerts.problemLoadingAlert(ForgotpassNewPassActivity.this, getResources().getString(R.string.reEnteredpass));
                                }
                            }
                            else
                            {
                                alerts.problemLoadingAlert(ForgotpassNewPassActivity.this,getResources().getString(R.string.password_sould_not_empty));
                            }
                        }

                        else
                        {
                            alerts.problemLoadingAlert(ForgotpassNewPassActivity.this,getResources().getString(R.string.pas_not_match_old));
                        }

                    }
                    else
                    {
                        alerts.problemLoadingAlert(ForgotpassNewPassActivity.this,getResources().getString(R.string.password_sould_not_empty));

                    }

                }
            });
        }
        else
        {
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!etPassword.getText().toString().isEmpty())
                    {
                        if(!etConfirmPass.getText().toString().trim().isEmpty())
                        {
                            if(etPassword.getText().toString().equals(etConfirmPass.getText().toString()))
                            {
                                submitPass();
                            }
                            else
                            {
                                alerts.problemLoadingAlert(ForgotpassNewPassActivity.this,getResources().getString(R.string.pas_no_match));
                            }
                        }
                        else
                        {
                            alerts.problemLoadingAlert(ForgotpassNewPassActivity.this,getResources().getString(R.string.reEnteredpass));
                        }

                    }
                    else
                    {
                        alerts.problemLoadingAlert(ForgotpassNewPassActivity.this,getResources().getString(R.string.password_sould_not_empty));

                    }

                }
            });
        }

        Typeface nexaBold = Typeface.createFromAsset(this.getAssets(), Variableconstant.regularfont);
        Typeface nexaLight = Typeface.createFromAsset(this.getAssets(), Variableconstant.lightfont);


        tvHeader.setTypeface(nexaLight);
        etPassword.setTypeface(nexaLight);
        etConfirmPass.setTypeface(nexaLight);
        etoldpass.setTypeface(nexaLight);
        submit.setTypeface(nexaBold);
    }


    private void submitPass()
    {
        pDialog.setMessage(getResources().getString(R.string.wait));
        pDialog.setCancelable(false);
        pDialog.show();

        JSONObject jsonObject=new JSONObject();
        try
        {
            jsonObject.put("ent_mobile", mobileNumber);
            jsonObject.put("ent_pass", etConfirmPass.getText().toString());
            jsonObject.put("ent_user_type","2");
            Log.i("TAG","params to login "+jsonObject);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        Utility.doJsonRequest(Variableconstant.service_url + "updatePasswordForUser", jsonObject, new Utility.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {

                String errFlag,errMsg;
                pDialog.cancel();
                if(result!=null)
                {
                    try
                    {
                        JSONObject jsonObject1=new JSONObject(result);
                        errFlag=jsonObject1.getString("errFlag");
                        errMsg=jsonObject1.getString("errMsg");
                        if(errFlag.equals("0"))
                        {
                            Toast.makeText(ForgotpassNewPassActivity.this,getResources().getString(R.string.passwordreset),Toast.LENGTH_SHORT).show();
                            finish();
                            overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
                        }
                        else
                        {
                            Utility.ShowAlert(errMsg,ForgotpassNewPassActivity.this);
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Toast.makeText(ForgotpassNewPassActivity.this, getResources().getString(R.string.network_alert_message), Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onError(String error)
            {
                pDialog.cancel();
                Utility.printLog("on error for the login "+error);
                Toast.makeText(ForgotpassNewPassActivity.this,error, Toast.LENGTH_LONG).show();
            }
        });


    }
    @Override
    public void onBackPressed()
    {
        Utility.hideInputMethod(this,getCurrentFocus());
        finish();
        overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
    }
}
