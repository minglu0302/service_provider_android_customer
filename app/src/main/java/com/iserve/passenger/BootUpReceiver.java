package com.iserve.passenger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * <h>BootUpReceiver</h>
 * Created by embed on 5/12/16.
 *
 */
public class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent serviceIntent = new Intent(context, ApplicationController.class);
            context.startService(serviceIntent);
        }
    }}
