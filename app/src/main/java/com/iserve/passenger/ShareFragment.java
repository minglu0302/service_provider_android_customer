package com.iserve.passenger;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.utility.Alerts;
import com.utility.SessionManager;
import com.utility.Utility;

import java.util.List;

/**
 * Created by embed on 14/12/15.
 *
 */
public class ShareFragment extends Fragment implements View.OnClickListener
{

    Alerts alerts;
    TextView share_text_Tv,share_code_Tv,txt_share_code_Tv,toolbarhometxv,tveditbarhometxv,youraddress,yourlocation;
    ImageView facebook_share_Iv,twitter_share_Iv,message_share_Iv,email_share_Iv,tolbarhomelogo;//,icwhatsapp_icon;
    Typeface sans_regular,sans_semibold,sans_bold;
    SessionManager sessionManager;
    String shareCOde;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    String download,andsignup,andearndiscount;
    RelativeLayout imageframelayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sessionManager=  new SessionManager(getActivity());
        shareCOde=sessionManager.getCoupon();
        alerts= new Alerts(getActivity());
    }

    /**
     * <p>inflating the view</p>
     * @param inflater inflate
     * @param container contain
     * @param savedInstanceState instance
     * @return view
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=  inflater.inflate(R.layout.sharefragment,container,false);
        intilizeVariable(view);
        return view;
    }



    /**
     * <p>Intiliazing view elements</p>
     * @param view main view
     */
    void intilizeVariable(View view){

        share_text_Tv= (TextView) view.findViewById(R.id.share_text_Tv);
        share_code_Tv= (TextView) view.findViewById(R.id.share_code_Tv);
        txt_share_code_Tv= (TextView) view.findViewById(R.id.txt_share_code_Tv);
        facebook_share_Iv= (ImageView) view.findViewById(R.id.facebook_share_Iv);
        twitter_share_Iv= (ImageView) view.findViewById(R.id.twitter_share_Iv);
        message_share_Iv= (ImageView) view.findViewById(R.id.message_share_Iv);
        email_share_Iv= (ImageView) view.findViewById(R.id.email_share_Iv);
       // icwhatsapp_icon = (ImageView) view.findViewById(R.id.icwhatsapp_icon);
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        toolbarhometxv = (TextView) getActivity().findViewById(R.id.toolbarhometxv);
        yourlocation = (TextView) getActivity().findViewById(R.id.yourlocation);
        youraddress = (TextView) getActivity().findViewById(R.id.youraddress);
        imageframelayout = (RelativeLayout) getActivity().findViewById(R.id.imageframelayout);
        imageframelayout.setVisibility(View.GONE);
        toolbarhometxv.setVisibility(View.VISIBLE);
        toolbarhometxv.setText(getResources().getString(R.string.invite));
        tolbarhomelogo = (ImageView) getActivity().findViewById(R.id.tolbarhomelogo);
        tolbarhomelogo.setVisibility(View.GONE);

        tveditbarhometxv = (TextView) getActivity().findViewById(R.id.tveditbarhometxv);
        youraddress.setVisibility(View.GONE);
        yourlocation.setVisibility(View.GONE);
        tveditbarhometxv.setVisibility(View.GONE);
        share_code_Tv.setText(shareCOde);

        download = getResources().getString(R.string.Download);
        andsignup = getResources().getString(R.string.andsignup);
        andearndiscount = getResources().getString(R.string.andearndiscount);

// set on click event
        facebook_share_Iv.setOnClickListener(this);
        twitter_share_Iv.setOnClickListener(this);
        message_share_Iv.setOnClickListener(this);
        email_share_Iv.setOnClickListener(this);
       // icwhatsapp_icon.setOnClickListener(this);
        setTypefaceFonts();

    }


    /**
     * intilize the typeface for font
     */

    void setTypefaceFonts() {
        sans_regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular_0.ttf");
        sans_semibold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Semibold_0.ttf");
        sans_bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Bold_0.ttf");
        share_text_Tv.setTypeface(sans_regular);
        txt_share_code_Tv.setTypeface(sans_regular);
        share_code_Tv.setTypeface(sans_semibold);
    }


    /**
     * <p>overriding onclick method</p>
     * @see View.OnClickListener
     * @param v id view
     */
    @Override
    public void onClick(View v) {

        /**
         * on click facebook icon start send intent (ACTION_SEND)
         */
        switch (v.getId())
        {
            case R.id.facebook_share_Iv:

                String text = download+" "+getResources().getString(R.string.app_name)+" "+andsignup+" '"+share_code_Tv.getText().toString()+"' "+andearndiscount;

               // String urlToShare = "https://www.facebook.com/911nerds";
                String urlToShare = "https://www.facebook.com/IServe-1801457180122864/";
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, urlToShare);
                boolean facebookAppFound = false;
                List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
                for (ResolveInfo info : matches) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook")) {
                        intent.setPackage(info.activityInfo.packageName);
                        facebookAppFound = true;
                        break;
                    }
                }

                if(facebookAppFound)
                {
                    startActivity(intent);
                }
                else
                {
                   /* if (ShareDialog.canShow(ShareLinkContent.class))
                    {
                        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                .setContentTitle(getResources().getString(R.string.app_name))
                                .setContentDescription(text)
                                .setContentUrl(Uri.parse("https://www.facebook.com/911nerds"))
                                .build();
                        shareDialog.show(linkContent);

                    }*/
                    /*else
                    {*/
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hello,"+"\n"+"  "+text
                        );
                        startActivity(Intent.createChooser(shareIntent, share_code_Tv.getText().toString()));
                   // }

                }
                break;
            case R.id.twitter_share_Iv:
                String Body=download+" "+getResources().getString(R.string.app_name)+" "+andsignup+" '"+share_code_Tv.getText().toString()+"' "+andearndiscount;

                Intent intent1 = new Intent(Intent.ACTION_SEND);
                intent1.setType("text/plain");
                intent1.putExtra(Intent.EXTRA_TEXT, Body);
                boolean twitterAppFound = false;
                List<ResolveInfo> matches1 = getActivity().getPackageManager().queryIntentActivities(intent1, 0);
                for (ResolveInfo info : matches1) {
                    if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                        intent1.setPackage(info.activityInfo.packageName);
                        twitterAppFound = true;
                        break;
                    }
                }

                if(twitterAppFound)
                {
                    startActivity(intent1);
                }
                else
                {

                    if (Utility.isNetworkAvailable(getActivity()))
                    {
                        Uri uri = Uri.parse("market://details?id=" + "com.twitter.android");
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        try
                        {
                            startActivity(goToMarket);
                        } catch (ActivityNotFoundException e)
                        {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android&hl=en")));
                        }
                    }
                    else
                    {
                        alerts.showNetworkAlert(getActivity());
                    }

                }
                break;
//            case R.id.icwhatsapp_icon:
//
//                String urlBody=download+" "+getResources().getString(R.string.app_name)+" "+andsignup+" '"+share_code_Tv.getText().toString()+"' "+andearndiscount;
//
//                Intent intent2 = new Intent(Intent.ACTION_SEND);
//                intent2.setType("text/plain");
//                intent2.putExtra(Intent.EXTRA_TEXT, urlBody);
//                boolean whatsappAppFound = false;
//                List<ResolveInfo> matches2 = getActivity().getPackageManager().queryIntentActivities(intent2, 0);
//                for (ResolveInfo info : matches2) {
//                    if (info.activityInfo.packageName.toLowerCase().startsWith("com.whatsapp")) {
//                        intent2.setPackage(info.activityInfo.packageName);
//                        whatsappAppFound = true;
//                        break;
//
//                    }
//                }
//
//                if(whatsappAppFound)
//                {
//                    startActivity(intent2);
//                }
//                else
//                {
//
//
//                    if (Utility.isNetworkAvailable(getActivity()))
//                    {
//                        Uri uri = Uri.parse("market://details?id=" + "com.whatsapp");
//                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
//                        try
//                        {
//                            startActivity(goToMarket);
//                        } catch (ActivityNotFoundException e)
//                        {
//                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp&hl=en")));
//                        }
//                    }
//                    else
//                    {
//                        alerts.showNetworkAlert(getActivity());
//                    }
//                }
//                break;
            case R.id.message_share_Iv:
                String smsBody=download+" "+getResources().getString(R.string.app_name)+" "+andsignup+" '"+share_code_Tv.getText().toString()+"' "+andearndiscount;

                Intent sms=new Intent(Intent.ACTION_VIEW,Uri.parse("sms:"));
                sms.putExtra("sms_body",smsBody);
                startActivity(sms);
                break;
            case R.id.email_share_Iv:

                String emailBody=download+" "+getResources().getString(R.string.app_name)+" "+andsignup+" '"+share_code_Tv.getText().toString()+"' "+andearndiscount;

                Intent email=new Intent(Intent.ACTION_SENDTO);
                email.putExtra(Intent.EXTRA_SUBJECT,getResources().getString(R.string.registeron)+" " +getResources().getString(R.string.app_name));
                email.putExtra(Intent.EXTRA_TEXT,emailBody);
                email.setType("text/plain");
                email.setType("message/rfc822");
                email.setData(Uri.parse("mailto:" + " "));
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
                break;

        }


    }
}
