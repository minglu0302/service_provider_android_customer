package com.iserve.passenger;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.pojo.Validator_Pojo;
import com.utility.Alerts;
import com.utility.OkHttp3Request;
import com.utility.Utility;
import com.utility.Variableconstant;
import okhttp3.FormBody;

/**
 * Created by rahul on 15/10/16.
 *
 */

public class ForgotPassOtpActivity extends AppCompatActivity
{
    private String mobileNumber="";
    private String country_code;
    private EditText stup2text1,stup2text2,stup2text3,stup2text4;
    Alerts alert;
    Button resend_button;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pass_otp_screen);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        alert = new Alerts(this);
        intialize();
    }

    private void intialize()
    {

        mobileNumber= getIntent().getStringExtra("NUMBER");
        country_code= getIntent().getStringExtra("COUNTRY_CODE");
        Typeface nexaBold = Typeface.createFromAsset(this.getAssets(), Variableconstant.regularfont);
        Typeface nexaLight = Typeface.createFromAsset(this.getAssets(), Variableconstant.lightfont);
        TextView tvDescription= (TextView) findViewById(R.id.tvDescription);
        TextView submit= (TextView) findViewById(R.id.submit);
        stup2text1= (EditText) findViewById(R.id.stup2text1);
        stup2text2= (EditText) findViewById(R.id.stup2text2);
        stup2text3= (EditText) findViewById(R.id.stup2text3);
        stup2text4= (EditText) findViewById(R.id.stup2text4);
        resend_button= (Button) findViewById(R.id.resend_button);
        FrameLayout fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(toolbar!=null)
        {
            toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
            getSupportActionBar().setTitle("");
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        fframelayt.setVisibility(View.VISIBLE);
        TextView  actionbartext = (TextView) findViewById(R.id.toolbarhometxv);
        actionbartext.setText(getResources().getString(R.string.forgotpassword));
        actionbartext.setVisibility(View.VISIBLE);


        stup2text1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                stup2text2.requestFocus();
            }
        });stup2text2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                stup2text3.requestFocus();
            }
        });stup2text3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                stup2text4.requestFocus();
            }
        });stup2text4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(count==1)
                {
                    callmethodtoVerifiy();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        resend_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utility.isNetworkAvailable(ForgotPassOtpActivity.this))
                    getVerficationcode();
                else
                    alert.showNetworkAlert(ForgotPassOtpActivity.this);

            }
        });


        String textis = getResources().getString(R.string.text_for_otp)+" "+mobileNumber;
        tvDescription.setText(textis);


        tvDescription.setTypeface(nexaLight);
        submit.setTypeface(nexaBold);
        stup2text1.setTypeface(nexaLight);
        stup2text2.setTypeface(nexaLight);
        stup2text3.setTypeface(nexaLight);
        stup2text4.setTypeface(nexaLight);
    }

    private void getVerficationcode()
    {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog .setMessage(getResources().getString(R.string.wait));
        pDialog.show();




        FormBody requestform = new FormBody.Builder()

                .add("ent_mobile", mobileNumber)
                .add("ent_user_type", 2+"")
                .add("ent_date_time", Utility.dateintwtfour())
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "ForgotPasswordWithOtp", requestform, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("mail validtion  onSuccess JSON DATA" + result);
                pDialog.dismiss();

            }

            @Override
            public void onError(String error) {
                pDialog.dismiss();
            }
        });
    }

    private void callmethodtoVerifiy()
    {
        if(!TextUtils.isEmpty(stup2text1.getText().toString()) && !TextUtils.isEmpty(stup2text2.getText().toString()) &&
                !TextUtils.isEmpty(stup2text3.getText().toString()) && !TextUtils.isEmpty(stup2text4.getText().toString()))
        {
            String s = (stup2text1.getText().toString()+""+stup2text2.getText().toString()+""+
                    stup2text3.getText().toString()+""+stup2text4.getText().toString());
            if(Utility.isNetworkAvailable(ForgotPassOtpActivity.this))
                Verfycode(s);
            else
                alert.showNetworkAlert(ForgotPassOtpActivity.this);

        }
    }

    private void Verfycode(String s)
    {
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.wait));
        pDialog.show();

        FormBody requestform = new FormBody.Builder()
                .add("ent_phone", country_code+""+mobileNumber)
                .add("ent_code", s)
                .add("ent_user_type", "2")
                .add("ent_service_type", "1")
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.VERIFYPHONE, requestform, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {

                pDialog.dismiss();
                Validator_Pojo phoneNumberValidator_pojo;
                Gson gson = new Gson();
                phoneNumberValidator_pojo = gson.fromJson(result, Validator_Pojo.class);

                if(phoneNumberValidator_pojo!=null)
                {
                    if(phoneNumberValidator_pojo.getErrFlag().equals("0"))
                    {

                        Intent intent=new Intent(ForgotPassOtpActivity.this,ForgotpassNewPassActivity.class);
                        intent.putExtra("MOBILE",mobileNumber);
                        intent.putExtra("ProfileData","Forgot");
                        startActivity(intent);
                        finish();
                    }
                    else
                    {
                        alert.problemLoadingAlert(ForgotPassOtpActivity.this,phoneNumberValidator_pojo.getErrMsg());
                        stup2text1.setText(null);
                        stup2text2.setText(null);
                        stup2text3.setText(null);
                        stup2text4.setText(null);

                    }
                }
                else
                {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgotPassOtpActivity.this);

                    // set title
                    alertDialogBuilder.setTitle(getResources().getString(R.string.alert));
                    // set dialog message
                    alertDialogBuilder
                            .setMessage(getResources().getString(R.string.server_error))
                            .setCancelable(false)
                            .setNegativeButton(getResources().getString(R.string.ok),new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog,int id)
                                {
                                    finish();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                }
            }

            @Override
            public void onError(String error)
            {
                pDialog.dismiss();
                Toast.makeText(ForgotPassOtpActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });

    }
    @Override
    public void onBackPressed()
    {
        finish();

        overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
    }

}
