package com.iserve.passenger;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adapter.ViewPagerAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by embed on 8/11/16.
 *
 */
public class Image_Gallery extends AppCompatActivity
{
    ArrayList<String> arrayList;
    private ViewPager viewPager;
    private LinearLayout thumbnailsContainer;
    View btnNext,btnPrev;
    Toolbar toolbar;
    FrameLayout fframelayt;
    TextView actionbartext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imagegallery);
        if(getIntent().getExtras()!=null)
            arrayList = getIntent().getStringArrayListExtra("Imagelist");
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        thumbnailsContainer = (LinearLayout) findViewById(R.id.container);
         btnNext = findViewById(R.id.next);
         btnPrev = findViewById(R.id.prev);


        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        assert toolbar != null;
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        actionbartext = (TextView) findViewById(R.id.toolbarhometxv);
        fframelayt.setVisibility(View.VISIBLE);
        actionbartext.setVisibility(View.VISIBLE);
        actionbartext.setText(getResources().getString(R.string.jobImages));

        btnPrev.setOnClickListener(onClickListener(0));
        btnNext.setOnClickListener(onClickListener(1));

        FragmentStatePagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), arrayList);
        viewPager.setAdapter(adapter);

        inflateThumbnails();
    }

    private View.OnClickListener onClickListener(final int i) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i > 0) {
                    //next page
                    if (viewPager.getCurrentItem() < viewPager.getAdapter().getCount() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    }
                } else {
                    //previous page
                    if (viewPager.getCurrentItem() > 0) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                    }
                }
            }
        };
    }


    private void inflateThumbnails() {
        for (int i = 0; i < arrayList.size(); i++) {
            View imageLayout = getLayoutInflater().inflate(R.layout.imagethumbnail, thumbnailsContainer,false);
            ImageView imageView = (ImageView) imageLayout.findViewById(R.id.img_thumb);
            imageView.setOnClickListener(onChagePageClickListener(i));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 3;
            options.inDither = false;

            Picasso.with(this).load(arrayList.get(i))
                    .into(imageView);
            thumbnailsContainer.addView(imageLayout);
        }
    }

    private View.OnClickListener onChagePageClickListener(final int i) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(i);
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
