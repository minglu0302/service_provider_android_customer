package com.iserve.passenger;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.adapter.Address_Adaptor;
import com.google.gson.Gson;
import com.pojo.Saving_Address;
import com.pojo.Validator_Pojo;
import com.utility.OkHttp3Request;
import com.utility.SessionManager;
import com.utility.Variableconstant;
import java.util.ArrayList;
import okhttp3.FormBody;

/**
 * Created by embed on 12/9/16.
 *
 */
public class Address_Confirm extends AppCompatActivity
{
    FrameLayout fframelayt;
    TextView actionbartext,addaddress,noadressadded;
    Typeface textfont;
    ListView addresslist;
    private ArrayList<Saving_Address> saving_addresses = new ArrayList<>();
    Address_Adaptor address_adaptor;
    ProgressDialog pdialog;
    Validator_Pojo apojo;
    SessionManager manager;
    Gson gson;
    String TAG = "Address_Confirm";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.address_confirm);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        manager = new SessionManager(this);
        gson = new Gson();
        textfont = Typeface.createFromAsset(this.getAssets(), Variableconstant.regularfont);
        pdialog = new ProgressDialog(this);
        pdialog.setMessage(getResources().getString(R.string.wait));
        pdialog.setCancelable(false);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        addresslist = (ListView) findViewById(R.id.addresslist);
        addaddress = (TextView) findViewById(R.id.addaddress);
        noadressadded = (TextView) findViewById(R.id.noadressadded);
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        actionbartext = (TextView) findViewById(R.id.toolbarhometxv);
        fframelayt.setVisibility(View.VISIBLE);
        actionbartext.setVisibility(View.VISIBLE);
        actionbartext.setText(getResources().getString(R.string.address));
        actionbartext.setTypeface(textfont);
        address_adaptor = new Address_Adaptor(this, saving_addresses, Address_Confirm.this,true);
        addaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Address_Confirm.this,Current_Location.class);
                startActivity(intent);
            }
        });
        addresslist.setAdapter(address_adaptor);
    }

    @Override
    public void onResume() {
        super.onResume();

        callgetAddress();
    }

    private void callgetAddress()
    {
        pdialog.show();

        FormBody request = new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_cust_id",manager.getCustomerId())
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "GetCustomerAddress", request, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                pdialog.dismiss();
                callresponceMethod(result);

            }

            @Override
            public void onError(String error) {
                pdialog.dismiss();
            }
        });
    }

    private void callresponceMethod(String result)
    {
        apojo = gson.fromJson(result,Validator_Pojo.class);
        if(apojo!=null && apojo.getErrNum().equals("113") && apojo.getErrFlag().equals("0"))
        {
            if(apojo.getAddlist().size()>0)
            {
                saving_addresses.clear();

                noadressadded.setVisibility(View.GONE);
                saving_addresses.addAll(apojo.getAddlist());
                address_adaptor.notifyDataSetChanged();
            }
            else
            {
                noadressadded.setVisibility(View.VISIBLE);
            }

        }
    }

    public void backaddress(int place)
    {

        Intent returnIntent = new Intent();
        returnIntent.putExtra("area",saving_addresses.get(place).getArea());
        returnIntent.putExtra("city",saving_addresses.get(place).getCity());
        returnIntent.putExtra("landmark",saving_addresses.get(place).getAddress2());
        returnIntent.putExtra("addrestype",saving_addresses.get(place).getAddresstype());
        returnIntent.putExtra("Latitud",saving_addresses.get(place).getLati());
        returnIntent.putExtra("Longitud",saving_addresses.get(place).getLongi());
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    /**
     * Delete the Address from the DataBase
     * @param positiondelete integer type delete the address at a particular position
     */
    public void deletefromdb(final int positiondelete)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.deleteaddress));
        builder.setMessage(getResources().getString(R.string.doyouwishtodelete));
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {

                deleteaddress(saving_addresses.get(positiondelete).getAddressid());
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void deleteaddress(String addressid) {


        pdialog.show();
        FormBody request = new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_cust_id",manager.getCustomerId())
                .add("ent_addressid",addressid)
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "DeleteCustomerAddress", request, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {

                pdialog.dismiss();
                if(result!=null)
                {
                    apojo = gson.fromJson(result,Validator_Pojo.class);
                    if(apojo!=null && apojo.getErrNum().equals("112") && apojo.getErrFlag().equals("0"))
                    {
                        saving_addresses.clear();
                        if(apojo.getAddlist().size()>0) {

                            noadressadded.setVisibility(View.GONE);
                            saving_addresses.addAll(apojo.getAddlist());
                            address_adaptor.notifyDataSetChanged();
                            address_adaptor.notifyDataSetChanged();
                        }
                        else
                        {
                            noadressadded.setVisibility(View.VISIBLE);
                            address_adaptor.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onError(String error)
            {
                pdialog.dismiss();
            }
        });
    }
}
