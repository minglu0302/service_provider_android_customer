package com.iserve.passenger;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.Gson;
import com.pojo.PubNubType;
import com.pojo.PubNub_pojo;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by embed on 15/9/16.
 *
 */
public class Later_Booking extends AppCompatActivity implements View.OnClickListener
{
    SessionManager manager;
    PubNub_pojo pubNubPojo ;
    double currentLatitude,currentLongitude;
    Gson gson;
    ArrayList<PubNubType> types;
    ViewPager viewPager;
    SampleFragmentPagerAdapter sfpa;
    TabLayout tabLayout;
    String TAG ="Later_Booking";
    public static String name = "";
    String scheduleday = "";
    private int  mHour, mMinute,sec;
    FrameLayout fframelayt;
    ImageView calenderimage,tolbarhomelogo;
    TextView yourlocation,youraddress,orderTV,tvmasterbokltr,actionbartext,choseslots,chosedate,tvwearenotavailable;
    Toolbar app_toobar;
    RelativeLayout timeRl,rlorbooklatter,imageframelayout,rllistmapicon,rlempty,mainrrlout;
    Typeface addresstext,booknowlatr,semibold,italicfont;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 99;
    int typenamecount = 1;
    private Timer myTimer_publish;
    private ArrayList<Pager_data_updater> listerner_list;
    // private ArrayList<String> cat_name_list;
    private boolean isAdapter_added=false;
    private boolean dontsendreq=false;
    private int data_visible_point;
    private int initial_Adapter_size;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testmsterdtls);
        manager = new SessionManager(this);
        gson = new Gson();
        types = new ArrayList<>();
        listerner_list=new ArrayList<>();
        mainrrlout = (RelativeLayout) findViewById(R.id.mainrrlout);
        if(getIntent().getExtras()!=null)
        {
            currentLatitude = getIntent().getDoubleExtra("currentLatitude",0.0);
            currentLongitude = getIntent().getDoubleExtra("currentLongitude",0.0);
        }


        intialize();

        socketListent();

        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorHeight(5);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.actionbar_color));
        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        sfpa = new SampleFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position)
            {
                String name = pubNubPojo.getMsg().getTypes().get(position).getCat_name();
                manager.setServiceProvdr(name);
                data_visible_point=position;
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });

    }


    private void intialize()
    {

        booknowlatr  = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular_0.ttf");
        addresstext= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Bold_0.ttf");
        semibold= Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Semibold_0.ttf");
        italicfont= Typeface.createFromAsset(getAssets(), Variableconstant.italicfont);
        app_toobar = (Toolbar) findViewById(R.id.app_toobar);
        setSupportActionBar(app_toobar);
        app_toobar.setNavigationIcon(R.drawable.login_back_icon_off);
        getSupportActionBar().setTitle("");
        app_toobar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        yourlocation = (TextView) findViewById(R.id.yourlocation);
        youraddress = (TextView) findViewById(R.id.youraddress);
        orderTV = (TextView) findViewById(R.id.orderTV);
        tvmasterbokltr = (TextView) findViewById(R.id.tvmasterbokltr);
        choseslots = (TextView) findViewById(R.id.choseslots);
        chosedate = (TextView) findViewById(R.id.chosedate);
        tvwearenotavailable = (TextView) findViewById(R.id.tvwearenotavailable);
        imageframelayout = (RelativeLayout) findViewById(R.id.imageframelayout);
        rllistmapicon = (RelativeLayout) findViewById(R.id.rllistmapicon);
        rlempty = (RelativeLayout) findViewById(R.id.rlempty);
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        timeRl = (RelativeLayout) findViewById(R.id.timeRl);
        fframelayt.setVisibility(View.GONE);
        rlorbooklatter = (RelativeLayout) findViewById(R.id.rlorbooklatter);
        tolbarhomelogo = (ImageView) findViewById(R.id.tolbarhomelogo);
        calenderimage = (ImageView) findViewById(R.id.calenderimage);
        youraddress.setText(manager.getSavedAddress());
        tolbarhomelogo.setVisibility(View.GONE);
        rllistmapicon.setVisibility(View.GONE);
        choseslots.setTypeface(booknowlatr);
        chosedate.setTypeface(semibold);
        youraddress.setTypeface(booknowlatr);
        tvwearenotavailable.setTypeface(italicfont);
        yourlocation.setTypeface(addresstext);
        orderTV.setTypeface(booknowlatr);
        if(!manager.getServiceProvdr().equals(""))
        {
            String name = manager.getServiceProvdr().substring(0, 1).toUpperCase() + manager.getServiceProvdr().substring(1).toLowerCase();
            String proavilablenw = name+""+getResources().getString(R.string.provideravailablenw);
            orderTV.setText(proavilablenw);
        }
        imageframelayout.setOnClickListener(this);
        tvmasterbokltr.setOnClickListener(this);
        if(Variableconstant.comingfromlaterbooking)
        {
            timermethod();
            imageframelayout.setVisibility(View.VISIBLE);
            timeRl.setVisibility(View.VISIBLE);
            rlorbooklatter.setVisibility(View.GONE);
            yourlocation.setVisibility(View.VISIBLE);
            youraddress.setVisibility(View.VISIBLE);
        }
        else
        {
            fframelayt.setVisibility(View.GONE);
            timeRl.setVisibility(View.GONE);
            rlorbooklatter.setVisibility(View.VISIBLE);
            yourlocation.setVisibility(View.GONE);
            youraddress.setVisibility(View.GONE);
            Variableconstant.ent_SlotDate = "";
        }

    }

    private void timermethod()
    {
        String myDate = Variableconstant.ent_dt;
        scheduleday = myDate.split(" ")[0];
        Calendar newCalendar = Calendar.getInstance();

        String split[] = myDate.split(" ")[0].split("-");

        newCalendar.set(Integer.parseInt(split[0]), Integer.parseInt(split[1]) - (1), Integer.parseInt(split[2]));


        String title = DateUtils.formatDateTime(this,
                newCalendar.getTimeInMillis(),
                DateUtils.FORMAT_SHOW_DATE
                        | DateUtils.FORMAT_SHOW_WEEKDAY
                        | DateUtils.FORMAT_SHOW_YEAR
                        | DateUtils.FORMAT_ABBREV_MONTH
                        | DateUtils.FORMAT_ABBREV_WEEKDAY);

        Utility.printLog(" title " + title + " time " + myDate.split(" ")[1]);
        chosedate = (TextView) findViewById(R.id.chosedate);
        String dateTime = title+ " "+myDate.split(" ")[1];
        chosedate.setText(dateTime);
        Variableconstant.ent_txtValue = chosedate.getText().toString();

        calenderimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDate_Picker(Later_Booking.this, chosedate);
            }
        });

        choseslots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDate_Picker(Later_Booking.this, chosedate);
            }
        });

    }


    /**
     * <h1>openDate_Picker</h1>
     * Method is used to open a date picker and Pict a date.
     * <p>
     *     This method open a Date Picker dialog by DatePickerDialog object.
     *     Hen set the Max time as Current Date by the help of the {@code fromDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime())}.
     *     Also listening the response From the DatePicker and setting the Date to the Given Edit text.
     * </p>
     * @param doctor_details contains the Activity reference of the calling Activity.
     * @param chosedate contains the Textview reference to which data has to set.
     */
    private void openDate_Picker(final Later_Booking doctor_details, final TextView chosedate)
    {
        typenamecount = 1;
        final Calendar newCalendar = Calendar.getInstance();

        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(doctor_details, new DatePickerDialog.OnDateSetListener()
        {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                newCalendar.set(year, monthOfYear, dayOfMonth);

                SimpleDateFormat dateFormate= new SimpleDateFormat("MM-dd-yyyy",Locale.getDefault());
                Variableconstant.ent_SlotDate = dateFormate.format(newCalendar.getTime());
                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                scheduleday = dateFormatter.format(newCalendar.getTime());


                String title = DateUtils.formatDateTime(doctor_details,
                        newCalendar.getTimeInMillis(),
                        DateUtils.FORMAT_SHOW_DATE
                                | DateUtils.FORMAT_SHOW_WEEKDAY
                                | DateUtils.FORMAT_SHOW_YEAR
                                | DateUtils.FORMAT_ABBREV_MONTH
                                | DateUtils.FORMAT_ABBREV_WEEKDAY);
                Utility.printLog("year " + year + " month  " + monthOfYear + " day " + dayOfMonth + " title "
                        + title+"schedule "+scheduleday+" dateis "+ Variableconstant.ent_SlotDate);

                chosedate.setText(title);
                Variableconstant.ent_txtValue = chosedate.getText().toString();
                calltimepicker();
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);// new Date().getTime()
        fromDatePickerDialog.show();

    }//End of selecting the date

    private void calltimepicker()
    {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute)
                    {



                        if(hourOfDay<10)
                        {
                            if(minute<10)
                            {
                                Variableconstant.ent_dt = scheduleday+" "+("0"+hourOfDay + ":0" + minute+":00");
                                String total = chosedate.getText().toString().concat(" " +("0"+hourOfDay + ":" + minute));
                                chosedate.setText(total);
                                Variableconstant.ent_txtValue = chosedate.getText().toString();
                            }
                            else
                            {
                                Variableconstant.ent_dt = scheduleday+" "+("0"+hourOfDay + ":" + minute+":00");
                                String total = chosedate.getText().toString().concat(" " +("0"+hourOfDay + ":" + minute));
                                chosedate.setText(total);
                                Variableconstant.ent_txtValue = chosedate.getText().toString();
                            }

                        }
                        else
                        {
                            if(minute<10)
                            {
                                Variableconstant.ent_dt = scheduleday+" "+(hourOfDay + ":0" + minute+":00");
                                String total = chosedate.getText().toString().concat(" " +(hourOfDay + ":" + minute));
                                chosedate.setText(total);
                                Variableconstant.ent_txtValue = chosedate.getText().toString();
                            }
                            else
                            {
                                Variableconstant.ent_dt = scheduleday+" "+(hourOfDay + ":" + minute+":00");
                                String total = chosedate.getText().toString().concat(" " +(hourOfDay + ":" + minute));
                                chosedate.setText(total);
                                Variableconstant.ent_txtValue = chosedate.getText().toString();
                            }

                        }
                        myTimer_publish.cancel();
                        myTimer_publish.purge();
                        myTimer_publish = null;
                        isAdapter_added=false;
                        startPublishingWithTimer();
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }
    @Override
    protected void onResume() {
        super.onResume();
        myTimer_publish = null;
        dontsendreq = false;
        startPublishingWithTimer();

    }

    private void startPublishingWithTimer()
    {
        TimerTask myTimerTask_publish;
        // TODO Auto-generated method stub
        Utility.printLog("CONTROL INSIDE startPublishingWithTimer");
        if(myTimer_publish!= null)
        {
            Utility.printLog("Timer already started");
            return;
        }
        myTimer_publish = new Timer();
        myTimerTask_publish = new TimerTask()
        {
            @Override
            public void run()
            {


                if(!dontsendreq)
                    sendMessage(currentLatitude,currentLongitude, Variableconstant.ent_forlaterbooking, Variableconstant.ent_dt);

            }
        };
        myTimer_publish.schedule(myTimerTask_publish, 0, 4000);
    }


    private void setadatper()
    {
        listerner_list.clear();
        //cat_name_list.clear();
        sfpa.clearFragment();
        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {

                if(typenamecount==1)
                {
                    typenamecount++;
                    String nameq = pubNubPojo.getMsg().getTypes().get(0).getCat_name();
                    manager.setServiceProvdr(nameq);
                }

                for(int i =0;i<types.size();i++)
                {

                    PageFragment pageFragment=PageFragment.newInstance(pubNubPojo.getMsg().getMasArr().get(i).getMas(),pubNubPojo.getMsg().getMasArr().get(i).getTid(),
                            types.get(i).getFtype(),types.get(i).getIsGroupContain()
                    );//types.get(i).getPrice_min()
                    listerner_list.add(pageFragment.getListener());
                    //cat_name_list.add(pubNubPojo.getMsg().getTypes().get(i).getCat_name());
                    sfpa.addFragment(pageFragment,types.get(i).getCat_name());
                    viewPager.setAdapter(sfpa);
                    sfpa.notifyDataSetChanged();
                }
                //  viewPager.setAdapter(sfpa);
                tabLayout.setupWithViewPager(viewPager);
                ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
                int tabsCount = vg.getChildCount();
                for (int j = 0; j < tabsCount; j++) {
                    ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
                    int tabChildsCount = vgTab.getChildCount();
                    for (int i = 0; i < tabChildsCount; i++) {
                        View tabViewChild = vgTab.getChildAt(i);
                        if (tabViewChild instanceof TextView) {
                            ((TextView) tabViewChild).setTypeface(booknowlatr);
                        }
                    }
                }
                //  sfpa.notifyDataSetChanged();
                viewPager.setCurrentItem(Variableconstant.pro_forlatrboking);
                Variableconstant.pro_forlatrboking = 0;
            }
        });

        isAdapter_added=true;
    }
    private void sendMessage(double currentLatitude, double currentLongitude,int btype,String datetime)
    {
        JSONObject sendText = new JSONObject();
        try
        {
            sendText.put("email",manager.getCoustomerEmail());
            sendText.put("lat",currentLatitude);
            sendText.put("long",currentLongitude);
            sendText.put("btype",btype);
            sendText.put("dt",datetime);
            ApplicationController.getInstance().emit("UpdateCustomer", sendText);
            Log.i("TestMasterDTl","requestLiveDTL "+sendText);
        }catch(JSONException e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v)
    {

        switch (v.getId())
        {
            case R.id.imageframelayout:
                openAutocompleteActivity();
                break;

        }

    }

    private void openAutocompleteActivity()
    {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {

            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {

            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);


            Log.e(TAG, message);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == -1) {

                final Place place = PlaceAutocomplete.getPlace(this, data);

                youraddress.setText(place.getAddress());
                Log.i(TAG,"addressin-1 "+youraddress.getText());
                double lat = place.getLatLng().latitude;
                double lng = place.getLatLng().longitude;
                currentLatitude = lat;
                currentLongitude = lng;
                manager.setJOBLATI(String.valueOf(lat));
                manager.setJOBLONGI(String.valueOf(lng));
                isAdapter_added = false;

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Log.e(TAG, "Error: Status = " + status.toString());
            }
        }

    }

    private void socketListent()
    {
        ApplicationController.initInstance(new SockEmitter(Later_Booking.this) {
            @Override
            public void execute(String socket_event, JSONObject jsonObject) throws JSONException
            {
                if(socket_event.equals("UpdateCustomer"))
                {
                    Log.i("TestMaster","responcema "+jsonObject.toString());

                    if(Variableconstant.comingfromlaterbooking)
                    {
                        manager.setSocketRes(jsonObject.toString());
                    }
                    pubNubPojo = gson.fromJson(jsonObject.toString(), PubNub_pojo.class);
                    types = pubNubPojo.getMsg().getTypes();
                    Log.i(TAG,"TYPESIZE "+types.size());
                    if(types.size()>0)
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                rlempty.setVisibility(View.GONE);
                                mainrrlout.setVisibility(View.VISIBLE);
                            }
                        });
                        if(initial_Adapter_size==0)
                        {
                            initial_Adapter_size=types.size();
                        }

                        if(initial_Adapter_size!=types.size())
                        {
                            isAdapter_added=false;
                        }

                        if(!isAdapter_added)
                        {
                            setadatper();

                        }else
                        {
                            Pager_data_updater updater=listerner_list.get(data_visible_point);
                            if(updater!=null)
                            {
                                updater.onUpdated(jsonObject);
                            }
                        }
                    }
                    else
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                rlempty.setVisibility(View.VISIBLE);
                                mainrrlout.setVisibility(View.GONE);
                            }
                        });

                    }

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Variableconstant.backpressforlist = true;
        dontsendreq = true;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        myTimer_publish.cancel();
        myTimer_publish.purge();
    }
}
