package com.iserve.passenger;

//import com.github.nkzawa.socketio.client.Socket;

import io.socket.client.Socket;

/**
 * Created by Yash on 4/4/16.
 */
public enum SocketEvents
{



    getGetMessagesAck("GetMessagesAck"),
    getUpdateCustomer("UpdateCustomer"),
    getMessage("Message"),
    getEvent_Connect(Socket.EVENT_CONNECT),
    getEvent_Disconnect(Socket.EVENT_DISCONNECT),
    getHeartbeat("CustomerStatus");

    public String value;


    SocketEvents(String value)
    {
        this.value = value;
    }
}
