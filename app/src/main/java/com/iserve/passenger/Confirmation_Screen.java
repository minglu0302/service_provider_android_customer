package com.iserve.passenger;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.adapter.SpinerAdaptor;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;
//import com.logger.ArrayListClass;
import com.pojo.AllServiceDtl;
import com.pojo.Cancellation_java;
import com.pojo.DataService;
import com.pojo.GetCard_pojo;
import com.pojo.LiveBookingPojo;
import com.pojo.PromoCodepojo;
import com.pojo.PubNubType;
import com.pojo.PubNub_pojo;
import com.pojo.ServiceGroup;
import com.pojo.ServiceJson;
import com.pojo.Validator_Pojo;
import com.squareup.picasso.Picasso;
import com.utility.Alerts;
import com.utility.AppPermissionsRunTime;
import com.utility.MyFirebaseInstanceIDService;
import com.utility.OkHttp3Request;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import com.utility.WaveDrawable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import eu.janmuller.android.simplecropimage.CropImage;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.FormBody;

import static android.os.Build.VERSION_CODES.N;

/**
 * Created by embed on 8/4/16.
 *
 */
public class Confirmation_Screen extends AppCompatActivity implements View.OnClickListener
{
    Toolbar toolbar;
    ImageView cancel_booking,iv_payment;
    RelativeLayout relativminifare,rrloutbooking,rrwaveout,choose_payment_screen,relativdisfare,relativeo_ut,rlmainconfirm,rlmaincontotal,relativservfare;
    LinearLayout llout,MainContainerservice;
    Button payment_cancel,payment_card,payment_cash;//paymentbuton,appbutton
    EditText calc_txt_Prise,jobdtltxt;//notesedtxt;
    Spinner spinerhourlyquant;
    TextView booktv,apptaddress,textconfirm,card_no_Tv,paymentbuton,appbutton,apptcngaddtxv,tvhourlyfee,tvhourlymount,tvtotlamount,
            tvminimumfeeamount,addaddgroup,tvdisfare,tvdisamount,tvgrouptotl,grouptotl,tvserviceamount;//tvjobservices,
    CardView cardaddgroup;
    private String landmark = "";
    SessionManager manager;
    Gson gson;
    ProgressDialog pDialog;
    ArrayList<String> mImageList;
    ArrayList<String> jobIds = new ArrayList<>();
    ArrayList<ServiceJson>rowservice = new ArrayList<>();
    JSONArray jsonArray;
    private PubNub_pojo pubNubPojo;
    LiveBookingPojo liveBookingPojo;
    PromoCodepojo promoresponce;
    AllServiceDtl allservicedtlpojo;
    private Socket socket;
    private  ArrayList<String> dispatchedQueue = new ArrayList<>();
    UploadAmazonS3 upload;
    String TAG = "Confirmation_Screen";
    private final int REQUEST_CODE_GALLERY      = 0x3;
    private final int REQUEST_CODE_TAKE_PICTURE = 0x4;
    private final int REQUEST_CODE_CROP_IMAGE   = 0x5;
    public static File mFileTemp;
    private File newFile,newRenamefile,jobpicDirectry;
    private Uri newProfileImageUri;
    private File[] jobPicsDirectory;
    private  File[] jobPicFiles;
    private ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList;
    private final int REQUEST_CODE_PERMISSION_MULTIPLE = 125;
    AlertDialog dialogg;
    Timer timer;
    Timer myTimer_publish;
    private String typeId;
    private CountDownTimer countDownTimer;
    private int counter=0;
    private int proIndex=0;
    public static int groupcounter ;
    private String proID="";
    private boolean isProListSame=false;
    private boolean pIctureTaken = false;
    private boolean timerpaused = false;
    private boolean dontallowforback = true;
    WaveDrawable waveDrawable;
    LinearInterpolator  interpolator;
    int count = 0;
    ImageView glv = null;
    ImageView glvdlt = null;
    private LayoutInflater inflater;
    private String state,bid="";
    String payment_type = 1+"";
    static  String cardId = "";
    String SelectedCard="",selectedpromotype="";
    Alerts alerts;
    private String takenNewImage="";
    boolean isComingFromoncreat = false;
    boolean isBookingDispatching=false;
    double total;
    double hourlyfee = 0.0;
    double promodiscount = 0.0;
    double discountpercent = 0.0;
    String currencySymbol;
    double servicetotal = 0.0;
    AlertDialog dialogcal;
    Typeface regularfont,lightbold;
    private final int addressrequestcode = 0x17;
    private  ArrayList<String> providerqueue = new ArrayList<>();
    public  ArrayList<String> providerqueueBackUp= new ArrayList<>();
    private int gotResponce = 1;
    boolean islastElement=false;
    private boolean activityIsRunin;
    public Socket socketForBooking,socketForGettingAll;
    {
        try {
            socket = IO.socket(Variableconstant.SOCKET_PATH);
        } catch (URISyntaxException e) {
         //   Log.d("errormsg123", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        activityIsRunin = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirmationscreenxml);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        timer = new Timer();
        manager = new SessionManager(this);
        gson = new Gson();
        jsonArray = new JSONArray();
        isComingFromoncreat = true;
        groupcounter = 0;
        Variableconstant.Service_group = false;
        typeId= getIntent().getStringExtra("SELECTED_ID");
        socket.connect();
        socket.on("CustomerStatus", handleIncomingMessages);
        socket.on("UpdateCustomer", handleIncomingCustomer);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        mImageList=new ArrayList<>();
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis()+""+ Variableconstant.TEMP_PHOTO_FILE_NAME);
        }
        else
        {
            mFileTemp = new File(getFilesDir(),System.currentTimeMillis()+""+ Variableconstant.TEMP_PHOTO_FILE_NAME);
        }
        initialize();
    }

    private void initialize()
    {
        currencySymbol = getResources().getString(R.string.currencySymbol);
        regularfont = Typeface.createFromAsset(this.getAssets(), Variableconstant.regularfont);
        lightbold = Typeface.createFromAsset(this.getAssets(), Variableconstant.SemiBoldFont);
        MainContainerservice = (LinearLayout) findViewById(R.id.MainContainerservice);
        addaddgroup = (TextView) findViewById(R.id.addaddgroup);
        cardaddgroup = (CardView) findViewById(R.id.cardaddgroup);
        relativminifare = (RelativeLayout) findViewById(R.id.relativminifare);
        relativdisfare = (RelativeLayout) findViewById(R.id.relativdisfare);
        relativeo_ut = (RelativeLayout) findViewById(R.id.relativeo_ut);
        spinerhourlyquant = (Spinner) findViewById(R.id.spinerhourlyquant);
        tvhourlyfee = (TextView) findViewById(R.id.tvhourlyfee);
        tvhourlymount = (TextView) findViewById(R.id.tvhourlymount);
        tvtotlamount = (TextView) findViewById(R.id.tvtotlamount);
        tvminimumfeeamount = (TextView) findViewById(R.id.tvminimumfeeamount);
        tvdisamount = (TextView) findViewById(R.id.tvdisamount);
        tvdisfare = (TextView) findViewById(R.id.tvdisfare);
        grouptotl = (TextView) findViewById(R.id.grouptotl);
        tvgrouptotl = (TextView) findViewById(R.id.tvgrouptotl);
        tvserviceamount = (TextView) findViewById(R.id.tvserviceamount);
        relativservfare = (RelativeLayout) findViewById(R.id.relativservfare);
        rlmaincontotal = (RelativeLayout) findViewById(R.id.rlmaincontotal);
        rlmainconfirm = (RelativeLayout) findViewById(R.id.rlmainconfirm);
        relativdisfare.setVisibility(View.GONE);
        pDialog= new ProgressDialog(this);
        pDialog.setMessage(getResources().getString(R.string.Loading));
        pDialog.setCancelable(false);
        alerts = new Alerts(Confirmation_Screen.this);
        upload =UploadAmazonS3.getInstance(Confirmation_Screen.this, Variableconstant.Amazoncognitoid);
        waveDrawable = new WaveDrawable(ContextCompat.getColor(this, R.color.whitee), 450);
        iv_payment = (ImageView) findViewById(R.id.iv_payment);
        card_no_Tv = (TextView) findViewById(R.id.card_no_Tv);
        paymentbuton = (TextView) findViewById(R.id.paymentbuton);
        choose_payment_screen = (RelativeLayout) findViewById(R.id.choose_payment_screen);
        payment_cancel = (Button) findViewById(R.id.payment_cancel);
        payment_card = (Button) findViewById(R.id.payment_card);
        payment_cash = (Button) findViewById(R.id.payment_cash);
        rrwaveout = (RelativeLayout) findViewById(R.id.rrwaveout);
        cancel_booking = (ImageView) findViewById(R.id.cancel_booking);
        appbutton = (TextView) findViewById(R.id.appbutton);
        calc_txt_Prise = (EditText) findViewById(R.id.calc_txt_Prise);
        booktv = (TextView) findViewById(R.id.booktv);
        apptaddress = (TextView) findViewById(R.id.apptaddress);
        apptcngaddtxv = (TextView) findViewById(R.id.apptcngaddtxv);
        jobdtltxt = (EditText) findViewById(R.id.jobdtltxt);
        rrloutbooking= (RelativeLayout) findViewById(R.id.rrloutbooking);
        llout = (LinearLayout) findViewById(R.id.llout);
        inflater = LayoutInflater.from(this);
        addNewImageView();
        jobIds.clear();
        manager.setJobIds(jobIds);
        apptcngaddtxv.setOnClickListener(this);
        booktv.setOnClickListener(this);
        appbutton.setOnClickListener(this);
        paymentbuton.setOnClickListener(this);
        payment_cancel.setOnClickListener(this);
        payment_card.setOnClickListener(this);
        payment_cash.setOnClickListener(this);
        addaddgroup.setOnClickListener(this);
        toolbar = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        textconfirm = (TextView) findViewById(R.id.confirmtoolbar);
        textconfirm.setText(getResources().getString(R.string.confirmation));
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        apptaddress.setText(manager.getSavedAddress());
        calc_txt_Prise.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(calc_txt_Prise.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    Backgrounddoctorcheckpromocode(calc_txt_Prise.getText().toString());

                    return true;
                }
                return false;
            }
        });
        if(payment_type.equals("1"))
        {
            card_no_Tv.setText(getResources().getString(R.string.cash));
        }
        PubNub_pojo pubNubPoj = gson.fromJson(manager.getPUNUB_RES(),PubNub_pojo.class);
        String ftype;

        for(int i =0; i<pubNubPoj.getMsg().getTypes().size(); i++)
        {
            if(manager.getServiceProviderNAme().equals(pubNubPoj.getMsg().getTypes().get(i).getFtype())
                    && typeId.equals(pubNubPoj.getMsg().getTypes().get(i).getType_id()))
            {
                ftype = pubNubPoj.getMsg().getTypes().get(i).getFtype();
                dynamicViewtype(ftype,pubNubPoj.getMsg().getTypes().get(i));
            }
        }
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if(ConnectionResult.SUCCESS != resultCode) {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }

            //If play service is available
        } else {
            //Starting intent to register device

            Intent intent = new Intent(this, MyFirebaseInstanceIDService.class);
            startService(intent);

        }
        /*End of googleplay service*/

        if(Variableconstant.now_servicegroup.equals("1"))
        {
            cardaddgroup.setVisibility(View.VISIBLE);
            if(Utility.isNetworkAvailable(this))
            {
                pDialog.show();
                callservicedtl();
            }
            else
            {
                alerts.showNetworkAlert(this);
            }
        }
        else
        {
            cardaddgroup.setVisibility(View.GONE);
        }
        typeface();
    }

    private void getDefaultCard()
    {
        if(pDialog!=null)
        {
            pDialog.show();
        }
        Utility.printLog(" in side get card service ");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ent_sess_token", manager.getSession());
            jsonObject.put("ent_dev_id",manager.getDevice_Id());
            jsonObject.put("ent_cust_id", manager.getCustomerId());
            jsonObject.put("ent_date_time", Utility.dateintwtfour());
            Log.i("TAG ","THEDATAIS "+jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utility.doJsonRequest(Variableconstant.GETCARD, jsonObject, new Utility.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("The Response get card " + result);
                defaltCardServiceResponse(result);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(Confirmation_Screen.this, getResources().getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
                if (pDialog != null) {
                    pDialog.cancel();
                    pDialog.dismiss();
                }
            }
        });
    }

    private void defaltCardServiceResponse(String result)
    {
        try {
            // Gson gson = new Gson();
            GetCard_pojo response = gson.fromJson(result, GetCard_pojo.class);
            if (response.getErrNum().equals("52") && response.getErrFlag().equals("0")) {
                if(response.getCards().size()==1)
                {

                        if(response.getDef().equals(response.getCards().get(0).getId()))
                        {
                            Bitmap cardbitmap =Utility.setCreditCardLogo(response.getCards().get(0).getType(),Confirmation_Screen.this);

                            String cardNo = response.getCards().get(0).getLast4();
                            cardId = response.getCards().get(0).getId();

                            String card = "**** **** **** " + " " + cardNo;
                            card_no_Tv.setText(card);
                            SelectedCard=cardId;
                            payment_type = "2";
                            iv_payment.setImageBitmap(cardbitmap);

                        }
                }
                else
                {
                    choose_payment_screen.setVisibility(View.GONE);
                    Intent cardsIntent = new Intent(Confirmation_Screen.this, ChangeCardActivity.class);
                    startActivityForResult(cardsIntent, 1);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
                    getSupportActionBar().show();
                }

            } else {
                Toast.makeText(Confirmation_Screen.this, response.getErrMsg(), Toast.LENGTH_LONG).show();

            }
            if (pDialog != null) {
                pDialog.cancel();
                pDialog.dismiss();
            }
        }
        catch (Exception e) {
            if (pDialog != null) {
                pDialog.cancel();
                pDialog.dismiss();
            }
            e.printStackTrace();
            Utility.printLog("" + e);
        }
    }
    private void typeface()
    {
        TextView apptaddtxv  = (TextView) findViewById(R.id.apptaddtxv);
       TextView tvaddaddgroup = (TextView) findViewById(R.id.tvaddaddgroup);
       TextView promocodetv = (TextView) findViewById(R.id.promocodetv);
       TextView chosepaymnt = (TextView) findViewById(R.id.chosepaymnt);
       TextView jobdetails = (TextView) findViewById(R.id.jobdetails);
       TextView addphoto = (TextView) findViewById(R.id.addphoto);
       TextView tvfeestimate = (TextView) findViewById(R.id.tvfeestimate);
       TextView tvminimumfee = (TextView) findViewById(R.id.tvminimumfee);
       TextView tvtotlfare = (TextView) findViewById(R.id.tvtotlfare);
        apptaddress.setTypeface(regularfont);
        apptcngaddtxv.setTypeface(regularfont);
        addaddgroup.setTypeface(regularfont);
        tvaddaddgroup.setTypeface(regularfont);
        apptaddtxv.setTypeface(regularfont);
        promocodetv.setTypeface(regularfont);
        calc_txt_Prise.setTypeface(regularfont);
        appbutton.setTypeface(regularfont);
        chosepaymnt.setTypeface(regularfont);
        card_no_Tv.setTypeface(regularfont);
        paymentbuton.setTypeface(regularfont);
        jobdetails.setTypeface(regularfont);
        jobdtltxt.setTypeface(regularfont);
        addphoto.setTypeface(regularfont);
        tvfeestimate.setTypeface(regularfont);
        tvminimumfee.setTypeface(regularfont);
        tvminimumfeeamount.setTypeface(regularfont);
        tvhourlyfee.setTypeface(regularfont);
        tvhourlymount.setTypeface(regularfont);
        tvdisfare.setTypeface(regularfont);
        tvdisamount.setTypeface(regularfont);
        tvtotlfare.setTypeface(lightbold);
        tvtotlamount.setTypeface(lightbold);
        booktv.setTypeface(regularfont);
        payment_cancel.setTypeface(regularfont);
        payment_card.setTypeface(regularfont);
        payment_cash.setTypeface(regularfont);
        textconfirm.setTypeface(regularfont);
        cancel_booking.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v)
            {
                if(bid!=null && !bid.equals(""))
                {
                    cancelapptservice("");
                }

                return false;
            }
        });
    }
    private void cancelapptservice(String reason) {
        if (Utility.isNetworkAvailable(this)) {
            pDialog.show();
            FormBody requestBody=new FormBody.Builder()
                    .add("ent_sess_token", manager.getSession())
                    .add("ent_dev_id", manager.getDevice_Id())
                    .add("ent_bid", bid)
                    .add("ent_reason", reason)
                    .add("ent_date_time", Utility.dateintwtfour())
                    .build();
            okio.Buffer sink = new okio.Buffer();
            try {
                requestBody.writeTo(sink);
                byte[] barry = sink.readByteArray();
                String req = new String(barry);
            } catch (Exception e) {
                e.printStackTrace();
            }
            OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "cancelAppointment", requestBody, new OkHttp3Request.JsonRequestCallback() {
                @Override
                public void onSuccess(String result) {
                    pDialog.dismiss();
                    cancelResponce(result);
                }
                @Override
                public void onError(String error) {
                    pDialog.dismiss();
                }
            });

        } else {

            Toast.makeText(this,getResources().getString(R.string.nointernet),Toast.LENGTH_SHORT).show();
        }
    }
    private void cancelResponce(String result) {

        Cancellation_java cancellationJava = gson.fromJson(result, Cancellation_java.class);

        if (cancellationJava.getErrFlag().equals("0"))
        {
            rrloutbooking.setVisibility(View.GONE);
            waveDrawable.stopAnimation();
            getSupportActionBar().show();
            counter=0;
            dispatchedQueue.clear();
            countDownTimer.cancel();
            Variableconstant.ent_btype = 3;
            makefree(proID);
            rlmainconfirm.setVisibility(View.VISIBLE);
            booktv.setVisibility(View.VISIBLE);
            if(activityIsRunin)
            {
                callalertDialog("Booking cancelled");
            }

        }
    }
    private void callalertDialog(String message)
    {
        countDownTimer.cancel();
        timerpaused = true;
         dialogcal = new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(""+message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {

                        Variableconstant.ent_btype = 3;
                        dialog.dismiss();
                        onBackPressed();
                    }
                })
                .setIcon(R.drawable.ic_launcher)
                .show();
    }

    private void callservicedtl()
    {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("ent_sess_token", manager.getSession());
            jsonObject.put("ent_dev_id", manager.getDevice_Id());
            jsonObject.put("ent_catid",typeId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utility.doJsonRequest(Variableconstant.service_url + "getAllServices", jsonObject, new Utility.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                pDialog.dismiss();
                allservicedtlpojo = gson.fromJson(result,AllServiceDtl.class);
                if(allservicedtlpojo.getErrFlag().equals("0") && allservicedtlpojo.getErrNum().equals("21"))
                {
                    gettingroup();
                    if(allservicedtlpojo.getData().size()>0)
                    {
                        for(int i = 0;i<allservicedtlpojo.getData().size();i++)
                        {
                            if(allservicedtlpojo.getData().get(i).getCmand().equals("1"))
                            {
                                groupcounter++;
                            }
                        }
                    }
                }else if(("1").equals(allservicedtlpojo.getErrFlag()) && (("83").equals(allservicedtlpojo.getErrNum())||("7").equals(allservicedtlpojo.getErrNum()) ))
                {
                    Toast.makeText(Confirmation_Screen.this, allservicedtlpojo.getErrMsg(),Toast.LENGTH_SHORT).show();
                    Utility.setMAnagerWithBID(Confirmation_Screen.this,manager);

                }
                else

                {
                    Toast.makeText(Confirmation_Screen.this,allservicedtlpojo.getErrMsg(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String error) {
                pDialog.dismiss();
            }
        });


    }

    private void addNewImageView()
    {

        glv = null;
        glvdlt = null;
        if(count < 4)
        {
            final View inflaterv = inflater.inflate(R.layout.testimageclasssingle, null, false);
            final ImageView imageview = (ImageView) inflaterv.findViewById(R.id.imageview);
            final ImageView ivdelete = (ImageView) inflaterv.findViewById(R.id.ivdelete);

            glv = imageview;
            imageview.setTag(glv);
            glvdlt = ivdelete;
            imageview.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        myPermissionConstantsArrayList = new ArrayList<>();
                        myPermissionConstantsArrayList.clear();
                        myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
                        myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
                        myPermissionConstantsArrayList.add(AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);

                        if (AppPermissionsRunTime.checkPermission(Confirmation_Screen.this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE)) {
                            //TODO: if all permissions are already granted

                            callDialogue();

                        }
                    } else {
                        callDialogue();
                    }
                }
            });

            ivdelete.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    llout.removeView(inflaterv);
                    count--;
                    if(count==3)
                    {
                        addNewImageView();
                    }

                }
            });
            llout.addView(inflaterv);
        }
    }


    private void dynamicViewtype(String ftype, final PubNubType pubNubType)
    {

        switch (ftype)
        {
            case  "Hourly":
                ArrayList<String> range = new ArrayList<>();
                SpinerAdaptor spinerAdaptor= new SpinerAdaptor(this,range);
                /*total = Double.parseDouble(pubNubType.getPrice_min())*60;
                hourlyfee = Double.parseDouble(pubNubType.getPrice_min())*60;*/
                total = Double.parseDouble(pubNubType.getPrice_min());
                hourlyfee = Double.parseDouble(pubNubType.getPrice_min());
                spinerhourlyquant.setAdapter(spinerAdaptor);
                spinerhourlyquant.setSelection(0, false);
                relativeo_ut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        spinerhourlyquant.performClick();
                    }
                });

                spinerhourlyquant.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                    {

                            spinerhourlyquant.setSelection(position);
                           // Log.d(TAG,"SelectedPositon "+position);
                            total = Double.parseDouble(pubNubType.getPrice_min())*(position+1);
                            // tvhourlymount.setText("₹ " + form.format(total));
                            String hourlyamt = currencySymbol+" "+total;
                            tvhourlymount.setText(hourlyamt);
                            switch (selectedpromotype) {
                                case "Percent":
                                    if (promodiscount == 0.0) {
                                        double fee = Double.parseDouble(pubNubType.getVisit_fees()) + total+servicetotal;
                                        String totalfee = currencySymbol + " " + fee;
                                        tvtotlamount.setText(totalfee);
                                    } else {
                                        double fee = Double.parseDouble(pubNubType.getVisit_fees()) + total+servicetotal;

                                        double discountis = (discountpercent / 100) * fee;
                                        promodiscount = discountis;
                                        double totalafterdiscount = fee  - (discountis);
                                        String totalfee = currencySymbol + " " + totalafterdiscount;
                                        tvtotlamount.setText(totalfee);
                                        @SuppressLint("DefaultLocale") String stringdiscount = String.format("%.2f", discountis);
                                        tvdisamount.setText(currencySymbol + " " + stringdiscount);
                                    }
                                    break;
                                case "Fixed":
                                    if (promodiscount == 0.0) {
                                        double fee = Double.parseDouble(pubNubType.getVisit_fees()) + total+servicetotal;
                                        String totalfee = currencySymbol + " " + fee;
                                        tvtotlamount.setText(totalfee);
                                    } else {
                                        double fee = Double.parseDouble(pubNubType.getVisit_fees()) + total+servicetotal;

                                        double totalafterdiscount = fee - (promodiscount);
                                        String totalfee = currencySymbol + " " + totalafterdiscount;
                                        tvtotlamount.setText(totalfee);
                                        @SuppressLint("DefaultLocale") String stringdiscount = String.format("%.2f", promodiscount);
                                        tvdisamount.setText(currencySymbol + " " + stringdiscount);
                                    }
                                    break;
                                default:
                                    double fee = Double.parseDouble(pubNubType.getVisit_fees()) + total+servicetotal;
                                    String totalfee = currencySymbol + " " + fee;
                                    tvtotlamount.setText(totalfee);
                                    break;
                            }




                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                String minimumfeeamt = currencySymbol+" "+pubNubType.getVisit_fees();

                tvminimumfeeamount.setText(minimumfeeamt);
                String fullstring = getResources().getString(R.string.hourly)+" @ "+currencySymbol+" "+total;
                tvhourlyfee.setText(fullstring);
                String hourlyamt = currencySymbol+" "+total;
                tvhourlymount.setText(hourlyamt);

                if(promodiscount!=0.0)
                {
                    double fee = Double.parseDouble(pubNubType.getVisit_fees())+total+servicetotal-promodiscount;
                    String totalfee = currencySymbol+" "+fee;
                    tvtotlamount.setText(totalfee);
                }
                else
                {
                    double fee = Double.parseDouble(pubNubType.getVisit_fees())+total+servicetotal;
                    String totalfee = currencySymbol+" "+fee;
                    tvtotlamount.setText(totalfee);
                }



                break;
            case "Fixed":
        }
    }


    @Override
    public void onClick(View v)
    {

        switch (v.getId())
        {


            case R.id.addaddgroup:
                viewServiceDtl();
                break;
            case R.id.paymentbuton:
                choose_payment_screen.setVisibility(View.VISIBLE);
                getSupportActionBar().hide();
                break;
            case R.id.payment_cash:
                choose_payment_screen.setVisibility(View.GONE);
                getSupportActionBar().show();
                payment_type = "1";
                card_no_Tv.setText(getResources().getString(R.string.cash));
                iv_payment.setImageResource(R.drawable.confirmation_cash_icon);
                break;
            case R.id.payment_cancel:
                choose_payment_screen.setVisibility(View.GONE);
                getSupportActionBar().show();
                break;
            case R.id.payment_card:
                getDefaultCard();
                choose_payment_screen.setVisibility(View.GONE);
                getSupportActionBar().show();

                break;

            case R.id.appbutton:
                if(appbutton.getText().toString().equals(getResources().getString(R.string.apply)))
                {
                    if (Utility.isNetworkAvailable(this)) {
                        if (!calc_txt_Prise.getText().toString().trim().equals("")) {

                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(calc_txt_Prise.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

                            Backgrounddoctorcheckpromocode(calc_txt_Prise.getText().toString());
                        } else {
                            Utility.ShowAlert("Please Enter the Promo Code", this);
                        }
                    } else {
                        alerts.showNetworkAlert(this);
                    }
                }
                else if(appbutton.getText().toString().equals(getResources().getString(R.string.clearpromocode)))
                {
                    appbutton.setText(getResources().getString(R.string.apply));
                    String splittxt[] = tvtotlamount.getText().toString().split(" ");
                    relativdisfare.setVisibility(View.GONE);
                    double amount = Double.parseDouble(splittxt[1])+promodiscount;
                    tvtotlamount.setText(currencySymbol+ " "+amount);
                    promodiscount = 0.0;
                    calc_txt_Prise.setText("");
                    selectedpromotype = "";
                }
                break;

            case R.id.booktv:


                if(Utility.isNetworkAvailable(this))
                {

                    if(groupcounter>0)
                    {
                        int temp = 0;
                        for (DataService dataService: allservicedtlpojo.getData())
                        {
                            boolean isManSelctd = false;
                            if("1".equals(dataService.getCmand()))
                            {
                                for (ServiceGroup serviceGroup: dataService.getServices())
                                {
                                    boolean isIdFound = false;
                                    for(String currentItemId :  jobIds)
                                    {
                                        if(currentItemId.equals(serviceGroup.getSid()))
                                        {
                                            isIdFound = true;
                                            break;
                                        }
                                    }

                                    if(isIdFound)
                                    {
                                        isManSelctd = true;
                                        break;
                                    }
                                }

                                if(isManSelctd)
                                {
                                    temp++;
                                }
                                else
                                {
                                    //TODO: show ur toast
                                    Toast.makeText(this,"Please select mandatory services",Toast.LENGTH_SHORT).show();
                                    break;
                                }
                            }
                        }

                        if(groupcounter == temp)
                        {
                            if(providerqueue.size()>0)
                            {
                                liveBooking();
                            }
                            else
                            {
                                Toast.makeText(this,getResources().getString(R.string.noprovideravailable),Toast.LENGTH_SHORT).show();
                            }


                        }
                    }
                    else
                    {
                        if(providerqueue.size()>0)
                        {
                            liveBooking();
                        }
                        else
                        {
                            Toast.makeText(this,getResources().getString(R.string.noprovideravailable),Toast.LENGTH_SHORT).show();
                        }
                    }

                }

                break;

            case R.id.apptcngaddtxv:
                Intent intnt = new Intent(this,Address_Confirm.class);
                startActivityForResult(intnt,addressrequestcode);
                break;

        }

    }

    private void viewServiceDtl() {
        if(allservicedtlpojo!=null && allservicedtlpojo.getData().size()>0)
        {
            Intent intent = new Intent(this,ServiceGroupDetials.class);
            intent.putExtra("DATA",allservicedtlpojo.getData());
            startActivity(intent);
        }
    }

    private void Backgrounddoctorcheckpromocode(String s)
    {
        pDialog.setMessage(getResources().getString(R.string.checkingpromocode));
        pDialog.show();
        FormBody requestBody=new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_coupon", s)
                .add("ent_lat", manager.getJOBLATI())
                .add("ent_long", manager.getJOBLONGI())
                .add("ent_date_time", Utility.dateintwtfour())
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.CHECKCOUPON, requestBody, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {

                pDialog.dismiss();

                try {
                    Gson gson = new Gson();
                    promoresponce = gson.fromJson(result, PromoCodepojo.class);

                    if(promoresponce.getErrFlag().equals("0"))
                    {
                        Utility.printLog(" add card response " + result);
                        appbutton.setText(getResources().getString(R.string.clearpromocode));
                        String splittxt[] = tvtotlamount.getText().toString().split(" ");
                        switch (promoresponce.getDtype()) {
                            case "Percent":
                                relativdisfare.setVisibility(View.VISIBLE);
                                selectedpromotype = "Percent";
                                double discountamtinper = Double.parseDouble(promoresponce.getDis());
                                discountpercent = discountamtinper;
                                double discountis = (discountamtinper / 100) * Double.parseDouble(splittxt[1]);
                                promodiscount = discountis;
                                double discount = Double.parseDouble(splittxt[1]) - (discountis);
                                tvtotlamount.setText(currencySymbol + " " + discount);
                                @SuppressLint("DefaultLocale") String stringdiscount = String.format("%.2f", discountis);
                                tvdisamount.setText(currencySymbol + " " + stringdiscount);
                                break;
                            case "Fixed":
                                relativdisfare.setVisibility(View.VISIBLE);
                                selectedpromotype = "Fixed";
                                promodiscount = Double.parseDouble(promoresponce.getDis());
                                double discountfied = Double.parseDouble(splittxt[1]) - (promodiscount);
                                tvtotlamount.setText(currencySymbol + " " + discountfied);
                                @SuppressLint("DefaultLocale") String stringdiscountamt = String.format("%.2f", promodiscount);
                                tvdisamount.setText(currencySymbol + " " + stringdiscountamt);

                                break;
                            default:
                                payment_type = "1";
                                break;
                        }
                        alerts.promoAlert(Confirmation_Screen.this, promoresponce.getErrMsg());

                    }
                    else if(("1").equals(promoresponce.getErrFlag()) && (("83").equals(promoresponce.getErrNum())||("7").equals(promoresponce.getErrNum()) ))
                    {
                        Toast.makeText(Confirmation_Screen.this, promoresponce.getErrMsg(),Toast.LENGTH_SHORT).show();
                        Utility.setMAnagerWithBID(Confirmation_Screen.this,manager);

                    }
                    else
                    {
                        Toast.makeText(Confirmation_Screen.this,promoresponce.getErrMsg(),Toast.LENGTH_SHORT).show();
                    }
                    if (pDialog != null) {
                        pDialog.cancel();
                        pDialog.dismiss();
                    }
                }
                catch (Exception e) {
                    if (pDialog != null) {
                        pDialog.cancel();
                        pDialog.dismiss();
                    }
                    e.printStackTrace();
                    Utility.printLog("" + e);
                }
            }

            @Override
            public void onError(String error) {

                Utility.printLog("error in add card " + error);
                pDialog.dismiss();
            }
        });

    }


    private void liveBooking()
    {
        if(pDialog!=null)
        {
            pDialog.setMessage(getResources().getString(R.string.booking));
            pDialog.show();
        }


        String jObPics;
        if(pIctureTaken)
        {
            jobPicFiles = jobpicDirectry.listFiles();
            jObPics = jobPicFiles.length+"";
        }
        else
        {
            jObPics = "";
        }

        String pricetopay = "";

        if(!tvtotlamount.getText().toString().trim().equals(""))
        {
            String splittxt[] = tvtotlamount.getText().toString().split(" ");
            pricetopay = splittxt[1];
        }
        else
        {
            pricetopay = "";
        }
        JSONObject jsonObj = new JSONObject();

          try {
                jsonObj.put("ent_sess_token", manager.getSession());
                jsonObj.put("ent_dev_id",manager.getDevice_Id());
                jsonObj.put("ent_custid",manager.getCustomerId());
                jsonObj.put("ent_btype", Variableconstant.ent_btype);
                jsonObj.put("ent_proid","");
                jsonObj.put("ent_slot_id","");
                jsonObj.put("ent_dtype",1);
                jsonObj.put("ent_a1", apptaddress.getText().toString());
                jsonObj.put("ent_a2",landmark+"");
                jsonObj.put("ent_lat",manager.getJOBLATI());
                jsonObj.put("ent_long",manager.getJOBLONGI());
                jsonObj.put("ent_cat_id",typeId);
                jsonObj.put("ent_card_id",cardId);
                jsonObj.put("ent_job_imgs",jObPics);
                jsonObj.put("ent_job_details",jobdtltxt.getText().toString());
                jsonObj.put("ent_pymt",payment_type);
                jsonObj.put("ent_coupon",calc_txt_Prise.getText().toString().trim());
                jsonObj.put("ent_hourly_amt",hourlyfee);
                jsonObj.put("ent_services",jsonArray);
                jsonObj.put("ent_cat_name",manager.getServiceProvdr());
                jsonObj.put("ent_total",pricetopay);
                jsonObj.put("ent_date_time", Utility.dateintwtfour());

             // Log.d(TAG,"CONFIRMLIVErequest "+jsonObj);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Utility.doJsonRequest(Variableconstant.LIVEBOOKING, jsonObj, new Utility.JsonRequestCallback() {
                @Override
                public void onSuccess(String result)
                {
                    if(pDialog!=null)
                    {
                        pDialog.dismiss();
                    }
                    calllivebookingResponse(result);
                    Log.d(TAG,"CONFIRMLIVEresponce "+result);
                }
                @Override
                public void onError(String error)
                {
                    if(pDialog!=null)
                    {
                        pDialog.dismiss();
                    }
                }
            });
    }
    private void calllivebookingResponse(String result)
    {
        if(result!=null)
        {
            liveBookingPojo = gson.fromJson(result, LiveBookingPojo.class);
            if(liveBookingPojo!=null) {
                if (liveBookingPojo.getErrFlag().equals("0")) {

                    rrloutbooking.setVisibility(View.VISIBLE);

                    getSupportActionBar().hide();

                    /*
                     * Checking the android version to avoid the deprecation problem..
                     * <h1>Note :</h1>
                     * {@code setBackgroundDrawable()} this method is deprecated method after  android JELLY_BEAN version .
                     * to avoid the deprecation problem first checking the SDK version then applying the method according
                     * to the required SDK version method.*/
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        rrwaveout.setBackgroundDrawable(waveDrawable);
                    } else {
                        rrwaveout.setBackground(waveDrawable);
                    }

                    /*
                     * <p>Defining a LinearInterpolator animation object for doing animation.
                     * and passing that object to the Wave animator class.
                     * @see LinearInterpolator
                     * @see WaveDrawable
                     * </p>
                     */
                    interpolator = new LinearInterpolator();
                    waveDrawable.setWaveInterpolator(interpolator);

                    waveDrawable.startAnimation();

                    bid = liveBookingPojo.getBid();
                    dontallowforback = false;
                    rlmainconfirm.setVisibility(View.GONE);
                    booktv.setVisibility(View.GONE);

                    if (pIctureTaken) {

                        state = Environment.getExternalStorageState();


                        jobPicFiles = jobpicDirectry.listFiles();


                        boolean isavailable;

                        for (int j = 0; j < jobPicFiles.length; j++) {
                            if (Environment.MEDIA_MOUNTED.equals(state)) {


                                File f = new File(jobpicDirectry + "/" + jobPicFiles[j].getName());
                                newRenamefile = new File(jobpicDirectry, liveBookingPojo.getBid() + "_" + j + ".png");
                                isavailable = f.renameTo(newRenamefile);
                                mImageList.add(newRenamefile.getPath());

                                if (isavailable) {

                                    uploadToAmazon(newRenamefile);
                                }
                            } else {

                                newRenamefile = new File(jobpicDirectry, liveBookingPojo.getBid() + "_" + j + ".png");
                                isavailable = jobpicDirectry.renameTo(newRenamefile);
                                if (isavailable) {
                                    uploadToAmazon(newRenamefile);
                                }
                            }
                        }

                    }
                    countDownTimer = new CountDownTimer(150000, 1000) {

                        public void onTick(long millisUntilFinished)
                        {

                        //    Log.d("TAG", "timer seconds remaining: " + millisUntilFinished / 1000);

                            if(timerpaused)
                            {
                                cancel();
                            }
                            else
                            {
                                counter++;
                                if(!isBookingDispatching)
                                {

                                    for (String pid : providerqueue)
                                    {


                                        if(!dispatchedQueue.contains(pid))
                                        {

                                            counter=0;
                                            dispatchedQueue.add(pid);
                                            proID = pid;
                                            sendMessageForLiveBooking(liveBookingPojo.getBid(),pid);
                                            islastElement = false;
                                            break;
                                        }

                                        if(dispatchedQueue.containsAll(providerqueue))
                                        {
                                         //   Log.d(TAG," COUNTER "+dispatchedQueue.containsAll(providerqueue)+" "+counter);
                                            islastElement = true;
                                        }

                                    }

                                }

                                /*
                                 * to send the booking for a driver for 30 sec
                                 * if more than 30 sec then send to next driver
                                 */
                                if (counter == 30)
                                {

                                    makefree(proID);
                                    isBookingDispatching = false;


                                    if(islastElement)
                                    {
                                        islastElement = false;
                                        countDownTimer.onFinish();
                                    }
                                    counter = 0;

                                }
                            }

                        }
                        public void onFinish() {
                            rrloutbooking.setVisibility(View.GONE);
                            waveDrawable.stopAnimation();
                            if (getSupportActionBar() != null)
                            {
                                getSupportActionBar().show();
                            }
                            counter = 0;
                            dispatchedQueue.clear();
                            countDownTimer.cancel();
                            if(activityIsRunin)
                            {
                                callalertDialog("Currently all providers are busy");
                            }


                        }
                    }.start();


               // } else if (liveBookingPojo.getErrNum().equals("7") && liveBookingPojo.getErrFlag().equals("1")) {
                } else if (("1").equals(liveBookingPojo.getErrFlag()) && (("83").equals(liveBookingPojo.getErrNum())||("7").equals(liveBookingPojo.getErrNum()) )) {

                    Utility.setMAnagerWithBID(Confirmation_Screen.this,manager);

                } else {
                    dontallowforback = true;
                    Toast.makeText(Confirmation_Screen.this, liveBookingPojo.getErrMsg(), Toast.LENGTH_SHORT).show();
                }

            }
        }
    }


    private void makefree(String s)
    {
        JSONObject sendText = new JSONObject();


        try{
            sendText.put("proid", s);
          //  Log.d("makefree", " chanel " + sendText.toString() +"pro index "+proIndex);
            socket.emit("makefree", sendText);
        }catch(JSONException e)
        {
            e.printStackTrace();
        }
    }
    /*UploaDingTo Amazon*/

    private void uploadToAmazon(File image)
    {

        upload.Upload_data(Variableconstant.AmazonJobbucket, image, new UploadAmazonS3.Upload_CallBack() {
            @Override
            public void sucess(String sucess) {

            }

            @Override
            public void error(String errormsg) {

            }
        });
    }

    Emitter.Listener handleIncomingCustomer = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //try {
                        JSONObject data = (JSONObject) args[0];
                        pubNubPojo = gson.fromJson(data.toString(), PubNub_pojo.class);
                        if (pubNubPojo.getMsg().getMasArr().size() > 0) {
                            providerqueue.clear();
                            for (int i = 0; i < pubNubPojo.getMsg().getMasArr().size(); i++)
                            {
                                if (pubNubPojo.getMsg().getMasArr().get(i).getTid().equals(typeId)) {
                                    for (int j = 0; j < pubNubPojo.getMsg().getMasArr().get(i).getMas().size(); j++)
                                    {
                                        providerqueue.add(pubNubPojo.getMsg().getMasArr().get(i).getMas().get(j).getPid());
                                    }
                                    if(providerqueueBackUp.size()==providerqueue.size())
                                    {
                                        for(int count=0;count<providerqueue.size();count++)
                                        {
                                            if(providerqueue.get(count).equals(providerqueueBackUp.get(count)))
                                            {
                                                isProListSame=true;
                                            }
                                            else
                                            {
                                                isProListSame=false;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        isProListSame=false;
                                    }
                                    if(!isProListSame)
                                    {
                                        providerqueueBackUp.clear();
                                        proIndex=0;
                                        providerqueueBackUp.addAll(providerqueue);
                                    }
                                }
                            }
                        }
                   // }
                }
            });
        }
    };

    private void sendMessageForLiveBooking(String bid, String pro_id)
    {
        isBookingDispatching=true;
        gotResponce = 1;
        JSONObject sendText = new JSONObject();
        try{
            sendText.put("cid",manager.getCustomerId());
            sendText.put("bid",bid);
            sendText.put("proid", pro_id);
            sendText.put("btype", Variableconstant.ent_btype);
            sendText.put("dt", Utility.dateintwtfour());
          //  Log.d("LiveBooking", "ALIlivebookingrequest " + sendText.toString() +"pro index "+pro_id);
            socket.emit("LiveBooking", sendText);
        }catch(JSONException e)
        {
            e.printStackTrace();
        }
    }
    private Emitter.Listener  handleIncomingMessages = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            Confirmation_Screen.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                 //   Log.d("LiveBooking", "ALIlivebookingresponce " + args[0].toString());
                    JSONObject data = (JSONObject) args[0];
                    try {
                        String bStatus = data.getString("st");
                        sendmessageBookingAck(bStatus);

                        /*
                         * check for pro rejection and increment the pro iNdex
                         */
                        if (bStatus.equals("3"))
                        {
                            if(gotResponce==1)
                            {
                                gotResponce++;
                                isBookingDispatching=false;
                                makefree(proID);
                                counter=0;
                                if(islastElement)
                                {

                                    islastElement = false;
                                    countDownTimer.onFinish();
                                }

                            }

                        }
                        else if(bStatus.equals("2"))
                        {
                            isBookingDispatching=false;
                            dispatchedQueue.clear();
                            rrloutbooking.setVisibility(View.GONE);
                            waveDrawable.stopAnimation();
                            counter=0;
                            countDownTimer.cancel();
                            if(gotResponce==1)
                            {
                                gotResponce++;
                                callpopupscreen();
                            }

                        }

                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private void callpopupscreen()
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(Confirmation_Screen.this);
        builder.setTitle(getResources().getString(R.string.thanku));
        builder.setCancelable(false);
        String fname = manager.getCustomerFnme().substring(0, 1).toUpperCase() + manager.getCustomerFnme().substring(1);
        String message = getResources().getString(R.string.hey)+" \""+fname+"\" "+getResources().getString(R.string.yourjobrequest)
                +" \""+getResources().getString(R.string.provider)+"\" "+getResources().getString(R.string.hewillbeonthewayshortly);
        builder.setMessage(message);
        builder.setPositiveButton(Confirmation_Screen.this.getResources().getString(R.string.action_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which)
            {
                jobIds.clear();
                manager.setJobIds(jobIds);
                Variableconstant.comgfrmConfirmscrn = true;
                Intent intent = new Intent(Confirmation_Screen.this,MenuActivity.class);
                startActivity(intent);
                finish();
                dialog.dismiss();
            }
        });
        dialogg = builder.create();
        dialogg.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
        try {
            dialogg.show();
        } catch(Exception e){
           e.printStackTrace();
        }
    }
    private void sendmessageBookingAck(String bStatus)
    {
        JSONObject sendText = new JSONObject();
        try{
            sendText.put("bStatus",bStatus);

         //   Log.d("LiveBooking", "ALIrequestlivebookingAck " + sendText.toString() +"pro index "+proIndex);
            socket.emit("LiveBookingAck", sendText);
        }catch(JSONException e)
        {
            e.printStackTrace();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();

        emitHeartbeat("1");
        myTimer_publish = null;
        startPublishingWithTimer();
        if(Variableconstant.Service_group)
        {
            Variableconstant.Service_group = false;
            gettingroup();
        }
    }
    private void gettingroup()
    {
        jobIds = manager.getJobIds();
        if(allservicedtlpojo!=null)
        {
            MainContainerservice.removeAllViews();
            rowservice.clear();
            servicetotal = 0.0;

            for(DataService serviceDtl : allservicedtlpojo.getData())
            {
                for (ServiceGroup servcegrp : serviceDtl.getServices())
                {
                    if (jobIds != null && jobIds.contains(servcegrp.getSid().trim())) {
                        ServiceJson serviceJson;
                        TextView group_name, group_price;
                        LayoutInflater inflatervehicle = LayoutInflater.from(this);
                        View inflatedLayout = inflatervehicle.inflate(R.layout.addgroupsinglelayout, null, false);
                        group_name = (TextView) inflatedLayout.findViewById(R.id.group_name);
                        group_price = (TextView) inflatedLayout.findViewById(R.id.group_price);
                        group_name.setText(servcegrp.getSname());
                        String price = getResources().getString(R.string.currencySymbol) +" "+
                                servcegrp.getFixed_price();
                        group_price.setText(price);
                        serviceJson = new ServiceJson(servcegrp.getSname(),
                                servcegrp.getSid(),
                                servcegrp.getFixed_price());
                        MainContainerservice.addView(inflatedLayout);
                        rowservice.add(serviceJson);
                    }
                }
            }

           /* for (int i = 0; i < allservicedtlpojo.getData().size(); i++) {

                for (int j = 0; j < allservicedtlpojo.getData().get(i).getServices().size(); j++) {
                    if (jobIds != null && jobIds.contains(allservicedtlpojo.getData().get(i).getServices().get(j).getSid().trim())) {


                        ServiceGroup serviceGroup = allservicedtlpojo.getData().get(i).getServices().get(j);
                        ServiceJson serviceJson;
                        TextView group_name, group_price;
                        LayoutInflater inflatervehicle = LayoutInflater.from(this);
                        View inflatedLayout = inflatervehicle.inflate(R.layout.addgroupsinglelayout, null, false);
                        group_name = (TextView) inflatedLayout.findViewById(R.id.group_name);
                        group_price = (TextView) inflatedLayout.findViewById(R.id.group_price);
                        group_name.setText(serviceGroup.getSname());
                        String price = getResources().getString(R.string.currencySymbol) +" "+
                                serviceGroup.getFixed_price();
                        group_price.setText(price);
                        serviceJson = new ServiceJson(serviceGroup.getSname(),
                                serviceGroup.getSid(),
                                serviceGroup.getFixed_price());
                        MainContainerservice.addView(inflatedLayout);
                        rowservice.add(serviceJson);
                    }
                }
            }*/
        }

        if(rowservice.size()>0)
        {

            for(ServiceJson serviceJson : rowservice)
            {
                JSONObject jsonitem = new JSONObject();
                try {
                    jsonitem.put("sname",serviceJson.getSname());
                    jsonitem.put("sid",serviceJson.getSid());
                    jsonitem.put("sprice",serviceJson.getSprice());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArray.put(jsonitem);

                servicetotal =servicetotal+ Double.parseDouble(serviceJson.getSprice());
            }
        //    Log.d(TAG,"ServiceTotal "+servicetotal);
            rlmaincontotal.setVisibility(View.VISIBLE);
            relativservfare.setVisibility(View.VISIBLE);
            String totalservice = currencySymbol+" "+servicetotal;
            grouptotl.setText(totalservice);
            tvserviceamount.setText(totalservice);
            if(!tvtotlamount.getText().toString().trim().equals(""))
            {
                String total = currencySymbol+" "+(Double.parseDouble(tvtotlamount.getText().toString().split(" ")[1])+servicetotal);
                tvtotlamount.setText(total);
            }
            else
            {
                String total = currencySymbol+" "+servicetotal;
                tvtotlamount.setText(total);
            }
            addaddgroup.setText(getResources().getString(R.string.editservice));
        }
        else
        {
            rlmaincontotal.setVisibility(View.GONE);
            relativservfare.setVisibility(View.GONE);
            if(!tvtotlamount.getText().toString().trim().equals(""))
            {
                String total = currencySymbol+" "+(Double.parseDouble(tvtotlamount.getText().toString().split(" ")[1])+servicetotal);
                tvtotlamount.setText(total);
            }
            else {
                String total = currencySymbol+" "+servicetotal;
                tvtotlamount.setText(total);
            }
            addaddgroup.setText(getResources().getString(R.string.addservice));
        }
    }

    public void emitHeartbeat(String status)
    {
        JSONObject obj = new JSONObject();
        try
        {
            obj.put("status",status);
            obj.put("cid", manager.getCustomerId());

        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        socket.emit("CustomerStatus", obj);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(dialogcal!=null)
        {
            dialogcal.dismiss();
            dialogcal.cancel();
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            if(!dontallowforback)
            {

                cancelapptservice("");
            }
            else
            {
                onBackPressed();
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    private void callalertDialforcanvel(String message)
    {
        AlertDialog aldialog = new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage(""+message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        onBackPressed();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })

                .setIcon(R.drawable.ic_launcher)
                .show();
    }
    public void startPublishingWithTimer()
    {
        TimerTask myTimerTask_publish;
        // TODO Auto-generated method stub
        if(myTimer_publish!= null)
        {
            return;
        }
        myTimer_publish = new Timer();
        myTimerTask_publish = new TimerTask()
        {
            @Override
            public void run()
            {
                if(manager.getLATITUDE().equals("") || manager.getLONGITUDE().equals(""))
                {
                    Utility.printLog("startPublishingWithTimer getServerChannel no location");
                }
                else
                {
                    sendMessage(Double.parseDouble(manager.getLATITUDE()), Double.parseDouble(manager.getLONGITUDE()));
                }
            }
        };
        myTimer_publish.schedule(myTimerTask_publish, 0, 5000);
    }
    private void sendMessage(double latitude,double logitude){

        JSONObject sendText = new JSONObject();
        try
        {
            sendText.put("email",manager.getCoustomerEmail());
            sendText.put("lat",latitude);
            sendText.put("long",logitude);
            sendText.put("btype",3);
            socket.emit("UpdateCustomer", sendText);
       //     Log.d("UpdateCustomer","request "+sendText);
        }catch(JSONException e)
        {
            e.printStackTrace();
        }
    }
    /**
     * predefined method to check run time permissions list call back
     * @param permissions: contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        boolean isDenine = false;
        switch (requestCode)
        {
            case REQUEST_CODE_PERMISSION_MULTIPLE:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine)
                {
                    //TODO: if permissions denied by user, recall it
              //     Log.d("Permission","Denied ");
                    // AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList, REQUEST_CODE_PERMISSION_MULTIPLE);
                }
                else
                {
                    callDialogue();

                    //TODO: if permissions granted by user, move forward
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }
    void callDialogue()
    {
        clearOrCreateDir();
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(Confirmation_Screen.this);

        // Setting Dialog Message
        alertDialog2.setMessage(getResources().getString(R.string.selecto_photo));


        // Setting Positive "Yes" Btn
        alertDialog2.setPositiveButton(getResources().getString(R.string.gallery),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        openGallery();
                    }
                });

        // Setting Negative "NO" Btn
        alertDialog2.setNegativeButton(getResources().getString(R.string.camera),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        takePicture();
                    }
                });

        // Showing Alert Dialog
        alertDialog2.show();

    }
    private void openGallery()
    {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void takePicture()
    {
        try
        {
            state = Environment.getExternalStorageState();
            takenNewImage = System.currentTimeMillis()+".png";

            if (Environment.MEDIA_MOUNTED.equals(state))
                newFile = new File(jobpicDirectry,takenNewImage);
            else
                newFile = new File(getFilesDir()+"/"+ Variableconstant.PARENT_FOLDER+"/Job_Pics/",takenNewImage);
            if(Build.VERSION.SDK_INT>=N)
                newProfileImageUri = FileProvider.getUriForFile(this,BuildConfig.APPLICATION_ID + ".provider",newFile);
            else
                newProfileImageUri = Uri.fromFile(newFile);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, newProfileImageUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent,REQUEST_CODE_TAKE_PICTURE);
        }
        catch (ActivityNotFoundException e)
        {
           e.printStackTrace();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode != RESULT_OK)
        {
            return;
        }
        switch (requestCode) {
            case addressrequestcode:
                if(data!=null)
                {
                    String aRea = data.getExtras().getString("area");
                    String landmar  = data.getExtras().getString("landmark");
                    landmark = landmar;
                    assert aRea != null;
                    if(!aRea.equals(""))
                    {
                        String fulladd =  data.getExtras().getString("area")+"\n"+data.getExtras().getString("city")+
                                "\n"+data.getExtras().getString("addrestype");
                        apptaddress.setText(fulladd);
                    }
                    else {
                        String fulladd =  data.getExtras().getString("city")+
                                "\n"+data.getExtras().getString("addrestype");
                        apptaddress.setText(fulladd);
                    }

                    manager.setJOBLATI(data.getExtras().getString("Latitud"));
                    manager.setJOBLONGI(data.getExtras().getString("Longitud"));
                }
                break;
            case REQUEST_CODE_GALLERY:
                try {

                    state = Environment.getExternalStorageState();
                    takenNewImage = System.currentTimeMillis()+".png";

                    if (Environment.MEDIA_MOUNTED.equals(state)) {
                        newFile = new File(jobpicDirectry, takenNewImage);
                    } else {
                        newFile = new File(getFilesDir() + "/" + Variableconstant.PARENT_FOLDER+ "/Job_Pics/", takenNewImage);
                    }
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(newFile);
                    Utility.copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                    newProfileImageUri = Uri.fromFile(newFile);
                    startCropImage();
                } catch (Exception e) {
                    Utility.printLog("SingUp_page error is as follows" + e);
                }
                break;
            case REQUEST_CODE_TAKE_PICTURE:
                startCropImage();
                break;
            case REQUEST_CODE_CROP_IMAGE:

                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                isComingFromoncreat = false;

                if(path == null)
                {
                    return;
                }
                pIctureTaken = true;
                Picasso.with(this).load(Uri.fromFile(newFile))
                        .skipMemoryCache()
                        .resize(glv.getWidth(), glv.getHeight())
                        .centerCrop()
                        .into(glv);

                glv.setClickable(false);
                glvdlt.setVisibility(View.VISIBLE);
                count++;
                addNewImageView();
                Utility.printLog("BitmapFactory file size before = " + newFile.length());
                break;
        }
        if (requestCode == 1) {

            if(data.getExtras()!=null) {

                String cardNo = data.getStringExtra("cardNo");
                cardId = data.getStringExtra("cardID");

                Bitmap cardbitmap = data.getParcelableExtra("cardImage");
                String card = "**** **** **** " + " " + cardNo;
                card_no_Tv.setText(card);
                SelectedCard=cardId;
                payment_type = "2";
                iv_payment.setImageBitmap(cardbitmap);



                boolean changeDefault=false;

                while(!changeDefault)
                {
                    if(Utility.isNetworkAvailable(Confirmation_Screen.this))
                    {

                        changeDefault=true;
                    }
                    else
                    {
                        Utility.ShowAlert("Check the Internet Connect",Confirmation_Screen.this);
                    }
                }
            }
        }
    }
    private void startCropImage()
    {
        Utility.printLog("profile fragment FilePAth CROP IMAGE CALLED: " + newFile.getPath());
        Intent intent = new Intent(Confirmation_Screen.this,CropImage.class );
        intent.putExtra(CropImage.IMAGE_PATH,newFile.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);

    }
    /*creat or delete directory*/
    private void clearOrCreateDir()
    {
        try
        {
            state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state))
            {
                jobpicDirectry = new File(Environment.getExternalStorageDirectory()+"/"+ Variableconstant.PARENT_FOLDER+"/Job_Pics");
            }
            else
            {

                jobpicDirectry = new File(this.getFilesDir()+"/"+ Variableconstant.PARENT_FOLDER+"/Job_Pics");

            }
            if(!jobpicDirectry.exists() && !jobpicDirectry.isDirectory())
            {
                jobpicDirectry.mkdirs();

            }
            else
            {
                if(isComingFromoncreat)
                {
                    jobPicsDirectory = jobpicDirectry.listFiles();

                    if(jobPicsDirectory.length > 0)
                    {
                        for (int i = 0; i < jobPicsDirectory.length; i++)
                        {
                            jobPicsDirectory[i].delete();
                        }

                    }

                }

            }
        }
        catch (Exception e)
        {
           e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(dontallowforback)
        {
            if(countDownTimer!=null)
            {
                countDownTimer.cancel();
                timerpaused = true;
            }

            overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
            finish();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        activityIsRunin = false;
        socket.disconnect();
        socket.close();
        socket.off();
        myTimer_publish.cancel();
        myTimer_publish.purge();
    }
}
