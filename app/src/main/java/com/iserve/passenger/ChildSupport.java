package com.iserve.passenger;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.adapter.ChildSupportAdapter;
import com.pojo.ChildViewDraw;
import java.util.ArrayList;

/**
 * Created by embed on 20/9/16.
 *
 */
public class ChildSupport extends AppCompatActivity implements AdapterView.OnItemClickListener
{
    ProgressBar progressbar;
    ListView supportchild_list;
    FrameLayout fframelayt;
    TextView actionbartext;
    ArrayList<ChildViewDraw> childViewDraws;
    String  title;
    int position;
    String TAG = "ChildSupport";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.childsupport);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        progressbar.setVisibility(View.VISIBLE);
        childViewDraws=new ArrayList<>();
        if(getIntent().getExtras()!=null)
        {
            title = getIntent().getStringExtra("Title");
            childViewDraws = (ArrayList<ChildViewDraw>)getIntent().getSerializableExtra("list");
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        fframelayt.setVisibility(View.VISIBLE);
        actionbartext = (TextView) findViewById(R.id.toolbarhometxv);
        actionbartext.setVisibility(View.VISIBLE);
        actionbartext.setText(title);
        supportchild_list = (ListView) findViewById(R.id.supportchild_list);
        ChildSupportAdapter adapter = new ChildSupportAdapter(this, childViewDraws);
        supportchild_list.setAdapter(adapter);
        progressbar.setVisibility(View.GONE);
        supportchild_list.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        if(!childViewDraws.get(position).getLink().equals(""))
        {
            Intent intent = new Intent(this, Web_ViewActivity.class);
            Log.i(TAG,"link "+childViewDraws.get(position).getLink());
            intent.putExtra("Title", childViewDraws.get(position).getTag());
            intent.putExtra("Link", childViewDraws.get(position).getLink());
            startActivity(intent);
        }

    }
}
