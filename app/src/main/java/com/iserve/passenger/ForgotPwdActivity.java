package com.iserve.passenger;


import org.json.JSONException;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.countrypic.Country;
import com.countrypic.CountryPicker;
import com.countrypic.CountryPickerListener;
import com.utility.OkHttp3Request;
import com.utility.Utility;
import com.utility.Variableconstant;

import okhttp3.FormBody;

public class ForgotPwdActivity extends AppCompatActivity implements OnClickListener
{
	private EditText email;
	TextView countryCode;
	String mobileNumberWithoutZero = "";//,fullMobileNumber;
	FrameLayout fframelayt;
	ProgressDialog pDialog ;
	private ImageView mCountryFlagImage;
	private CountryPicker mCountryPicker;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
		setContentView(R.layout.forgot_password);
		intialize();
		setListener();
	}

	private void intialize()
	{
		email=(EditText)findViewById(R.id.etPass);
		Button submit = (Button) findViewById(R.id.submit);
		TextView tvHeader1 = (TextView) findViewById(R.id.tvHeader1);
		TextView tvHeader2 = (TextView) findViewById(R.id.tvHeader2);
		TextView tvDescription = (TextView) findViewById(R.id.tvDescription);
		RelativeLayout countryPicker= (RelativeLayout) findViewById(R.id.countryPicker);
		countryCode= (TextView) findViewById(R.id.code);
		mCountryFlagImage = (ImageView) findViewById(R.id.mCountryFlagImage);
		mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.selectcountry));

		fframelayt = (FrameLayout) findViewById(R.id.fframelayt);


		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		if(toolbar!=null)
		{
			toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
			getSupportActionBar().setTitle("");
			toolbar.setNavigationOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});
		}

		fframelayt.setVisibility(View.VISIBLE);

		TextView actionbartext = (TextView) findViewById(R.id.toolbarhometxv);

		actionbartext.setText(getResources().getString(R.string.forgotpassword));
		actionbartext.setVisibility(View.VISIBLE);


		if(email.getText().toString().length()>0)
		{


			/*if(email.getText().toString().charAt(0)=='0')
			{

			}
			else
			{

			}*/
		}

		Typeface nexaBold = Typeface.createFromAsset(this.getAssets(), Variableconstant.regularfont);
		Typeface nexaLight = Typeface.createFromAsset(this.getAssets(), Variableconstant.lightfont);
		tvHeader1.setTypeface(nexaBold);
		tvHeader2.setTypeface(nexaBold);
		tvDescription.setTypeface(nexaLight);
		email.setTypeface(nexaLight);
		submit.setTypeface(nexaBold);
		submit.setOnClickListener(this);
		countryPicker.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{

		if(v.getId()==R.id.submit)
		{
			if(email.getText().toString().length()>0)
			{
				String mobileNumer = email.getText().toString().trim();

				switch (mobileNumer.charAt(0))
				{
					case '+':
						mobileNumberWithoutZero = mobileNumer.substring(3);
						break;
					case '0':
						mobileNumberWithoutZero=mobileNumer.substring(1);
						break;
					default:
						mobileNumberWithoutZero=mobileNumer;
				}

			}

			if(!mobileNumberWithoutZero.isEmpty())
			{

				if(Utility.isNetworkAvailable(ForgotPwdActivity.this))
					BackgroundFrgtPwd();
				else
					Utility.ShowAlert(getResources().getString(R.string.network_alert_message), ForgotPwdActivity.this);
			}
			else
			{
				Utility.ShowAlert(getResources().getString(R.string.entermobilenumber), ForgotPwdActivity.this);
			}
		}
	}

	private void BackgroundFrgtPwd() {
		pDialog = new ProgressDialog(this);
		pDialog.setMessage(getString(R.string.wait));
		pDialog.show();

		Utility.printLog("ForgotPasswordWithOtp req "+mobileNumberWithoutZero);
		FormBody requestbody = new FormBody.Builder()
				.add("ent_mobile", mobileNumberWithoutZero)
				.add("ent_user_type", 2+"")
				.add("ent_date_time", Utility.dateintwtfour())
				.build();

		OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "ForgotPasswordWithOtp", requestbody, new OkHttp3Request.JsonRequestCallback() {
			@Override
			public void onSuccess(String result)
			{
				pDialog.dismiss();
				Utility.printLog("ForgotPasswordWithOtp res "+result);
				try
				{
					if(result!=null)
					{
						JSONObject jsonObject=new JSONObject(result);
						String errFlag=jsonObject.getString("errFlag");
						String errMsg=jsonObject.getString("errMsg");

						if(errFlag.equals("0"))
						{
							Intent intent=new Intent(ForgotPwdActivity.this,ForgotPassOtpActivity.class);
							intent.putExtra("NUMBER",mobileNumberWithoutZero);
							intent.putExtra("COUNTRY_CODE",countryCode.getText().toString());
							Utility.printLog("MSENDING "+countryCode.getText().toString());
							startActivity(intent);
							finish();
						}
						else
						{
							Utility.ShowAlert(errMsg, ForgotPwdActivity.this);
						}
					}
					else
					{
						Toast.makeText(ForgotPwdActivity.this, getResources().getString(R.string.network_alert_message), Toast.LENGTH_LONG).show();
					}
				}
				catch (JSONException e)
				{
					e.printStackTrace();
					pDialog.dismiss();
				}

			}

			@Override
			public void onError(String error)
			{
				pDialog.dismiss();
			}
		});
	}

	private void setListener() {
		mCountryPicker.setListener(new CountryPickerListener() {
			@Override public void onSelectCountry(String name, String code, String dialCode,
												  int flagDrawableResID) {

				countryCode.setText(dialCode);
				mCountryFlagImage.setImageResource(flagDrawableResID);
				mCountryPicker.dismiss();
			}
		});
		countryCode.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v) {
				mCountryPicker.show(getSupportFragmentManager(), getResources().getString(R.string.Countrypicker));
			}
		});
		getUserCountryInfo();
	}
	private void getUserCountryInfo() {
		Country country = mCountryPicker.getUserCountryInfo(this);
		mCountryFlagImage.setImageResource(country.getFlag());
		countryCode.setText(country.getDialCode());
	}


	@Override
	public void onBackPressed()
	{
		finish();

		overridePendingTransition(R.anim.anim_three, R.anim.anim_four);
	}

}
