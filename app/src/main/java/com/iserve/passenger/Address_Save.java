package com.iserve.passenger;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.pojo.Validator_Pojo;
import com.utility.OkHttp3Request;
import com.utility.SessionManager;
import com.utility.Variableconstant;
import okhttp3.FormBody;

/**
 * Created by embed on 12/9/16.
 *
 */
public class Address_Save extends AppCompatActivity
{
    FrameLayout fframelayt;
    TextView actionbartext, tvaddredtls,tvtagaddress,SaveAddress,tvhomeadd,tvotheradd,tvofficeadd;
    TextInputEditText etstreetlocaty,etbuildinflat,etlandmark,ettagtheaddress;
    TextInputLayout etloutstreetlocaty,etloutbuildinflat,etloutlandmark,etlltagtheaddress;
    ImageView ivhomeselectr,ivoffceselectr,ivothrselectr;
    Typeface regularfont,lightfont;
    String fulladdress;
    double latitude,longitude;
    String TAG = "Address_Save";
    ProgressDialog progressDialog;
    SessionManager manager;
    Gson gson;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.saveaddress);
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        regularfont = Typeface.createFromAsset(this.getAssets(), Variableconstant.regularfont);
        lightfont = Typeface.createFromAsset(this.getAssets(), Variableconstant.lightfont);
        if(getIntent().getExtras()!=null)
        {
            fulladdress = getIntent().getStringExtra("fulladdress");
            latitude = getIntent().getDoubleExtra("latitude",0.0);
            longitude = getIntent().getDoubleExtra("longitude",0.0);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        assert toolbar != null;
        toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        actionbartext = (TextView) findViewById(R.id.toolbarhometxv);
        fframelayt.setVisibility(View.VISIBLE);
        actionbartext.setVisibility(View.VISIBLE);
        actionbartext.setText(getResources().getString(R.string.addaddress));
        progressDialog = new ProgressDialog(this);
        manager = new SessionManager(this);
        progressDialog.setMessage(getResources().getString(R.string.wait));
        gson = new Gson();
        initialize();
    }
    private void initialize() {
        tvofficeadd = (TextView) findViewById(R.id.tvofficeadd);
        tvhomeadd = (TextView) findViewById(R.id.tvhomeadd);
        tvotheradd = (TextView) findViewById(R.id.tvotheradd);
        tvtagaddress = (TextView) findViewById(R.id.tvtagaddress);
        tvaddredtls = (TextView) findViewById(R.id.tvaddredtls);
        SaveAddress = (TextView) findViewById(R.id.SaveAddress);
        ivhomeselectr = (ImageView) findViewById(R.id.ivhomeselectr);
        ivoffceselectr = (ImageView) findViewById(R.id.ivoffceselectr);
        ivothrselectr = (ImageView) findViewById(R.id.ivothrselectr);
        etstreetlocaty = (TextInputEditText) findViewById(R.id.etstreetlocaty);
        etbuildinflat = (TextInputEditText) findViewById(R.id.etbuildinflat);
        etlandmark = (TextInputEditText) findViewById(R.id.etlandmark);
        ettagtheaddress = (TextInputEditText) findViewById(R.id.ettagtheaddress);
        etloutstreetlocaty = (TextInputLayout) findViewById(R.id.etloutstreetlocaty);
        etloutbuildinflat = (TextInputLayout) findViewById(R.id.etloutbuildinflat);
        etloutlandmark = (TextInputLayout) findViewById(R.id.etloutlandmark);
        etlltagtheaddress = (TextInputLayout) findViewById(R.id.etlltagtheaddress);
        etloutstreetlocaty.setHintTextAppearance(R.style.QText);
        etloutbuildinflat.setHintTextAppearance(R.style.QText);
        etloutlandmark.setHintTextAppearance(R.style.QText);
        etlltagtheaddress.setHintTextAppearance(R.style.QText);
        etloutbuildinflat.setFocusable(true);
        etbuildinflat.setFocusable(true);
        etstreetlocaty.setText(fulladdress);
        typeface();
        selectype();
        SaveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                    if(!ettagtheaddress.getText().toString().trim().equals(""))
                    {
                        String lati = latitude+"";
                        String longi = longitude+"";
                        addCustomerAddres(etbuildinflat.getText().toString(),etstreetlocaty.getText().toString(),etlandmark.getText().toString()
                                ,ettagtheaddress.getText().toString()
                                ,lati,longi);
                    }
                    else
                    {
                        Toast.makeText(Address_Save.this,getResources().getString(R.string.pleaseentertag),Toast.LENGTH_SHORT).show();
                    }
            }
        });
    }
    private void selectype() {
        ivhomeselectr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivhomeselectr.setSelected(true);
                ivoffceselectr.setSelected(false);
                ivothrselectr.setSelected(false);
                tvhomeadd.setTextColor(Color.parseColor("#2598ED"));
                tvotheradd.setTextColor(Color.GRAY);
                tvofficeadd.setTextColor(Color.GRAY);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                etlltagtheaddress.setVisibility(View.GONE);
                ettagtheaddress.setText(getResources().getString(R.string.homeaddress));
            }
        });
        ivoffceselectr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivhomeselectr.setSelected(false);
                ivoffceselectr.setSelected(true);
                ivothrselectr.setSelected(false);
                tvhomeadd.setTextColor(Color.GRAY);
                tvotheradd.setTextColor(Color.GRAY);
                tvofficeadd.setTextColor(Color.parseColor("#2598ED"));
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                etlltagtheaddress.setVisibility(View.GONE);
                ettagtheaddress.setText(getResources().getString(R.string.officeaddress));
            }
        });
        ivothrselectr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivhomeselectr.setSelected(false);
                ivoffceselectr.setSelected(false);
                ivothrselectr.setSelected(true);
                tvotheradd.setTextColor(Color.parseColor("#2598ED"));
                tvhomeadd.setTextColor(Color.GRAY);
                tvofficeadd.setTextColor(Color.GRAY);
                ettagtheaddress.setText("");
                ettagtheaddress.requestFocus();
                etlltagtheaddress.setVisibility(View.VISIBLE);
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });
    }
    private void typeface() {
        actionbartext.setTypeface(regularfont);
        SaveAddress.setTypeface(regularfont);
        tvaddredtls.setTypeface(lightfont);
        etstreetlocaty.setTypeface(lightfont);
        etbuildinflat.setTypeface(lightfont);
        etlandmark.setTypeface(lightfont);
        tvtagaddress.setTypeface(lightfont);
        ettagtheaddress.setTypeface(lightfont);
        tvofficeadd.setTypeface(lightfont);
        tvhomeadd.setTypeface(lightfont);
        tvotheradd.setTypeface(lightfont);
    }

    /**
     * <h1>addCustomerAddres</h1>
     * this method is saving the address using the api
     * @param road street
     * @param city  address of the city
     * @param land_mark Landmark of the address
     * @param country country of the address
     * @param lat latitude of the address
     * @param log longitude of the address
     */
    private void addCustomerAddres(String road, String city, String land_mark, String country, String lat, String log)
    {
        progressDialog.show();
        final FormBody request = new FormBody.Builder()
                .add("ent_sess_token",manager.getSession())
                .add("ent_dev_id",manager.getDevice_Id())
                .add("ent_cust_id",manager.getCustomerId())
                .add("ent_address1",city)
                .add("ent_address2",land_mark)
                .add("ent_suite_num",road)
                .add("ent_tag_address",country)
                .add("ent_latitude",lat)
                .add("ent_longitude",log)
                .build();
        OkHttp3Request.doOkHttp3Request(Variableconstant.service_url + "AddCustomerAddress", request, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                progressDialog.dismiss();
                Log.d(TAG,"Addressresul "+result);
                Validator_Pojo pojo = gson.fromJson(result,Validator_Pojo.class);
                if(pojo.getErrFlag().equals("0") && pojo.getErrNum().equals("111"))
                {
                    finish();
                }
            }
            @Override
            public void onError(String error) {
                progressDialog.dismiss();
            }
        });
    }
}
