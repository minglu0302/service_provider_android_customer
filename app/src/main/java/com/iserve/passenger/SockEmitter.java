package com.iserve.passenger;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author 3embed
 * <h>SockEmitter</h>
 * <p>this is an abstract class </p>
 * Created by embed on 21/6/16.
 */
abstract class SockEmitter
{
    private Context context;

    /**
     * <h2>execute</h2>
     * <p>this method execute the socket event and json data</p>
     * @param socket_event is the channel name
     * @param jsonObject JSONObject is the data responce
     * @throws JSONException if execute methods have any jsonException
     */

    public abstract void execute (String socket_event,JSONObject jsonObject) throws JSONException;

    SockEmitter(Context ctx)
    {
        this.context = ctx.getApplicationContext();
    }

    /**
     * <h2>handleObject</h2>
     * <p>this method handle the responce of the socket event and jsonObject</p>
     * @see ApplicationController
     * @param socket_event Socekt Event is basically channel
     * @param jsonObject data responce in json object
     * @throws Exception may contains jsonexception
     */

    void handleObject(String socket_event, JSONObject jsonObject) throws Exception
    {
        execute(socket_event,jsonObject);

    }
}