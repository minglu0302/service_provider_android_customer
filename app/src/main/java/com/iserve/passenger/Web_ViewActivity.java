package com.iserve.passenger;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


public class Web_ViewActivity extends AppCompatActivity
{
	private WebView webView;
	private ProgressBar progress;
	private String title,url;
	FrameLayout backArrow;

	/**
	 * <p>Setting content view</p>
	 * @param savedInstanceState view saved
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.webview);
		overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);

		Intent intent=getIntent();
		/*
		* checking if not null fetching the link and title*/
		if(intent!=null)
		{

			url=getIntent().getStringExtra("Link");
			title=getIntent().getStringExtra("Title");

		}
		Toolbar toolbar = (Toolbar) findViewById(R.id.app_toobar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setTitle("");
		TextView bar_tittle= (TextView) findViewById(R.id.confirmtoolbar);
		bar_tittle.setText(title);
		toolbar.setNavigationIcon(R.drawable.login_back_icon_off);
		toolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
		webView = (WebView) findViewById(R.id.webView1);
		webView.setWebViewClient(new MyWebViewClient());
		webView.setSaveFromParentEnabled(true);

		webView.getSettings().setJavaScriptEnabled(true);
		progress = (ProgressBar) findViewById(R.id.progressBar_splsh);
		progress.setVisibility(View.GONE);
		webView.loadUrl(url);



		if (validateUrl(url))
		{
			webView.getSettings().setJavaScriptEnabled(true);
			webView.loadUrl(url);
		}

	}

	//on back pressed with slide animation
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
		overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
	}

	private boolean validateUrl(String url)
	{
		return true;
	}


	private class MyWebViewClient extends WebViewClient
	{
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url)
		{
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url)
		{
			progress.setVisibility(View.GONE);
			Web_ViewActivity.this.progress.setProgress(100);
			super.onPageFinished(view, url);
		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon)
		{
			progress.setVisibility(View.VISIBLE);
			Web_ViewActivity.this.progress.setProgress(0);
			super.onPageStarted(view, url, favicon);
		}
	}


	public void setValue(int progress)
	{
		this.progress.setProgress(progress);
	}




}
