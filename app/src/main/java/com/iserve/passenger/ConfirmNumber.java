package com.iserve.passenger;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.pojo.Language_Type;
import com.pojo.Signup_pojo;
import com.pojo.Validator_Pojo;
import com.utility.Alerts;
import com.utility.OkHttp3Request;
import com.utility.ReadSms;
import com.utility.SessionManager;
import com.utility.Utility;
import com.utility.Variableconstant;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.FormBody;

public class ConfirmNumber extends AppCompatActivity implements View.OnClickListener {

    TextView smsverify_Tv,notRecive_Tv,confirmtoolbar;
    FrameLayout fframelayt;
    Button resend_button;
    EditText et_stno,et_sdno,et_rdno,et_frno;
    Alerts alerts;
    Resources resources;
    RelativeLayout otp_Rl;
    ReadSms readSms;
    SessionManager manager ;
    Signup_pojo signup_pojo;

    ProgressDialog pDialog;
    Gson gson;
    Toolbar myToolBar;
    JSONObject jsonObject;
    UploadAmazonS3 upload;
    Language_Type lantype_pojo;
    IntentFilter intentFilter;


    String ent_first_name,ent_last_name,ent_email,ent_mobile,ent_password,country_code,appversion,referalcode,
            ent_dev_id,ent_push_token,ent_signup_type,fbid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_number);
        jsonObject = new JSONObject();
        gson = new Gson();
        upload =UploadAmazonS3.getInstance(ConfirmNumber.this, Variableconstant.Amazoncognitoid);
        initialization();
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);

        readSms = new ReadSms() {
            @Override
            protected void onSmsReceived(String s) {
                Log.i("smsrecv ", "sms " + s);
                String splitted[] =s.split(" ");
                if(splitted[0].length()==4){


                    String one = splitted[0].charAt(0)+"";
                    String tw = splitted[0].charAt(1)+"";
                    String thr = splitted[0].charAt(2)+"";
                    String fur = splitted[0].charAt(3)+"";
                    et_stno.setText(one);
                    et_sdno.setText(tw);
                    et_rdno.setText(thr);
                    et_frno.setText(fur);

                }
            }
        };
        intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null)
        {

            ent_first_name=bundle.getString("ent_first_name");
            ent_last_name = bundle.getString("ent_last_name");
            ent_email=bundle.getString("ent_email");
            fbid=bundle.getString("ent_fbid");
            country_code = bundle.getString("country_code");
            ent_mobile=bundle.getString("ent_mobile");
            ent_password=bundle.getString("ent_password");
            appversion=bundle.getString("ent_app_version");
            referalcode=bundle.getString("ent_referal_code");
            ent_signup_type=bundle.getString("ent_signup_type");
            Log.i("Confirm","profile "+referalcode+" appversion "+appversion);
            ent_push_token= bundle.getString("ent_push_token");
            ent_dev_id=bundle.getString("ent_dev_id");

        }


    }
    void initialization() {
        resources = getResources();
        manager = new SessionManager(ConfirmNumber.this);
        lantype_pojo = new Language_Type();
        alerts=new Alerts(this);
        myToolBar = (Toolbar) findViewById(R.id.myToolBar);
        setSupportActionBar(myToolBar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        fframelayt = (FrameLayout) findViewById(R.id.fframelayt);
        confirmtoolbar = (TextView) findViewById(R.id.toolbarhometxv);
        smsverify_Tv = (TextView) findViewById(R.id.smsverify_Tv);
        notRecive_Tv = (TextView) findViewById(R.id.notRecive_Tv);
        et_stno = (EditText) findViewById(R.id.et_stno);
        et_sdno = (EditText) findViewById(R.id.et_sdno);
        et_rdno = (EditText) findViewById(R.id.et_rdno);
        et_frno = (EditText) findViewById(R.id.et_frno);
        otp_Rl = (RelativeLayout) findViewById(R.id.otp_Rl);
        resend_button= (Button) findViewById(R.id.resend_button);
        fframelayt.setVisibility(View.VISIBLE);
        confirmtoolbar.setVisibility(View.VISIBLE);
        confirmtoolbar.setText(getResources().getString(R.string.verfyotp));
        otp_Rl.setOnClickListener(this);
        /*
         * setting type face all fields
         */

        setTypefaceFonts();

        pDialog = Utility.GetProcessDialog(ConfirmNumber.this);
        pDialog.setCancelable(false);
        resend_button.setOnClickListener(this);

        et_stno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if(et_stno.getText().toString().length()==1)
                {
                    et_sdno.requestFocus();
                }
            }
        });

        et_sdno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if(et_sdno.getText().toString().length()==1)
                {
                    et_rdno.requestFocus();
                }
            }
        });

        et_rdno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if(et_rdno.getText().toString().length()==1)
                {
                    et_frno.requestFocus();
                }
            }
        });

        et_frno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                callmethodtoVerifiy();
            }
        });
    }

    private void callmethodtoVerifiy()
    {
        if(!TextUtils.isEmpty(et_stno.getText().toString()) && !TextUtils.isEmpty(et_sdno.getText().toString()) &&
                !TextUtils.isEmpty(et_rdno.getText().toString()) && !TextUtils.isEmpty(et_frno.getText().toString()))
        {
            if(Utility.isNetworkAvailable(ConfirmNumber.this))
                Verfycode();
            else
                alerts.showNetworkAlert(ConfirmNumber.this);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        ConfirmNumber.this.registerReceiver(readSms, intentFilter);

    }
    void setTypefaceFonts() {

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.side_slide_out, R.anim.side_slide_in);
    }


    @Override
    public void onClick(View v) {

        if(v.getId()== R.id.resend_button){
            if(Utility.isNetworkAvailable(ConfirmNumber.this))
            getVerficationcode();
            else
                alerts.showNetworkAlert(ConfirmNumber.this);
        }

        if(v.getId()== R.id.otp_Rl){
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }

    }
    void getVerficationcode(){

        pDialog.setMessage(resources.getString(R.string.wait));
        pDialog.show();

        FormBody requestform = new FormBody.Builder()
                .add("ent_phone", country_code+""+ent_mobile)
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.GETVERIFICATIONCODE, requestform, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                Utility.printLog("mail validtion  onSuccess JSON DATA" + result);
                pDialog.dismiss();


            }

            @Override
            public void onError(String error) {
                pDialog.dismiss();
            }
        });


    }


    void Verfycode(){

        pDialog.setMessage(resources.getString(R.string.wait));
        pDialog.show();

        String s = (et_stno.getText().toString()+""+et_sdno.getText().toString()+""+
                et_rdno.getText().toString()+""+et_frno.getText().toString());

        Utility.printLog("VERIFYPHONE ONSUCCESS " + ent_mobile);

        FormBody requestform = new FormBody.Builder()
                .add("ent_phone", country_code+""+ent_mobile)
                .add("ent_code", s)
                .add("ent_user_type", "2")
                .add("ent_service_type", "1")
                .build();

        OkHttp3Request.doOkHttp3Request(Variableconstant.VERIFYPHONE, requestform, new OkHttp3Request.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {

                pDialog.dismiss();
                Utility.printLog(" VERIFYPHONE ONSUCCESS" + result);

                Validator_Pojo phoneNumberValidator_pojo;
                Gson gson = new Gson();
                phoneNumberValidator_pojo = gson.fromJson(result, Validator_Pojo.class);

                if(Utility.isNetworkAvailable(ConfirmNumber.this))

                    if(phoneNumberValidator_pojo.getErrFlag().equals("0"))
                    {
                        Signupservice();
                    }
                else
                    {
                        alerts.problemLoadingAlert(ConfirmNumber.this,phoneNumberValidator_pojo.getErrMsg());
                        et_stno.setText(null);
                        et_sdno.setText(null);
                        et_rdno.setText(null);
                        et_frno.setText(null);
                    }

            }

            @Override
            public void onError(String error)
            {
                pDialog.dismiss();
                Toast.makeText(ConfirmNumber.this, error, Toast.LENGTH_SHORT).show();
            }
        });


    }


    void Signupservice(){
        pDialog.setMessage(resources.getString(R.string.singingup));
        pDialog.show();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ent_first_name", ent_first_name);
            jsonObject.put("ent_last_name", ent_last_name);
            jsonObject.put("ent_email",ent_email);
            jsonObject.put("ent_country_code",country_code);
            jsonObject.put("ent_password", ent_password);
            jsonObject.put("ent_mobile",ent_mobile);
            jsonObject.put("ent_profile_pic", Register_Page.uplodedImage);
            jsonObject.put("ent_terms_cond", 1+"");
            jsonObject.put("ent_pricing_cond",1+"");
            jsonObject.put("ent_push_token", ent_push_token);
            jsonObject.put("ent_dev_id",ent_dev_id);
            jsonObject.put("ent_device_type", "2");
            jsonObject.put("ent_app_version", appversion);
            jsonObject.put("ent_dev_os", Build.VERSION.RELEASE);
            jsonObject.put("ent_dev_model", Build.MODEL);
            jsonObject.put("ent_manf", Build.BRAND);
            jsonObject.put("ent_referral_code",referalcode);
            jsonObject.put("ent_fbid",fbid);
            jsonObject.put("ent_signup_type",ent_signup_type);
            jsonObject.put("ent_latitude", manager.getLATITUDE() + "");
            jsonObject.put("ent_longitude", manager.getLONGITUDE()+"");
            jsonObject.put("ent_date_time", Utility.dateintwtfour());
            Log.d("TAG","signupJsonObjct "+jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Utility.doJsonRequest(Variableconstant.SIGNUPURL, jsonObject, new Utility.JsonRequestCallback() {
            @Override
            public void onSuccess(String result) {
                pDialog.dismiss();
                if(result!=null)
                {
                    Log.i("Register","signupresponce "+result);
                    singupsuccess(result);
                }
            }

            @Override
            public void onError(String error) {
                pDialog.dismiss();
            }
        });


    }

    private void singupsuccess(String result)
    {
        signup_pojo = gson.fromJson(result, Signup_pojo.class);

        if (signup_pojo.getErrFlag().equals("0")) {
            manager.setIsLogin(true);
            manager.setSession(signup_pojo.getToken());
            manager.SetChannel(signup_pojo.getChn());
            manager.setCustomerFnme(ent_first_name);
            manager.setCustomerLnme(ent_last_name);
            manager.storecoustomerEmail(signup_pojo.getEmail());
            manager.setCoupon(signup_pojo.getCoupon());
            manager.setProfilePic(Register_Page.uplodedImage);
            manager.setCustomerId(signup_pojo.getCustId());
            manager.setOldpassword(ent_password);
            manager.setStripeKey(signup_pojo.getStripe_pub_key());
            lantype_pojo.setLanguages(signup_pojo.getLanguages());
            String languagetype = gson.toJson(lantype_pojo,Language_Type.class);
            manager.setLanguage(languagetype);
            Intent intent = new Intent(ConfirmNumber.this, TestSignup_Payment.class);
            intent.putExtra("coming_From", "confirm_no");
            startActivity(intent);
            finish();
        } else {

            Toast.makeText(ConfirmNumber.this, signup_pojo.getErrMsg(), Toast.LENGTH_LONG).show();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(readSms);
        if(pDialog!= null)
        {
            pDialog.cancel();
            pDialog.dismiss();
        }
    }
}
