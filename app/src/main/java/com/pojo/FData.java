package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 18/8/16.
 *
 */
public class FData  implements Serializable
{
    /*"sign_url":"https:\/\/s3.amazonaws.com\/iserve\/ISERVEPRO\/invoice\/image2016-08-18-18:28:39.jpg",
"misc_fees":"0",
"total_pro":"101",
"pro_disc":"0"*/


    /*"payment_type":"1",
"price_min":"5",
"visit_fees":"10",
"coupon_discount":"",
"pro_note":"No Notes",
"misc_fees":"14",
"fee_type":"Hourly",
"mat_fees":"12",
"sign_url":"https:\/\/s3.amazonaws.com\/iserve\/invoice\/2016-10-12-21:23:32.jpg",
"total_pro":"31",
"pro_disc":"10",
"time_fees":"",
"sub_total_pro":""*/


    String sign_url,misc_fees,total_pro,pro_disc,payment_type,price_min,visit_fees,coupon_discount,pro_note,fee_type,mat_fees,time_fees,sub_total_cust;

    public String getSign_url() {
        return sign_url;
    }

    public String getMisc_fees() {
        return misc_fees;
    }

    public String getTotal_pro() {
        return total_pro;
    }

    public String getPro_disc() {
        return pro_disc;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public String getPrice_min() {
        return price_min;
    }

    public String getVisit_fees() {
        return visit_fees;
    }

    public String getCoupon_discount() {
        return coupon_discount;
    }

    public String getPro_note() {
        return pro_note;
    }

    public String getFee_type() {
        return fee_type;
    }

    public String getMat_fees() {
        return mat_fees;
    }

    public String getTime_fees() {
        return time_fees;
    }

    public String getSub_total_cust() {
        return sub_total_cust;
    }
}
