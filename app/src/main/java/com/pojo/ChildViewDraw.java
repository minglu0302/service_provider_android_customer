package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 20/9/16.
 */
public class ChildViewDraw implements Serializable
{

    /*"tag":"How to request ride",
"link":"support\/HowToUseUber\/RequestingARide\/howTorequest_aRide.html"*/
    String tag,link;

    public String getTag() {
        return tag;
    }

    public String getLink() {
        return link;
    }
}
