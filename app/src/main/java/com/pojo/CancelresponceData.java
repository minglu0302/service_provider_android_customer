package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 25/10/16.
 */
public class CancelresponceData implements Serializable
{
    /*{"errNum":"21","errFlag":"0","errMsg":"Got the details!",
    "data":["provider Not Shown","Reason5","Reason 6","address not found, since customer was far so not reachablke","hello","hey","cancel","cancel1","canecl2"]}*/

    private String errNum,errFlag,errMsg;
    private ArrayList<String> data;

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }


    public String getErrNum() {

        return errNum;
    }

    public ArrayList<String> getGetdata() {
        return data;
    }
}
