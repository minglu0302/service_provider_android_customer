package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 24/9/16.
 */
public class DataService implements Serializable
{
    /*"group_id":"57c59b82e127c9910d9af19c",
"group_name":"MUSICGROUP",
"cmand":"",
"cmult":"",
"services":[]*/
    String group_id,group_name,cmand,cmult;
    boolean mandatorydata;
    ArrayList<ServiceGroup>services;

    public boolean isMandatorydata() {
        return mandatorydata;
    }

    public void setMandatorydata(boolean mandatorydata) {
        this.mandatorydata = mandatorydata;
    }

    public String getGroup_id() {
        return group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public String getCmand() {
        return cmand;
    }

    public String getCmult() {
        return cmult;
    }

    public ArrayList<ServiceGroup> getServices() {
        return services;
    }

    public void setServices(ArrayList<ServiceGroup> services) {
        this.services = services;
    }
}
