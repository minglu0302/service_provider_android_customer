package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 5/1/16.
 */
public class MasterDetails_Slots implements Serializable
{
    ArrayList<Master_Slotes> slots;

    public ArrayList<Master_Slotes> getSlots() {
        return slots;
    }

    public void setSlots(ArrayList<Master_Slotes> slots) {
        this.slots = slots;
    }
}
