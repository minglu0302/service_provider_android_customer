package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 9/8/16.
 */
public class TrackLive implements Serializable
{
    /*"lt":13,
"lg":77,
"e":"pk@gmail.com",
"i":"https:\/\/s3.amazonaws.com\/iserve\/IserveProAndroid\/temp_pic.jpg",
"n":"Prashant",
"pid":141*/


    /*"lt":13.0286304,
"lg":77.5894812,
"e":"v@gmail.com",
"i":"https:\/\/s3.amazonaws.com\/onthewaybucket\/ProfileImages\/povider_profile1478766495357.png",
"n":"Venga",
"pid":149,
"rat":0*/

    String lt,lg,e,i,n,pid;

    public String getLt() {
        return lt;
    }

    public String getLg() {
        return lg;
    }

    public String getE() {
        return e;
    }

    public String getI() {
        return i;
    }

    public String getN() {
        return n;
    }

    public String getPid() {
        return pid;
    }
}
