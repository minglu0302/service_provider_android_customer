package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 8/8/16.
 */
public class LiveBookinResponce implements Serializable
{
    /*"errNum":"21",
"errFlag":"0",
"errMsg":"Got the details!",
"bid":"1423",
"devId":"351971070311557",
"fName":"Prashant",
"lName":"2",
"mobile":"14253",
"email":"pk@gmail.com",
"addr1":"53, RBI Colony, Hebbal",
"addr2":"h",
"can_fees":"50",
"pPic":"https:\/\/s3.amazonaws.com\/iserve\/IserveProAndroid\/temp_pic.jpg",
"apptDate":"08-05-2016",
"apptTime":"08:09 am",
"bookType":"1",
"rating":0,
"apptLat":"13.0289533",
"apptLong":"77.5897201",
"chn":"",
"pid":"141",
"rev_sub":0,
"timer":"0",
"timer_status":"0",
"timer_start_time":"0000-00-00 00:00:00",
"date_time":"2016-08-06 12:41:00"*/


   private String errNum,errFlag,errMsg,bid,devId,fName,lName,mobile,email,addr1,addr2,can_fees,pPic,apptDate,apptTime,bookType,rating,
            apptLat,apptLong,chn,pid,rev_sub,timer,timer_status,timer_start_time,date_time,status;




    public String getStatus() {
        return status;
    }

    public String getErrNum() {
        return errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getBid() {
        return bid;
    }

    public String getDevId() {
        return devId;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public String getAddr1() {
        return addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public String getCan_fees() {
        return can_fees;
    }

    public String getpPic() {
        return pPic;
    }

    public String getApptDate() {
        return apptDate;
    }

    public String getApptTime() {
        return apptTime;
    }

    public String getBookType() {
        return bookType;
    }

    public String getRating() {
        return rating;
    }

    public String getApptLat() {
        return apptLat;
    }

    public String getApptLong() {
        return apptLong;
    }

    public String getChn() {
        return chn;
    }

    public String getPid() {
        return pid;
    }

    public String getRev_sub() {
        return rev_sub;
    }

    public String getTimer() {
        return timer;
    }

    public String getTimer_status() {
        return timer_status;
    }

    public String getTimer_start_time() {
        return timer_start_time;
    }

    public String getDate_time() {
        return date_time;
    }
}
