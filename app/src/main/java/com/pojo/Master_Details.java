package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 24/8/16.
 */
public class Master_Details implements Serializable
{
   /*"errNum":"21",
"errFlag":"0",
"errMsg":"Got the details!",
"fee":"0",
"email":"piyush@mobifyi.com",
"fName":"Piyush",
"lName":"Malviya",
"deg":"",
"mobile":"9590204678",
"pPic":"https:\/\/s3.amazonaws.com\/iserve\/IserveProAndroid\/profile_pics\/povider_profile1473158393571.png",
"about":"experienced provider",
"expertise":"doctor,nutrition,nurse,Psychology",
"rating":5,
"totalRev":"",
"reviews":[
{
"patPic":"https:\/\/s3.amazonaws.com\/iserve\/ProfileImages\/mukesh@gmail.com.png",
"rating":"5",
"review":"",
"by":"Mukesh",
"dt":"2016-09-15"
},
{
"patPic":"https:\/\/s3.amazonaws.com\/iserve\/ProfileImages\/devraj@mobifyi.com.png",
"rating":"5",
"review":"",
"by":"Devraj",
"dt":"2016-09-08"
}
],
"education":[
],
"slots":[
],
"languages":"english,Hindi,Marathi",
"pid":"200",
"num_of_job_images":"4",
"check":""
*/
    String errNum,errFlag,errMsg,fee,email,fName,lName,deg,mobile,pPic,about,expertise,rating,totalRev,languages,pid;

    ArrayList<MAster_Review>reviews;

    ArrayList<Master_Slotes>slots;
    ArrayList<String>job_images;


    public ArrayList<String> getJob_images() {
        return job_images;
    }


    public String getErrNum() {
        return errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getFee() {
        return fee;
    }

    public String getEmail() {
        return email;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getDeg() {
        return deg;
    }

    public String getMobile() {
        return mobile;
    }

    public String getpPic() {
        return pPic;
    }

    public String getAbout() {
        return about;
    }

    public String getExpertise() {
        return expertise;
    }

    public String getRating() {
        return rating;
    }

    public String getTotalRev() {
        return totalRev;
    }

    public String getLanguages() {
        return languages;
    }

    public String getPid() {
        return pid;
    }



    public ArrayList<MAster_Review> getReviews() {
        return reviews;
    }


    public ArrayList<Master_Slotes> getSlots() {
        return slots;
    }
}
