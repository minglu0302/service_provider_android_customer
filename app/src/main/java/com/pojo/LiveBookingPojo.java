package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 20/6/16.
 */
public class LiveBookingPojo implements Serializable
{
    /*{"errorFlag":"0","Message":"Appointment Created","Data":{"bid":725}}*/

    /*"errFlag":"0",
"errMsg":"Appointment Created",
"bid":{
"code":"ER_BAD_NULL_ERROR",
"errno":1048,
"sqlState":"23000",
"index":0
}*/

    //{"errFlag":"0","errMsg":"Appointment Created","bid":1087}
    String errFlag,errMsg,bid,errNum;



    public String getBid() {
        return bid;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getErrNum() {
        return errNum;
    }
}
