package com.pojo;

import java.util.ArrayList;

/**
 * Created by embed on 17/11/16.
 */
public class Language_Type
{
    ArrayList<Language_Class> Languages;

    public ArrayList<Language_Class> getLanguages() {
        return Languages;
    }

    public void setLanguages(ArrayList<Language_Class> languages) {
        Languages = languages;
    }
}
