package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 18/8/16.
 */
public class InvoiceResponce implements Serializable
{
    /*"errNum":"21",
"errFlag":"0",
"errMsg":"Got the details!",
"bid":"1499",
"visit_fees":"100",
"cat_name":"Doctors",
"apptDate":"08-18-2016",
"apptTime":"01:34 am",
"fName":"Akbar",
"lName":"Attar",
"mobile":"+914123654",
"email":"akbar@gmail.com",
"addr1":"1st Main Road, RBI Colony, Ganganagar",
"addr2":"h",
"can_fees":"50",
"rating":0,
"chn":"\/#MvpK3uuruOH9ZKjZAAD-",
"pid":"153",
"fdata":{
"sign_url":"https:\/\/s3.amazonaws.com\/iserve\/ISERVEPRO\/invoice\/image2016-08-18-18:28:39.jpg",
"misc_fees":"0",
"total_pro":"101",
"pro_disc":"0"
},
"apptLat":"13.0285711",
"apptLong":"77.5894717",
"rev_sub":0,
"timer":"1",
"timer_status":"2",
"timer_start_time":"2016-08-18 15:35:12",
"date_time":"2016-08-18 18:28:42"
"helpTag":"Need Help",
"helpLink":"support\/need_help.html"*/

    /*"errNum":"21",
"errFlag":"0",
"errMsg":"Got the details!",
"bid":"148",
"visit_fees":"10",
"cat_name":"BEAUTY",
"price_min":"5",
"apptDate":"10-12-2016",
"apptTime":"09:14 am",
"fName":"Akbar",
"lName":"",
"mobile":"+918880675003",
"email":"akbar@gmail.com",
"addr1":"10th Cross Street, RBI Colony, Ganga Nagar",
"addr2":"",
"can_fees":"2",
"pid":"2",
"fdata":{},
"apptLat":"13.028875043529867",
"apptLong":"77.5895080715418",
"rev_sub":0,
"timer":"81",
"rating":0,
"rev":"",
"star_rating":0,
"apptret":"",
"timer_status":3,
"timer_start_time":"2016-10-12 21:18:33",
"date_time":"2016-10-12 09:14:48",
"helpTag":"Need Help",
"helpLink":"support\/need_help.html",
"pPic":"https:\/\/s3.amazonaws.com\/iserve\/ProfileImages\/akbar@gmail.com.jpg"*/


   private FData fdata;

   private String errNum,errFlag,errMsg,bid,visit_fees,cat_name,apptDate,apptTime,fName,lName,mobile,email,addr1,addr2,can_fees,rating,pid,helpLink,
            timer_status,timer,pPic,disputed,dispute_msg,cancel_reason,cancel_amount;

  private   ArrayList<InvoiceService> services;

    public String getDisputed() {
        return disputed;
    }

    public String getDispute_msg() {
        return dispute_msg;
    }

    public String getCancel_reason() {
        return cancel_reason;
    }

    public String getpPic() {
        return pPic;
    }

    public ArrayList<InvoiceService> getServices() {
        return services;
    }

    public String getTimer_status() {
        return timer_status;
    }

    public String getTimer() {
        return timer;
    }

    public String getHelpLink() {
        return helpLink;
    }

    public FData getFdata() {
        return fdata;
    }

    public String getErrNum() {
        return errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getBid() {
        return bid;
    }

    public String getVisit_fees() {
        return visit_fees;
    }

    public String getCat_name() {
        return cat_name;
    }

    public String getApptDate() {
        return apptDate;
    }

    public String getApptTime() {
        return apptTime;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public String getAddr1() {
        return addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public String getCan_fees() {
        return can_fees;
    }

    public String getCancel_amount() {
        return cancel_amount;
    }

    public String getRating() {
        return rating;
    }

    public String getPid() {
        return pid;
    }
}
