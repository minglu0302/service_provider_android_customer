package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 3/8/16.
 */
public class OnGoing_Booking implements Serializable
{
   /*    "apntDt":"2016-08-02 13:16:47",
    "pPic":"9379585118211.png",
    "email":"a1@gmail.com",
    "status":"on the way.",
    "fname":"Venga",
    "lname":"",
    "deg":"",
    "phone":"8784548845",
    "pro_reason":"",
    "dispute_msg":"",
    "disputed":"",
    "apntTime":"01:16 pm",
    "apntDate":"2016-08-02",
    "apptLat":"13.02889156341553",
    "apptLong":"77.58959197998047",
    "bid":"1232",
    "cat_name":"hair stylist",
    "addrLine1":"10th Cross Road, RT Nagar, Bengaluru, Karnataka 560024, India",
    "addrLine2":"",
    "notes":"",
    "bookType":"1",
    "statCode":"5",
    "amount":"",
     "star_rating"
    "fdata":[

],
    "cancelAmount":0,
    "dt":"20160802131647"*/

 //   boolean isFirst;

   // FData fdata;

    String apntDt,pPic,email,status,fname,lname,deg,phone,apntTime,apntDate,apptLat,apptLong,bid,cat_name,addrLine1,addrLine2,notes,bookType,statCode
            ,amount,star_rating,cancelAmount,dt,pid;//,cancel_reason,dispute_msg,disputed;

    public String getPid() {
        return pid;
    }




    public String getStar_rating() {
        return star_rating;
    }


    public String getApntDt() {
        return apntDt;
    }

    public String getpPic() {
        return pPic;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getDeg() {
        return deg;
    }

    public String getPhone() {
        return phone;
    }

    public String getApntTime() {
        return apntTime;
    }

    public String getApntDate() {
        return apntDate;
    }

    public String getApptLat() {
        return apptLat;
    }

    public String getApptLong() {
        return apptLong;
    }

    public String getBid() {
        return bid;
    }

    public String getCat_name() {
        return cat_name;
    }

    public String getAddrLine1() {
        return addrLine1;
    }

    public String getAddrLine2() {
        return addrLine2;
    }

    public String getNotes() {
        return notes;
    }

    public String getBookType() {
        return bookType;
    }

    public String getStatCode() {
        return statCode;
    }

    public String getAmount() {
        return amount;
    }

    public String getCancelAmount() {
        return cancelAmount;
    }

    public String getDt() {
        return dt;
    }

}
