package com.pojo;

import java.util.ArrayList;

/**
 * Created by admin-3embed on 13/6/15.
 */
public class MyProfile_pojo {

    /*    "errNum":"33",
    "errFlag":"0",
    "errMsg":"Got the details!",
    "fName":"akbar",
    "lName":"attar",
    "email":"akbarattar@gmail.com",
    "country_code":"+91",
    "phone":"1236541236",
    "pPic":"https:\/\/s3.amazonaws.com\/iserve\/ProfileImages\/akbarattar@gmail.com.png"*/


    private String errNum;
    private String errFlag;
    private String errMsg;
    private String fName;
    private String lName;
    private String email;
    private int country_code;
    private String phone;
    private String pPic;
    ArrayList<Language_Class> Languages;

    public ArrayList<Language_Class> getLanguages() {
        return Languages;
    }

    /*{"errNum":"33","errFlag":"0","errMsg":"Got the details!","fName":"Akbar","lName":"Attar","email":"ali@mobifyi.com",
    "country_code":"+91","phone":"9620407863","pPic":"aa_default_profile_pic.gif"}
*/



    public String getErrNum() {
        return errNum;
    }

    public String getfName() {
        return fName;
    }

    public String getlName() {
        return lName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getpPic() {
        return pPic;
    }

    public int getCountry_code() {
        return country_code;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }
}

