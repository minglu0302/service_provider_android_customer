package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 24/9/16.
 */
public class ServiceGroup implements Serializable
{


    String sid,sname,fixed_price;

    boolean servicegroupflag;

    public boolean isServicegroupflag() {
        return servicegroupflag;
    }

    public void setServicegroupflag(boolean servicegroupflag) {
        this.servicegroupflag = servicegroupflag;
    }

    public String getSid() {
        return sid;
    }

    public String getSname() {
        return sname;
    }


    public String getFixed_price() {
        return fixed_price;
    }
}
