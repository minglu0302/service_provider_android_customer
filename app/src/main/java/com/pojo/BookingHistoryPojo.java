package com.pojo;

import com.google.gson.annotations.SerializedName;
import com.utility.SessionManager;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 3/8/16.
 */
public class BookingHistoryPojo implements Serializable
{


    String errNum,errFlag,errMsg;

    ArrayList<Past_Booking> past_appts;
    ArrayList<OnGoing_Booking> ongoing_appts;

    public String getErrNum() {
        return errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public ArrayList<Past_Booking> getPast_appts() {
        return past_appts;
    }

    public ArrayList<OnGoing_Booking> getOngoing_appts() {
        return ongoing_appts;
    }
}
