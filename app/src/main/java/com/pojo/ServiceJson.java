package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 6/10/16.
 */
public class ServiceJson implements Serializable
{
    String sname,sid,sprice;


    public ServiceJson(String sname, String sid, String sprice)
    {
        this.sname = sname;
        this.sid = sid;
        this.sprice = sprice;
    }

    public String getSname() {
        return sname;
    }

    public String getSid() {
        return sid;
    }

    public String getSprice() {
        return sprice;
    }

}
