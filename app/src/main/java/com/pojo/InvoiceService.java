package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 10/11/16.
 */
public class InvoiceService implements Serializable
{
/*"sname":"HEAD SPA",
"sid":"581ef9a894e59e2c4d1b18ae",
"sprice":"100"
*/
    String sname,sid,sprice;

    public String getSname() {
        return sname;
    }

    public String getSid() {
        return sid;
    }

    public String getSprice() {
        return sprice;
    }
}
