package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 19/11/16.
 */
public class NotReviewedAppt implements Serializable
{
    /*{"errNum":"30","errFlag":"1","errMsg":"No appointments on this date.","appointments":[],"NotReviewedAppts":[]}*/

    String errNum,errFlag,errMsg;//,appointments,NotReviewedAppts;

    ArrayList<String>NotReviewedAppts;

    public String getErrNum() {
        return errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public ArrayList<String> getNotReviewedAppts() {
        return NotReviewedAppts;
    }
}
