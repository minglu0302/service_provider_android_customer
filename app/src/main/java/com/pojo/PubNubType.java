package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 22/2/16.
 */
public class PubNubType implements Serializable
{
    /*{
"tid":1,
"name":"Handyman",
"desc":"Test ",
"selImg":"1608348676220.png",
"unSelImg":"13904574573692.png",
"mapIcn":"10027071520796.png"
}*/

    /*"type_id":"57bb1b85e127c9563b89beee",
"cat_name":"TEACHER",
"cat_desc":"",
"ftype":"Hourly",
"cityid":"19085",
"pay_com":"Admin",
"price_set_by":"Admin",
"fixed_price":"",
"can_fees":"4",
"price_mile":"",
"price_min":"40",
"visit_fees":"95",
"min_fees":"",
"base_fees":"",
"selImg":"10042637710787.jpg",
"unSelImg":"7784773684941.png",
"banImg":"12172448170630.png"*/

    /*"type_id":"57c596cbe127c95e109af175",
"cat_name":"DOCTOR",
"cat_desc":"",
"ftype":"Hourly",
"cityid":"19085",
"pay_com":"Admin",
"price_set_by":"Admin",
"fixed_price":"",
"can_fees":"2",
"price_mile":"",
"price_min":"7",
"visit_fees":"70",
"min_fees":"",
"base_fees":"",
"selImg":"3345672194464.png",
"unSelImg":"11397668479380.jpg",
"banImg":"5444080150939.png",
"IsGroupContain":1*/

    String type_id,cat_name,cat_desc,ftype,pay_com,price_set_by,fixed_price,can_fees,price_mile,price_min,
            visit_fees,min_fees,selImg,unSelImg,mapIcn,StartingPrice,cityid,base_fees,banImg,IsGroupContain;

    public String getIsGroupContain() {
        return IsGroupContain;
    }

    public String getBanImg() {
        return banImg;
    }

    public String getType_id() {
        return type_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public String getCat_desc() {
        return cat_desc;
    }

    public String getFtype() {
        return ftype;
    }

    public String getPay_com() {
        return pay_com;
    }

    public String getPrice_set_by() {
        return price_set_by;
    }

    public String getFixed_price() {
        return fixed_price;
    }

    public String getCan_fees() {
        return can_fees;
    }

    public String getPrice_mile() {
        return price_mile;
    }

    public String getPrice_min() {
        return price_min;
    }

    public String getVisit_fees() {
        return visit_fees;
    }

    public String getMin_fees() {
        return min_fees;
    }

    public String getSelImg() {
        return selImg;
    }

    public String getUnSelImg() {
        return unSelImg;
    }

    public String getStartingPrice() {
        return StartingPrice;
    }

    public String getCityid() {
        return cityid;
    }

    public String getBase_fees() {
        return base_fees;
    }





    /* public String getType_id() {
        return type_id;
    }

    public String getType_name() {
        return cat_name;
    }

    public String getType_desc() {
        return cat_desc;
    }

    public String getSelImg() {
        return selImg;
    }

    public String getUnSelImg() {
        return unSelImg;
    }

    public String getMapIcn() {
        return mapIcn;
    }

    public String getStartingPrice() {
        return StartingPrice;
    }

    public String getCityid() {
        return cityid;
    }

    public String getBasefare() {
        return base_fees;
    }*/
}
