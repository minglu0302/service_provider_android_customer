package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 19/11/15.
 */
public class Signup_pojo implements Serializable{


/*    "errNum":"5",
"errFlag":"0",
"errMsg":"Signup completed!",
"coupon":"BkEHHAK",
"codeErr":"",
"token":"333536353535303539373039353533C0MOyBKMYC0MOyBKMYW2PwydwMJ34W2PwydwMJ34",
"email":"bgff@cdd.com",
"serverChn":"UpdateCustomer",
"CustId":33,
"apiKey":"AIzaSyBJ1oXlqQZRC5VqUsqwk22f2Fu0PvlwXAI",
"Languages":[
{
"lan_id":0,
"lan_name":"English"
},
{
"lan_id":1,
"lan_name":"Spanish"
},
{
"lan_id":2,
"lan_name":"French"
},
{
"lan_id":3,
"lan_name":"Germer"
},
{
"lan_id":4,
"lan_name":"Chinise"
}
]*/






   // private String name;



    private ArrayList<Validator_Pojo> card;
    private String errNum,errFlag,errMsg,coupon,codeErr,token,expiryLocal,expiryGMT,email,serverChn,CustId,flag,joined,apiKey,mail,chn;
    private ArrayList<Language_Class>Languages;

    private String stripe_pub_key;
    public ArrayList<Language_Class> getLanguages() {
        return Languages;
    }


    public String getStripe_pub_key() {
        return stripe_pub_key;
    }

    public ArrayList<Validator_Pojo> getCard() {
        return card;
    }

    public String getErrNum() {
        return errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getCoupon() {
        return coupon;
    }

    public String getCodeErr() {
        return codeErr;
    }

    public String getToken() {
        return token;
    }

    public String getExpiryLocal() {
        return expiryLocal;
    }

    public String getExpiryGMT() {
        return expiryGMT;
    }

    public String getEmail() {
        return email;
    }

    public String getServerChn() {
        return serverChn;
    }

    public String getCustId() {
        return CustId;
    }

    public String getFlag() {
        return flag;
    }

    public String getJoined() {
        return joined;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getMail() {
        return mail;
    }

    public String getChn() {
        return chn;
    }
}
