package com.pojo;

/**
 * Created by embed on 26/12/16.
 */
public class TotalMessage
{
    /*"msgtype":"0",
"payload":"hiiii",
"msgid":"2016-12-26 08:00:35",
"dt":"2016-12-26 20:00:35",
"usertype":2,
"status":2,
"timestamp":1482762647097*/

    String msgtype,payload,msgid,dt,usertype,status;

    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
