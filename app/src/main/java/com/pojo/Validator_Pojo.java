package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 6/2/16.
 *
 */
public class Validator_Pojo implements Serializable
{
    private String errNum;
    private String errFlag;
    private String errMsg;
    private String addressid;
    ArrayList<Support_values> support;
    private ArrayList<Saving_Address>addlist;

    public String getErrNum() {
        return errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getAddressid() {
        return addressid;
    }

    public ArrayList<Support_values> getSupport() {
        return support;
    }

    public ArrayList<Saving_Address> getAddlist() {
        return addlist;
    }
}
