package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 19/11/15.
 */
public class LoginActivity_pojo implements Serializable
{
   /* "errNum":"9",
"errFlag":"0",
"errMsg":"Login completed!",
"coupon":"",
"fname":"Akbar",
"lname":"Attar",
"token":"333536353535303539373039353533X668L2EV1JrTX668L2EV1JrTmWsjSgupmWsjSgup",
"email":"akbar@gmail.com",
"serverChn":"UpdateCustomer",
"profilePic":"https:\/\/s3.amazonaws.com\/iserve\/ProfileImages\/akbar@gmail.com.png",
"apiKey":"AIzaSyBJ1oXlqQZRC5VqUsqwk22f2Fu0PvlwXAI",
"cards":[
],
"Languages":[
{
"lan_id":0,
"lan_name":"English"
},
{
"lan_id":1,
"lan_name":"Chinese"
},
{
"lan_id":2,
"lan_name":"French"
}
],
"CustId":"4"*/

    private String apiKey,errNum,errFlag,errMsg,coupon,fname,lname,token,expiryLocal,expiryGMT,email,serverChn,profilePic,flag,joined,chn,CustId;
    private ArrayList<Language_Class>Languages;
    private String stripe_pub_key;

    public ArrayList<Language_Class> getLanguages() {
        return Languages;
    }

    /*public void setLanguages(ArrayList<Language_Class> languages) {
        Languages = languages;
    }*/

    public String getStripe_pub_key() {
        return stripe_pub_key;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getCustId() {
        return CustId;
    }

    public String getServerChn() {
        return serverChn;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getErrNum() {
        return errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getCoupon() {
        return coupon;
    }

    public String getFlag() {
        return flag;
    }

    public String getToken() {
        return token;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getExpiryLocal() {
        return expiryLocal;
    }

    public String getExpiryGMT() {
        return expiryGMT;
    }

    public String getEmail() {
        return email;
    }

    public String getChn() {
        return chn;
    }

}
