package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 20/1/17.
 *
 */
public class ChatPushPojo implements Serializable
{
    /*{"msg":"Tujf","name":"Piyush malviya","pic":"https:\/\/s3.amazonaws.com\/iserve\/ProfileImages\/laxman@mobifyi.com.png","bid":"241"}
*/
    String msg,name,pic,bid;

    public String getMsg() {
        return msg;
    }

    public String getName() {
        return name;
    }

    public String getPic() {
        return pic;
    }

    public String getBid() {
        return bid;
    }
}
