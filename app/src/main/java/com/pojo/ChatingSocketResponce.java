package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 26/12/16.
 */
public class ChatingSocketResponce implements Serializable
{

    /*"errNum":"21",
"errFlag":"0",
"errMsg":"Got the details!",
"messages":[]*/
    String errNum,errFlag,errMsg;
    ArrayList<TotalMessage>messages;


    public ArrayList<TotalMessage> getMessages() {
        return messages;
    }

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }


}
