package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 17/8/16.
 *
 */
public class Cancellation_java implements Serializable
{
    /*"errNum":"43",
"errFlag":"0",
"errMsg":"Appointment cancelled, you will be charged for cancellation as cancellation period is not with in the range.",
"amount":99*/
    String errNum,errFlag,errMsg,amount;


    public String getErrNum() {
        return errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public String getAmount() {
        return amount;
    }
}
