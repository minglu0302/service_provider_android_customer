package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 26/2/16.
 *
 */
public class MAster_Review implements Serializable
{
/*"patPic":"https:\/\/s3.amazonaws.com\/iserve\/ProfileImages\/mukesh@gmail.com.png",
"rating":"5",
"review":"",
"by":"Mukesh",
"dt":"2016-09-15"*/

    String patPic,rating,review,by,dt;

    public String getPatPic() {
        return patPic;
    }

    public String getRating() {
        return rating;
    }

    public String getReview() {
        return review;
    }

    public String getBy() {
        return by;
    }

    public String getDt() {
        return dt;
    }
}
