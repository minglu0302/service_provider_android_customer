package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 4/5/16.
 */
public class SocketMsg implements Serializable
{

    /*"track":{
"lt":13,
"lg":77,
"e":"pk@gmail.com",
"i":"https:\/\/s3.amazonaws.com\/iserve\/IserveProAndroid\/temp_pic.jpg",
"n":"Prashant",
"pid":141
}*/

    TrackLive track;

    private ArrayList<PubNubMasArr> masArr;
   private ArrayList<PubNubType>types;

    public TrackLive getTrack() {
        return track;
    }

    public ArrayList<PubNubMasArr> getMasArr() {
        return masArr;
    }

    public ArrayList<PubNubType> getTypes() {
        return types;
    }
}
