package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 22/2/16.
 */
public class PubNubMasArr implements Serializable
{
    /*{
"tid":1,
"mas":[]
}*/

    String tid;
    ArrayList<PubNubMas>mas;

    public String getTid() {
        return tid;
    }

    public ArrayList<PubNubMas> getMas() {
        return mas;
    }

    public void setMas(ArrayList<PubNubMas> mas) {
        this.mas = mas;
    }
}
