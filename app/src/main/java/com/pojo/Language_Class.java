package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 17/11/16.
 */
public class Language_Class implements Serializable
{
    /*"lan_id":0,
"lan_name":"English"*/
    String lan_id,lan_name;

    public String getLan_id() {
        return lan_id;
    }

    public String getLan_name() {
        return lan_name;
    }
}
