package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 3/8/16.
 */
public class Past_Booking implements Serializable
{
     /*    "apntDt":"2016-08-02 16:05:48",
    "pPic":"aa_default_profile_pic.gif",
    "email":"venga@gmail.com",
    "status":"Raise Invoice",
    "fname":"Venga",
    "lname":"",
    "deg":"",
    "phone":"+918686871987",
    "apntTime":"04:05 pm",
    "apntDate":"2016-08-02",
    "apptLat":"13.02886390686035",
    "apptLong":"77.589599609375",
    "bid":"1238",
    "fdata":{},
    "cat_name":"tutor",
    "addrLine1":"10th Cross Road, RT Nagar, Bengaluru, Karnataka 560024, India",
    "addrLine2":"",
    "notes":"Fhcfhvg",
    "star_rating"
    "bookType":"1",
    "statCode":"7",
    "amount":"0",
    "cancelAmount":0,
    "dt":"20160802160548"*/

    String apntDt,pPic,email,status,fname,lname,deg,phone,apntTime,apntDate,apptLat,apptLong,bid,cat_name,addrLine1,addrLine2,notes,star_rating,bookType,statCode,
            amount,cancelAmount,dt,pid,cancel_reason,dispute_msg,disputed;

    FData fdata;

  //  boolean isFirst;



    public Past_Booking(String apntDt, String pPic, String email,String status,
                           String fname,String lname,String deg,String phone,String apntTime,String apntDate,String apptLat
            ,String apptLong,String bid,String cat_name,String addrLine1,String addrLine2,String notes,String star_rating,String bookType,String statCode
            ,String amount,String cancelAmount,String dt,FData fdata,String pid,String cancel_reason,String dispute_msg,String disputed) {
        super();
        this.apntDt = apntDt;
        this.pPic = pPic;
        this.email = email;
        this.status = status;
       // this.isFirst = isFirst;
        this.fname = fname;
        this.lname = lname;
        this.deg = deg;
        this.phone = phone;
        this.apntTime = apntTime;
        this.apntDate = apntDate;
        this.apptLat = apptLat;
        this.apptLong = apptLong;
        this.bid = bid;
        this.cat_name = cat_name;
        this.addrLine1 = addrLine1;
        this.addrLine2 = addrLine2;
        this.notes = notes;
        this.star_rating = star_rating;
        this.bookType = bookType;
        this.statCode = statCode;
        this.amount = amount;
        this.cancelAmount = cancelAmount;
        this.dt = dt;
        this.fdata = fdata;
        this.pid = pid;
        this.cancel_reason = cancel_reason;
        this.dispute_msg = dispute_msg;
        this.disputed = disputed;


    }


    public String getPid() {
        return pid;
    }

    public String getStar_rating() {
        return star_rating;
    }



    public String getApntDt() {
        return apntDt;
    }

    public String getpPic() {
        return pPic;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getDeg() {
        return deg;
    }

    public String getPhone() {
        return phone;
    }

    public String getApntTime() {
        return apntTime;
    }

    public String getApntDate() {
        return apntDate;
    }

    public String getApptLat() {
        return apptLat;
    }

    public String getApptLong() {
        return apptLong;
    }

    public String getBid() {
        return bid;
    }

    public String getCat_name() {
        return cat_name;
    }

    public String getAddrLine1() {
        return addrLine1;
    }

    public String getAddrLine2() {
        return addrLine2;
    }

    public String getNotes() {
        return notes;
    }

    public String getBookType() {
        return bookType;
    }

    public String getStatCode() {
        return statCode;
    }

    public String getAmount() {
        return amount;
    }

    public String getCancelAmount() {
        return cancelAmount;
    }

    public String getDt() {
        return dt;
    }

    public FData getFdata() {
        return fdata;
    }

    public String getCancel_reason() {
        return cancel_reason;
    }

    public String getDispute_msg() {
        return dispute_msg;
    }

    public String getDisputed() {
        return disputed;
    }
}
