package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 25/11/15.
 */
public class GetCard_pojo implements Serializable {

    private String errNum;

    private String errMsg;
    private String errFlag;
    private String def;
    private ArrayList<Get_all_cards> cards;

    public String getDef() {
        return def;
    }

    public String getErrNum() {
        return errNum;
    }

    public void setErrNum(String errNum) {
        this.errNum = errNum;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public void setErrFlag(String errFlag) {
        this.errFlag = errFlag;
    }

    public ArrayList<Get_all_cards> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Get_all_cards> cards) {
        this.cards = cards;
    }



}
