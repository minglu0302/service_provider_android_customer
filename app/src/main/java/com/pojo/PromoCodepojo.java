package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 16/5/16.
 */
public class PromoCodepojo implements Serializable
{
    /*"errNum":"98",
"errFlag":"0",
"discount":"100",
"distype":"1",
"errMsg":"Congratulations, you just got 100% discount for your booking"*/

    /*{"errNum":"98","errFlag":"0","dtype":"Percent","dis":10,"errMsg":"Congratulations, you just got 10% discount for your booking"}
*/

    String errNum,errFlag,dtype,dis,errMsg;

    public String getErrNum() {
        return errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getDtype() {
        return dtype;
    }

    public String getDis() {
        return dis;
    }

    public String getErrMsg() {
        return errMsg;
    }
}
