package com.pojo;

/**
 * Created by embed on 3/8/16.
 *
 */
public class ContainRow_Item
{

    String apntDt,pPic,email,status,fname,lname,deg,phone,apntTime,apntDate,apptLat,apptLong,bid,cat_name,addrLine1,addrLine2,notes,star_rating,bookType,statCode,
            amount,cancelAmount,dt,Title,pid,cancel_reason,dispute_msg,disputed;
    FData fdata;
    boolean isFirst = false;


    public ContainRow_Item(String apntDt, String pPic, String email, String status, String fname, String lname, String deg,
                           String phone, String apntTime, String apntDate, String apptLat, String apptLong, String bid,
                           String cat_name, String addrLine1, String addrLine2, String notes, String star_rating,
                           String bookType, String statCode, String amount, String cancelAmount, String dt, FData fdata,String pid,
                           String cancel_reason,String dispute_msg,String disputed) {
        this.apntDt = apntDt;
        this.pPic = pPic;
        this.email = email;
        this.status = status;
        this.fname = fname;
        this.lname = lname;
        this.deg = deg;
        this.phone = phone;
        this.apntTime = apntTime;
        this.apntDate = apntDate;
        this.apptLat = apptLat;
        this.apptLong = apptLong;
        this.bid = bid;
        this.cat_name = cat_name;
        this.addrLine1 = addrLine1;
        this.addrLine2 = addrLine2;
        this.notes = notes;
        this.star_rating = star_rating;
        this.bookType = bookType;
        this.statCode = statCode;
        this.amount = amount;
        this.cancelAmount = cancelAmount;
        this.dt = dt;
        this.fdata = fdata;
        this.pid = pid;
        this.cancel_reason = cancel_reason;
        this.dispute_msg = dispute_msg;
        this.disputed = disputed;
    }

    public String getCancelAmount() {
        return cancelAmount;
    }

    public String getPid() {
        return pid;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public String getStar_rating() {
        return star_rating;
    }

    public String getApntDt() {
        return apntDt;
    }


    public String getpPic() {
        return pPic;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getDeg() {
        return deg;
    }


    public String getApntTime() {
        return apntTime;
    }


    public String getApntDate() {
        return apntDate;
    }


    public String getApptLat() {
        return apptLat;
    }


    public String getApptLong() {
        return apptLong;
    }


    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getCat_name() {
        return cat_name;
    }


    public String getAddrLine1() {
        return addrLine1;
    }


    public String getAddrLine2() {
        return addrLine2;
    }




    public String getStatCode() {
        return statCode;
    }


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public FData getFdata() {
        return fdata;
    }

    public String getCancel_reason() {
        return cancel_reason;
    }

    public String getDispute_msg() {
        return dispute_msg;
    }

    public String getDisputed() {
        return disputed;
    }
}
