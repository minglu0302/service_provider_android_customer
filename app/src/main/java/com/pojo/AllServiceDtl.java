package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by embed on 24/9/16.
 *
 */
public class AllServiceDtl implements Serializable
{
    /*"errNum":"21",
"errFlag":"0",
"errMsg":"Got the details!",
"data":[]*/
    String errNum,errFlag,errMsg;
    ArrayList<DataService>data;

    public String getErrNum() {
        return errNum;
    }

    public String getErrFlag() {
        return errFlag;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public ArrayList<DataService> getData() {
        return data;
    }
}
