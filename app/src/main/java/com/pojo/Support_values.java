package com.pojo;

import java.io.Serializable;
import java.util.ArrayList;

public class Support_values implements Serializable
{
	private String tag;
	private String link;

	ArrayList<ChildViewDraw>childs;

	public String getTag() {
		return tag;
	}

	public String getLink() {
		return link;
	}

	public ArrayList<ChildViewDraw> getChilds() {
		return childs;
	}
}
