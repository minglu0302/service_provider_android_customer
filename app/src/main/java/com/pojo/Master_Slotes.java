package com.pojo;

import java.io.Serializable;

/**
 * Created by embed on 26/2/16.
 */
public class Master_Slotes implements Serializable
{

    /*"stime":1476504000,
"etime":1476507600*/
    String from,to,sid,slotPrice,stime,etime;

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getSid() {
        return sid;
    }

    public String getSlotPrice() {
        return slotPrice;
    }

    public String getStime() {
        return stime;
    }

    public String getEtime() {
        return etime;
    }
}
